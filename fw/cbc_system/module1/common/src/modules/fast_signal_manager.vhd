----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 10.11.2016 21:28:43
-- Design Name: 
-- Module Name: fast_signal_manager - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;
use work.cbc_system_package.all;

entity fast_signal_manager is
    port (
    clk_320MHz                   : in  std_logic;
    clk_40MHz                    : in  std_logic;
    tdc_trig_out                 : in  std_logic;
    tdc_reg                      : in  std_logic_vector(31 downto 0);
    l1a_veto                     : in  std_logic;
    fmc_cbc_fast_signal_40MHz    : in  std_logic_vector(3 downto 0);
    fscbram_fast_signal_40MHz    : in  fast_signal_40MHz_in_type;
    ipb_ctrl                     : in  fast_signal_manager_ctrl_type;
    ipb_cnfg                     : in  fast_signal_manager_cnfg_type;
    ipb_stat                     : out fast_signal_manager_stat_type;
    l1a_320MHz                   : out std_logic;
    l1a_count_o                  : out std_logic_vector(28 downto 0);
    l1a_tdc_data                 : out std_logic_vector(31 downto 0);
--    l1a_tdccount                 : out std_logic_vector(2 downto 0);
--    int_cbc_fast_signal_40MHz_o  : out std_logic_vector(3 downto 0);
    cbc_fast_signal_40MHz_o      : out std_logic_vector(3 downto 0);
    cbc_fast_signal_par          : out std_logic_vector(7 downto 0);
    clk_sync_bit                 : in  std_logic
);
end fast_signal_manager;

architecture Behavioral of fast_signal_manager is

    signal trigger_fsm              : trigger_fsm_type;

    signal ext_async_l1a_tdccount   : std_logic_vector(2 downto 0);

    signal ext_l1a_signal_40MHz     : std_logic_vector(3 downto 0); 
    signal int_cbc_fast_signal_40MHz: std_logic_vector(3 downto 0);
    
    signal cbc_fast_signal_40MHz    : std_logic_vector(3 downto 0);

    signal header                   : std_logic_vector( 2 downto 0 ) := "110";
    signal trailer                  : std_logic := '1';

    signal l1a_veto_40MHz           : std_logic;
    
    signal l1a_count                : unsigned(28 downto 0);
    signal tdc_reg_i,tdc_reg_j      : std_logic_vector(31 downto 0);
    signal tdc_trig_out_i           : std_logic;
 	signal clk_sync_bit_1d          : std_logic;
 
begin

    l1a_count_o <= std_logic_vector(l1a_count);

    process ( clk_40MHz, l1a_veto )
    begin
        if  l1a_veto = '1' then
            l1a_veto_40MHz <= '1';
        elsif rising_edge(clk_40MHz) then
            l1a_veto_40MHz <= '0';

        end if;
    end process;

--    process ( clk_40MHz )
--    begin
--        if rising_edge(clk_40MHz) then
            cbc_fast_signal_40MHz_o <= cbc_fast_signal_40MHz;
            cbc_fast_signal_par     <= header & cbc_fast_signal_40MHz & trailer;
--        end if;
--    end process;
    
-- trigger signals are sent to cbc only when trigger_fsm is running
--process( clk_320MHz, ipb_ctrl.reset )
	process( clk_40MHz, ipb_ctrl.reset )
    begin
        if ipb_ctrl.reset = '1' then
            trigger_fsm   <= idle;           
        elsif rising_edge( clk_40MHz ) then
            case trigger_fsm is
                when idle =>
                    if ipb_ctrl.start_trigger = '1' then
                        trigger_fsm <= running;
                    end if;
                when running =>
                    if ipb_ctrl.stop_trigger = '1' then
                        trigger_fsm <= idle;
                    end if;
            end case;
        end if;
    end process; 
    ipb_stat.trigger_fsm <= trigger_fsm;
    
-- all the fast signals are merged here.
    cbc_fast_signal_40MHz(fast_signal_40MHz_indices.fast_reset) <=     
    (fmc_cbc_fast_signal_40MHz(fast_signal_40MHz_indices.fast_reset)     and ipb_cnfg.fast_signal_fmc_en) 
    or (ipb_ctrl.cbc_fast_signal(fast_signal_40MHz_indices.fast_reset)   and ipb_cnfg.fast_signal_ipbus_en) 
    or (int_cbc_fast_signal_40MHz(fast_signal_40MHz_indices.fast_reset)  and ipb_cnfg.fast_signal_internal_en)
    or (ext_l1a_signal_40MHz(fast_signal_40MHz_indices.fast_reset)       and ipb_cnfg.ext_async_l1a_en)
    or fscbram_fast_signal_40MHz.fast_reset; 

    cbc_fast_signal_40MHz(fast_signal_40MHz_indices.trigger) <=     
    ( (fmc_cbc_fast_signal_40MHz(fast_signal_40MHz_indices.trigger)     and ipb_cnfg.fast_signal_fmc_en) 
    or (ipb_ctrl.cbc_fast_signal(fast_signal_40MHz_indices.trigger)   and ipb_cnfg.fast_signal_ipbus_en) 
    or (int_cbc_fast_signal_40MHz(fast_signal_40MHz_indices.trigger)  and ipb_cnfg.fast_signal_internal_en)
    or (ext_l1a_signal_40MHz(fast_signal_40MHz_indices.trigger)       and ipb_cnfg.ext_async_l1a_en) 
    or fscbram_fast_signal_40MHz.trigger )
    when trigger_fsm = running and l1a_veto_40MHz = '0' else '0';

    cbc_fast_signal_40MHz(fast_signal_40MHz_indices.test_pulse) <=     
    (fmc_cbc_fast_signal_40MHz(fast_signal_40MHz_indices.test_pulse)     and ipb_cnfg.fast_signal_fmc_en) 
    or (ipb_ctrl.cbc_fast_signal(fast_signal_40MHz_indices.test_pulse)   and ipb_cnfg.fast_signal_ipbus_en) 
    or (int_cbc_fast_signal_40MHz(fast_signal_40MHz_indices.test_pulse)  and ipb_cnfg.fast_signal_internal_en)
    or (ext_l1a_signal_40MHz(fast_signal_40MHz_indices.test_pulse)       and ipb_cnfg.ext_async_l1a_en)
    or fscbram_fast_signal_40MHz.test_pulse; 

    cbc_fast_signal_40MHz(fast_signal_40MHz_indices.orbit_reset) <=     
    (fmc_cbc_fast_signal_40MHz(fast_signal_40MHz_indices.orbit_reset)     and ipb_cnfg.fast_signal_fmc_en) 
    or (ipb_ctrl.cbc_fast_signal(fast_signal_40MHz_indices.orbit_reset)   and ipb_cnfg.fast_signal_ipbus_en) 
    or (int_cbc_fast_signal_40MHz(fast_signal_40MHz_indices.orbit_reset)  and ipb_cnfg.fast_signal_internal_en)
    or (ext_l1a_signal_40MHz(fast_signal_40MHz_indices.orbit_reset)       and ipb_cnfg.ext_async_l1a_en)
    or fscbram_fast_signal_40MHz.orbit_reset; 
        
    -- external asynchronous trigger
--    tdc : entity work.l1a_tdc
--    Port map(   reset           => ipb_ctrl.reset,
--                clk40           => clk_40MHz,
--                clk320          => clk_320MHz,
--                l1a_nosync      => ext_async_l1a,
--                l1a_o           => ext_sync_l1a,
--                l1a_tdccount_o  => ext_async_l1a_tdccount
--             );

    ext_l1a_signal_40MHz(fast_signal_40MHz_indices.trigger) <= tdc_trig_out_i;
    ext_l1a_signal_40MHz(fast_signal_40MHz_indices.fast_reset)    <= '0';
    ext_l1a_signal_40MHz(fast_signal_40MHz_indices.test_pulse)    <= '0';
    ext_l1a_signal_40MHz(fast_signal_40MHz_indices.orbit_reset)   <= '0';
   
--    l1a_tdccount <= ext_async_l1a_tdccount when trigger_fsm = running else (others => '0');
--    l1a_320MHz_gen : entity work.dff_sync_edge_detect port map(reset => ipb_ctrl.reset, clkb => clk_320MHz, dina => cbc_fast_signal_40MHz(fast_signal_40MHz_indices.trigger), doutb => l1a_320MHz);
--    l1a_count_gen : 
--        entity work.para_binary_counter 
--            generic map(WIDTH => 29, INIT_VALUE => 0)
--            port map(reset => ipb_ctrl.reset, clk => clk_320MHz, din => cbc_fast_signal_40MHz(fast_signal_40MHz_indices.trigger), m => (others => '1'), q => l1a_count );

   process( clk_320MHz, ipb_ctrl.reset )
    variable l1a_ready : boolean;
    begin
        if ipb_ctrl.reset = '1' then
            l1a_count <= (others => '0');
            l1a_ready := false;    
            tdc_trig_out_i<='0';
        elsif rising_edge( clk_320MHz ) then              
            tdc_trig_out_i<=tdc_trig_out;
            tdc_reg_i<=tdc_reg;
        
        	clk_sync_bit_1d <= clk_sync_bit;  
        	if l1a_ready = true then
                l1a_320MHz <= '1';
--        		l1a_count <= l1a_count + 1; 
        		l1a_tdc_data<=tdc_reg_i;       	
            else 
				l1a_320MHz <= '0';
            end if;            
                     
            if clk_sync_bit_1d = '1' and cbc_fast_signal_40MHz(fast_signal_40MHz_indices.trigger) = '1' then
				l1a_ready := true;
            else
                l1a_ready := false;
            end if;
        end if;
    end process;
    ipb_stat.l1a_count <= std_logic_vector(l1a_count);
    
-- internal fast signal generation
    fast_signal_generator_inst : entity work.fast_signal_generator
    port map(
        clk                      => clk_40MHz,
        reset                    => ipb_ctrl.fast_signal_generator.reset,
        load_cnfg                => ipb_ctrl.fast_signal_generator.load_cnfg,            
        start                    => ipb_ctrl.fast_signal_generator.start,
        stop                     => ipb_ctrl.fast_signal_generator.stop,
        Ncycle_i                 => ipb_cnfg.fast_signal_generator.Ncycle,
        fast_reset_en_i          => ipb_cnfg.fast_signal_generator.fast_reset_en,
        test_pulse_en_i          => ipb_cnfg.fast_signal_generator.test_pulse_en,
        trigger_en_i             => ipb_cnfg.fast_signal_generator.trigger_en,
        orbit_reset_en_i         => ipb_cnfg.fast_signal_generator.orbit_reset_en,
        cycle_T_i                => ipb_cnfg.fast_signal_generator.cycle_T,
        test_pulse_t_i           => ipb_cnfg.fast_signal_generator.test_pulse_t,
        trigger_t_i              => ipb_cnfg.fast_signal_generator.trigger_t,
        orbit_reset_t_i          => ipb_cnfg.fast_signal_generator.orbit_reset_t,
        fast_signal_40MHz        => int_cbc_fast_signal_40MHz,
        fsm_o                    => ipb_stat.fast_signal_generator_fsm
    );    

--   int_cbc_fast_signal_40MHz_o <= int_cbc_fast_signal_40MHz;

end Behavioral;
