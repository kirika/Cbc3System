----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 02.10.2017 16:39:11
-- Design Name: 
-- Module Name: ttc_dec - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;


use work.ipbus.all;
--use work.system_package.all;

--use work.user_package.all;
--use work.user_version_package.all;


entity ttc_dec is
    Port ( 
           clk : in STD_LOGIC; --ibpus clock
           reset : in STD_LOGIC;
           ipb_mosi_i : in ipb_wbus;
           ipb_miso_o : out ipb_rbus;
               
           fabric_clk_in : in STD_LOGIC;
           clk40_out : out std_logic;
           clk_40 : in std_logic;
           clk_320 : in std_logic;
           clk_640 : in std_logic;
           
--           f_adj : in STD_LOGIC_VECTOR (15 downto 0);
--           f_adj_strobe : in std_logic;
           ttc_in : in STD_LOGIC;
           ttc_trig : out STD_LOGIC;
           ttc_rst : out STD_LOGIC;
           ttc_cal : out STD_LOGIC);
end ttc_dec;

architecture Behavioral of ttc_dec is
 signal ipbus_ack : std_logic;
 
 signal rst_once,rst_once_i,rst_once_j,rst_once_k : std_logic;
 
 signal ttc_Di, ttc_D : std_logic_vector(3 downto 0);
 signal ttc_Ds,ttc_Dss : std_logic_vector(31 downto 0);

 signal f_adj_strobe,f_adj_strobe_i,f_adj_strobe_j :std_logic;
 signal f_adj,f_adj_i, f_adj_j,f_adj_acc: std_logic_vector(15 downto 0);
 signal f_adj_en : std_logic_vector(1 downto 0);
 signal f_adj_c : std_logic_vector(3 downto 0);
 signal f_adj_state,f_adj_delayed_strobe : std_logic;
 signal f_adj_diff,f_adj_diff_prev : std_logic_vector(5 downto 0);
 signal f_adj_P,f_adj_D : std_logic_vector(15 downto 0);
 signal f_adj_val : std_logic_vector(21 downto 0);
 
 signal f_adj_freeze : std_logic;
 
 signal ttc_invert_in : std_logic;
 
 signal psen,psincdec : std_logic;
  

  signal ttc_c : std_logic_vector(2 downto 0);
-- signal clk40, clk160, clk640 : std_logic;
 signal locked_ttc,reset_ttc: std_logic;
 
 attribute dont_touch: boolean;
 attribute dont_touch of f_adj_strobe_i : signal is true;
  

 

component ttc_mmcm1
port
 (-- Clock in ports
  -- Clock out ports
  clk40          : out    std_logic;
--  clk160          : out    std_logic;
--  clk640          : out    std_logic;
  -- Dynamic phase shift ports
  psclk             : in     std_logic;
  psen              : in     std_logic;
  psincdec          : in     std_logic;
  psdone            : out    std_logic;
  -- Status and control signals
  reset             : in     std_logic;
  locked            : out    std_logic;
  clk_in1           : in     std_logic
 );
end component;

begin

  phaseadj_inst : process(clk_320,reset_ttc)
   variable i : std_logic_vector(17 downto 0); 
  begin
    if reset_ttc='1' then
      f_adj_strobe_i<='0';
      f_adj_strobe_j<='0';
      f_adj_acc<=(others=>'0');
      f_adj_en<="01";
      f_adj_c<="0000";
      
      psen<='0';
      psincdec<='0';
      ttc_D<="0000";
      ttc_Ds<=(others=> '0');
    elsif rising_edge(clk_320) then
--      ttc_clk<=f_adj_c(1);
      if ttc_invert_in='1' then
        ttc_D<=not ttc_Di;
      else
        ttc_D<=ttc_Di;
      end if;  
      ttc_Ds(31 downto 4)<=ttc_Ds(27 downto 0);
      ttc_Ds(3 downto 0)<=ttc_D;
      
      
      
      f_adj_strobe_i<=f_adj_strobe;
      f_adj_strobe_j<=f_adj_strobe_i;
      
      f_adj_c<=std_logic_vector(unsigned(f_adj_c)+1);
    
      if f_adj_strobe_i='1' and f_adj_strobe_j='0' then
        f_adj_i<=f_adj;      
      end if;
      
      f_adj_en<="01";
      if f_adj_c="0000" then 
        i:=std_logic_vector(unsigned("00"&f_adj_acc)+unsigned("00"&f_adj_i)+32768);
        f_adj_acc<=i(15 downto 0);
        f_adj_en<=i(17 downto 16);
      end if;  
      
      psen<='0';
      psincdec<='0';
      case f_adj_en is
        when "00" => 
          psincdec<='0';
          psen<='1';
        when "10" =>  
          psincdec<='1';
          psen<='1';
        when others => 
          null;
      end case;
          
    end if;
  end process; 
  
  
  
 datarec_inst : process(clk_40,reset_ttc)
   variable se,sl : std_logic_vector(3 downto 0);
   variable i : integer;
   variable s : std_logic; 
  begin
    if reset_ttc='1' then
     f_adj_delayed_strobe<='0';
     f_adj_j<=x"8000";
    elsif rising_edge(clk_40) then
    
     if f_adj_freeze='0' then
       ttc_Dss<=ttc_Ds;
     end if;
     
     se:=(others=>'0');
     for i in 0 to 7 loop
      if ttc_Dss(i+8)='1' then --should be all 0's
       se:=std_logic_vector(unsigned(se)+1);
      end if;
      if ttc_Dss(i+16)='0' then -- should be all 1's
       sl:=std_logic_vector(unsigned(sl)+1);
      end if;
     end loop;  

     f_adj_diff<=std_logic_vector(unsigned("00"&sl)-unsigned("00"&se)); --negative if the rising edge was early and the oscillator needs to slow down
     
     
     f_adj_val<=std_logic_vector(signed(f_adj_diff)*signed(f_adj_P) + (signed(f_adj_diff)-signed(f_adj_diff_prev))*signed(f_adj_D));
     
     
     if f_adj_freeze='0' then
       f_adj_j<=std_logic_vector(signed(f_adj_j)+signed(f_adj_val(21 downto 6)));
       f_adj_diff_prev<=f_adj_diff;
     end if;  
     

     f_adj<=f_adj_j;    --the 320MHz clock is anyway synchronized with the 40MHz clock, so there is no problem with updating anytime we want in this domain?
     
--     f_adj_strobe<='0';
--     if s='1' then
--      if f_adj_state='0' then
--       f_adj<=f_adj_j;
--       f_adj_state<='1';   
--       f_adj_delayed_strobe<='0';     
--      else 
--       f_adj_delayed_strobe<='1';       
--      end if;
--     end if;
--     if f_adj_state='1' then
--      f_adj_strobe<='1';
--      f_adj_state<='0';
--     end if;
         
    end if;
  end process;
  
  ttc_rst<='0';  
   
  
    
  ttc_mmcm1_inst : ttc_mmcm1
       port map ( 
      -- Clock out ports  
       clk40 => clk40_out,
      -- Dynamic phase shift ports                 
       psclk => clk_320,
       psen => psen,
       psincdec => psincdec,
       psdone => open,
      -- Status and control signals                
       reset => reset,
       locked => locked_ttc,
       -- Clock in ports
       clk_in1 => fabric_clk_in
     );
  
  reset_ttc<=not locked_ttc;
  
  
  ttc_serdes0: unisim.vcomponents.ISERDESE2
      generic map(
        DATA_RATE => "DDR",
        DATA_WIDTH => 4,
        DYN_CLKDIV_INV_EN => "FALSE",
        DYN_CLK_INV_EN => "FALSE",
        INIT_Q1 => '0',
        INIT_Q2 => '0',
        INIT_Q3 => '0',
        INIT_Q4 => '0',
        INTERFACE_TYPE => "NETWORKING",
        IOBDELAY => "NONE",
        IS_CLKB_INVERTED => '1',
        IS_CLKDIVP_INVERTED => '0',
        IS_CLKDIV_INVERTED => '0',
        IS_CLK_INVERTED => '0',
        IS_D_INVERTED => '0',
        IS_OCLKB_INVERTED => '0',
        IS_OCLK_INVERTED => '0',
        NUM_CE => 2,
        OFB_USED => "FALSE",
        SERDES_MODE => "MASTER",
        SRVAL_Q1 => '0',
        SRVAL_Q2 => '0',
        SRVAL_Q3 => '0',
        SRVAL_Q4 => '0'
      )
          port map (
        BITSLIP => '0',
        CE1 => '1',
        CE2 => '1',
        CLK => clk_640,
        CLKB => clk_640,
        CLKDIV => clk_320,
        CLKDIVP => '0',
        D => ttc_in,
        DDLY => '0',
        DYNCLKDIVSEL => '0',
        DYNCLKSEL => '0',
        O => open,
        OCLK => '0',
        OCLKB => '0',
        OFB => '0',
        Q1 => ttc_Di(0),
        Q2 => ttc_Di(1),
        Q3 => ttc_Di(2),
        Q4 => ttc_Di(3),
        Q5 => open,
        Q6 => open,
        Q7 => open,
        Q8 => open,
        RST => reset,
        SHIFTIN1 => '0',
        SHIFTIN2 => '0',
        SHIFTOUT1 => open,
        SHIFTOUT2 => open
      );
  
  
  process(reset, clk)
  begin
    if reset='1' then
      ipbus_ack      <= '0';
      rst_once<='0';
      f_adj_freeze<='0';
      f_adj_P<=x"0001";
      f_adj_D<=x"0001";
      ttc_invert_in<='0';
    elsif rising_edge(clk) then
          
        ipbus_ack<='0';
        if ipb_mosi_i.ipb_strobe='1' and ipbus_ack='0' and ipb_mosi_i.ipb_write='1' then       
           case ipb_mosi_i.ipb_addr(2 downto 0) is
             when "000" => 
                rst_once<=ipb_mosi_i.ipb_wdata(0);
                ttc_invert_in<=ipb_mosi_i.ipb_wdata(1);
                f_adj_freeze<=ipb_mosi_i.ipb_wdata(2);
             when "001" => 
                f_adj_P<=      ipb_mosi_i.ipb_wdata(15 downto 0);                  
                f_adj_D<=      ipb_mosi_i.ipb_wdata(31 downto 16);
             when others =>  
                
           end case;             
           ipbus_ack<='1'; 
         end if;  
                    
  
         if ipb_mosi_i.ipb_strobe='1' and ipbus_ack='0' and ipb_mosi_i.ipb_write='0' then
           case ipb_mosi_i.ipb_addr(2 downto 0) is
             when "000" => 
               ipb_miso_o.ipb_rdata(5 downto 0)<=f_adj_diff;
             when "001" => 
               ipb_miso_o.ipb_rdata(21 downto 0)<=f_adj_val;           
             when "010" => 
               ipb_miso_o.ipb_rdata(15 downto 0)<=f_adj;
             when "011" => 
               ipb_miso_o.ipb_rdata<=ttc_Dss;
             when others => 
               ipb_miso_o.ipb_rdata<=X"12345679";             
           end case;       
         
           ipbus_ack<='1'; 
      
         end if;
         
    end if;
  end process;               
             
             
  ipb_miso_o.ipb_ack <=ipbus_ack;
  ipb_miso_o.ipb_err <= '0';
end Behavioral;
