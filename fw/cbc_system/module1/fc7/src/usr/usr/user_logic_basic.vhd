--===========================--
-- cbc_system_inst : entity work.cbc_system(single_cbc3)
-- is added.
-- rarp & i2c_eeprom are disabled
-- 28.10.2016 Kirika Uchida
--===========================--
library ieee;
use ieee.std_logic_1164.all;
--use ieee.std_logic_unsigned.all;
use IEEE.NUMERIC_STD.ALL;

use work.ipbus.all;
use work.system_package.all;
--! user packages
use work.user_package.all;
use work.cbc_system_package.all;
library unisim;
use unisim.vcomponents.all;

entity user_logic is 
port
(

	--# led
	usrled1_r						: out	std_logic; -- fmc_l12_spare[8]
	usrled1_g						: out	std_logic; -- fmc_l12_spare[9]
	usrled1_b						: out	std_logic; -- fmc_l12_spare[10]
	usrled2_r						: out	std_logic; -- fmc_l12_spare[11]
	usrled2_g						: out	std_logic; -- fmc_l12_spare[12]
	usrled2_b						: out	std_logic; -- fmc_l12_spare[13]

	--# on-board fabric clk
    fabric_clk_p                    : in    std_logic; -- new port [PV 2015.08.19]
    fabric_clk_n                    : in    std_logic; -- new port [PV 2015.08.19]
    fabric_coax_or_osc_p 			: in 	std_logic;
	fabric_coax_or_osc_n 			: in 	std_logic;

	--# on-board mgt clk
	pcie_clk_p						: in	std_logic;
    pcie_clk_n                      : in    std_logic;
	osc_xpoint_a_p					: in	std_logic;
	osc_xpoint_a_n					: in	std_logic;
	osc_xpoint_b_p					: in	std_logic;
	osc_xpoint_b_n					: in	std_logic;
	osc_xpoint_c_p					: in	std_logic;
	osc_xpoint_c_n					: in	std_logic;
	osc_xpoint_d_p					: in	std_logic;
	osc_xpoint_d_n					: in	std_logic;
	ttc_mgt_xpoint_a_p				: in	std_logic;
	ttc_mgt_xpoint_a_n				: in	std_logic;
	ttc_mgt_xpoint_b_p				: in	std_logic;
	ttc_mgt_xpoint_b_n				: in	std_logic;
	ttc_mgt_xpoint_c_p				: in	std_logic;
	ttc_mgt_xpoint_c_n				: in	std_logic;
			
	--# fmc mgt clk		
	fmc_l12_gbtclk0_a_p				: in	std_logic; 
	fmc_l12_gbtclk0_a_n				: in	std_logic; 
	fmc_l12_gbtclk1_a_p				: in	std_logic; 
	fmc_l12_gbtclk1_a_n				: in	std_logic; 
	fmc_l12_gbtclk0_b_p				: in	std_logic; 
	fmc_l12_gbtclk0_b_n				: in	std_logic; 
	fmc_l12_gbtclk1_b_p				: in	std_logic; 
	fmc_l12_gbtclk1_b_n				: in	std_logic; 
	fmc_l8_gbtclk0_p				: in	std_logic; 
	fmc_l8_gbtclk0_n				: in	std_logic; 
	fmc_l8_gbtclk1_p				: in	std_logic; 
	fmc_l8_gbtclk1_n				: in	std_logic; 

	--# fmc mgt
	fmc_l12_dp_c2m_p				: out	std_logic_vector(11 downto 0);
	fmc_l12_dp_c2m_n				: out	std_logic_vector(11 downto 0);
	fmc_l12_dp_m2c_p				: in	std_logic_vector(11 downto 0);
	fmc_l12_dp_m2c_n				: in	std_logic_vector(11 downto 0);
	fmc_l8_dp_c2m_p					: out	std_logic_vector( 7 downto 0);
	fmc_l8_dp_c2m_n					: out	std_logic_vector( 7 downto 0);
	fmc_l8_dp_m2c_p					: in	std_logic_vector( 7 downto 0);
	fmc_l8_dp_m2c_n					: in	std_logic_vector( 7 downto 0);
	
	--# fmc fabric clk	
    fmc_l8_clk0                     : in    std_logic; 
    fmc_l8_clk1                     : in    std_logic;
    fmc_l12_clk0                    : in    std_logic;
    fmc_l12_clk1                    : in    std_logic;    

	--# fmc gpio		
	fmc_l8_la_p						: inout	std_logic_vector(33 downto 0);
	fmc_l8_la_n						: inout	std_logic_vector(33 downto 0);
	fmc_l12_la_p					: inout	std_logic_vector(33 downto 0);
	fmc_l12_la_n					: inout	std_logic_vector(33 downto 0);
	
	--# amc mgt		
	k7_amc_rx_p						: inout	std_logic_vector(15 downto 1);
	k7_amc_rx_n						: inout	std_logic_vector(15 downto 1);
	amc_tx_p						: inout	std_logic_vector(15 downto 1);
	amc_tx_n						: inout	std_logic_vector(15 downto 1);
	
	--# amc fabric
	k7_fabric_amc_rx_p03			: inout	std_logic;
	k7_fabric_amc_rx_n03    		: inout	std_logic;
	k7_fabric_amc_tx_p03    		: inout	std_logic;
	k7_fabric_amc_tx_n03    		: inout	std_logic;

	--# ddr3
	ddr3_sys_clk_p 					: in	std_logic;
	ddr3_sys_clk_n 					: in	std_logic;
	ddr3_dq                 		: inout std_logic_vector( 31 downto 0);
	ddr3_dqs_p              		: inout std_logic_vector(  3 downto 0);
	ddr3_dqs_n              		: inout std_logic_vector(  3 downto 0);
	ddr3_addr               		: out   std_logic_vector( 13 downto 0);
	ddr3_ba                 		: out   std_logic_vector(  2 downto 0);
	ddr3_ras_n              		: out   std_logic;
	ddr3_cas_n              		: out   std_logic;
	ddr3_we_n               		: out   std_logic;
	ddr3_reset_n            		: out   std_logic;
	ddr3_ck_p               		: out   std_logic_vector(  0 downto 0);
	ddr3_ck_n               		: out   std_logic_vector(  0 downto 0);
	ddr3_cke                		: out   std_logic_vector(  0 downto 0);
	ddr3_cs_n               		: out   std_logic_vector(  0 downto 0);
	ddr3_dm                 		: out   std_logic_vector(  3 downto 0);
	ddr3_odt                		: out   std_logic_vector(  0 downto 0);

    --# cdce
	cdce_pll_lock_i                 : in    std_logic; -- new port [PV 2015.08.19]  
    cdce_pri_clk_bufg_o 		    : out 	std_logic; -- new port [PV 2015.08.19] 
    cdce_ref_sel_o                  : out   std_logic; -- new port [PV 2015.08.19]   
    cdce_pwrdown_o                  : out   std_logic; -- new port [PV 2015.08.19]  
    cdce_sync_o                     : out   std_logic; -- new port [PV 2015.08.19]  
    cdce_sync_clk_o                 : out   std_logic; -- new port [PV 2015.08.19]  

	--# system clk		
	osc125_a_bufg_i					: in	std_logic;
	osc125_a_mgtrefclk_i			: in	std_logic;
	osc125_b_bufg_i					: in 	std_logic;
	osc125_b_mgtrefclk_i			: in	std_logic;
    clk_31_250_bufg_i		        : in	std_logic; -- new port [PV 2015.08.19]
    
    --# ipbus comm    
	ipb_clk_o				        : out	std_logic;
	ipb_rst_i				        : in	std_logic;
	ipb_miso_o			            : out	ipb_rbus_array(0 to nbr_usr_slaves-1);
	ipb_mosi_i			            : in	ipb_wbus_array(0 to nbr_usr_slaves-1);

    --# ipbus conf
	ip_addr_o						: out	std_logic_vector(31 downto 0);
    mac_addr_o                      : out   std_logic_vector(47 downto 0);
    rarp_en_o                       : out   std_logic;
    use_i2c_eeprom_o                : out   std_logic;
    
    fmc_i2c_scl                     : inout  std_logic;
    fmc_i2c_sda                     : inout  std_logic                        
    
    
);
end user_logic;

architecture usr of user_logic is

	signal ipb_clk					: std_logic;
    
    signal fabric_clk_pre_buf       : std_logic;                
    signal fabric_clk               : std_logic;
    
    signal ctrl_reg		            : array_32x32bit;
	signal stat_reg		            : array_32x32bit;

    signal cdce_pri_clk_bufg        : std_logic;

    signal cdce_sync_from_ipb       : std_logic;     
    signal cdce_sel_from_ipb        : std_logic;                    
    signal cdce_pwrdown_from_ipb    : std_logic;
    signal cdce_ctrl_from_ipb       : std_logic;  
--  signal cdce_sel_from_fabric     : std_logic;                    
--  signal cdce_pwrdown_from_fabric : std_logic;
--  signal cdce_ctrl_from_fabric    : std_logic; 

    signal test_signal              : std_logic_vector(31 downto 0);
    
    signal usrreg_i2c_settings2           : std_logic_vector(31 downto 0); -- 13
    signal usrreg_i2c_settings           : std_logic_vector(31 downto 0); -- 13 
    signal usrreg_i2c_command            : std_logic_vector(31 downto 0); -- 14 
    signal usrreg_i2c_reply              : std_logic_vector(31 downto 0); -- 15
    
    signal ext_trig_ua9,ext_rst_ua9 : std_logic;
    signal tdc_reg : std_logic_vector(31 downto 0);
    signal tdc_ua9_count : std_logic_vector(31 downto 0);
    signal tdc_trig_out : std_logic;    
    signal fmc_clk,fmc_clk_i  : std_logic;
    signal ext_async_l1a_0,ext_async_l1a_1 : std_logic;
    signal clk_40MHz,clk_320MHz,clk_640MHz : std_logic;
    
    
begin

	--===========================================--
	-- ipbus management
	--===========================================--
	ipb_clk 		      <= clk_31_250_bufg_i; 	-- select the frequency of the ipbus clock from osc125_b_bufg
	ipb_clk_o 	          <= ipb_clk;				-- always forward the selected ipb_clk to system core
    --
	ip_addr_o 	          <= x"c0_a8_00_40";
	mac_addr_o	          <= x"aa_bb_cc_dd_ee_50";
	rarp_enable_gen : 
	if RARP_EN = true generate 
	   rarp_en_o 		      <= '1';
	end generate;
	rarp_disable_gen : 
    if RARP_EN = false generate 
       rarp_en_o               <= '0';
    end generate;	
	use_i2c_eeprom_o      <= '0';
	--===========================================--


    --===========================================--
	-- other clock inputs 
	--===========================================--
	fclk_ibuf:      ibufgds     generic map( DIFF_TERM => TRUE ) port map (i => fabric_clk_p, ib => fabric_clk_n, o => fabric_clk_pre_buf);
    fclk_bufg:      bufg        port map (i => fabric_clk_pre_buf,               o => fabric_clk);
	--===========================================--
        
    --===================================--
    cdce_synch: entity work.cdce_synchronizer
    --===================================--
    generic map
    (    
        pwrdown_delay     => 1000,
        sync_delay        => 1000000    
    )
    port map
    (    
        reset_i           => ipb_rst_i,
        ------------------
        ipbus_ctrl_i      => not cdce_ctrl_from_ipb,          -- default: 1 (ipb controlled) 
        ipbus_sel_i       =>     cdce_sel_from_ipb,           -- default: 0 (select secondary clock)
        ipbus_pwrdown_i   => not cdce_pwrdown_from_ipb,       -- default: 1 (powered up)
        ipbus_sync_i      => not cdce_sync_from_ipb,          -- default: 1 (disable sync mode), rising edge needed to resync
        ------------------                                                                       
        user_sel_i        => '1', -- cdce_sel_from_fabric     -- effective only when ipbus_ctrl_i = '0'            
        user_pwrdown_i    => '1', -- cdce_pwrdown_from_fabric -- effective only when ipbus_ctrl_i = '0'
        user_sync_i       => '1', -- cdce_sync_from_fabric    -- effective only when ipbus_ctrl_i = '0'  
        ------------------
        pri_clk_i         => cdce_pri_clk_bufg,
        sec_clk_i         => fabric_clk,         -- copy of what is actually send to the secondary clock input
        pwrdown_o         => cdce_pwrdown_o,
        sync_o            => cdce_sync_o,
        ref_sel_o         => cdce_ref_sel_o,    
        sync_clk_o        => cdce_sync_clk_o
    );
        
        cdce_pri_clk_bufg_o <= cdce_pri_clk_bufg; cdce_pri_clk_bufg   <= '0'; -- [note: in this example, the cdce_pri_clk_bufg is not used]
    --===================================--

	--===========================================--
	stat_regs_inst: entity work.ipb_user_status_regs
	--===========================================--
	port map
	(
		clk					=> ipb_clk,
		reset				=> ipb_rst_i,
		ipb_mosi_i			=> ipb_mosi_i(user_ipb_stat_regs),
		ipb_miso_o			=> ipb_miso_o(user_ipb_stat_regs),
		regs_i				=> stat_reg
	);
	--===========================================--
    
	--===========================================--
	ctrl_regs_inst: entity work.ipb_user_control_regs
	--===========================================--
	port map
	(
		clk					=> ipb_clk,
		reset				=> ipb_rst_i,
		ipb_mosi_i			=> ipb_mosi_i(user_ipb_ctrl_regs),
		ipb_miso_o			=> ipb_miso_o(user_ipb_ctrl_regs),
		regs_o				=> ctrl_reg
	);
	--===========================================--

    cbc_system_inst : entity work.cbc_system(cbc3)
    port map
    (
    fmc_l8_la_n  => fmc_l8_la_n,
    fmc_l8_la_p  => fmc_l8_la_p,
    fmc_l12_la_n => fmc_l12_la_n,
    fmc_l12_la_p => fmc_l12_la_p,
    fabric_clk   => fabric_clk,
    ipb_clk	     => ipb_clk,
    ipb_rst_i    => ipb_rst_i,
    ipb_mosi_i   => ipb_mosi_i,
    ipb_miso_o   => ipb_miso_o,
    usrled1_r    => usrled1_r,
    usrled1_g    => usrled1_g,
    usrled1_b    => usrled1_b,
    usrled2_r    => usrled2_r,
    usrled2_g    => usrled2_g,
    usrled2_b    => usrled2_b,
    clk_40MHz_o  => clk_40MHz,
    clk_320MHz_o  => clk_320MHz,
    clk_640MHz_o  => clk_640MHz,
    tdc_trig      => tdc_trig_out,
    tdc_reg       => tdc_reg,
    tdc_ua9_count => tdc_ua9_count  
    );
    
--===================================--
    
    i2c_m: entity work.i2c_master_top
    
    --===================================--
    generic map (nbr_of_busses => 1) port map (
        clk               => ipb_clk,
        reset             => ipb_rst_i,
        ------------------
        id_o              => usrreg_i2c_settings2(31 downto 24), -- read only
        id_i              => usrreg_i2c_settings(23 downto 16),
        enable            => usrreg_i2c_settings(15),
        bus_select        => usrreg_i2c_settings(12 downto 10),
        prescaler         => usrreg_i2c_settings( 9 downto  0),
        command           => usrreg_i2c_command,
        reply             => usrreg_i2c_reply,
        ------------------
        scl_io(0)         => fmc_i2c_scl,           
        sda_io(0)         => fmc_i2c_sda
    );           
    --===================================--
    usrreg_i2c_settings(23 downto  0)     <= ctrl_reg(1)(23 downto 0);   
    stat_reg(1)(31 downto 24)             <= usrreg_i2c_settings2(31 downto 24);            
    usrreg_i2c_command                    <= ctrl_reg(2);
    stat_reg(3)                           <= usrreg_i2c_reply;   
    
    
    
    
    --fmc_clk<=fmc_l12_clk0;
        --===========================================--
        ua9_clkgen_inst: entity work.ua9_clkgen
        --===========================================--
        port map
        (
            clk                   => ipb_clk,
            reset                 => ipb_rst_i,
            ipb_mosi_i            => ipb_mosi_i(user_ua9_clkgen_regs),
            ipb_miso_o            => ipb_miso_o(user_ua9_clkgen_regs),
            
            clk160               => fmc_clk, 
            ext_trig             => ext_trig_ua9,
            ua9_clkstream_p      => fmc_l12_la_p(30),     
            ua9_clkstream_n      => fmc_l12_la_n(30),
            ua9_trig             => open, --fmc_l12_la_p(31),
            ua9_rst              => open,--fmc_l12_la_n(31),        
            ua9_cal1             => open, --fmc_l12_la_p(28),
            ua9_cal2             => open --fmc_l12_la_n(28)
            
            
            
        );
        --===========================================--
        
IBUFDS_inst : IBUFDS_GTE2  generic map ( CLKRCV_TRST => true )  
           port map (
              O => fmc_clk_i,  -- Buffer output
              I => fmc_l12_gbtclk0_a_p,  -- Diff_p buffer input (connect directly to top-level port)
              IB => fmc_l12_gbtclk0_a_n, -- Diff_n buffer input (connect directly to top-level port)
              ceb => '0'
           );        
           
       
           
           bufg_inst : BUFG port map (
                i=> fmc_clk_i,
                o=> fmc_clk
           );

     
--     ext_trig_ua9 <= fmc_l12_la_p(28);
--     ext_trig_ua9 <= '0
     ext_trig_ua9 <= ext_async_l1a_0;
     
     fmcl8_la_ibuf_async_l1a0 : ibufds generic map( DIFF_TERM => TRUE ) port map (i => fmc_l12_la_p(21), ib => fmc_l12_la_n(21), o => ext_async_l1a_0 );
     fmcl8_la_ibuf_async_l1a1 : ibufds generic map( DIFF_TERM => TRUE ) port map (i => fmc_l12_la_p(32), ib => fmc_l12_la_n(32), o => ext_async_l1a_1 ); 
 
 --    ext_async_l1a <=  ext_async_l1a_int when ipb_cnfg.ext_async_l1a_pol = '1' else not ext_async_l1a_int;
 

     trigger_tdc_inst : entity work.trigger_tdc
         port map(   
                 clk                    => ipb_clk,
                 reset                => ipb_rst_i,
                 ipb_mosi_i            => ipb_mosi_i(user_ua9_tdc_regs),
                 ipb_miso_o            => ipb_miso_o(user_ua9_tdc_regs),
         
                 clk_40MHz           => clk_40MHz,
                 clk_320MHz          => clk_320MHz,
                 clk_640MHz          => clk_640MHz,                
                 
                 trigger0            =>  ext_async_l1a_0, 
                 trigger1            =>  ext_async_l1a_1,
                 ua9_trig            => ext_trig_ua9,
                 ua9_rst             => ext_rst_ua9,
                 trig_out            =>  tdc_trig_out,                
--                 trig_invert         =>  ipb_cnfg.ext_async_l1a_pol,
                 tdc_reg             =>  tdc_reg,
                 
--                 holdoff_val         =>  x"19",
--                 trigger_stretch_val =>  "0100",
--                 trigger0_delay      =>  "0001000000",
--                 trigger1_delay      =>  "0001000000",
--                 ua9_override        =>  '0',
--                 ua9_auto_override   =>  '1',
                 ua9_count_out       =>  tdc_ua9_count                                          
     );
     
    process(ipb_rst_i,clk_40MHz)
    --variable toggle : std_logic; 
    begin
     if ipb_rst_i='1' then 
      --toggle:='0';
     elsif rising_edge(clk_40MHz) then
       fmc_l12_la_n(31)<=tdc_trig_out;
--       fmc_l12_la_p(28)<=toggle;
--       toggle:=not toggle;
     end if;
    end process;   
     
     fmc_l12_la_p(28)<=clk_40MHz;
     
     
     ttc_inst :  entity  work.ttc_dec 
     PORT MAP (
     
                clk                   => ipb_clk,
                reset                 => ipb_rst_i,
                ipb_mosi_i            => ipb_mosi_i(user_ua9_clkdec_regs),
                ipb_miso_o            => ipb_miso_o(user_ua9_clkdec_regs),
     
                fabric_clk_in => fabric_clk,
                clk_40           => clk_40MHz,
                clk_320          => clk_320MHz,
                clk_640          => clk_640MHz,
                clk40_out        => fmc_l12_la_p(31),  
                ttc_in => '0',                
                ttc_trig => open,
                ttc_rst => ext_rst_ua9,
                ttc_cal => open                
     );
     
    
   --fmcl8_la_ibuf_async_l1a0_2 : ibufds generic map( DIFF_TERM => TRUE ) port map (i => fmc_l12_la_p(7), ib => fmc_l12_la_n(7), o => ext_trig_ua9  ); 

   --ext_trig_ua9 <= fmc_l12_la_p(28);
   --ext_trig_ua9 <= '0';
end usr;
