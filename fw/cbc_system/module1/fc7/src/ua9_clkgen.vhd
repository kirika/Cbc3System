----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09/12/2017 10:46:42 AM
-- Design Name: 
-- Module Name: ua9_clkgen - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------




library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.ipbus.all;
use work.system_package.all;

use work.user_package.all;
use work.user_version_package.all;

Library UNISIM;
use UNISIM.vcomponents.all;


-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ua9_clkgen is
generic(addr_width : natural := 8);
    Port ( clk : in STD_LOGIC;
       reset : in STD_LOGIC;
       ipb_mosi_i : in ipb_wbus;
       ipb_miso_o : out ipb_rbus;
       
       ext_trig : in std_logic; 
       clk160 : in std_logic    ;   
       ua9_clkstream_p,ua9_clkstream_n : out STD_LOGIC;       
       ua9_trig : out STD_LOGIC;
       ua9_rst : out STD_LOGIC;
       ua9_cal1 : out STD_LOGIC;
       ua9_cal2 : out STD_LOGIC
     
       
       );
end ua9_clkgen;




architecture Behavioral of ua9_clkgen is

signal clk_ua9 : std_logic;
signal ua9_clkstream,ua9_clkstream_i : std_logic; 
signal ua9_toggle : std_logic;

signal ipbus_ack : std_logic;

signal ua9_period, ua9_trig_pos, ua9_rst_pos, ua9_cal1_pos, ua9_cal2_pos : std_logic_vector(31 downto 0);
signal ua9_c : std_logic_vector(31 downto 0);
signal ua9_clkrst : std_logic;
signal ua9_trig_int,ua9_rst_int,ua9_cal1_int, ua9_cal2_int: std_logic;
signal ua9_trig_delay : std_logic_vector(7 downto 0);

signal ua9_state : std_logic_vector(1 downto 0);
signal ua9_cd : std_logic_vector(1 downto 0);

signal ext_trig_i,rst_once,rst_once_i,rst_once_j,rst_once_k : std_logic;
signal ext_trig_xor : std_logic;

signal dbg : std_logic;

begin


OBUFDS_inst : OBUFDS 
 generic map
( IOSTANDARD => "DEFAULT",
SLEW => "FAST")
port map ( O => ua9_clkstream_p,
OB => ua9_clkstream_n, 
I => ua9_clkstream
);

clk_ua9<=clk160;

process(reset, clk_ua9)
variable h : std_logic_vector(2 downto 0);
begin
  if reset='1' then
    ua9_c<=(others=>'0');
    ua9_trig_int<='0';
    ua9_cal1_int<='0';
    ua9_cal2_int<='0';
    ua9_rst_int<='0';
    ua9_toggle<='0';
    ua9_state<="00";
    
    ext_trig_i<='0';
    rst_once_i<='0';
    rst_once_j<='0';
    rst_once_k<='0';    
    
    ua9_cd<=(others => '0');
    
  elsif rising_edge(clk_ua9) then

  
    ua9_clkstream<=ua9_clkstream_i;
    ext_trig_i<=ext_trig xor ext_trig_xor; 
    
    rst_once_i<=rst_once;
    rst_once_j<=rst_once_i;
    rst_once_k<='0';
    if rst_once_i='1' and rst_once_j='0' then
      rst_once_k<='1';
    end if;
  
    ua9_cd<=std_logic_vector(unsigned(ua9_cd)+1);
    
    if ua9_cd="00" then      
      ua9_trig<=ua9_trig_int or ext_trig_i;
      ua9_rst<=ua9_rst_int or rst_once_k;
      
      ua9_cal1<=ua9_cal1_int;
      ua9_cal2<=ua9_cal2_int;
        
        
         
      case ua9_state is 
        when "00" =>
          if rst_once_k='1' then
            h:="010";
            rst_once_k<='0';
          else
            h:=(ua9_trig_int or ext_trig_i)&ua9_rst_int&(ua9_cal1_int or ua9_cal2_int);
          end if;
          
          ua9_trig_int<='0';
          ua9_rst_int<='0';
          ua9_cal1_int<='0';
          ua9_cal2_int<='0';
     
          case h is
            when "100" => --trig, inhibit a single pulse
              ua9_state<="00";
              ua9_clkstream_i<='0';
            when "010" => 
              ua9_state<="01";
              ua9_clkstream_i<='0';
            when "001" => 
              ua9_state<="11";
              ua9_clkstream_i<='0';
            when others => 
              ua9_clkstream_i<='1';  
               
          end case;
           
        when "01" => -- reset, keep this one
          ua9_state<="10";
          ua9_clkstream_i<='1';

        when "10" => -- reset, skip a second pulse
          ua9_state<="00";
          ua9_clkstream_i<='0';

        when "11" => --trig, skip a second pulse
          ua9_state<="00";
          ua9_clkstream_i<='0';             
         
        when others => 
          ua9_state<="00";
           
      end case;  
        
      if ua9_clkrst='1' then
        ua9_clkstream_i<='0';
      end if;  
              
      if ua9_clkrst='1' or ua9_c=ua9_period then
        ua9_c<=(others=>'0');
      else 
        ua9_c<=std_logic_vector(unsigned(ua9_c)+1);
      end if;
                
      if ua9_c=ua9_trig_pos then
        ua9_trig_int<='1';
      end if;
            
      if ua9_c=ua9_rst_pos then
        ua9_rst_int<='1';
      end if;
    
      if ua9_c=ua9_cal1_pos then
        ua9_cal1_int<='1';
      end if;
    
      if ua9_c=ua9_cal2_pos then
        ua9_cal2_int<='1';
      end if;        
    end if;
    
    if ua9_cd="10" then
      ua9_clkstream_i<='0';    
    end if;
    
    if dbg='1' then
      ua9_clkstream_i<=ua9_cd(1);
    end if;   
           
  end if;
end process;


process(reset, clk)
begin
  if reset='1' then
    ipbus_ack 	 <= '0';
    ua9_clkrst<='1';
    rst_once<='0';
    
    dbg<='0';
  elsif rising_edge(clk) then
        
      ipbus_ack<='0';
	  if ipb_mosi_i.ipb_strobe='1' and ipbus_ack='0' and ipb_mosi_i.ipb_write='1' then       
         case ipb_mosi_i.ipb_addr(2 downto 0) is 
           when "000" =>
             ua9_period<=ipb_mosi_i.ipb_wdata;
           when "001" => 
             ua9_trig_pos<=ipb_mosi_i.ipb_wdata;
           when "010" =>
             ua9_rst_pos<=ipb_mosi_i.ipb_wdata;
             
           when "011" => 
             ua9_clkrst<=ipb_mosi_i.ipb_wdata(0);
             rst_once<=ipb_mosi_i.ipb_wdata(1);   
             ext_trig_xor<=ipb_mosi_i.ipb_wdata(2);             
             dbg<=ipb_mosi_i.ipb_wdata(3);
             
           when "100" => 
              ua9_cal1_pos<=ipb_mosi_i.ipb_wdata;
              
           when "101" => 
              ua9_cal2_pos<=ipb_mosi_i.ipb_wdata;
              
                
           when others =>  
              
         end case;             
         ipbus_ack<='1'; 
       end if;  
                  

       if ipb_mosi_i.ipb_strobe='1' and ipbus_ack='0' and ipb_mosi_i.ipb_write='0' then
         ipb_miso_o.ipb_rdata<=X"1234567c";
               
       
         ipbus_ack<='1'; 
    
       end if;
       
  end if;
end process;               
           
           
     ipb_miso_o.ipb_ack <=ipbus_ack;
     ipb_miso_o.ipb_err <= '0';


end Behavioral;
