----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 10.10.2017 17:48:46
-- Design Name: 
-- Module Name: trigger_tdc - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

use work.ipbus.all;
use work.system_package.all;

use work.user_package.all;
use work.user_version_package.all;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity trigger_tdc is
    Port ( 
           clk : in STD_LOGIC; --ibpus clock
           reset : in STD_LOGIC;
           ipb_mosi_i : in ipb_wbus;
           ipb_miso_o : out ipb_rbus;    
    
           clk_40MHz : in STD_LOGIC;
           clk_320MHz : in STD_LOGIC;
           clk_640MHz : in STD_LOGIC;
           
           trigger0 : in STD_LOGIC;
           trigger1 : in STD_LOGIC;
           ua9_trig : in STD_LOGIC;
           ua9_rst  : in STD_LOGIC;
           trig_out : out STD_LOGIC;
           tdc_reg  : out std_logic_vector(31 downto 0);
           ua9_count_out : out std_logic_vector(31 downto 0)          
           );
end trigger_tdc;

architecture Behavioral of trigger_tdc is

signal ipbus_ack : std_logic;
signal reset_i  : std_logic;

signal trigger_c : std_logic_vector(2 downto 0);
signal trigger_320_rst : std_logic;

signal holdoff_enabled_i, holdoff_enabled : std_logic;


signal trigger0_Di,trigger1_Di : std_logic_vector(3 downto 0);
signal trigger0_D,trigger1_D : std_logic_vector(3 downto 0);
signal trigger0_Dp,trigger1_Dp : std_logic_vector(3 downto 0);
signal trigger0_dec,trigger1_dec : std_logic_vector(1 downto 0);
signal trigger0_dec_i,trigger1_dec_i : std_logic_vector(1 downto 0);
signal trigger0_Dp0,trigger1_Dp0 : std_logic;

signal trigger0_reg,trigger0_reg_i,trigger0_reg_j  : std_logic_vector(4 downto 0);
signal trigger0_slow,trigger0_slow_i, trigger0_stored : std_logic;

signal trigger0_active, trigger0_holdoff : std_logic;
signal trigger0_holdoff_c: std_logic_vector(7 downto 0);

signal trigger1_active, trigger1_holdoff : std_logic;
signal trigger1_holdoff_c: std_logic_vector(7 downto 0);

signal trigger1_reg,trigger1_reg_i,trigger1_reg_j  : std_logic_vector(4 downto 0);
signal trigger1_slow,trigger1_slow_i , trigger1_stored: std_logic;


signal  trig_invert, ua9_override, ua9_auto_override : std_logic;
signal  holdoff_val :  std_logic_vector(7 downto 0);
signal  trigger_stretch_val :  std_logic_vector(3 downto 0);
signal  trigger0_delay :  std_logic_vector(9 downto 0);
signal  trigger1_delay :  std_logic_vector(9 downto 0);

signal trig_out_i : std_logic;
signal tdc_reg_i : std_logic_vector(31 downto 0);


signal ua9_trig_int : std_logic;
signal ua9_trig_c   : std_logic_vector(31 downto 0);
signal ua9_override_i : std_logic;
signal trigger_slow_active : std_logic;
signal trigger_stretch_c : std_logic_vector(3 downto 0);

signal trigger_tdc_reg : std_logic_vector(31 downto 0);

signal trigger0_ena,trigger1_ena,ua9_trig_ena : std_logic;


COMPONENT tdc_trigger_shr
  PORT (
    A : IN STD_LOGIC_VECTOR(9 DOWNTO 0);
    D : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    CLK : IN STD_LOGIC;
    Q : OUT STD_LOGIC_VECTOR(3 DOWNTO 0)
  );
END COMPONENT;

begin


serdes_c : process(clk_320MHz, reset_i) 
begin
  if reset_i='1' then
    trigger_c<="000";
    holdoff_enabled<='0';
  elsif rising_edge(clk_320MHz) then
    holdoff_enabled<=holdoff_enabled_i;
    if trigger_320_rst='1' then
      trigger_c<="000";
    else      
      trigger_c<=std_logic_vector(unsigned(trigger_c)+1);
    end if;
  end if;
end process;  

serdes_l1_0 : process(clk_320MHz, reset_i)
variable trigger_stored_i : std_logic; 
begin
  if reset_i='1' then
    trigger0_Dp<=(others=>'0');
    trigger0_Dp0<='1';
    trigger0_dec<=(others=>'0');    
    trigger0_active<='0';
    trigger0_holdoff<='0';
    trigger0_holdoff_c<=(others => '0');
    trigger0_slow<='0';
    trigger0_reg<=(others => '0');
    trigger0_reg_j<=(others => '0');
    trigger0_stored<='0';
  
  elsif rising_edge(clk_320MHz) then
  
         
    if trig_invert='1' then 
     trigger0_Dp<=not trigger0_D;
    else
     trigger0_Dp<=trigger0_D;
    end if; 
    
    trigger0_Dp0<='0';
    if trigger0_Dp="0000" then
      trigger0_Dp0<='1';
    end if;
    
    trigger0_dec_i<=trigger0_dec;
    if trigger0_Dp(0)='1' then
      trigger0_dec<="00"; 
    elsif trigger0_Dp(1)='1' then
      trigger0_dec<="01"; 
    elsif trigger0_Dp(2)='1' then
      trigger0_dec<="10"; 
    elsif trigger0_Dp(3)='1' then
      trigger0_dec<="11";
    end if; 
        
    

    trigger0_active<='0';
    if trigger0_Dp0='0' and trigger0_holdoff='0' then
      trigger0_active<='1';
      if holdoff_enabled='1' then
       trigger0_holdoff<='1';
       trigger0_holdoff_c<=holdoff_val;
      end if; 
    end if; 
          
    if trigger0_holdoff='1' then
      if trigger0_Dp0='1' then
        if unsigned(trigger0_holdoff_c)/=0 then
          trigger0_holdoff_c<=std_logic_vector(unsigned(trigger0_holdoff_c)-1);
        else
          trigger0_holdoff<='0';
        end if;
      else
        trigger0_holdoff_c<=holdoff_val;
      end if;          
    end if;
    
    
    trigger_stored_i:='0';
    if trigger0_active='1' and (trigger0_stored='0' or trigger_c="011") then
     trigger0_reg_j(1 downto 0)<=trigger0_dec_i;
     trigger0_reg_j(4 downto 2)<=trigger_c;
     trigger_stored_i:='1';
     trigger0_stored<='1';
    end if; 
    
    if trigger_c="011"  then
      trigger0_stored<=trigger_stored_i;
      trigger0_slow<=trigger0_stored;
      trigger0_reg<=trigger0_reg_j;
    end if;  
        
  end if;
end process;






serdes_l1_1 : process(clk_320MHz, reset_i)
variable trigger_stored_i : std_logic; 
begin
  if reset_i='1' then
    trigger1_Dp<=(others=>'0');
    trigger1_Dp0<='1';
    trigger1_dec<=(others=>'0');    
    trigger1_active<='0';
    trigger1_holdoff<='0';
    trigger1_holdoff_c<=(others => '0');
    trigger1_slow<='0';
    trigger1_reg<=(others => '0');
    trigger1_reg_j<=(others => '0');
    trigger1_stored<='0';
  
  elsif rising_edge(clk_320MHz) then
  
         
    if trig_invert='1' then 
     trigger1_Dp<=not trigger1_D;
    else
     trigger1_Dp<=trigger1_D;
    end if; 
    
    trigger1_Dp0<='0';
    if trigger1_Dp="0000" then
      trigger1_Dp0<='1';
    end if;
    
    trigger1_dec_i<=trigger1_dec;
    if trigger1_Dp(0)='1' then
      trigger1_dec<="00"; 
    elsif trigger1_Dp(1)='1' then
      trigger1_dec<="01"; 
    elsif trigger1_Dp(2)='1' then
      trigger1_dec<="10"; 
    elsif trigger1_Dp(3)='1' then
      trigger1_dec<="11";
    end if; 
        
    

    trigger1_active<='0';
    if trigger1_Dp0='0' and trigger1_holdoff='0' then
      trigger1_active<='1';
      if holdoff_enabled='1' then
       trigger1_holdoff<='1';
       trigger1_holdoff_c<=holdoff_val;
      end if; 
    end if; 
          
    if trigger1_holdoff='1' then
      if trigger1_Dp0='1' then
        if unsigned(trigger1_holdoff_c)/=0 then
          trigger1_holdoff_c<=std_logic_vector(unsigned(trigger1_holdoff_c)-1);
        else
          trigger1_holdoff<='0';
        end if;
      else
        trigger1_holdoff_c<=holdoff_val;
      end if;          
    end if;
    
    
    trigger_stored_i:='0';
    if trigger1_active='1' and (trigger1_stored='0' or trigger_c="011") then
     trigger1_reg_j(1 downto 0)<=trigger1_dec_i;
     trigger1_reg_j(4 downto 2)<=trigger_c;
     trigger_stored_i:='1';
     trigger1_stored<='1';
    end if; 
    
    if trigger_c="011"  then
      trigger1_stored<=trigger_stored_i;
      trigger1_slow<=trigger1_stored;
      trigger1_reg<=trigger1_reg_j;
    end if;  
        
  end if;
end process;











serdes_l2 : process(clk_40MHz, reset_i) 
begin
  if reset_i='1' then
    trigger_slow_active<='0';

    ua9_trig_int<='0';    
    trigger0_slow_i<='0';
    trigger1_slow_i<='0';
    
    ua9_trig_c<=(others=>'0');
    
    trigger_320_rst<='1';
    
    trigger_tdc_reg<=(others => '0');
    tdc_reg<=(others => '0');

    trig_out<='0';
    
    holdoff_enabled_i<='0';
    
    ua9_override_i<='0';
    
  elsif rising_edge(clk_40MHz) then  
    trigger_320_rst<='0';
    
    ua9_count_out<=ua9_trig_c;
    
    if holdoff_val=x"ff" then
      holdoff_enabled_i<='0';
    else  
      holdoff_enabled_i<='1';
    end if;
    
--    tdc_reg_i<=trigger_tdc_reg;
--    tdc_reg<=tdc_reg_i;
    tdc_reg<=trigger_tdc_reg;
    
    trig_out_i<=trigger_slow_active;    
    trig_out<=trig_out_i;
  
    ua9_trig_int<=ua9_trig;
    
    trigger0_slow_i<=trigger0_slow;
    trigger1_slow_i<=trigger1_slow;
    
    trigger0_reg_i<=trigger0_reg;
    trigger1_reg_i<=trigger1_reg;
    
    
    if ua9_trig_int='1' then
      ua9_trig_c<=std_logic_vector(unsigned(ua9_trig_c)+1);
    end if;
    
    if ua9_rst='1' then
      ua9_trig_c<=(others=>'0');
    end if;
    

    if unsigned(trigger_stretch_c)=0 then
      trigger_slow_active<='0';
    else
      trigger_stretch_c<=std_logic_vector(unsigned(trigger_stretch_c)-1);
    end if;
    

    trigger_tdc_reg(15 downto 13)<="000";   

    if (trigger0_slow_i='1' and trigger0_ena='1') or (trigger1_slow_i='1' and trigger1_ena='1')  or (ua9_trig_int='1' and ua9_trig_ena='1') then
      trigger_slow_active<='1';
      
      trigger_tdc_reg(13)<=ua9_trig_int;
      trigger_tdc_reg(14)<=trigger0_slow_i ;
      trigger_tdc_reg(15)<=trigger1_slow_i;        
      
      trigger_tdc_reg(31 downto 16)<=std_logic_vector(unsigned(trigger_tdc_reg(31 downto 16))+1);
      
      
      trigger_tdc_reg(2 downto 0)<=ua9_trig_c(2 downto 0);
      trigger_tdc_reg(7 downto 3)<=trigger0_reg_i;
      trigger_tdc_reg(12 downto 8)<=trigger1_reg_i;
      
           
      if (trigger0_slow_i='0' and trigger1_slow_i='0') or (ua9_trig_int='1' and ua9_override_i='1') then
        trigger_tdc_reg(15 downto 14)<="00";
        trigger_tdc_reg(12 downto 0)<=ua9_trig_c(12 downto 0);
      end if;         
      
      if ua9_auto_override='1' and ua9_trig_c(5 downto 0)="000000" then 
        ua9_override_i<='1';
      else
        ua9_override_i<=ua9_override;        
      end if;
      
      trigger_stretch_c<=trigger_stretch_val;
--      trigger_tdc_reg<=x"01234567";
    else
      trigger_tdc_reg(3 downto 0)<=trigger_stretch_c;
--      trigger_tdc_reg<=x"2345789a";  
    end if;  
    

  end if;
end process;

process(reset, clk)
  begin
    if reset='1' then
      ipbus_ack      <= '0';
      reset_i<='1';
      trigger0_ena<='1';
      trigger1_ena<='1';
      ua9_trig_ena<='1';
    elsif rising_edge(clk) then
          
        ipbus_ack<='0';
        if ipb_mosi_i.ipb_strobe='1' and ipbus_ack='0' and ipb_mosi_i.ipb_write='1' then       
           case ipb_mosi_i.ipb_addr(2 downto 0) is
             when "000" => 
                reset_i<=ipb_mosi_i.ipb_wdata(0);
                ua9_override<=ipb_mosi_i.ipb_wdata(1);
                ua9_auto_override<=ipb_mosi_i.ipb_wdata(2);
                trig_invert<=ipb_mosi_i.ipb_wdata(3);
                ua9_trig_ena<=ipb_mosi_i.ipb_wdata(4);
                trigger0_ena<=ipb_mosi_i.ipb_wdata(5);
                trigger1_ena<=ipb_mosi_i.ipb_wdata(6);
                
             when "001" =>    
                holdoff_val<=ipb_mosi_i.ipb_wdata(7 downto 0);
             when "010" =>                 
                trigger_stretch_val<=ipb_mosi_i.ipb_wdata(3 downto 0);                
                
             when "100" =>     
                trigger0_delay<=ipb_mosi_i.ipb_wdata(9 downto 0);
             when "101" =>                 
                trigger1_delay<=ipb_mosi_i.ipb_wdata(9 downto 0);
             when others => 
               null;     
           end case;             
           ipbus_ack<='1'; 
         end if;  
                    
  
         if ipb_mosi_i.ipb_strobe='1' and ipbus_ack='0' and ipb_mosi_i.ipb_write='0' then
         
           case ipb_mosi_i.ipb_addr(2 downto 0) is
             when "110" =>
               ipb_miso_o.ipb_rdata(7 downto 0)<=trigger1_Dp&trigger0_Dp;
  
               
             when "111" => 
               ipb_miso_o.ipb_rdata<=ua9_trig_c;  
             when others => 
               ipb_miso_o.ipb_rdata<=X"1234567b";             
           end case;       
           
                 
         
           ipbus_ack<='1'; 
      
         end if;
         
    end if;
  end process;               
             
             
  ipb_miso_o.ipb_ack <=ipbus_ack;
  ipb_miso_o.ipb_err <= '0';




tdc_serdes0: unisim.vcomponents.ISERDESE2
    generic map(
      DATA_RATE => "DDR",
      DATA_WIDTH => 4,
      DYN_CLKDIV_INV_EN => "FALSE",
      DYN_CLK_INV_EN => "FALSE",
      INIT_Q1 => '0',
      INIT_Q2 => '0',
      INIT_Q3 => '0',
      INIT_Q4 => '0',
      INTERFACE_TYPE => "NETWORKING",
      IOBDELAY => "NONE",
      IS_CLKB_INVERTED => '1',
      IS_CLKDIVP_INVERTED => '0',
      IS_CLKDIV_INVERTED => '0',
      IS_CLK_INVERTED => '0',
      IS_D_INVERTED => '0',
      IS_OCLKB_INVERTED => '0',
      IS_OCLK_INVERTED => '0',
      NUM_CE => 2,
      OFB_USED => "FALSE",
      SERDES_MODE => "MASTER",
      SRVAL_Q1 => '0',
      SRVAL_Q2 => '0',
      SRVAL_Q3 => '0',
      SRVAL_Q4 => '0'
    )
        port map (
      BITSLIP => '0',
      CE1 => '1',
      CE2 => '1',
      CLK => clk_640MHz,
      CLKB => clk_640MHz,
      CLKDIV => clk_320MHz,
      CLKDIVP => '0',
      D => trigger0,
      DDLY => '0',
      DYNCLKDIVSEL => '0',
      DYNCLKSEL => '0',
      O => open,
      OCLK => '0',
      OCLKB => '0',
      OFB => '0',
      Q1 => trigger0_Di(3),
      Q2 => trigger0_Di(2),
      Q3 => trigger0_Di(1),
      Q4 => trigger0_Di(0),
      Q5 => open,
      Q6 => open,
      Q7 => open,
      Q8 => open,
      RST => reset_i,
      SHIFTIN1 => '0',
      SHIFTIN2 => '0',
      SHIFTOUT1 => open,
      SHIFTOUT2 => open
    );
tdc_serdes1: unisim.vcomponents.ISERDESE2
        generic map(
          DATA_RATE => "DDR",
          DATA_WIDTH => 4,
          DYN_CLKDIV_INV_EN => "FALSE",
          DYN_CLK_INV_EN => "FALSE",
          INIT_Q1 => '0',
          INIT_Q2 => '0',
          INIT_Q3 => '0',
          INIT_Q4 => '0',
          INTERFACE_TYPE => "NETWORKING",
          IOBDELAY => "NONE",
          IS_CLKB_INVERTED => '1',
          IS_CLKDIVP_INVERTED => '0',
          IS_CLKDIV_INVERTED => '0',
          IS_CLK_INVERTED => '0',
          IS_D_INVERTED => '0',
          IS_OCLKB_INVERTED => '0',
          IS_OCLK_INVERTED => '0',
          NUM_CE => 2,
          OFB_USED => "FALSE",
          SERDES_MODE => "MASTER",
          SRVAL_Q1 => '0',
          SRVAL_Q2 => '0',
          SRVAL_Q3 => '0',
          SRVAL_Q4 => '0'
        )
            port map (
          BITSLIP => '0',
          CE1 => '1',
          CE2 => '1',
          CLK => clk_640MHz,
          CLKB => clk_640MHz,
          CLKDIV => clk_320MHz,
          CLKDIVP => '0',
          D => trigger1,
          DDLY => '0',
          DYNCLKDIVSEL => '0',
          DYNCLKSEL => '0',
          O => open,
          OCLK => '0',
          OCLKB => '0',
          OFB => '0',
          Q1 => trigger1_Di(3),
          Q2 => trigger1_Di(2),
          Q3 => trigger1_Di(1),
          Q4 => trigger1_Di(0),
          Q5 => open,
          Q6 => open,
          Q7 => open,
          Q8 => open,
          RST => reset_i,
          SHIFTIN1 => '0',
          SHIFTIN2 => '0',
          SHIFTOUT1 => open,
          SHIFTOUT2 => open
        );

  trigger0_shr_inst : tdc_trigger_shr
  PORT MAP (
    A => trigger0_delay,
    D => trigger0_Di,
    CLK => clk_320MHz,
    Q => trigger0_D
  );

  trigger1_shr_inst : tdc_trigger_shr
  PORT MAP (
    A => trigger1_delay,
    D => trigger1_Di,
    CLK => clk_320MHz,
    Q => trigger1_D
  );
  
  
  
  

end Behavioral;
