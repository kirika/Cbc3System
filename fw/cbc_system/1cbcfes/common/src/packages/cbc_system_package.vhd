--===========================--
-- cbc_system_package
-- 28.10.2016 Kirika Uchida
--===========================--

library ieee;
use ieee.std_logic_1164.all;
use IEEE.NUMERIC_STD.ALL;
--library UNISIM;
--use UNISIM.VComponents.all;


package cbc_system_package is

    --=== version ========--
    constant cbc_system_ver_major:integer range 0 to 15 :=2;
    constant cbc_system_ver_minor:integer range 0 to 15 :=11;
    constant cbc_system_ver_build:integer range 0 to 255:=2;
    constant cbc_system_ver_year :integer range 0 to 99 :=17;
    constant cbc_system_ver_month:integer range 0 to 12 :=09;
    constant cbc_system_ver_day  :integer range 0 to 31 :=29;

  
	constant usr_id_0		 	:std_logic_vector(31 downto 0):= x"43_42_43_33";	-- 'C' 'B' 'C' '3'
	constant firmware_id	 	:std_logic_vector(31 downto 0):= std_logic_vector(to_unsigned(cbc_system_ver_major,4)) &
																 std_logic_vector(to_unsigned(cbc_system_ver_minor,4)) &
																 std_logic_vector(to_unsigned(cbc_system_ver_build,8)) &
																 std_logic_vector(to_unsigned(cbc_system_ver_year ,7)) &
																 std_logic_vector(to_unsigned(cbc_system_ver_month,4)) &
																 std_logic_vector(to_unsigned(cbc_system_ver_day  ,5)) ;


    --=======================================--
    constant firmware_type          : std_logic_vector(2 downto 0) := "001";
    constant NCBC                   : integer := 7; -- 1 to (CBCID_WIDTH-1)**2
    constant NFE                    : integer := 2; -- 1 to (FEID_WIDTH-1)**2
    constant RARP_EN                : boolean := false;
    --=======================================--
--    type fe_cbc_i0s_type is array (0 to NFE-1) of integer;
--   --=======================================--
--    constant fe_cbc_i0s                    : fe_cbc_i0s_type := ( 0, 1 );
   --=======================================--
--   type cbc_to_fe_map_type is array (0 to NCBC-1) of integer;
--   --=======================================--   
--   constant cbc_to_fe_map                  : cbc_to_fe_map_type := ( 0, 0, 0,  );
--   --=======================================--
      
    constant BEID_WIDTH                    : integer :=  7;
    constant FEID_WIDTH                    : integer :=  3;
    constant CBCID_WIDTH                   : integer :=  5;
    constant CBC_I2C_REPLY_FIFO_ADDR_WIDTH : integer := 16;
    constant DATA_BUFFER_ADDR_WIDTH        : integer := 17;    
    constant EVENT_DATA_SIZE_WIDTH         : integer := 12;
    constant DATA_SECTION_DATA_SIZE_WIDTH  : integer := 6;
    constant DEFAULT_SAFE_NWORD_FREE       : integer := 32 * 14;
    constant NDCTT_FAN                     : integer := 4;
    
    type cbcdata_type is ( raw, hit );    
    type max_section_packet_size_type is
    record
        cbc_data_raw : positive;
    end record;
    type data_section_id_type is
    record
        cbc_data_raw : integer;
    end record;
    constant max_section_packet_size : max_section_packet_size_type := (cbc_data_raw => 41);
    constant data_section_id         : data_section_id_type := (cbc_data_raw => 1);   

	constant CBC_I2C_PRESCALE		            : integer := 40; -- 1MHz with 40MHz clock
--	constant CBC_I2C_PRESCALE		            : integer := 80; -- 0.5MHz
--	constant CBC_I2C_PRESCALE		            : integer := 400; -- 100kHz
    constant I2C_BROADCAST_CBCID : std_logic_vector(CBCID_WIDTH-1 downto 0) := (others => '0');
    constant I2C_BROADCAST_FEID  : std_logic_vector(FEID_WIDTH-1 downto 0)  := (others => '0');

    type cbc_par_data_type is array (1 to 6) of std_logic_vector(7 downto 0);       
    type cbc_par_data_set_type is array (0 to NCBC-1) of cbc_par_data_type;

    type cbc_data_set_type is array (0 to NCBC-1) of std_logic_vector(6 downto 1);

    type cbc_triggered_data_buffer_type is array (0 to 34) of std_logic_vector(7 downto 0);
     
    type mmcm_for_iserdes_sig_type is
    record
        daddr                          : std_logic_vector(6 downto 0);
        dclk                           : std_logic;
        den                            : std_logic;
        din                            : std_logic_vector(15 downto 0);
        dout                           : std_logic_vector(15 downto 0);
        dwe                            : std_logic;
        drdy                           : std_logic;
        locked                         : std_logic;
        rst_mmcm                       : std_logic;    
    end record;     
    type mmcm_for_iserdes_sig_set_type is array (0 to NFE-1) of mmcm_for_iserdes_sig_type;
    
     type sigs_to_mmcme2_drp_type is
    record
        sclk  : std_logic;
        sen   : std_logic;
        saddr : std_logic_vector(1 downto 0);
        rst   : std_logic;
    end record;
    type  sigs_to_mmcme2_drp_set_type is array (0 to NFE-1) of sigs_to_mmcme2_drp_type;

    type  sigs_from_mmcme2_drp_type is
    record
        srdy  : std_logic;
        saddr : std_logic_vector(1 downto 0);
    end record;
    type  sigs_from_mmcme2_drp_set_type is array (0 to NFE-1) of  sigs_from_mmcme2_drp_type;
 
    type cbci2c_fecdata_set_type    is array (0 to NCBC-1) of std_logic_vector(7 downto 0);
    type cbc_i2c_bus_miso_type is
    record
        sda                : std_logic;
    end record;
    type cbc_i2c_bus_miso_set_type is array (0 to NFE-1) of cbc_i2c_bus_miso_type;
    type cbc_i2c_bus_mosi_type is
    record
        scl                : std_logic;
        sda                : std_logic;
    end record;
    type cbc_i2c_bus_mosi_set_type is array (0 to NFE-1) of cbc_i2c_bus_mosi_type;
    
    type fast_signal_40MHz_indices_type is
    record
        orbit_reset        : integer;
        test_pulse         : integer;
        trigger            : integer;
        fast_reset         : integer;
    end record;

    type fast_signal_40MHz_in_type is
    record
        fast_reset         : std_logic;
        trigger            : std_logic;
        test_pulse         : std_logic;
        orbit_reset        : std_logic;
    end record;

    constant fast_signal_40MHz_indices : fast_signal_40MHz_indices_type :=
    ( orbit_reset => 0, test_pulse => 1, trigger => 2, fast_reset => 3 );

    type ci2cbmgr_out_type is
    record
        reply_fifo_empty                : std_logic;
        reply_fifo_dout                 : std_logic_vector(31 downto 0);
    end record;
    type ci2cbmgr_out_set_type is array (0 to NFE-1) of ci2cbmgr_out_type;  

    type ci2cbmgr_in_type is
    record
        ipb_clk                         : std_logic;
        command_fifo_reset              : std_logic;
        command_fifo_wclk               : std_logic;
        command_fifo_we                 : std_logic;
        command_fifo_din                : std_logic_vector(31 downto 0);
        reply_fifo_reset                : std_logic;
        reply_fifo_read_next            : std_logic;
    end record;
    type ci2cbmgr_in_set_type is array (0 to NFE-1) of ci2cbmgr_in_type;  
    
    type dsoebi_type is
    record
        data_packet_out      : std_logic_vector(31 downto 0);
        data_packet_valid    : std_logic;
        data_packet_ready    : std_logic;
        data_packet_nword    : std_logic_vector(DATA_SECTION_DATA_SIZE_WIDTH-1 downto 0);
        cbc_data_l1_count    : std_logic_vector(8 downto 0);
    end record;
    type dsoebi_set_type is array (0 to NCBC-1) of dsoebi_type;

    type dsiebo_type is
    record
        read_en                  : std_logic;
    end record;
    type dsiebo_set_type is array (0 to NCBC-1) of dsiebo_type;

    type ebodbi_type is
    record
        data                  : std_logic_vector(31 downto 0);
        we                    : std_logic;
        end_of_event          : std_logic;    
    end record;

    type ebidbo_type is
    record
        n_word_free           : std_logic_vector(DATA_BUFFER_ADDR_WIDTH downto 0);
    end record;
    
    type db_to_ipbif_type is
    record
        next_data             : std_logic_vector(31 downto 0);
        next_addr             : std_logic_vector(DATA_BUFFER_ADDR_WIDTH - 1 downto 0);
        load_next             : std_logic;       
    end record;
    
    type ipbif_to_db_type is
    record
        addr                  : std_logic_vector(DATA_BUFFER_ADDR_WIDTH - 1 downto 0);
    end record;
    
    type bram_to_dcts_type is
    record
        ready   : std_logic;
    end record;
    type bram_to_dcts_set_type is array (0 to NFE-1) of bram_to_dcts_type;
  
    type dcts_to_bram_type is
    record
        req    : std_logic;
        wen    : std_logic;
        waddr  : std_logic_vector(9 downto 0);
        din    : std_logic_vector(15 downto 0);
    end record;   
    type dcts_to_bram_set_type is array (0 to NFE-1) of dcts_to_bram_type;
   
   type dctt_to_iserdes_type is
   record
        in_delay_reset          :  std_logic;
        in_delay_ce             :  std_logic_vector(5 downto 0);
        in_delay_tap_in         :  std_logic_vector(5*6-1 downto 0);
        in_delay_data_inc       :  std_logic_vector(5 downto 0);
        iserdes_io_reset        :  std_logic;
        bitslip                 :  std_logic_vector(5 downto 0);     
   end record;
   type dctt_to_iserdes_set_type is array (0 to NFE-1) of dctt_to_iserdes_type;

    type in_delay_tap_out_type is array (0 to NCBC-1) of std_logic_vector(5*6-1 downto 0);
    type in_delay_tap_in_type  is array (0 to NCBC-1) of std_logic_vector(5*6-1 downto 0);

    type in_delay_tap_out_to_dctt_type is array (0 to NFE-1) of std_logic_vector(5*6-1 downto 0);

    type fscbram_in_type is
    record
        wen         : std_logic;
        din         : std_logic_vector(31 downto 0);
        count       : unsigned(11 downto 0);
        addr        : std_logic_vector(11 downto 0);
    end record;
   
---------------------------     
-- ipb_cbc_system_cnfg_type
---------------------------    
    -- global
    type cbc_cnfg_type is
    record
        id                       : std_logic_vector(CBCID_WIDTH-1 downto 0);
        fe_id                    : std_logic_vector(FEID_WIDTH-1 downto 0);
        i2c_address              : std_logic_vector(6 downto 0);
        active                   : std_logic;
    end record;
    type cbc_cnfg_set_type is array (0 to NCBC-1) of cbc_cnfg_type;
    type fe_cnfg_type is
    record
        id                       : std_logic_vector(FEID_WIDTH-1 downto 0);
        n_active_cbc             : std_logic_vector(CBCID_WIDTH-1 downto 0);
        active_cbcs              : cbc_cnfg_set_type;
    end record;
    type fe_cnfg_set_type is array (0 to NFE-1) of fe_cnfg_type;
	type global_cnfg_type is
    record
        be_id                    : std_logic_vector(BEID_WIDTH-1 downto 0);
        n_active_cbc             : std_logic_vector(CBCID_WIDTH - 1 downto 0 );
        cbcs                     : cbc_cnfg_set_type;
        fes                      : fe_cnfg_set_type;  
        cbc_clk_phase_shift      : std_logic;
        trigger_master_external  : std_logic;
        test_out_sel0            : std_logic_vector(15 downto  0);  
        test_out_sel1            : std_logic_vector(15 downto  0);                
    end record;    
    
    type data_clock_timing_tuning_cnfg_type is
    record
       min_tuning_idelay_tap     : std_logic_vector(4 downto 0);
       max_tuning_idelay_tap     : std_logic_vector(4 downto 0);
       idelay_offset             : std_logic_vector(4 downto 0);
    end record;
    
    type data_clock_timing_tuning_fe_cnfg_type is
    record
        clock_timing_tuning_cbc_sel : std_logic_vector(CBCID_WIDTH-1 downto 0);
    end record;
        
    type data_clock_timing_tuning_fe_cnfg_set_type is array (0 to NFE-1) of data_clock_timing_tuning_fe_cnfg_type;

    type io_cnfg_type is
    record
        data_clock_timing_tuning  : data_clock_timing_tuning_cnfg_type;
        fes                       : data_clock_timing_tuning_fe_cnfg_set_type;
        ext_async_l1a_pol         : std_logic;
    end record;
    
    -- system clock
    type cbc_system_clk_cnfg_type is ( extclk_fmcl8_clk_a, intclk );
    
    type mmcme2_drp_cnfg_type is
    record
        ipb_ctrl_en             : std_logic;
        saddr                   : std_logic_vector(1 downto 0);
    end record;
    
    type cbc_i2c_bus_managers_cnfg_type is    
    record
        ipbus_en                : std_logic;
    end record;

    type fscbram_controller_cnfg_type is
    record
        ipbus_en                : std_logic;
    end record;    
    
    -- fast signal generator
    type fast_signal_generator_cnfg_type is
    record
        fast_reset_en           : std_logic;
        trigger_en              : std_logic;
        test_pulse_en           : std_logic;  
        orbit_reset_en          : std_logic;
        Ncycle                  : std_logic_vector( 31 downto 0 );
        cycle_T                 : std_logic_vector( 31 downto 0 );
        trigger_t               : std_logic_vector( 31 downto 0 );
        test_pulse_t            : std_logic_vector( 31 downto 0 );
        orbit_reset_t           : std_logic_vector( 31 downto 0 );       
    end record;
    type fast_signal_manager_cnfg_type is
    record
        ext_async_l1a_en        : std_logic;
        fast_signal_fmc_en      : std_logic;
        fast_signal_ipbus_en    : std_logic;
        fast_signal_internal_en : std_logic;
        fast_signal_generator   : fast_signal_generator_cnfg_type;
    end record;

	type rdb_controller_cnfg_type is
	record
		latency           : std_logic_vector(7 downto 0);
		write_block_size  : std_logic_vector(15 downto 0);	
		cbc_id            : std_logic_vector(CBCID_WIDTH-1 downto 0);
	end record;

    -- cbc data processor
    type cbc_data_processor_cnfg_type is
    record
        l1a_latency             : std_logic_vector(8 downto 0);
        trig_data_latency       : std_logic_vector(8 downto 0);
    end record;
    type cbc_data_processor_cnfg_set_type is array (0 to NCBC-1) of cbc_data_processor_cnfg_type;

    type event_builder_cnfg_type is
    record
        be_id                   : std_logic_vector(BEID_WIDTH-1 downto 0);
    end record;
    
    type data_buffer_cnfg_type is
    record
        safe_n_word_free         : std_logic_vector(DATA_BUFFER_ADDR_WIDTH downto 0);
    end record;
    
    -- cbc emulator
    type cbc_emulator_data_pattern_type is
    record
        s1_addr        : std_logic_vector( 7 downto 0 );
        s2_addr        : std_logic_vector( 7 downto 0 );
        s3_addr        : std_logic_vector( 7 downto 0 );
        s1_bend        : std_logic_vector( 3 downto 0 );
        s2_bend        : std_logic_vector( 3 downto 0 );
        s3_bend        : std_logic_vector( 3 downto 0 );        
        sof            : std_logic;
        or254          : std_logic;
        error          : std_logic;
        hit254         : std_logic_vector( 254 downto 1 );
    end record;    
    type cbc_emulator_cnfg_type is
    record
        data_pattern        : cbc_emulator_data_pattern_type;
    end record;
    type cbc_emulator_cnfg_set_type is array (0 to NCBC-1) of cbc_emulator_cnfg_type;
    -- config signal container
    type ipb_cbc_system_cnfg_type is
    record
        global                   : global_cnfg_type;
        io                       : io_cnfg_type;
        cbc_system_clk           : cbc_system_clk_cnfg_type;
        mmcme2_drp               : mmcme2_drp_cnfg_type;
        cbc_i2c_bus_managers     : cbc_i2c_bus_managers_cnfg_type;
        fscbram_controller       : fscbram_controller_cnfg_type;
        fast_signal_manager      : fast_signal_manager_cnfg_type;
        rdb_controller           : rdb_controller_cnfg_type;
        cbc_data_processors      : cbc_data_processor_cnfg_set_type;
        event_builder            : event_builder_cnfg_type;
        data_buffer              : data_buffer_cnfg_type;
        cbc_emulators            : cbc_emulator_cnfg_set_type;     
    end record;
---------------------------
-- ipb_cbc_system_ctrl_type
---------------------------
    -- global
    type global_ctrl_type is
    record
        cbc_hard_reset     : std_logic;
    end record;
 
    type mmcme2_drp_ctrl_type is
    record
        rst              : std_logic;
        sen                : std_logic;
    end record;
 
    type data_clock_timing_tuning_ctrl_type is
    record
        reset              : std_logic;
        tune               : std_logic;
        scan_pulse         : std_logic;
    end record;
    type data_clock_timing_tuning_ctrl_set_type is array (0 to NCBC-1) of data_clock_timing_tuning_ctrl_type;

    type io_ctrl_type is
    record
        fs_oserdes_reset                  : std_logic;  
        dctsb_reset                       : std_logic;
        data_clock_timing_tunings         : data_clock_timing_tuning_ctrl_set_type;
    end record;    
    
    type veto_ctrl_type is
    record
        reset                 : std_logic;
    end record;
        
    -- fast command generator
    type fast_signal_generator_ctrl_type is
    record
        reset                 : std_logic;
        load_cnfg             : std_logic;
        start                 : std_logic;
        stop                  : std_logic;
    end record;
    type fast_signal_manager_ctrl_type is
    record
        reset                 : std_logic;
        start_trigger         : std_logic;
        stop_trigger          : std_logic;
        cbc_fast_signal       : std_logic_vector( 3 downto 0 );               
        fast_signal_generator : fast_signal_generator_ctrl_type;
    end record;  
    -- cbc i2c bus manager
    type cbc_i2c_bus_manager_ctrl_type is
    record
        reset                 : std_logic;
        init                  : std_logic;
        reset_fifos           : std_logic;
    end record;
   type cbc_i2c_bus_manager_ctrl_set_type is array (0 to NFE-1) of cbc_i2c_bus_manager_ctrl_type;

	type rdb_controller_ctrl_type is
	record
		reset                 : std_logic;
	end record;

    -- cbc data processor
     type cbc_data_processor_ctrl_type is
    record
        reset                           : std_logic;
        reset_40MHz                     : std_logic;
        frame_counter_reset             : std_logic;
        cbc_ser_data_delay_reset        : std_logic;
        cbc_ser_data_delay_start_tuning : std_logic;
     end record;
    type cbc_data_processor_ctrl_set_type is array (0 to NCBC-1) of cbc_data_processor_ctrl_type;
    -- event builder
    type event_builder_ctrl_type is
    record
        reset             : std_logic;
    end record;
    -- data buffer
    type data_buffer_ctrl_type is
    record
        reset             : std_logic;
        trig_readall      : std_logic;
    end record;
    
    type fscbram_controller_ctrl_type is
    record
        reset             : std_logic;
        start             : std_logic;
        resume            : std_logic;
    end record;
    
    -- cbc emulator
    type cbc_emulator_ctrl_type is
    record
        reset             : std_logic;
        load_data_pattern : std_logic;
    end record;
    type cbc_emulator_ctrl_set_type is  array (0 to NCBC-1) of cbc_emulator_ctrl_type;
    -- control signal container   
    type ipb_cbc_system_ctrl_type is
    record
        global                   : global_ctrl_type;
        mmcme2_drp               : mmcme2_drp_ctrl_type;
        io                       : io_ctrl_type;
        veto                     : veto_ctrl_type;
        fast_signal_manager      : fast_signal_manager_ctrl_type;
        cbc_i2c_bus_managers     : cbc_i2c_bus_manager_ctrl_set_type;
        rdb_controller           : rdb_controller_ctrl_type;             
        cbc_data_processors      : cbc_data_processor_ctrl_set_type;
        event_builder            : event_builder_ctrl_type;
        data_buffer              : data_buffer_ctrl_type;   
        fscbram_controller       : fscbram_controller_ctrl_type;     
        cbc_emulators             : cbc_emulator_ctrl_set_type; 
    end record;
---------------------------
-- ipb_cbc_system_stat_type
---------------------------
    type global_stat_type is
    record
        n_active_cbc : std_logic_vector(CBCID_WIDTH-1 downto 0);   
    end record;

    type cbc_system_clocks_0_stat_type is
    record
        locked : std_logic;
    end record;
    
    type cbc_system_clocks_1_stat_type is
    record
        locked : std_logic;
        saddr  : std_logic_vector(1 downto 0);
    end record;
    type cbc_system_clocks_1_set_stat_type is array(0 to NCBC-1) of cbc_system_clocks_1_stat_type;

    type trig_veto_stat_type is
    record
        internal_l1a_veto         : std_logic;
        external_l1a_veto         : std_logic;
    end record;
    
    type mmcme2_drp_stat_type is
    record
        srdy                    : std_logic;
    end record;
    type mmcme2_drp_stat_set_type is array (0 to NCBC-1) of mmcme2_drp_stat_type;
    
    -- fast command generator
    type fast_signal_generator_fsm_type    is (idle, running);
    type trigger_fsm_type                  is (idle, running);  
    type fast_signal_manager_fsm_type is (init, running);
    type fast_signal_manager_stat_type is
    record
        fast_signal_generator_fsm : fast_signal_generator_fsm_type;
        trigger_fsm               : trigger_fsm_type;
        fsm                       : fast_signal_manager_fsm_type;
        l1a_count                 : std_logic_vector(28 downto 0);
    end record;

    type iserdes_idelay_ctrl_fsm_type is ( wait_for_commands, in_delay_reset_process, in_delay_set_process, in_delay_ce_process, reset_iserdes_process, bitslip_process, pattern_check );

--    type data_clock_timing_tuning_fsm_type is (init, wait_bram_ready, idelay_reset, idelay_scan, pre_idelay_tuning, idelay_tuning, set_idelay, change_clock_phase, reset_iserdes_idelay_ctrl, bitslip_tuning, tuned);
    type data_clock_timing_tuning_fsm_type is (init, wait_bram_ready, idelay_reset, idelay_scan, idelay_tuning, set_idelay, change_clock_phase, reset_iserdes_idelay_ctrl, bitslip_tuning, tuned);

     
--    type idelay_scan_pattern0_type is array (0 to 59) of std_logic_vector(7 downto 0);
--    type idelay_scan_stat_type is
--    record
--        pattern0                     : idelay_scan_pattern_0_type;
--        pattern_changed              : std_logic_vector(59 downto 0);
--        pattern_unstable             : std_logic_vector(59 downto 0);
--        pattern_bad                  : std_logic_vector(59 downto 0);           
--    end record;
    
--    type cbcio_tuning_stat_type is
--    record
--        idelay_reset_tuning_tap      : std_logic_vector(4 downto 0);
--        pre_idelay_tuning_tap        : std_logic_vector(4 downto 0);     
--        pattern_at_idelay_reset      : std_logic_vector(7 downto 0);
--        fsm_processed                : std_logic_vector(9 downto 0);
--    end record;
--    type cbcio_tuning_stat_set_type is array (0 to 1) of cbcio_tuning_stat_type;

        
    type cbcio_stat_type is
    record
        delay_locked                 : std_logic;
        in_delay_tap_out             : std_logic_vector(5*6-1 downto 0);         
        slvs5                        : std_logic_vector(7 downto 0);
    end record;       
    type cbcio_stat_set_type is array (0 to NCBC - 1) of cbcio_stat_type;
        
    type dctt_stat_type is
    record
        iserdes_bitslip_counter      : std_logic_vector(2 downto 0);
        fsm                          : data_clock_timing_tuning_fsm_type;
        iserdes_idelay_ctrl_fsm      : iserdes_idelay_ctrl_fsm_type;
--        tuning_stat_set              : cbcio_tuning_stat_set_type;
    end record;
    type dctt_stat_set_type is array (0 to NFE-1) of dctt_stat_type;
   
    
    type io_stat_type is
    record
        cbcios           : cbcio_stat_set_type;
        dctts            : dctt_stat_set_type;
    end record;
  

    
    type cbc_i2c_command_fifo_stat_type is
    record
        empty     : std_logic;
        full      : std_logic;
    end record;

    type cbc_i2c_reply_fifo_stat_type is
    record
        empty     : std_logic;
        full      : std_logic;
        ndata     : std_logic_vector(CBC_I2C_REPLY_FIFO_ADDR_WIDTH downto 0);
        nrdata    : std_logic_vector(CBC_I2C_REPLY_FIFO_ADDR_WIDTH downto 0);
    end record;
    
    type cbc_i2c_bus_manager_stat_type is
    record
--        cbc_i2c_transaction            : cbc_i2c_transaction_stat_type;
        n_active_cbc                   : std_logic_vector(CBCID_WIDTH-1 downto 0);
        bus_waiting                    : std_logic;
        bus_ready                      : std_logic;
        command_fifo                   : cbc_i2c_command_fifo_stat_type;
        reply_fifo                     : cbc_i2c_reply_fifo_stat_type;       
    end record;
    type cbc_i2c_bus_manager_stat_set_type is array (0 to NFE-1) of cbc_i2c_bus_manager_stat_type;

	type rdb_controller_write_fsm_type is (write_process, waiting_read_done);
    
    type rdb_controller_stat_type is
    record
    	write_fsm : rdb_controller_write_fsm_type;
    	read_ready : std_logic;
    	waddr     : std_logic_vector(14 downto 0);
    end record;
    
--    -- cbc data processor
--    type idelay_tuning_fsm_type is ( init, step1, step2, step3, tuned );
--    type cbc_ser_data_delay_stat_type is
--    record
--        idelay_tuning_fsm : idelay_tuning_fsm_type;
--        delay             : std_logic_vector(4 downto 0);
--    end record;
--    type cbc_data_deser_fsm_type is (detect_sync, running);  
    type cbc_data_fifo_write_fsm_type is (idle, write_trig_data, wait_triggered_data, write_triggered_data, write_data_paddings, write_data_info);
    type cbc_data_packet_send_fsm_type is (idle, send_data_ready, send_packet);

    type cbc_data_processor_stat_type is
    record
--        cbc_ser_data_delay     : cbc_ser_data_delay_stat_type;
--        cbc_data_deser_fsm     : cbc_data_deser_fsm_type;
        data_fifo_empty        : std_logic;
        data_fifo_full         : std_logic;
        data_info_fifo_empty        : std_logic;
        data_info_fifo_full         : std_logic;        
        trig_data_fifo_empty   : std_logic;
        trig_data_fifo_full    : std_logic;
        fifo_write_fsm         : cbc_data_fifo_write_fsm_type;
        packet_send_fsm        : cbc_data_packet_send_fsm_type;
        triggered_data         : cbc_triggered_data_buffer_type; 
        cbc_data_frame_counter : std_logic_vector(31 downto 0);       
    end record;
    type cbc_data_processor_stat_set_type is array (0 to NCBC-1) of cbc_data_processor_stat_type;
    -- event builder
    type event_buffer_write_fsm_type is (idle, wait_for_data, write_header_data_size, write_event_data, set_ds, read_ds);     
    type event_packet_send_fsm_type is (idle, check_data_buffer, send_data);     
    type event_builder_stat_type is
    record
        event_buffer_write_fsm                          : event_buffer_write_fsm_type;
        event_packet_send_fsm                           : event_packet_send_fsm_type;
    end record;
    -- data buffer
    type data_buffer_stat_type is
    record
        n_word_all         : std_logic_vector(DATA_BUFFER_ADDR_WIDTH downto 0);
        n_word_events      : std_logic_vector(DATA_BUFFER_ADDR_WIDTH downto 0);       
        n_word_free        : std_logic_vector(DATA_BUFFER_ADDR_WIDTH downto 0);
        werr               : std_logic;
        rerr               : std_logic;
        waddr              : std_logic_vector(DATA_BUFFER_ADDR_WIDTH-1 downto 0);
        raddr              : std_logic_vector(DATA_BUFFER_ADDR_WIDTH-1 downto 0);       
    end record;

    type fscbram_controller_fsm_type is (sync40MHz, wait_data_ready, bram_ready, running, waiting);	

    type fscbram_controller_stat_type is
    record
        fsm : fscbram_controller_fsm_type;
        dcount : unsigned(11 downto 0);
        dout   : std_logic_vector(11 downto 0);
    end record;    
    
    type cbc_emulator_fsm_type is (init, running);
    type cbc_emulator_stat_type is
    record
        fsm                      : cbc_emulator_fsm_type;
        tp_data_pattern          : cbc_emulator_data_pattern_type;
    end record;
    type cbc_emulator_stat_set_type is array (0 to NCBC-1) of cbc_emulator_stat_type;
    -- status signal container   
    type ipb_cbc_system_stat_type is
    record
        global                   : global_stat_type;
        cbc_system_clocks_0      : cbc_system_clocks_0_stat_type;
        cbc_system_clocks_1_set  : cbc_system_clocks_1_set_stat_type;
        trig_veto                : trig_veto_stat_type;
        mmcme2_drps              : mmcme2_drp_stat_set_type;
        io                       : io_stat_type;
        fast_signal_manager      : fast_signal_manager_stat_type;
        cbc_i2c_bus_managers     : cbc_i2c_bus_manager_stat_set_type;
        rdb_controller           : rdb_controller_stat_type;
        cbc_data_processors      : cbc_data_processor_stat_set_type;
        event_builder            : event_builder_stat_type;
        data_buffer              : data_buffer_stat_type;
        fscbram_controller       : fscbram_controller_stat_type;
        cbc_emulators            : cbc_emulator_stat_set_type;
    end record;

	function n_active_cbc( signal a : in cbc_cnfg_set_type ) return unsigned;
	function mk_fe_cnfg_set( signal a : in cbc_cnfg_set_type ) return fe_cnfg_set_type;
    function get_cbc_cnfg( signal cbc_id : in std_logic_vector(CBCID_WIDTH-1 downto 0); signal a : in cbc_cnfg_set_type ) return cbc_cnfg_type;
	function get_cbc_index( signal a : in cbc_cnfg_set_type; signal cbc_id : in std_logic_vector(CBCID_WIDTH-1 downto 0) ) return integer;
	function bitswap ( signal a : in std_logic_vector ) return std_logic_vector;
	function get_fe_active_and( signal sig : in std_logic_vector(NFE-1 downto 0); signal fe_cnfg_set : in fe_cnfg_set_type ) return std_logic;

end cbc_system_package;

package body cbc_system_package is

	function n_active_cbc( signal a : in cbc_cnfg_set_type ) return unsigned is
        variable result : unsigned( CBCID_WIDTH - 1 downto 0 );
        begin
            result := ( others => '0' );
            for i in a'RANGE loop
                if a(i).active = '1' then
                    result := result + 1;
                end if;
            end loop;
        return result;
    	end n_active_cbc;
    	
	function mk_fe_cnfg_set( signal a : in cbc_cnfg_set_type ) return fe_cnfg_set_type is
        variable fe_cnfg_set : fe_cnfg_set_type;      
        variable curr_index : integer;
	begin
        	for fe_index in fe_cnfg_set'RANGE loop
        	    fe_cnfg_set(fe_index).id := std_logic_vector(to_unsigned(fe_index + 1, FEID_WIDTH));
               	curr_index := 0;
            	for cbc_index in a'RANGE loop
                	if a(cbc_index).fe_id = std_logic_vector(to_unsigned(fe_index+1,FEID_WIDTH)) and a(cbc_index).active = '1' then
                    		fe_cnfg_set(fe_index).active_cbcs(curr_index).id := a(cbc_index).id;
                    		fe_cnfg_set(fe_index).active_cbcs(curr_index).fe_id := a(cbc_index).fe_id;
                    		fe_cnfg_set(fe_index).active_cbcs(curr_index).i2c_address := a(cbc_index).i2c_address;                    		
                    		fe_cnfg_set(fe_index).active_cbcs(curr_index).active := a(cbc_index).active;   
                    		curr_index := curr_index + 1;
                	end if;
                	fe_cnfg_set(fe_index).n_active_cbc := std_logic_vector(to_unsigned(curr_index,CBCID_WIDTH));
            	end loop;     
        end loop;
	return fe_cnfg_set;
	end mk_fe_cnfg_set;


    function get_cbc_cnfg( signal cbc_id : in std_logic_vector(CBCID_WIDTH-1 downto 0); signal a : in cbc_cnfg_set_type ) return cbc_cnfg_type is
    begin
        for cbc_index in a'RANGE loop
            if a(cbc_index).id = cbc_id then
                return a(cbc_index);
            end if;
        end loop;
        return a(0);
    end get_cbc_cnfg;


	function get_cbc_index( signal a : in cbc_cnfg_set_type; signal cbc_id : in std_logic_vector(CBCID_WIDTH-1 downto 0) ) return integer is
	variable cbc_index : integer  range 0 to 2**CBCID_WIDTH - 1;	
	begin	
		cbc_index := 0;
		for i in a'RANGE loop
			if cbc_id = a(i).id then
				cbc_index := i;
			end if;
		end loop;
		return cbc_index;
	end get_cbc_index;	


    function get_fe_active_and( signal sig : in std_logic_vector(NFE-1 downto 0); signal fe_cnfg_set : in fe_cnfg_set_type ) return std_logic is
        variable result : std_logic;
    begin
        result := '1';
        for fe_index in fe_cnfg_set'RANGE loop
            if( unsigned(fe_cnfg_set(fe_index).n_active_cbc) > 0 ) then
                result := result and sig(fe_index);
            end if;
        end loop;
        return result;
    end get_fe_active_and;




	function bitswap ( signal a : in std_logic_vector ) return std_logic_vector is
		variable result : std_logic_vector( a'RANGE );
		alias ra : std_logic_vector( a'REVERSE_RANGE ) is a;
		begin
			for i in a'RANGE loop
				result(i) := ra(i);
			end loop;
			return result;	
	end bitswap;

end cbc_system_package;

