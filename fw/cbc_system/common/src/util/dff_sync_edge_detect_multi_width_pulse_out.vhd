----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    11:23:01 12/02/2015 
-- Design Name: 
-- Module Name:    DFFSyncEdgeDetectMultiWidthPulseOut - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity dff_sync_edge_detect_multi_width_pulse_out is
generic ( REDGE 	: boolean := true;
			 WIDTH	: natural := 1
);
port (	reset	: in std_logic;
			clkb  : in std_logic;
			dina  : in std_logic;
			doutb : out std_logic
);
end dff_sync_edge_detect_multi_width_pulse_out;

architecture Behavioral of dff_sync_edge_detect_multi_width_pulse_out is
	signal shift_reg : std_logic_vector( 2 downto 0 );
	signal edge 	  : std_logic;
begin

	process ( clkb, reset )
	variable counter : natural range 0 to WIDTH;
	begin
	if reset = '1' then
		shift_reg <= ( others => '0');
		counter := 0;
	elsif rising_edge( clkb ) then
	
		shift_reg <= shift_reg( 1 downto 0 ) & dina;
		if REDGE = true then
			edge <= not shift_reg(2) and shift_reg(1);
		else
			edge	<= shift_reg(2) and not shift_reg(1);
		end if;
		
		if edge = '1' then
			counter := counter + 1;
		end if;

		doutb <= '0';
		if counter /= 0 then
			doutb <= '1';
			if counter >= WIDTH then
				counter := 0;
			else
				counter := counter + 1;
			end if;
		end if;			
	end if;
	
	end process;

end Behavioral;


