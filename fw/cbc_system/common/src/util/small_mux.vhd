-- Company: 
-- Engineer: 
-- 
-- Create Date: 04.04.2017 13:14:31
-- Design Name: 
-- Module Name: mux - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity small_mux is
    generic (   N_ENTRIES  : natural := 2;
                DATA_WIDTH : natural := 8
    );
    port (
    signal sig_in  : in  std_logic_vector( N_ENTRIES * DATA_WIDTH - 1 downto 0 );
    signal sig_sel : in  natural range 0 to N_ENTRIES - 1;           
    signal sig_out : out std_logic_vector (DATA_WIDTH - 1 downto 0 )
    );
end small_mux;

architecture Behavioral of small_mux is
    type mux_array is array ( natural range 0 to N_ENTRIES - 1 ) of std_logic_vector(sig_out'RANGE);
    signal array_val : mux_array;
begin
    GEN: for i in array_val'RANGE generate
        array_val(i) <= sig_in ((i + 1) * DATA_WIDTH - 1 downto i * DATA_WIDTH);
    end generate;
    sig_out <= array_val(sig_sel);
    
end Behavioral;
