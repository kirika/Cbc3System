----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 21.04.2017 20:40:44
-- Design Name: 
-- Module Name: fscbram_controller - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;
use work.cbc_system_package.all;

entity fscbram_controller is
port(		
    clk_320MHz              : in  std_logic;
    clk_40MHz               : in  std_logic;
	reset                   : in std_logic;	
	start                   : in std_logic;	
	resume                  : in std_logic;	
	bram_in                 : in fscbram_in_type;
	cbci2cbus_waiting       : in std_logic;	
	cbci2cbus_ready         : in std_logic;	
	ci2cbmgr_in_set         : out ci2cbmgr_in_set_type;
	fast_signal_40MHz_in    : out fast_signal_40MHz_in_type;	
	ipb_stat                : out fscbram_controller_stat_type;
	ipb_cnfg_cbc_cnfg_set   : in  cbc_cnfg_set_type;
	clk_sync_bit            : in  std_logic;
	rdb_write_trigger_o     : out std_logic;
	rdb_write_ready         : in std_logic
);		
end fscbram_controller;		

architecture behavioral of fscbram_controller is		
		
	constant SLOW_CMD   : std_logic_vector(2 downto 0)  := "100";	
	constant FAST_CMD   : std_logic_vector(2 downto 0)  := "101";	
	constant PAUSE      : std_logic_vector(2 downto 0)  := "001";	
	constant STOP       : std_logic_vector(2 downto 0)  := "010";	
	constant LOOP_BEGIN : std_logic_vector(2 downto 0)  := "011";	
    constant LOOP_END   : std_logic_vector(2 downto 0)  := "110";		
    constant END_DATA   : std_logic_vector(2 downto 0)  := "111";	
    
    constant WAIT_UNTIL_RESUME            : std_logic_vector(3 downto 0)  := "0001";
    constant WAIT_UNTIL_I2CBUS_WAITING    : std_logic_vector(3 downto 0)  := "0010";    
 	constant WAIT_UNTIL_RDB_WRITE_READY   : std_logic_vector(3 downto 0)  := "0100";  
    signal cbci2cbus_waiting_1d : std_logic;
    signal cbci2cbus_ready_1d   : std_logic;
    
    signal uaddra               : unsigned(11 downto 0);	
        
    signal data_ready          : boolean;
    signal send_slow_com       : boolean;
    signal send_fast_com       : boolean;
    signal reset_reply_fifo    : boolean;
	signal addr                : std_logic_vector(11 downto 0);	
	signal dout                : std_logic_vector(31 downto 0);	
	signal uaddr               : unsigned(11 downto 0);	
	signal fsm                 : fscbram_controller_fsm_type;	
	signal error               : std_logic;	

	signal fe_index            : integer range 0 to NFE-1;		
    signal cmd_fifo_din        : std_logic_vector(31 downto 0);
    signal cbc_id              : std_logic_vector(CBCID_WIDTH-1 downto 0);
	signal fast_reset          : std_logic;
	signal trigger             : std_logic;
	signal test_pulse          : std_logic;
    signal orbit_reset         : std_logic;
    	
	signal dcount              : unsigned(11 downto 0);
		
	alias slow_cmd_cbc_id: std_logic_vector(CBCID_WIDTH-1 downto 0) is dout(28 downto 24);	
	alias slow_cmd_rw    : std_logic_vector(1 downto 0)             is dout(21 downto 20);	
	alias slow_cmd_page  : std_logic                                is dout(16);
	alias slow_cmd_addr  : std_logic_vector(7 downto 0)             is dout(15 downto  8);
	alias slow_cmd_value : std_logic_vector(7 downto 0)             is dout( 7 downto  0);	
	
	signal rdb_write_trigger      : std_logic;
	signal rdb_write_ready_320MHz : std_logic;  	
begin		


    ipb_stat.fsm <= fsm;
    ipb_stat.dcount <= uaddr;
    ipb_stat.dout <= dout;
    
    fscbram_inst : entity work.fscbram
      PORT MAP (
      clka   => clk_40MHz,
      wea(0) => bram_in.wen,
      addra  => std_logic_vector(uaddra),
      dina   => bram_in.din,
      clkb   => clk_320MHz,
      addrb  => addr,
      doutb  => dout
      );
	process (clk_40MHz, reset)	
	variable end_data_flag : boolean;	

    begin
        if ( reset = '1' ) then    
            end_data_flag := false;
            data_ready <= false;
            dcount <= (others => '0');
            uaddra <= (others => '0'); 
        elsif rising_edge(clk_40MHz) then

            for i in 0 to NFE-1 loop                                
                ci2cbmgr_in_set(i).reply_fifo_reset <= '0'; 
                ci2cbmgr_in_set(i).command_fifo_din <= (others => '0');                           
                ci2cbmgr_in_set(i).command_fifo_we <= '0';   
            end loop;       
              
 
            fast_signal_40MHz_in.fast_reset           <= '0';                        
            fast_signal_40MHz_in.trigger              <= '0';                        
            fast_signal_40MHz_in.test_pulse           <= '0';                        
            fast_signal_40MHz_in.orbit_reset          <= '0';  
            rdb_write_trigger_o                       <= '0';  
            if  end_data_flag then 
                end_data_flag := false;                  
                data_ready <= true;
            end if;                     
            if bram_in.wen = '1' then
                dcount <= dcount + 1;
                uaddra <= uaddra + 1;
                if bram_in.din(31 downto 29) = END_DATA then
                    end_data_flag := true;
                end if;
            end if;            
            if send_slow_com then

                ci2cbmgr_in_set(fe_index).command_fifo_we <= '1';                    
                ci2cbmgr_in_set(fe_index).command_fifo_din <= cmd_fifo_din;      
            end if;
            if send_fast_com then
                fast_signal_40MHz_in.fast_reset           <= fast_reset;                        
                fast_signal_40MHz_in.trigger              <= trigger;                        
                fast_signal_40MHz_in.test_pulse           <= test_pulse;                        
                fast_signal_40MHz_in.orbit_reset          <= orbit_reset;    
                rdb_write_trigger_o                       <= rdb_write_trigger;                     
            end if;
            
            if reset_reply_fifo then
                for i in 0 to NFE-1 loop                    
                    ci2cbmgr_in_set(i).reply_fifo_reset <= '1';               
                end loop;                  
            end if;
        end if;                         
    end process;
    
	process (clk_320MHz, reset)				
	variable timer            : integer range 0 to 7;	
	variable loop_begin_flag  : boolean;
	variable loop_end_flag    : boolean;
	variable pause_flag       : boolean;
	variable addr_inc         : boolean;
	variable addr_jump_to_loop_begin : boolean;					
	variable count_inc        : boolean;
	variable count_reset      : boolean;	
	variable loop_reset       : boolean;
	variable check_count      : boolean;
	variable wait_resume      : boolean;
	variable wait_ci2bmgr     : boolean;
	variable wait_rdb_write_ready : boolean;
	variable jump_flag        : std_logic;
    variable count            : unsigned (11 downto 0);   
    variable ncount           : unsigned (11 downto 0);       
    variable n_loop           : unsigned (11 downto 0);
    variable loop_count       : unsigned (11 downto 0);  
    variable uaddr_loop_begin : unsigned (11 downto 0);  
    variable wait_type        : std_logic_vector(3 downto 0);  
    variable ctrl             : std_logic_vector(2 downto 0);	
    variable wait_count       : unsigned (7 downto 0);    
              
    begin                                        
        if ( reset = '1' ) then    
            loop_begin_flag  := false;
            loop_end_flag    := false;
            pause_flag       := false;   
            addr_inc         := false;
            addr_jump_to_loop_begin := false;  
            count_inc        := false;                           
            count_reset      := false;
            check_count      := false; 
            loop_reset       := false;
            wait_resume      := false;
            wait_ci2bmgr     := false;
            jump_flag        := '0';
            count            := (others => '0');
            ncount           := (others => '0');
            loop_count       := (others => '0');
            uaddr_loop_begin := (others => '0'); 
            wait_type        := (others => '0');
            ctrl             := (others => '0');
            fsm              <= sync40MHz;                                
            error            <= '0'; 
            uaddr            <= (others => '0');                       
            send_slow_com    <= false;
            send_fast_com    <= false;                      
            reset_reply_fifo <= false;
            wait_count       := (others => '0');   
            rdb_write_trigger  <= '0';      
            fast_reset         <= '0';
            trigger            <= '0';
            test_pulse         <= '0';
    		orbit_reset        <= '0';   
--            rdb_write_trigger <= '0';
        elsif rising_edge(clk_320MHz) then     

            cbci2cbus_waiting_1d <= cbci2cbus_waiting;            
            cbci2cbus_ready_1d   <= cbci2cbus_ready;
            addr  <= std_logic_vector(uaddr);                      
--            rdb_write_trigger <= '0';
            rdb_write_ready_320MHz <= rdb_write_ready;           
            timer := timer + 1;     
                                              
            case fsm is    
            when sync40MHz =>
                if clk_sync_bit = '1' then
                    timer := 0;
                    fsm <= wait_data_ready;
                end if;
            when wait_data_ready =>
                if timer = 0 and data_ready then
                    fsm <= bram_ready;
                end if;
            when bram_ready =>                                
                count      := (others => '0');
                ncount     := (others => '0');
                loop_count := (others => '0');                                                   
                uaddr      <= (others => '0');                       
                if start = '1' then                     
                    fsm        <= running;                        
                end if;                            
            when running =>        

                wait_count := (others => '0');      

                if send_slow_com then
                    fe_index     <= to_integer(unsigned(get_cbc_cnfg(cbc_id, ipb_cnfg_cbc_cnfg_set).fe_id)-1);  
                end if;


                if count_reset then -- timer = 7
                     count       := (others => '0');
                     count_reset := false;
                end if;
                if addr_inc then -- timer = 7 for fc, 5 for sc                        
                    uaddr <= uaddr + 1;
                    addr_inc := false;
                end if;
                
                if addr_jump_to_loop_begin then
                	uaddr <= uaddr_loop_begin;	
                	addr_jump_to_loop_begin := false;
                end if;                
                
                if check_count then -- timer = 6
                  if count = ncount then
                     count_reset := true;
                     addr_inc    := true; 
                  end if;   
                   check_count := false;
                end if;

                if count_inc then -- timer = 5         
                    count := count + 1;
                    check_count := true;
                    count_inc := false;
                end if;               

                if pause_flag then  
                    fsm <= waiting;
                    if jump_flag = '1' then
                        uaddr <= unsigned(dout(11 downto 0)); 
                        jump_flag := '0';               
                    end if;
                    pause_flag := false;            
                end if;

                if loop_reset then
                    loop_count := (others => '0');
                    loop_reset := false;
                end if;

                if loop_end_flag then
                    if loop_count /= n_loop or n_loop = 0 then
                        addr_jump_to_loop_begin := true;
--                        uaddr <= uaddr_loop_begin;
                    else
                        addr_inc := true; 
                        loop_reset := true;
                    end if;         
                    loop_end_flag := false;
                    loop_count := loop_count + 1;    
                end if;

                if loop_begin_flag then
                    loop_count       := loop_count + 1;
                    uaddr_loop_begin := uaddr + 1;        
                    addr_inc         := true;             
                    loop_begin_flag  := false;            
                end if;               
 
                if timer = 3 then
                    send_slow_com      <= false;
                    send_fast_com      <= false;
                    ctrl := dout(31 downto 29);
                elsif timer = 4 then
                    if ctrl = SLOW_CMD then                        
                       addr_inc      := true;
	                   send_slow_com <= true;
	                   cbc_id        <= slow_cmd_cbc_id; 
	                   cmd_fifo_din  <=  "00000000" & slow_cmd_cbc_id & slow_cmd_rw & slow_cmd_page & slow_cmd_addr & slow_cmd_value; 
                    elsif ctrl = FAST_CMD then
                        count_inc := true;
                        ncount               := unsigned(dout(27 downto 16));
                        fast_reset           <= dout(12);                        
                        trigger              <= dout(8);                        
                        test_pulse           <= dout(4);                        
                        orbit_reset          <= dout(0); 
                        rdb_write_trigger    <= dout(9);  
                        send_fast_com <= true;
                    elsif ctrl = PAUSE then                        
                        pause_flag := true;                 
                        wait_type  := dout(27 downto 24);    
                        jump_flag  := dout(16); 
                        addr_inc := true;                                                  
                    elsif ctrl=LOOP_BEGIN then  
                        loop_begin_flag := true;         
                        n_loop := unsigned(dout(11 downto 0)); 
                    elsif ctrl=LOOP_END then
                        loop_end_flag := true;                                      
                    elsif ctrl=END_DATA then
                         fsm <= bram_ready;                   
                    end if;                                                                        
                end if;

            when waiting =>                                
                if timer = 6 then
                    
                    if wait_count = 0 then
                        if wait_type = WAIT_UNTIL_RESUME then                                                      
                            wait_resume  := true;
                        elsif wait_type = WAIT_UNTIL_I2CBUS_WAITING then
                            wait_ci2bmgr := true;
                        elsif wait_type = WAIT_UNTIL_RDB_WRITE_READY then
                        	wait_rdb_write_ready := true;
                        end if;
                    end if;
                    wait_count := wait_count + 1;
                    if wait_resume and resume = '1' then  
                        wait_resume := false;
--                        wait_count := (others => '0');                          
                        fsm <= running;                        
                    elsif wait_ci2bmgr then
                        if wait_count > 10 then                            
                            if cbci2cbus_waiting_1d = '1' then                        
                                reset_reply_fifo <= true; 
                                wait_ci2bmgr := false;                                               
                            elsif cbci2cbus_ready_1d = '0' then 
                                wait_count := (others => '0');                       
--                                fsm <= bram_ready;  
                     			fsm <= running;                    
                                error <= '1';                    
                            end if;       
                        end if;                 
                    elsif wait_rdb_write_ready and rdb_write_ready_320MHz = '1' then
                    	wait_rdb_write_ready := false;                   
                    	fsm <= running;   
                    end if;                            
              elsif timer = 5 then
                if reset_reply_fifo = true then
                    reset_reply_fifo <= false;   
                    wait_count := (others => '0');
                    fsm <= running; 
                end if;
              end if; 
            end case;                                
                                            
        end if;                                    
                                       
    end process;                                        

   
	                                              
                                           

end Behavioral;
