--===========================--
-- ipb_cbc_system
-- 28.10.2016 Kirika Uchida
--===========================--

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;
use work.user_package.all;
use work.ipbus.all;
use work.cbc_system_package.all;
use work.system_package.all;

entity ipb_cbc_system is
port 
(
    ipb_rst_i              : in  std_logic;
    ipb_clk                : in  std_logic;
    clk_40MHz_iserdes_ctrl : in  std_logic_vector(NFE-1 downto 0);
    clk_320MHz_iserdes_ctrl: in  std_logic_vector(NFE-1 downto 0);
    clk_40MHz_cbc_ctrl     : in  std_logic;
    clk_320MHz_cbc_ctrl    : in  std_logic;
    clk_40MHz_daq          : in  std_logic;
    clk_320MHz_daq          : in  std_logic;
    mmcme2_drp_sclk        : in  std_logic;
    ipb_mosi_i             : in  ipb_wbus_array(0 to nbr_usr_slaves-1);
    ipb_miso_o             : out ipb_rbus_array(0 to nbr_usr_slaves-1);
    cbc_system_stat        : in  ipb_cbc_system_stat_type;
    cbc_system_cnfg        : out ipb_cbc_system_cnfg_type;
    cbc_system_ctrl        : out ipb_cbc_system_ctrl_type;
    db_to_ipbif            : in  db_to_ipbif_type;
    ipbif_to_db            : out ipbif_to_db_type;
    ci2cbmgr_to_ipbif_set  : in  ci2cbmgr_out_set_type;
    ipbif_to_ci2cbmgr_set  : out ci2cbmgr_in_set_type;
    dctsb_dout             : in  std_logic_vector(31 downto 0);
    dctsb_read_next        : out std_logic;
    ipbif_to_fscbram       : out fscbram_in_type;
    rdb_dout               : in  std_logic_vector(31 downto 0);
    rdb_read_next          : out std_logic;
    test_sig               : out std_logic
);    
end ipb_cbc_system;

architecture Behavioral of ipb_cbc_system is

begin



    --===========================================--
    ipb_cbc_system_stat_inst: entity work.ipb_cbc_system_stat
    --===========================================--
    port map
    (
        clk                   => ipb_clk,
        reset                 => ipb_rst_i,
        ipb_mosi_i            => ipb_mosi_i(ipb_cbc_system_stat_sel),
        ipb_miso_o            => ipb_miso_o(ipb_cbc_system_stat_sel),
        cbc_system_stat       => cbc_system_stat
    );
    
	--===========================================--
    ipb_cbc_system_cnfg_inst: entity work.ipb_cbc_system_cnfg
    --===========================================--
    port map
    (
        clk                   => ipb_clk,
        reset                 => ipb_rst_i,
        ipb_mosi_i            => ipb_mosi_i(ipb_cbc_system_cnfg_sel),
        ipb_miso_o            => ipb_miso_o(ipb_cbc_system_cnfg_sel),
        cbc_system_cnfg       => cbc_system_cnfg
    );
    
    --===========================================--
    ipb_cbc_system_ctrl_inst: entity work.ipb_cbc_system_ctrl
    --===========================================--
    port map
    (
        clk                   => ipb_clk,
	    clk_40MHz_iserdes_ctrl => clk_40MHz_iserdes_ctrl,
        clk_320MHz_iserdes_ctrl=> clk_320MHz_iserdes_ctrl,
        clk_40MHz_cbc_ctrl     => clk_40MHz_cbc_ctrl,
        clk_320MHz_cbc_ctrl    => clk_320MHz_cbc_ctrl,
        clk_40MHz_daq          => clk_40MHz_daq,
        clk_320MHz_daq         => clk_320MHz_daq,       
        mmcme2_drp_sclk       => mmcme2_drp_sclk,
        reset                 => ipb_rst_i,
        ipb_mosi_i            => ipb_mosi_i(ipb_cbc_system_ctrl_sel),
        ipb_miso_o            => ipb_miso_o(ipb_cbc_system_ctrl_sel),
        cbc_system_ctrl       => cbc_system_ctrl
    );
    
    --===========================================--
    ipb_ring_buffer_data_readout_inst: entity work.ipb_ring_buffer_data_readout
    --===========================================--
    generic map( BUFFER_ADDR_WIDTH => DATA_BUFFER_ADDR_WIDTH)
    port map
    (   clk                   => ipb_clk,
        reset                 => ipb_rst_i,
        ipb_mosi_i            => ipb_mosi_i(ipb_cbc_system_event_data_sel),
        ipb_miso_o            => ipb_miso_o(ipb_cbc_system_event_data_sel),
        next_data             => db_to_ipbif.next_data,
        next_addr             => db_to_ipbif.next_addr,
        load_next             => db_to_ipbif.load_next,       
        addr                  => ipbif_to_db.addr
    );

	--===========================================--		
	ipb_cbc_i2c_reg_inst: entity work.ipb_cbc_i2c_regs
	--===========================================--	
	port map(	ipb_clk 							=> ipb_clk,
	            cbc_ctrl_clk                        => clk_40MHz_cbc_ctrl,
                reset								=> ipb_rst_i,
                ipb_mosi_i							=> ipb_mosi_i(ipb_cbc_system_cbc_i2c_regs_sel),
                ipb_miso_o							=> ipb_miso_o(ipb_cbc_system_cbc_i2c_regs_sel),
                ci2cbmgr_to_ipbif_set               => ci2cbmgr_to_ipbif_set,
                ipbif_to_ci2cbmgr_set               => ipbif_to_ci2cbmgr_set,
                read_ack_o                          => test_sig                    
	);

	--===========================================--		
	ipb_cbc_data_clock_timing_scan_bram_inst: entity work.ipb_cbc_data_clock_timing_scan_bram
	--===========================================--	
	port map(	ipb_clk 							=> ipb_clk,
                reset								=> ipb_rst_i,
                ipb_mosi_i							=> ipb_mosi_i(ipb_cbc_data_clock_timing_scan_bram_sel),
                ipb_miso_o							=> ipb_miso_o(ipb_cbc_data_clock_timing_scan_bram_sel),
                dout                                => dctsb_dout,
                read_next_o                         => dctsb_read_next         
	);

	ipb_rdb_inst : entity work.ipb_rdb
	port map(	ipb_clk 							=> ipb_clk,
                reset								=> ipb_rst_i,
                ipb_mosi_i							=> ipb_mosi_i(ipb_rdb_sel),
                ipb_miso_o							=> ipb_miso_o(ipb_rdb_sel),
                dout                                => rdb_dout,
                read_next_o                         => rdb_read_next         
	);	

    ipb_fscbram_fifo_inst : entity work.ipb_fscbram
    port map( ipb_clk                => ipb_clk,
              cbc_ctrl_clk           => clk_40MHz_cbc_ctrl,
              reset                  => ipb_rst_i,
              ipb_mosi_i             => ipb_mosi_i(ipb_cfscbram_sel),
              ipb_miso_o             => ipb_miso_o(ipb_cfscbram_sel),
              ipbif_to_fscbram       => ipbif_to_fscbram
    );
end Behavioral;
