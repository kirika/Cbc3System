--===========================--
-- ipb_cbc_system_stat
-- 28.10.2016 Kirika Uchida
--===========================--

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use work.ipbus.all;
use work.system_package.all;
use work.user_package.all;
use work.cbc_system_package.all;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ipb_cbc_system_stat is
generic( ADDR_WIDTH     : natural := 8 );
port (
        clk                   : in  std_logic;
        reset                 : in  std_logic;
        ipb_mosi_i            : in  ipb_wbus;
        ipb_miso_o            : out ipb_rbus;
        cbc_system_stat       : in  ipb_cbc_system_stat_type
        );
end ipb_cbc_system_stat;
    
architecture Behavioral of ipb_cbc_system_stat is

    signal regs: array_256x32bit;		
    signal reg_sigs: array_256x32bit;	

    signal sel: integer range 0 to 255;
    signal ack: std_logic;
    
    attribute keep: boolean;
    attribute keep of sel: signal is true;

    
    constant SYSTEM_ID_SEL                     : integer := 16#00#;
    constant SYSTEM_VERSION_SEL                : integer := 16#01#;
    constant SYSTEM_TYPE_SEL                   : integer := 16#02#;
    
    constant GLOBAL_SEL                        : integer := 16#03#;
    constant NFE_OFFSET                        : integer := 0;
    constant NCBC_OFFSET                       : integer := NFE_OFFSET + FEID_WIDTH;
    constant N_ACTIVE_CBC_OFFSET               : integer := NCBC_OFFSET + CBCID_WIDTH;
    
    constant FSM_SEL                           : integer := 16#04#;
    constant FSM_FSM_OFFSET                    : integer := 0;
    constant FSM_FSM_WIDTH                     : integer := 1;
    constant FSM_TRIG_FSM_OFFSET               : integer := 4;
    constant FSM_TRIG_FSM_WIDTH                : integer := 1;
    constant FSM_FSG_FSM_OFFSET                : integer := 8;
    constant FSM_FSG_FSM_WIDTH                 : integer := 1;
    
        
    constant EB_SEL                            : integer := 16#05#;
    constant EB_EBW_FSM_OFFSET                 : integer := 0;
    constant EB_EBW_FSM_WIDTH                  : integer := 3;    
    constant EB_EPS_FSM_OFFSET                 : integer := 4;
    constant EB_EPS_FSM_WIDTH                  : integer := 2;
    
    constant DB_NWORD_ALL_SEL                  : integer := 16#06#;
    constant DB_NWORD_EVENTS_SEL               : integer := 16#07#;
    constant DB_NWORD_FREE_SEL                 : integer := 16#08#;         
    constant DB_WADDR_SEL                      : integer := 16#09#;
    constant DB_RADDR_SEL                      : integer := 16#0a#;  
    constant DB_STAT_SEL                       : integer := 16#0b#;
    constant DB_WERR_BIT                       : integer := 0;
    constant DB_RERR_BIT                       : integer := 1;    

    constant CBC_SYSTEM_CLOCKS_0_SEL                     : integer := 16#10#;
    constant CBC_SYSTEM_CLOCKS_0_LOCKED_BIT              : integer := 0;
    constant CBC_SYSTEM_CLOCKS_1_SEL                     : integer := 16#11#;
    constant CBC_SYSTEM_CLOCKS_1_LOCKED_BIT              : integer := 0;
    constant CBC_SYSTEM_CLOCKS_1_SADDR_OFFSET            : integer := 1;
    constant CBC_SYSTEM_CLOCKS_1_SADDR_WIDTH             : integer := 2;

	constant RDB_CNTRL_SEL                               : integer := 16#21#;
	constant RDB_WRITE_FSM_BIT                              : integer := 0;
	constant RDB_READ_READY_BIT                             : integer := 1;
	constant RDB_WADDR_OFFSET								: integer := 2;
	constant RDB_WADDR_WIDTH                                : integer := 15;
	
    constant TRIG_VETO_SEL                     : integer := 16#20#;
    constant TRIG_VETO_TO_CBC_BIT              : integer := 16;
    constant TRIG_VETO_OUT_BIT                 : integer := 17;
    
    constant CDP_SEL                           : integer := 16#30#;
    constant CDP_DATA_FIFO_EMPTY_BIT           : integer := 0;
    constant CDP_DATA_FIFO_FULL_BIT            : integer := 1;  
    constant CDP_DATA_INFO_FIFO_EMPTY_BIT      : integer := 2;
    constant CDP_DATA_INFO_FIFO_FULL_BIT       : integer := 3;  
    constant CDP_TRIG_DATA_FIFO_EMPTY_BIT      : integer := 4;
    constant CDP_TRIG_DATA_FIFO_FULL_BIT       : integer := 5; 
    constant CDP_FIFO_WRITE_FSM_OFFSET         : integer := 6;
    constant CDP_FIFO_WRITE_FSM_WIDTH          : integer := 3;
    constant CDP_PACKET_SEND_FSM_OFFSET        : integer := 9;
    constant CDP_PACKET_SEND_FSM_WIDTH         : integer := 2; 
    constant CDP_DATA_FRAME_COUNTER_SUBSEL     : integer := 1;
    constant CDP_NREGS                         : integer := 2; 

    type cbc_data_fifo_write_fsm_type is (idle, write_trig_data, wait_triggered_data, write_triggered_data, write_data_paddings, write_data_info);
    type cbc_data_packet_send_fsm_type is (idle, send_data_ready, send_packet);

    constant CIBM_SEL                          : integer := 16#50#;
    constant CIBM_CF_SUBSEL                    : integer := 0;
    constant CIBM_CF_EMPTY_BIT                 : integer := 0;
    constant CIBM_CF_FULL_BIT                  : integer := 1;
    constant CIBM_RF_SUBSEL                    : integer := 1;
    constant CIBM_RF_EMPTY_BIT                 : integer := 0;
    constant CIBM_RF_FULL_BIT                  : integer := 1;
    constant CIBM_RF_NDATA_SUBSEL              : integer := 2;
    constant CIBM_BUS_SUBSEL                   : integer := 3;
    constant CIBM_BUS_READY_BIT                : integer := 0;
    constant CIBM_BUS_WAITING_BIT              : integer := 1;
    constant CIBM_N_ACTIVE_CBC_OFFSET          : integer := 2;
    constant CIBM_NREGS                        : integer := 4;

    constant FSCBC_SEL                                  : integer := 16#58#;

    constant IO_SEL                                     : integer := 16#60#;
    constant IO_DCTT_SUBSEL                             : integer := 0;
    constant IO_DCTT_CBC_FSM_OFFSET                     : integer := 0;
    constant IO_DCTT_CBC_FSM_WIDTH                      : integer := 4; 
    constant IO_DCTT_CBC_ISERDES_IDELAY_CTRL_FSM_OFFSET : integer := 4;
    constant IO_DCTT_CBC_ISERDES_IDELAY_CTRL_FSM_WIDTH  : integer := 3;        
    constant IO_DCTT_CBC_ISERDES_BITSLIP_COUNTER_OFFSET : integer := 7;
    constant IO_DCTT_CBC_ISERDES_BITSLIP_COUNTER_WIDTH  : integer := 3;
    constant IO_CBC_SUBSEL                              : integer := 16;
    constant IO_CBC_IDELAY_TAP_OFFSET                   : integer := 0;
    constant IO_CBC_IDELAY_TAP_WIDTH                    : integer := 5;
    constant IO_CBC_DELAY_LOCKED_BIT                    : integer := 5;       
    constant IO_CBC_DATA_SLVS5_OFFSET                   : integer := 6;
    constant IO_CBC_DATA_SLVS5_WIDTH                    : integer := 8;

    
--    constant IO_CBC_INPUT_TUNING_CLK0_SUBSEL            : integer := 2;
--    constant IO_CBC_INPUT_TUNING_CLK1_SUBSEL            : integer := 3;    
--    constant IO_CBC_PRE_IDELAY_TUNING_TAP_OFFSET        : integer := 0;
--    constant IO_CBC_PRE_IDELAY_TUNING_TAP_WIDTH         : integer := 5;
--    constant IO_CBC_PATTERN_AT_IDELAY_RESET_OFFSET      : integer := 8;
--    constant IO_CBC_PATTERN_AT_IDELAY_RESET_WIDTH       : integer := 8;          
--    constant IO_CBC_FSM_PROCESSED_OFFSET                : integer := 16;
--    constant IO_CBC_FSM_PROCESSED_WIDTH                 : integer := 10;        
--    constant IO_CBC_IDELAY_RESET_TUNING_TAP_OFFSET        : integer := 26;
--    constant IO_CBC_IDELAY_RESET_TUNING_TAP_WIDTH         : integer := 5;
            
--    constant IO_CBC_NREGS                               : integer := 4;

    constant MMCME2_DRP_SEL                          : integer := 16#90#;

    constant CDP_DATA_FRAME_BUF_SEL           : integer := 16#a0#;
    constant L1A_COUNTER_SEL                  : integer              := 16#b0#;

    signal nword_events_1d                    :  std_logic_vector(DATA_BUFFER_ADDR_WIDTH downto 0); 
    signal nword_events_ipb                   :  std_logic_vector(DATA_BUFFER_ADDR_WIDTH downto 0);    
    signal nword_all_1d                       :  std_logic_vector(DATA_BUFFER_ADDR_WIDTH downto 0); 
    signal nword_all_ipb                      :  std_logic_vector(DATA_BUFFER_ADDR_WIDTH downto 0);        
    signal nword_free_1d                      :  std_logic_vector(DATA_BUFFER_ADDR_WIDTH downto 0); 
    signal nword_free_ipb                     :  std_logic_vector(DATA_BUFFER_ADDR_WIDTH downto 0);         
begin

   --=============================--
     process(reset, clk)
     --=============================--
      begin
      if rising_edge(clk) then
        nword_all_1d    <= cbc_system_stat.data_buffer.n_word_all;
        nword_events_1d <= cbc_system_stat.data_buffer.n_word_events;
        nword_free_1d   <= cbc_system_stat.data_buffer.n_word_free;
        if nword_events_1d = cbc_system_stat.data_buffer.n_word_events then
            nword_events_ipb <= nword_events_1d;
        end if;        
        if nword_all_1d = cbc_system_stat.data_buffer.n_word_all then
            nword_all_ipb <= nword_all_1d;
        end if;  
        if nword_free_1d = cbc_system_stat.data_buffer.n_word_free then
            nword_free_ipb <= nword_free_1d;
        end if;            
     end if;        
     end process;
     
    --=============================--
    -- regs mapping
    --=============================--
    reg_sigs(SYSTEM_ID_SEL)                                    <= usr_id_0;                        -- usr_id        = 'C' 'B' 'C' '3'      
    reg_sigs(SYSTEM_VERSION_SEL)                               <= firmware_id;
    reg_sigs(SYSTEM_TYPE_SEL)(2 downto 0)                      <= firmware_type;

    reg_sigs(GLOBAL_SEL)(NFE_OFFSET + FEID_WIDTH - 1 downto NFE_OFFSET)    <= std_logic_vector(to_unsigned(NFE,FEID_WIDTH));
    reg_sigs(GLOBAL_SEL)(NCBC_OFFSET + CBCID_WIDTH - 1 downto NCBC_OFFSET) <= std_logic_vector(to_unsigned(NCBC,CBCID_WIDTH));

    reg_sigs(GLOBAL_SEL)(N_ACTIVE_CBC_OFFSET + CBCID_WIDTH - 1 downto N_ACTIVE_CBC_OFFSET) <= cbc_system_stat.global.n_active_cbc;  
   
    reg_sigs(CBC_SYSTEM_CLOCKS_0_SEL)(CBC_SYSTEM_CLOCKS_0_LOCKED_BIT) <= cbc_system_stat.cbc_system_clocks_0.locked;

    cbc_system_clock_1_stat_gen :
    for i in 0 to NFE-1 generate  
        reg_sigs(CBC_SYSTEM_CLOCKS_1_SEL+i)(CBC_SYSTEM_CLOCKS_1_LOCKED_BIT) <= cbc_system_stat.cbc_system_clocks_1_set(i).locked;
        reg_sigs(CBC_SYSTEM_CLOCKS_1_SEL+i)(CBC_SYSTEM_CLOCKS_1_SADDR_OFFSET+CBC_SYSTEM_CLOCKS_1_SADDR_WIDTH-1 downto CBC_SYSTEM_CLOCKS_1_SADDR_OFFSET) <= cbc_system_stat.cbc_system_clocks_1_set(i).saddr;   
    end generate;

	reg_sigs(RDB_CNTRL_SEL)(RDB_WRITE_FSM_BIT) <= '0' when cbc_system_stat.rdb_controller.write_fsm = write_process else
								     			  '1' when cbc_system_stat.rdb_controller.write_fsm = waiting_read_done;
											   
	reg_sigs(RDB_CNTRL_SEL)(RDB_READ_READY_BIT) <= cbc_system_stat.rdb_controller.read_ready;
	reg_sigs(RDB_CNTRL_SEL)(RDB_WADDR_OFFSET + RDB_WADDR_WIDTH -1 downto RDB_WADDR_OFFSET ) <= cbc_system_stat.rdb_controller.waddr;
	
--    reg_sigs(GLOBAL_SEL)(GLOBAL_IDELAYCTRL_RDY_BIT)   <= cbc_system_stat.global.idelayctrl_rdy;
    reg_sigs(TRIG_VETO_SEL)(TRIG_VETO_TO_CBC_BIT) <= cbc_system_stat.trig_veto.internal_l1a_veto;
    reg_sigs(TRIG_VETO_SEL)(TRIG_VETO_OUT_BIT)    <= cbc_system_stat.trig_veto.external_l1a_veto;
        
    reg_sigs(FSM_SEL)(FSM_FSM_OFFSET+FSM_FSM_WIDTH-1 downto FSM_FSM_OFFSET) 
    <= "0" when cbc_system_stat.fast_signal_manager.fsm = init else
       "1";
    reg_sigs(FSM_SEL)(FSM_TRIG_FSM_OFFSET+FSM_TRIG_FSM_WIDTH-1 downto FSM_TRIG_FSM_OFFSET)
    <= "0" when cbc_system_stat.fast_signal_manager.trigger_fsm = idle else
       "1";
    reg_sigs(FSM_SEL)(FSM_FSG_FSM_OFFSET+FSM_FSG_FSM_WIDTH-1 downto FSM_FSG_FSM_OFFSET) 
        <= "0" when cbc_system_stat.fast_signal_manager.fast_signal_generator_fsm = idle else
           "1";       
    reg_sigs(EB_SEL)(EB_EBW_FSM_OFFSET+EB_EBW_FSM_WIDTH-1 downto EB_EBW_FSM_OFFSET)
    <= "000" when cbc_system_stat.event_builder.event_buffer_write_fsm = idle else
       "001" when cbc_system_stat.event_builder.event_buffer_write_fsm = wait_for_data else
       "010" when cbc_system_stat.event_builder.event_buffer_write_fsm = write_header_data_size else
       "011" when cbc_system_stat.event_builder.event_buffer_write_fsm = write_event_data else
       "100" when cbc_system_stat.event_builder.event_buffer_write_fsm = set_ds else
       "101"; 
    reg_sigs(EB_SEL)(EB_EPS_FSM_OFFSET+EB_EPS_FSM_WIDTH-1 downto EB_EPS_FSM_OFFSET)
    <= "00" when cbc_system_stat.event_builder.event_packet_send_fsm = idle else
       "01" when cbc_system_stat.event_builder.event_packet_send_fsm = check_data_buffer else
       "10";
    
    
    
    reg_sigs(DB_WADDR_SEL)(DATA_BUFFER_ADDR_WIDTH-1 downto 0)      <= cbc_system_stat.data_buffer.waddr;
    reg_sigs(DB_RADDR_SEL)(DATA_BUFFER_ADDR_WIDTH-1 downto 0)      <= cbc_system_stat.data_buffer.raddr;
    reg_sigs(DB_STAT_SEL)(DB_WERR_BIT) <= cbc_system_stat.data_buffer.werr;
 
    cdp_sig_gen :
    for i in 0 to NCBC-1 generate

        reg_sigs(CDP_SEL + i * CDP_NREGS)(CDP_FIFO_WRITE_FSM_OFFSET +CDP_FIFO_WRITE_FSM_WIDTH - 1 downto CDP_FIFO_WRITE_FSM_OFFSET)
        <= "000" when cbc_system_stat.cbc_data_processors(i).fifo_write_fsm = idle else
           "001" when cbc_system_stat.cbc_data_processors(i).fifo_write_fsm = write_trig_data else
           "010" when cbc_system_stat.cbc_data_processors(i).fifo_write_fsm = wait_triggered_data else
           "011" when cbc_system_stat.cbc_data_processors(i).fifo_write_fsm = write_triggered_data else
           "100" when cbc_system_stat.cbc_data_processors(i).fifo_write_fsm = write_data_paddings else
           "101" when cbc_system_stat.cbc_data_processors(i).fifo_write_fsm = write_data_info;                  
        
        reg_sigs(CDP_SEL + i * CDP_NREGS)(CDP_PACKET_SEND_FSM_OFFSET +CDP_PACKET_SEND_FSM_WIDTH - 1 downto CDP_PACKET_SEND_FSM_OFFSET)
           <= "00" when cbc_system_stat.cbc_data_processors(i).packet_send_fsm = idle else
              "01" when cbc_system_stat.cbc_data_processors(i).packet_send_fsm = send_data_ready else
              "10" when cbc_system_stat.cbc_data_processors(i).packet_send_fsm = send_packet;
              
        reg_sigs(CDP_SEL + i * CDP_NREGS)(CDP_TRIG_DATA_FIFO_EMPTY_BIT)    <= cbc_system_stat.cbc_data_processors(i).trig_data_fifo_empty;
        reg_sigs(CDP_SEL + i * CDP_NREGS)(CDP_TRIG_DATA_FIFO_FULL_BIT)     <= cbc_system_stat.cbc_data_processors(i).trig_data_fifo_full; 
        reg_sigs(CDP_SEL + i * CDP_NREGS)(CDP_DATA_FIFO_EMPTY_BIT)         <= cbc_system_stat.cbc_data_processors(i).data_fifo_empty;
        reg_sigs(CDP_SEL + i * CDP_NREGS)(CDP_DATA_FIFO_FULL_BIT)          <= cbc_system_stat.cbc_data_processors(i).data_fifo_full;       
        reg_sigs(CDP_SEL + i * CDP_NREGS)(CDP_DATA_INFO_FIFO_EMPTY_BIT)    <= cbc_system_stat.cbc_data_processors(i).data_info_fifo_empty;
        reg_sigs(CDP_SEL + i * CDP_NREGS)(CDP_DATA_INFO_FIFO_FULL_BIT)     <= cbc_system_stat.cbc_data_processors(i).data_info_fifo_full;                   
        reg_sigs(CDP_SEL + i * CDP_NREGS + CDP_DATA_FRAME_COUNTER_SUBSEL)  <= cbc_system_stat.cbc_data_processors(i).cbc_data_frame_counter;       
    end generate;

    cibm_sig_gen :
    for i in 0 to NFE-1 generate
        reg_sigs(CIBM_SEL + i * CIBM_NREGS + CIBM_CF_SUBSEL)(CIBM_CF_EMPTY_BIT)    <= cbc_system_stat.cbc_i2c_bus_managers(i).command_fifo.empty;
        reg_sigs(CIBM_SEL + i * CIBM_NREGS + CIBM_CF_SUBSEL)(CIBM_CF_FULL_BIT)     <= cbc_system_stat.cbc_i2c_bus_managers(i).command_fifo.full;
        reg_sigs(CIBM_SEL + i * CIBM_NREGS + CIBM_RF_SUBSEL)(CIBM_RF_EMPTY_BIT)    <= cbc_system_stat.cbc_i2c_bus_managers(i).reply_fifo.empty;
        reg_sigs(CIBM_SEL + i * CIBM_NREGS + CIBM_RF_SUBSEL)(CIBM_RF_FULL_BIT)     <= cbc_system_stat.cbc_i2c_bus_managers(i).reply_fifo.full;
        reg_sigs(CIBM_SEL + i * CIBM_NREGS + CIBM_RF_NDATA_SUBSEL)(CBC_I2C_REPLY_FIFO_ADDR_WIDTH downto 0) <= cbc_system_stat.cbc_i2c_bus_managers(i).reply_fifo.ndata;  
        reg_sigs(CIBM_SEL + i * CIBM_NREGS + CIBM_BUS_SUBSEL)(CIBM_BUS_READY_BIT)                          <= cbc_system_stat.cbc_i2c_bus_managers(i).bus_ready;  
        reg_sigs(CIBM_SEL + i * CIBM_NREGS + CIBM_BUS_SUBSEL)(CIBM_BUS_WAITING_BIT)                        <= cbc_system_stat.cbc_i2c_bus_managers(i).bus_waiting;          
        reg_sigs(CIBM_SEL + i * CIBM_NREGS + CIBM_BUS_SUBSEL)(CIBM_N_ACTIVE_CBC_OFFSET + CBCID_WIDTH - 1 downto CIBM_N_ACTIVE_CBC_OFFSET) <= cbc_system_stat.cbc_i2c_bus_managers(i).n_active_cbc;
    end generate;

    reg_sigs(FSCBC_SEL)(2 downto 0) <=   
                           "000" when cbc_system_stat.fscbram_controller.fsm = sync40MHz       else
                           "001" when cbc_system_stat.fscbram_controller.fsm = wait_data_ready else                            
                           "010" when cbc_system_stat.fscbram_controller.fsm = bram_ready      else 
                           "011" when cbc_system_stat.fscbram_controller.fsm = running         else
                           "100" when cbc_system_stat.fscbram_controller.fsm = waiting;                           
    reg_sigs(FSCBC_SEL)(15 downto 4) <=  std_logic_vector( cbc_system_stat.fscbram_controller.dcount );
--    reg_sigs(FSCBC_SEL)(27 downto 16) <=  cbc_system_stat.fscbram_controller.dout;
                   
    io_dctt_gen :
        for i in 0 to NFE-1 generate
           reg_sigs(IO_SEL + IO_DCTT_SUBSEL + i)(IO_DCTT_CBC_FSM_OFFSET + IO_DCTT_CBC_FSM_WIDTH - 1 downto IO_DCTT_CBC_FSM_OFFSET)
         <= "0000" when cbc_system_stat.io.dctts(i).fsm = init               else
            "0001" when cbc_system_stat.io.dctts(i).fsm = wait_bram_ready    else
            "0010" when cbc_system_stat.io.dctts(i).fsm = idelay_reset       else
            "0011" when cbc_system_stat.io.dctts(i).fsm = idelay_scan        else
--               "0100" when cbc_system_stat.io.cbcios(i).data_clock_timing_tuning_fsm = pre_idelay_tuning  else
            "0100" when cbc_system_stat.io.dctts(i).fsm = idelay_tuning      else       
            "0101" when cbc_system_stat.io.dctts(i).fsm = set_idelay         else                      
            "0110" when cbc_system_stat.io.dctts(i).fsm = change_clock_phase else
            "0111" when cbc_system_stat.io.dctts(i).fsm = reset_iserdes_idelay_ctrl else
            "1000" when cbc_system_stat.io.dctts(i).fsm = bitslip_tuning     else               
            "1001" when cbc_system_stat.io.dctts(i).fsm = tuned;

         reg_sigs(IO_SEL + IO_DCTT_SUBSEL + i)
         (IO_DCTT_CBC_ISERDES_IDELAY_CTRL_FSM_OFFSET + IO_DCTT_CBC_ISERDES_IDELAY_CTRL_FSM_WIDTH - 1 downto IO_DCTT_CBC_ISERDES_IDELAY_CTRL_FSM_OFFSET)
         <= "000" when cbc_system_stat.io.dctts(i).iserdes_idelay_ctrl_fsm = wait_for_commands      else
            "001" when cbc_system_stat.io.dctts(i).iserdes_idelay_ctrl_fsm = in_delay_reset_process else
            "010" when cbc_system_stat.io.dctts(i).iserdes_idelay_ctrl_fsm = in_delay_set_process   else
            "011" when cbc_system_stat.io.dctts(i).iserdes_idelay_ctrl_fsm = in_delay_ce_process    else               
            "100" when cbc_system_stat.io.dctts(i).iserdes_idelay_ctrl_fsm = reset_iserdes_process  else   
            "101" when cbc_system_stat.io.dctts(i).iserdes_idelay_ctrl_fsm = bitslip_process        else   
            "110" when cbc_system_stat.io.dctts(i).iserdes_idelay_ctrl_fsm = pattern_check;

         reg_sigs(IO_SEL + IO_DCTT_SUBSEL + i)(IO_DCTT_CBC_ISERDES_BITSLIP_COUNTER_OFFSET + IO_DCTT_CBC_ISERDES_BITSLIP_COUNTER_WIDTH - 1 downto IO_DCTT_CBC_ISERDES_BITSLIP_COUNTER_OFFSET)
         <= cbc_system_stat.io.dctts(i).iserdes_bitslip_counter;  
    end generate;

    io_cbc_gen :  
        for i in 0 to NCBC-1 generate                       
        reg_sigs(IO_SEL + IO_CBC_SUBSEL + i)(IO_CBC_IDELAY_TAP_OFFSET + IO_CBC_IDELAY_TAP_WIDTH - 1 downto IO_CBC_IDELAY_TAP_OFFSET)
        <= cbc_system_stat.io.cbcios(i).in_delay_tap_out(4 downto 0);  
        reg_sigs(IO_SEL + IO_CBC_SUBSEL + i)(IO_CBC_DELAY_LOCKED_BIT)
        <= cbc_system_stat.io.cbcios(i).delay_locked;                               
        reg_sigs(IO_SEL + IO_CBC_SUBSEL + i)(IO_CBC_DATA_SLVS5_OFFSET + IO_CBC_DATA_SLVS5_WIDTH - 1 downto IO_CBC_DATA_SLVS5_OFFSET) <= cbc_system_stat.io.cbcios(i).slvs5;        

--            io_cbc_input_tuning_sets : for j in 0 to 1 generate
--                reg_sigs(IO_SEL + IO_CBC_SEL + i * IO_CBC_NREGS + IO_CBC_INPUT_TUNING_CLK0_SUBSEL+j)(IO_CBC_PRE_IDELAY_TUNING_TAP_OFFSET + IO_CBC_PRE_IDELAY_TUNING_TAP_WIDTH - 1 downto IO_CBC_PRE_IDELAY_TUNING_TAP_OFFSET)
--                <= cbc_system_stat.io.cbcios(i).tuning_stat_set(j).pre_idelay_tuning_tap;                        
--                reg_sigs(IO_SEL + IO_CBC_SEL + i * IO_CBC_NREGS + IO_CBC_INPUT_TUNING_CLK0_SUBSEL+j)(IO_CBC_PATTERN_AT_IDELAY_RESET_OFFSET + IO_CBC_PATTERN_AT_IDELAY_RESET_WIDTH - 1 downto IO_CBC_PATTERN_AT_IDELAY_RESET_OFFSET)
--                <= cbc_system_stat.io.cbcios(i).tuning_stat_set(j).pattern_at_idelay_reset;                                        
--                reg_sigs(IO_SEL + IO_CBC_SEL + i * IO_CBC_NREGS + IO_CBC_INPUT_TUNING_CLK0_SUBSEL+j)(IO_CBC_FSM_PROCESSED_OFFSET + IO_CBC_FSM_PROCESSED_WIDTH - 1 downto IO_CBC_FSM_PROCESSED_OFFSET) 
--                <= cbc_system_stat.io.cbcios(i).tuning_stat_set(j).fsm_processed;     
--               reg_sigs(IO_SEL + IO_CBC_SEL + i * IO_CBC_NREGS + IO_CBC_INPUT_TUNING_CLK0_SUBSEL+j)(IO_CBC_IDELAY_RESET_TUNING_TAP_OFFSET + IO_CBC_IDELAY_RESET_TUNING_TAP_WIDTH - 1 downto IO_CBC_IDELAY_RESET_TUNING_TAP_OFFSET)
--                 <= cbc_system_stat.io.cbcios(i).tuning_stat_set(j).idelay_reset_tuning_tap;                                                                    
--        end generate;
        
    end generate;

    mmcme2_drp_sig_gen:
    for i in 0 to NCBC-1 generate
        reg_sigs(MMCME2_DRP_SEL)(i) <= cbc_system_stat.mmcme2_drps(i).srdy;
    end generate;
    reg_sigs(L1A_COUNTER_SEL)        <= "000" & cbc_system_stat.fast_signal_manager.l1a_count;
    

    --=============================--
    sel <= to_integer(unsigned(ipb_mosi_i.ipb_addr(addr_width-1 downto 0))) when addr_width>0 else 0;
    --=============================--
    
    
    --=============================--
    process(reset, clk)
    --=============================--
    variable index : integer range 0 to 8;
    begin
    if reset='1' then
        ack      <= '0';
        index := 0;
    elsif rising_edge(clk) then
        
        ipb_miso_o.ipb_rdata <= (others => '0');        
        -- read 
        if sel = DB_NWORD_EVENTS_SEL then
            ipb_miso_o.ipb_rdata(DATA_BUFFER_ADDR_WIDTH downto 0) <=  nword_events_ipb;       
        elsif sel = DB_NWORD_ALL_SEL then
            ipb_miso_o.ipb_rdata(DATA_BUFFER_ADDR_WIDTH downto 0) <= nword_all_ipb;
        elsif sel = DB_NWORD_FREE_SEL then               
            ipb_miso_o.ipb_rdata(DATA_BUFFER_ADDR_WIDTH downto 0) <= nword_free_ipb;
        elsif sel >= CDP_DATA_FRAME_BUF_SEL and sel < CDP_DATA_FRAME_BUF_SEL + NCBC then
            if ipb_mosi_i.ipb_strobe = '1' and ipb_mosi_i.ipb_write = '0' then
                if index = 8 then      
                    ipb_miso_o.ipb_rdata <= x"00"
                                          & cbc_system_stat.cbc_data_processors(sel - CDP_DATA_FRAME_BUF_SEL).triggered_data(index*4+2)
                                          & cbc_system_stat.cbc_data_processors(sel - CDP_DATA_FRAME_BUF_SEL).triggered_data(index*4+1)
                                          & cbc_system_stat.cbc_data_processors(sel - CDP_DATA_FRAME_BUF_SEL).triggered_data(index*4+0);
                    if ack = '1' then                     
                        index := 0;
                    end if;
                else
                    ipb_miso_o.ipb_rdata <= cbc_system_stat.cbc_data_processors(sel - CDP_DATA_FRAME_BUF_SEL).triggered_data(index*4+3) 
                                          & cbc_system_stat.cbc_data_processors(sel - CDP_DATA_FRAME_BUF_SEL).triggered_data(index*4+2)
                                          & cbc_system_stat.cbc_data_processors(sel - CDP_DATA_FRAME_BUF_SEL).triggered_data(index*4+1)
                                          & cbc_system_stat.cbc_data_processors(sel - CDP_DATA_FRAME_BUF_SEL).triggered_data(index*4+0);
                    if ack = '1' then
                        index := index + 1; 
                    end if; 
                end if;
            end if;              
        else
            ipb_miso_o.ipb_rdata <= regs(sel);
        end if;

        -- ack        
        ack <= ipb_mosi_i.ipb_strobe and not ack;
    
        regs <= reg_sigs;      

    end if;
    end process;
    
    ipb_miso_o.ipb_ack <= ack;
    ipb_miso_o.ipb_err <= '0';
    
end Behavioral;
