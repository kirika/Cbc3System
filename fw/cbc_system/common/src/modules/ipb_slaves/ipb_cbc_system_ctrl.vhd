--===========================--
-- ipb_cbc_system_ctrl
-- 28.10.2016 Kirika Uchida
--===========================--

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use work.ipbus.all;
use work.system_package.all;
use work.user_package.all;
use work.cbc_system_package.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ipb_cbc_system_ctrl is
generic( ADDR_WIDTH     : natural := 8 );
port (
        clk                    : in  std_logic;
        clk_40MHz_iserdes_ctrl : in  std_logic_vector(NFE-1 downto 0);
        clk_320MHz_iserdes_ctrl: in  std_logic_vector(NFE-1 downto 0);
        clk_40MHz_cbc_ctrl     : in  std_logic;
        clk_320MHz_cbc_ctrl    : in  std_logic;
        clk_40MHz_daq          : in  std_logic;
        clk_320MHz_daq         : in  std_logic;
        mmcme2_drp_sclk        : in  std_logic;
        reset                  : in  std_logic;
        ipb_mosi_i             : in  ipb_wbus;
        ipb_miso_o             : out ipb_rbus;
        cbc_system_ctrl        : out ipb_cbc_system_ctrl_type
        );
end ipb_cbc_system_ctrl;

architecture Behavioral of ipb_cbc_system_ctrl is

    signal regs: array_256x32bit;  
    
    signal sel: integer range 0 to 255;
    signal ack: std_logic;
    
    attribute keep: boolean;
    attribute keep of sel: signal is true;
    
    -- global control
    constant GLOBAL_SEL                          : integer := 16#00#;
    constant GLOBAL_RESET_BIT                    : integer := 0;
    constant GLOBAL_DAQ_RESET_BIT                : integer := 1;
    constant GLOBAL_CBC_HARD_RESET_BIT           : integer := 16;

    -- fast signal manger control
    constant FSM_SEL                             : integer := 16#01#;
    constant FSM_RESET_BIT                       : integer := 0;
    constant FSM_START_TRIGGER_BIT               : integer := 1;
    constant FSM_STOP_TRIGGER_BIT                : integer := 2;
    constant FSM_FAST_RESET_BIT                  : integer := 16;
    constant FSM_TEST_PULSE_REQ_BIT              : integer := 17;
    constant FSM_TRIGGER_BIT                     : integer := 18;
    constant FSM_ORBIT_RESET_BIT                 : integer := 19;
    constant FSM_FSG_RESET_BIT                   : integer := 20;
    constant FSM_FSG_LOAD_CNFG_BIT               : integer := 21;
    constant FSM_FSG_START_BIT                   : integer := 22;
    constant FSM_FSG_STOP_BIT                    : integer := 23;

    -- event builder control 
    constant EB_SEL                              : integer := 16#02#;
    constant EB_RESET_BIT                        : integer := 0;
    -- data buffer control    
    constant DB_SEL                              : integer := 16#03#;
    constant DB_RESET_BIT                        : integer := 0;
    constant DB_TRIG_READALL_BIT                 : integer := 1;
    -- cbc emulator control
    constant CE_SEL                              : integer := 16#04#;
    constant CE_RESET_BIT                        : integer := 0;
    constant CE_LOAD_DATA_PATTERN_BIT            : integer := 1;
    -- cbc data processor control
    constant CDP_SEL                             : integer := 16#10#;
    constant CDP_RESET_BIT                       : integer := 0;
    constant CDP_FRAME_COUNTER_RESET_BIT         : integer := 1;
    -- cbc i2c transaction control
    constant CIBM_SEL                             : integer := 16#30#;
    constant CIBM_RESET_BIT                       : integer := 0;
    constant CIBM_INIT_BIT                        : integer := 1;
    constant CIBM_RESET_FIFOS_BIT                 : integer := 2;

    constant FSCBC_SEL                            : integer := 16#32#;
    constant FSCBC_RESET_BIT                      : integer := 0;
    constant FSCBC_START_BIT                      : integer := 1;
    constant FSCBC_RESUME_BIT                     : integer := 2;
    
    constant IO_SEL                               : integer := 16#40#;
    constant IO_FS_OSERDES_RESET_BIT              : integer := 0;    
    constant IO_DCTSB_RESET_BIT                   : integer := 1;
    constant IO_DCTT_RESET_BIT                    : integer := 2;
    constant IO_DCTT_TUNE_BIT                     : integer := 3; 
    constant IO_DCTT_SUBSEL                       : integer := 1;
    constant IO_DCTT_SCAN_PULSE_BIT               : integer := 4; 
    
    constant MMCME2_DRP_SEL                         : integer := 16#50#;
    constant MMCME2_DRP_RST_BIT                     : integer := 0;
    constant MMCME2_DRP_SEN_BIT                     : integer := 1;
    
    constant RDB_CONTROLLER_SEL                     : integer := 16#60#;
    constant RDB_CONTROLLER_RESET_BIT               : integer := 0;
    
    constant WAIT_FOR_MASK                            : integer := 50;
    
    signal ipb_global                                 : std_logic_vector(31 downto 0);
    signal ipb_fsm                                    : std_logic_vector(31 downto 0);
    signal ipb_eb                                     : std_logic_vector(0 downto 0);
    signal ipb_db                                     : std_logic_vector(1 downto 0);

--    signal global                                     : std_logic_vector(0 downto 0);
--    signal scg                                        : std_logic_vector(8 downto 0);
--    signal eb                                         : std_logic_vector(0 downto 0);
--    signal db                                         : std_logic_vector(1 downto 0);

    type ce_signals_type is array (0 to NCBC-1) of std_logic_vector(1 downto 0);
    signal ipb_ce_signals                             : ce_signals_type;
--    signal ce_signals                                 : ce_signals_type;    

    type cdp_signals_type is array (0 to NCBC-1) of std_logic_vector(2 downto 0);
    signal ipb_cdp_signals                            : cdp_signals_type; 
--    signal cdp_signals                                : cdp_signals_type;   

    type cibm_signals_type is array (0 to NFE-1) of std_logic_vector(2 downto 0);
    signal ipb_cibm_signals                           : cibm_signals_type;

    signal ipb_fscbc_signals                          : std_logic_vector(2 downto 0); 

    signal ipb_io                                     : std_logic_vector(31 downto 0);
    signal ipb_mmcme2_drp                             : std_logic_vector(31 downto 0); 
    signal ipb_rdb_controller                         : std_logic_vector(31 downto 0);

	type ipb_io_dctt_type is array (0 to NFE-1) of std_logic_vector(31 downto 0);
	signal ipb_io_dctt_signals                        : ipb_io_dctt_type;

begin

    --=============================--
    sel <= to_integer(unsigned(ipb_mosi_i.ipb_addr(addr_width-1 downto 0))) when addr_width>0 else 0;
    --=============================--
 
    --=============================--
    process(reset, clk)
    --=============================--
    variable timer : natural;
    variable wsel  : integer range 0 to 255;
    begin
    if reset='1' then
      regs   <= (others=> (others=>'0'));
      ack    <= '0';
      timer    := 0;
      wsel     := 0;
      ipb_global <= (others => '0'); 
      ipb_fsm    <= (others => '0'); 
      ipb_eb     <= (others => '0'); 
      ipb_db     <= (others => '0');       
      ipb_ce_signals  <= (others => (others => '0'));
      ipb_cdp_signals <= (others => (others => '0'));
      ipb_cibm_signals <= (others => (others => '0'));
      ipb_fscbc_signals    <= (others => '0');
      ipb_io     <= (others => '0'); 
      ipb_mmcme2_drp     <= (others => '0');    
      ipb_rdb_controller <= (others => '0'); 
      ipb_io_dctt_signals <= (others => (others => '0')); 
    elsif rising_edge(clk) then
 
        ipb_global <= (others => '0'); 
        ipb_fsm    <= (others => '0'); 
        ipb_eb     <= (others => '0'); 
        ipb_db     <= (others => '0');       
        ipb_ce_signals  <= (others => (others => '0'));
        ipb_cdp_signals <= (others => (others => '0'));
        ipb_cibm_signals <= (others => (others => '0'));        
        ipb_fscbc_signals    <= (others => '0');
        ipb_io     <= (others => '0');       
        ipb_mmcme2_drp     <= (others => '0');   
        ipb_rdb_controller <= (others => '0'); 
        ipb_io_dctt_signals <= (others => (others => '0'));
        -- write
        if ipb_mosi_i.ipb_strobe='1' and ipb_mosi_i.ipb_write='1' then
            regs(sel) <= ipb_mosi_i.ipb_wdata;
            timer :=   1;
            wsel  := sel;
        end if;
        
        if timer /= 0 then
            if timer = WAIT_FOR_MASK then
                
                timer := 0;
            
                if wsel = GLOBAL_SEL then
                    ipb_global <= regs(wsel)(31 downto 0);     
                elsif wsel = FSM_SEL then
                    ipb_fsm    <= regs(wsel);
                elsif wsel = EB_SEL then
                    ipb_eb     <= regs(wsel)(0 downto 0);                  
                elsif wsel = DB_SEL then
                    ipb_db     <= regs(wsel)(1 downto 0); 
                elsif wsel >= CE_SEL and wsel < CDP_SEL then
                    ipb_ce_signals(wsel - CE_SEL)   <= regs(wsel)(1 downto 0);
                elsif wsel >= CDP_SEL and wsel < CDP_SEL + NCBC then
                    ipb_cdp_signals(wsel - CDP_SEL) <= regs(wsel)(2 downto 0);
                elsif wsel >= CIBM_SEL and wsel < CIBM_SEL + NFE then
                    ipb_cibm_signals(wsel - CIBM_SEL) <= regs(wsel)(2 downto 0);
                elsif wsel = FSCBC_SEL then
                    ipb_fscbc_signals <= regs(wsel)(2 downto 0);
                elsif wsel = IO_SEL then
                    ipb_io <= regs(wsel);
                elsif wsel >= IO_SEL + IO_DCTT_SUBSEL and wsel < IO_SEL + IO_DCTT_SUBSEL + NFE then
                	ipb_io_dctt_signals(wsel - IO_SEL - IO_DCTT_SUBSEL) <= regs(wsel);
                elsif wsel = MMCME2_DRP_SEL then
                    ipb_mmcme2_drp <= regs(wsel);
                elsif wsel = RDB_CONTROLLER_SEL then
                	ipb_rdb_controller <= regs(wsel);
                end if;
            
                regs(wsel) <= (others => '0');
            
            else
                timer := timer + 1;        
            end if;    
        end if;
    
    -- read 
    ipb_miso_o.ipb_rdata <= regs(sel);
    -- ack
    ack <= ipb_mosi_i.ipb_strobe and not ack;
    
    end if;
    end process;

    ipb_miso_o.ipb_ack <= ack;
    ipb_miso_o.ipb_err <= '0';

    --=================--
    -- cbc hard reset 1us pulse
    --=================--    
    hard_reset_gen : entity work.dff_sync_edge_detect_multi_width_pulse_out
    generic map( WIDTH => 40 )
    port map( reset => reset, 
              clkb => clk_40MHz_cbc_ctrl, 
              dina => ipb_global(GLOBAL_CBC_HARD_RESET_BIT), 
              doutb => cbc_system_ctrl.global.cbc_hard_reset
    );
    --=================--
    -- io serdes & data clock timing tuning control signals
    --=================--    
    -- oserdes reset should be deasserted synchronized with clk_div
    io_fs_oserdes_reset_gen : entity work.dff_sync_edge_detect
    port map( reset => reset, 
              clkb => clk_40MHz_cbc_ctrl, 
              dina => ipb_io(IO_FS_OSERDES_RESET_BIT) or ipb_global(GLOBAL_RESET_BIT), 
              doutb => cbc_system_ctrl.io.fs_oserdes_reset
    );   

    io_dctsb_reset_gen : entity work.dff_sync_edge_detect
    port map( reset => reset, 
              clkb => clk_40MHz_cbc_ctrl, 
              dina => ipb_io(IO_DCTT_RESET_BIT) or ipb_global(GLOBAL_RESET_BIT), 
              doutb => cbc_system_ctrl.io.dctsb_reset
    );         

    dctt_ctrl_gen :
    for i in 0 to NFE-1 generate       
        io_dctt_reset : entity work.dff_sync_edge_detect
        port map( reset => reset,
                  clkb  => clk_40MHz_iserdes_ctrl(i),
                  dina  => ipb_io(IO_DCTT_RESET_BIT) or ipb_io_dctt_signals(i)(IO_DCTT_RESET_BIT) or ipb_global(GLOBAL_RESET_BIT),
                  doutb => cbc_system_ctrl.io.data_clock_timing_tunings(i).reset );
        io_dctt_tune : entity work.dff_sync_edge_detect
        port map( reset => reset,
                  clkb  => clk_40MHz_iserdes_ctrl(i),
                  dina  => ipb_io(IO_DCTT_TUNE_BIT) or ipb_io_dctt_signals(i)(IO_DCTT_TUNE_BIT),
                  doutb => cbc_system_ctrl.io.data_clock_timing_tunings(i).tune );
    
        io_dctt_scan_pulse : entity work.dff_sync_edge_detect
        port map( reset => reset,
                  clkb  => clk_40MHz_iserdes_ctrl(i),
                  dina  => ipb_io_dctt_signals(i)(IO_DCTT_SCAN_PULSE_BIT),
                  doutb => cbc_system_ctrl.io.data_clock_timing_tunings(i).scan_pulse );
    end generate;    

    --=================--
    -- trigger veto control signals
    --=================--                  
    veto_reset : entity work.dff_sync_edge_detect
    port map(reset => reset,
                  clkb  => clk_40MHz_cbc_ctrl,
                  dina  => ipb_global(GLOBAL_RESET_BIT) or ipb_global(GLOBAL_DAQ_RESET_BIT),
                  doutb => cbc_system_ctrl.veto.reset
    ); 
    --=================--
    -- fast signal manager control signals
    --=================--               
    fsm_reset : entity work.dff_sync_edge_detect
    port map( reset => reset,
              clkb  => clk_40MHz_cbc_ctrl,
              dina  => ipb_fsm(FSM_RESET_BIT) or ipb_global(GLOBAL_RESET_BIT),
              doutb => cbc_system_ctrl.fast_signal_manager.reset
    );
    fsm_40MHz_fast_reset : entity work.dff_sync_edge_detect
	port map( reset => reset, 
		      clkb => clk_40MHz_cbc_ctrl, 
		      dina => ipb_fsm(FSM_FAST_RESET_BIT), 
		      doutb => cbc_system_ctrl.fast_signal_manager.cbc_fast_signal(fast_signal_40MHz_indices.fast_reset)
	);
    fsm_40MHz_test_pulse_req : entity work.dff_sync_edge_detect
	port map( reset => reset, 
		      clkb => clk_40MHz_cbc_ctrl, 
		      dina => ipb_fsm(FSM_TEST_PULSE_REQ_BIT), 
		      doutb => cbc_system_ctrl.fast_signal_manager.cbc_fast_signal(fast_signal_40MHz_indices.test_pulse)
	);
    fsm_40MHz_trigger : entity work.dff_sync_edge_detect
	port map( reset => reset, 
		      clkb => clk_40MHz_cbc_ctrl, 
		      dina => ipb_fsm(FSM_TRIGGER_BIT), 
		      doutb => cbc_system_ctrl.fast_signal_manager.cbc_fast_signal(fast_signal_40MHz_indices.trigger)
	);
    fsm_40MHz_orbit_reset : entity work.dff_sync_edge_detect
	port map( reset => reset, 
		      clkb => clk_40MHz_cbc_ctrl, 
		      dina => ipb_fsm(FSM_ORBIT_RESET_BIT), 
		      doutb => cbc_system_ctrl.fast_signal_manager.cbc_fast_signal(fast_signal_40MHz_indices.orbit_reset)
	);
    --=================--
    -- fast signal generator control signals
    --=================--               
    fsg_reset : entity work.dff_sync_edge_detect
	port map( reset => reset, 
		      clkb => clk_40MHz_cbc_ctrl, 
		      dina => ipb_fsm(FSM_FSG_RESET_BIT) or ipb_fsm(FSM_RESET_BIT) or ipb_global(GLOBAL_RESET_BIT), 
		      doutb => cbc_system_ctrl.fast_signal_manager.fast_signal_generator.reset
	);
    fsg_load_cnfg : entity work.dff_sync_edge_detect
	port map( reset => reset, 
		      clkb => clk_40MHz_cbc_ctrl, 
		      dina => ipb_fsm(FSM_FSG_LOAD_CNFG_BIT), 
		      doutb => cbc_system_ctrl.fast_signal_manager.fast_signal_generator.load_cnfg
	);
    fsg_start : entity work.dff_sync_edge_detect
	port map( reset => reset, 
		      clkb => clk_40MHz_cbc_ctrl, 
		      dina => ipb_fsm(FSM_FSG_START_BIT), 
		      doutb => cbc_system_ctrl.fast_signal_manager.fast_signal_generator.start
	);
    fsg_stop : entity work.dff_sync_edge_detect
    port map( reset => reset, 
              clkb => clk_40MHz_cbc_ctrl, 
	          dina => ipb_fsm(FSM_FSG_STOP_BIT), 
	          doutb => cbc_system_ctrl.fast_signal_manager.fast_signal_generator.stop
	);
    cibm_sig_gen :
    for i in 0 to NFE-1 generate
        cibm_reset : entity work.dff_sync_edge_detect
        port map(   reset => reset,
                    clkb  => clk_40MHz_cbc_ctrl,
                    dina  => ipb_cibm_signals(i)(CIBM_RESET_BIT) or ipb_global(GLOBAL_RESET_BIT) or ipb_global(GLOBAL_CBC_HARD_RESET_BIT),
                    doutb => cbc_system_ctrl.cbc_i2c_bus_managers(i).reset
        );
        cibm_init_gen : entity work.dff_sync_edge_detect
        port map(   reset => reset,
                    clkb  => clk_40MHz_cbc_ctrl,
                    dina  => ipb_cibm_signals(i)(CIBM_INIT_BIT),
                    doutb => cbc_system_ctrl.cbc_i2c_bus_managers(i).init
        );
       cibm_reset_fifos_gen : entity work.dff_sync_edge_detect
         port map(   reset => reset,
                     clkb  => clk_40MHz_cbc_ctrl,
                     dina  => ipb_cibm_signals(i)(CIBM_RESET_FIFOS_BIT) or ipb_cibm_signals(i)(CIBM_RESET_BIT) or ipb_global(GLOBAL_RESET_BIT) or ipb_global(GLOBAL_CBC_HARD_RESET_BIT),
                     doutb => cbc_system_ctrl.cbc_i2c_bus_managers(i).reset_fifos
         );
   end generate;

   fscbc_reset : entity work.dff_sync_edge_detect
    port map(   reset => reset,
                clkb  => clk_40MHz_cbc_ctrl,
                dina  => ipb_fscbc_signals(FSCBC_RESET_BIT),
                doutb => cbc_system_ctrl.fscbram_controller.reset
    );
   fscbc_start : entity work.dff_sync_edge_detect
     port map(   reset => reset,
                 clkb  => clk_40MHz_cbc_ctrl,
                 dina  => ipb_fscbc_signals(FSCBC_START_BIT),
                 doutb => cbc_system_ctrl.fscbram_controller.start
     );    
   fscbc_resume : entity work.dff_sync_edge_detect
       port map(   reset => reset,
                   clkb  => clk_320MHz_cbc_ctrl,
                   dina  => ipb_fscbc_signals(FSCBC_RESUME_BIT),
                   doutb => cbc_system_ctrl.fscbram_controller.resume
       );      

    fsm_start_trigger : entity work.dff_sync_edge_detect
    port map(   reset => reset,
                clkb  => clk_40MHz_cbc_ctrl,
                dina  => ipb_fsm(FSM_START_TRIGGER_BIT),
                doutb => cbc_system_ctrl.fast_signal_manager.start_trigger
    );
    fsm_stop_trigger : entity work.dff_sync_edge_detect
    port map(   reset => reset,
                clkb  => clk_40MHz_cbc_ctrl,
                dina  => ipb_fsm(FSM_STOP_TRIGGER_BIT),
                doutb => cbc_system_ctrl.fast_signal_manager.stop_trigger
    );    
    --=================--
    -- DAQ control signals
    --=================--
    cdp_sig_gen :
     for i in 0 to NCBC-1 generate
         cdp_reset : entity work.dff_sync_edge_detect
         port map(   reset => reset,
                     clkb  => clk_320MHz_daq,
                     dina  => ipb_cdp_signals(i)(CDP_RESET_BIT) or ipb_global(GLOBAL_RESET_BIT) or ipb_global(GLOBAL_DAQ_RESET_BIT),
                     doutb => cbc_system_ctrl.cbc_data_processors(i).reset
         );
        cdp_trig_data_shift_reg_reset : entity work.dff_sync_edge_detect
          port map(   reset => reset,
                      clkb  => clk_40MHz_daq,
                      dina  => ipb_cdp_signals(i)(CDP_RESET_BIT) or ipb_global(GLOBAL_RESET_BIT) or ipb_global(GLOBAL_DAQ_RESET_BIT),
                      doutb => cbc_system_ctrl.cbc_data_processors(i).reset_40MHz
          );        
        cdp_frame_counter_reset : entity work.dff_sync_edge_detect
        port map( reset => reset,
                  clkb  => clk_40MHz_daq,
                  dina  =>  ipb_cdp_signals(i)(CDP_FRAME_COUNTER_RESET_BIT) or ipb_global(GLOBAL_RESET_BIT) or ipb_global(GLOBAL_DAQ_RESET_BIT),
                  doutb => cbc_system_ctrl.cbc_data_processors(i).frame_counter_reset
                  );
     end generate;
    eb_reset : entity work.dff_sync_edge_detect
    port map(   reset => reset,
                clkb  => clk_320MHz_daq,
                dina  => ipb_eb(EB_RESET_BIT) or ipb_global(GLOBAL_RESET_BIT) or ipb_global(GLOBAL_DAQ_RESET_BIT),
                doutb => cbc_system_ctrl.event_builder.reset
    );
    db_reset : entity work.dff_sync_edge_detect
    port map(   reset => reset,
                clkb  => clk_320MHz_daq,
                dina  => ipb_db(DB_RESET_BIT) or ipb_global(GLOBAL_RESET_BIT) or ipb_global(GLOBAL_DAQ_RESET_BIT),
                doutb => cbc_system_ctrl.data_buffer.reset
    );
    db_trig_readall : entity work.dff_sync_edge_detect
    port map(   reset => reset,
                clkb  => clk_320MHz_daq,
                dina  => ipb_db(DB_TRIG_READALL_BIT),
                doutb => cbc_system_ctrl.data_buffer.trig_readall
    );
    --=================--
    -- CBC emulator control signals
    --=================--
   ce_sig_gen :
    for i in 0 to NCBC-1 generate
    ce_reset : entity work.dff_sync_edge_detect
    port map( reset => reset,
              clkb  => clk_40MHz_cbc_ctrl,
              dina  => ipb_ce_signals(i)(CE_RESET_BIT) or ipb_global(GLOBAL_RESET_BIT),
              doutb => cbc_system_ctrl.cbc_emulators(i).reset
              );
    ce_load_data_pattern : entity work.dff_sync_edge_detect
              port map( reset => reset,
                        clkb  => clk_40MHz_cbc_ctrl,
                        dina  => ipb_ce_signals(i)(CE_LOAD_DATA_PATTERN_BIT),
                        doutb => cbc_system_ctrl.cbc_emulators(i).load_data_pattern
                        );              
    end generate;

    --=================--
    -- mmcme2_drp control signals
    --=================--
    mmcme2_drp_rst : entity work.dff_sync_edge_detect
    port map(  reset => reset,
               clkb  => mmcme2_drp_sclk,
               dina  => ipb_mmcme2_drp(MMCME2_DRP_RST_BIT),
               doutb => cbc_system_ctrl.mmcme2_drp.rst );
    mmcme2_drp_sen : entity work.dff_sync_edge_detect
               port map(  reset => reset,
                          clkb  => mmcme2_drp_sclk,
                          dina  => ipb_mmcme2_drp(MMCME2_DRP_SEN_BIT),
                          doutb => cbc_system_ctrl.mmcme2_drp.sen );               


	rdb_controller_rst : entity work.dff_sync_edge_detect
	port map( reset => reset,
              clkb  => clk_40MHz_cbc_ctrl,
			  dina  => ipb_rdb_controller(RDB_CONTROLLER_RESET_BIT) or ipb_global(GLOBAL_RESET_BIT),
			  doutb => cbc_system_ctrl.rdb_controller.reset  );			

end Behavioral;

