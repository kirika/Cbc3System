--===========================--
-- ipb_cbc_system_cnfg 
-- 28.10.2016 Kirika Uchida
--===========================--

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use work.ipbus.all;
use work.system_package.all;
use work.user_package.all;
use work.cbc_system_package.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ipb_cbc_system_cnfg is
generic( ADDR_WIDTH     : natural := 8 );
port (
        clk                   : in  std_logic;
        reset                 : in  std_logic;
        ipb_mosi_i            : in  ipb_wbus;
        ipb_miso_o            : out ipb_rbus;
        cbc_system_cnfg       : out ipb_cbc_system_cnfg_type
        );
end ipb_cbc_system_cnfg;

architecture Behavioral of ipb_cbc_system_cnfg is

    signal regs: array_256x32bit;  
    
    signal sel: integer range 0 to 255;
    signal ack: std_logic;
    
    attribute keep: boolean;
    attribute keep of sel: signal is true;

     --=====================--    
    -- global configurations
    --=====================--    
    -- be
    constant GLOBAL_BE_CNFG_SEL                       : integer := 16#00#;
    constant GLOBAL_BE_ID_OFFSET                      : integer := 0;
    constant GLOBAL_BE_ID_WIDTH                       : integer := 7; 
    
    constant GLOBAL_CBC_CNFG_SEL                      : integer := 16#01#;
    constant GLOBAL_CBC_ACTIVE_BIT                    : integer := 0;
    constant GLOBAL_CBC_ID_OFFSET                     : integer := 4;
    constant GLOBAL_CBC_ID_WIDTH                      : integer := 5;
    constant GLOBAL_CBC_FE_ID_OFFSET                  : integer := 9;
    constant GLOBAL_CBC_FE_ID_WIDTH                   : integer := 3;
    constant GLOBAL_CBC_I2CADDRESS_OFFSET             : integer := 12;
    constant GLOBAL_CBC_I2CADDRESS_WIDTH              : integer := 7;

    constant GLOBAL_MISC_SEL                          : integer := 16#10#;
    constant GLOBAL_MISC_TRIGGER_MASTER_EXTERNAL_BIT  : integer := 1;
    constant GLOBAL_TEST_OUT_SEL                      : integer := 16#11#;

 
    -- cbc system clock
    constant CSC_SEL                                  : integer := 16#20#;
    constant CSC_TYPE_OFFSET                               : integer := 0;
    constant CSC_TYPE_WIDTH                                : integer := 4;
    -- clock selection values
    constant CLK_TYPE_EXT_CLK_A                       : std_logic_vector(3 downto 0) := "0000";
    constant CLK_TYPE_INTCLK                          : std_logic_vector(3 downto 0) := "0001";
    --=====================--    
    -- fast command manager configurations
    --=====================--
    constant FCM_SEL                                  : integer := 16#21#;        
    constant FCM_FS_EN_EXT_ASYNC_L1A_BIT              : integer := 0;
    constant FCM_FS_EN_FMC_BIT                        : integer := 1;
    constant FCM_FS_EN_IPBUS_BIT                      : integer := 2;
    constant FCM_FS_EN_INTERNAL_BIT                   : integer := 3;

    -- fast signal generator   
    constant FCM_FSG_EN_SEL                           : integer := 16#22#;
    constant FCM_FSG_EN_FAST_RESET_BIT                : integer := 0;
    constant FCM_FSG_EN_TRIGGER_BIT                   : integer := 1;
    constant FCM_FSG_EN_TEST_PULSE_BIT                : integer := 2;
    constant FCM_FSG_EN_ORBIT_RESET_BIT               : integer := 3;
    constant FCM_FSG_NCYCLE_SEL                       : integer := 16#23#;
    constant FCM_FSG_CYCLE_PERIOD_SEL                 : integer := 16#24#;
    constant FCM_FSG_TRIG_TIMING_SEL                  : integer := 16#25#;
    constant FCM_FSG_TPREQ_TIMING_SEL                 : integer := 16#26#;
    constant FCM_FSG_OR_TIMING_SEL                    : integer := 16#27#;

    constant CIBM_SEL                                 : integer := 16#28#;
    constant CIBM_IPBUS_EN_BIT                        : integer := 0;
    constant FSCBC_SEL                                : integer := 16#29#;
    constant FSCBC_IPBUS_EN_BIT                       : integer := 0;

	constant RDB_CONTROLLER_SEL                       : integer := 16#12#;
	constant RDB_CONTROLLER_CBC_ID_OFFSET             : integer := 0;
	constant RDB_CONTROLLER_CBC_ID_WIDTH              : integer := CBCID_WIDTH;
	constant RDB_CONTROLLER_LATENCY_OFFSET            : integer := 7;
	constant RDB_CONTROLLER_LATENCY_WIDTH             : integer := 8;
	constant RDB_CONTROLLER_WRITE_BLOCK_SIZE_OFFSET   : integer := 15;
	constant RDB_CONTROLLER_WRITE_BLOCK_SIZE_WIDTH    : integer := 15;	

    --=====================--    
    -- data buffer
    --=====================--
    constant DB_SEL                                   : integer := 16#2a#;
    constant DB_SAFE_NWORD_FREE_SUBSEL                : integer := 0;
    --=====================--    
    -- cbc emulator configurations
    --=====================--    
    constant CE_SEL                                   : integer := 16#30#;
    constant CE_DP_SLVS1234_SUBSEL                     : integer := 0;
    constant CE_DP_SLVS1234_S1_ADDR_OFFSET             : integer := 0;
    constant CE_DP_SLVS1234_S1_ADDR_WIDTH              : integer := 8;
    constant CE_DP_SLVS1234_S2_ADDR_OFFSET             : integer := 8;
    constant CE_DP_SLVS1234_S2_ADDR_WIDTH              : integer := 8;
    constant CE_DP_SLVS1234_S3_ADDR_OFFSET             : integer := 16;
    constant CE_DP_SLVS1234_S3_ADDR_WIDTH              : integer := 8;    
    constant CE_DP_SLVS1234_S1_BEND_OFFSET             : integer := 24;
    constant CE_DP_SLVS1234_S1_BEND_WIDTH              : integer := 4;
    constant CE_DP_SLVS1234_S2_BEND_OFFSET             : integer := 28;
    constant CE_DP_SLVS1234_S2_BEND_WIDTH              : integer := 4;            
    constant CE_DP_SLVS5_SUBSEL                        : integer := 1;
    constant CE_DP_SLVS1234_S3_BEND_OFFSET             : integer := 0;
    constant CE_DP_SLVS1234_S3_BEND_WIDTH              : integer := 4;
    constant CE_DP_SLVS1234_SOF_BIT                    : integer := 4;
    constant CE_DP_SLVS1234_OR254_BIT                  : integer := 5;
    constant CE_DP_SLVS1234_ERROR_BIT                  : integer := 6;     
    constant CE_DP_HIT001_TO_HIT032_SUBSEL            : integer := 2;
    constant CE_DP_HIT033_TO_HIT064_SUBSEL            : integer := 3;
    constant CE_DP_HIT065_TO_HIT096_SUBSEL            : integer := 4;
    constant CE_DP_HIT097_TO_HIT128_SUBSEL            : integer := 5;
    constant CE_DP_HIT129_TO_HIT160_SUBSEL            : integer := 6;
    constant CE_DP_HIT161_TO_HIT192_SUBSEL            : integer := 7;
    constant CE_DP_HIT193_TO_HIT224_SUBSEL            : integer := 8;
    constant CE_DP_HIT225_TO_HIT254_SUBSEL            : integer := 9;
    constant CE_NREGS                                 : integer := 10;
    
    --=====================--        
    -- cbc data processor
    --=====================--        
    constant CDP_SEL                                  : integer := 16#50#;
    constant CDP_LAT_SEL                              : integer := 0;
    constant CDP_LAT_L1A_OFFSET                       : integer := 0;
    constant CDP_LAT_L1A_WIDTH                        : integer := 9;
    constant CDP_LAT_TRIG_DATA_OFFSET                 : integer := 9;
    constant CDP_LAT_TRIG_DATA_WIDTH                  : integer := 9;    
    constant CDP_NREGS                                : integer := 1;
    
    constant MMCME2_DRP_SEL                             : integer := 16#60#;
    constant MMCME2_DRP_IPB_CTRL_EN_BIT                 : integer := 0;
    constant MMCME2_DRP_SADDR_OFFSET                    : integer := 1;
    constant MMCME2_DRP_SADDR_WIDTH                     : integer := 2;


    constant IO_SEL                                    : integer := 16#70#;
    constant IO_DCTT_SUBSEL                            : integer := 0;
    constant IO_DCTT_MIN_TUNING_IDELAY_TAP_OFFSET      : integer := 0;
    constant IO_DCTT_MIN_TUNING_IDELAY_TAP_WIDTH       : integer := 5;    
    constant IO_DCTT_MAX_TUNING_IDELAY_TAP_OFFSET      : integer := 5;
    constant IO_DCTT_MAX_TUNING_IDELAY_TAP_WIDTH       : integer := 5;        
    constant IO_DCTT_IDELAY_OFFSET_OFFSET              : integer := 10;
    constant IO_DCTT_IDELAY_OFFSET_WIDTH               : integer := 5;
    constant IO_DCTT_FE_SUBSEL                         : integer := 1;
	constant IO_TRIG_SUBSEL                            : integer := 10;
	constant IO_TRIG_EXT_ASYNC_L1A_POL_BIT             : integer := 0;
        
    constant TEST_SEL                                 : integer := 16#FF#;
    
    signal cbc_cnfg_set                               : cbc_cnfg_set_type;
    signal fe_cnfg_set                                : fe_cnfg_set_type;
begin
    --=============================--
    sel <= to_integer(unsigned(ipb_mosi_i.ipb_addr(addr_width-1 downto 0))) when addr_width>0 else 0;
    --=============================--
    
    --=============================--
    process(reset, clk)
    --=============================--
    variable timer : natural;
    variable wsel  : integer range 0 to 255;
    variable sel_offset : integer range 0 to 15;
    begin
 
        if reset='1' then
        
            regs   <= (others=> (others=>'0'));
            --================--
            -- global settings
            --================--            
            -- be
            regs(GLOBAL_BE_CNFG_SEL)(GLOBAL_BE_ID_OFFSET + GLOBAL_BE_ID_WIDTH - 1 downto GLOBAL_BE_ID_OFFSET) <= std_logic_vector(to_unsigned(1,GLOBAL_BE_ID_WIDTH));
            -- cbc_id = 1
            sel_offset := 0;
            regs(GLOBAL_CBC_CNFG_SEL+0)(GLOBAL_CBC_ACTIVE_BIT) <= '1';
            regs(GLOBAL_CBC_CNFG_SEL+0)(GLOBAL_CBC_ID_OFFSET         + GLOBAL_CBC_ID_WIDTH         - 1 downto GLOBAL_CBC_ID_OFFSET)         <= "00001";
            regs(GLOBAL_CBC_CNFG_SEL+0)(GLOBAL_CBC_FE_ID_OFFSET      + GLOBAL_CBC_FE_ID_WIDTH      - 1 downto GLOBAL_CBC_FE_ID_OFFSET)      <= "001";          
            regs(GLOBAL_CBC_CNFG_SEL+0)(GLOBAL_CBC_I2CADDRESS_OFFSET + GLOBAL_CBC_I2CADDRESS_WIDTH - 1 downto GLOBAL_CBC_I2CADDRESS_OFFSET) <=  "1000001";
            
            regs(GLOBAL_CBC_CNFG_SEL+1)(GLOBAL_CBC_ACTIVE_BIT) <= '1';
            regs(GLOBAL_CBC_CNFG_SEL+1)(GLOBAL_CBC_ID_OFFSET         + GLOBAL_CBC_ID_WIDTH         - 1 downto GLOBAL_CBC_ID_OFFSET)         <= "00010";
            regs(GLOBAL_CBC_CNFG_SEL+1)(GLOBAL_CBC_FE_ID_OFFSET      + GLOBAL_CBC_FE_ID_WIDTH      - 1 downto GLOBAL_CBC_FE_ID_OFFSET)      <= "001";          
            regs(GLOBAL_CBC_CNFG_SEL+1)(GLOBAL_CBC_I2CADDRESS_OFFSET + GLOBAL_CBC_I2CADDRESS_WIDTH - 1 downto GLOBAL_CBC_I2CADDRESS_OFFSET) <=  "1000010";            

            regs(GLOBAL_CBC_CNFG_SEL+2)(GLOBAL_CBC_ACTIVE_BIT) <= '1';
            regs(GLOBAL_CBC_CNFG_SEL+2)(GLOBAL_CBC_ID_OFFSET         + GLOBAL_CBC_ID_WIDTH         - 1 downto GLOBAL_CBC_ID_OFFSET)         <= "00011";
            regs(GLOBAL_CBC_CNFG_SEL+2)(GLOBAL_CBC_FE_ID_OFFSET      + GLOBAL_CBC_FE_ID_WIDTH      - 1 downto GLOBAL_CBC_FE_ID_OFFSET)      <= "001";          
            regs(GLOBAL_CBC_CNFG_SEL+2)(GLOBAL_CBC_I2CADDRESS_OFFSET + GLOBAL_CBC_I2CADDRESS_WIDTH - 1 downto GLOBAL_CBC_I2CADDRESS_OFFSET) <=  "1000011";                      

            regs(GLOBAL_CBC_CNFG_SEL+3)(GLOBAL_CBC_ACTIVE_BIT) <= '1';
            regs(GLOBAL_CBC_CNFG_SEL+3)(GLOBAL_CBC_ID_OFFSET         + GLOBAL_CBC_ID_WIDTH         - 1 downto GLOBAL_CBC_ID_OFFSET)         <= "00100";
            regs(GLOBAL_CBC_CNFG_SEL+3)(GLOBAL_CBC_FE_ID_OFFSET      + GLOBAL_CBC_FE_ID_WIDTH      - 1 downto GLOBAL_CBC_FE_ID_OFFSET)      <= "001";          
            regs(GLOBAL_CBC_CNFG_SEL+3)(GLOBAL_CBC_I2CADDRESS_OFFSET + GLOBAL_CBC_I2CADDRESS_WIDTH - 1 downto GLOBAL_CBC_I2CADDRESS_OFFSET) <=  "1000100";                      

            regs(GLOBAL_CBC_CNFG_SEL+4)(GLOBAL_CBC_ACTIVE_BIT) <= '1';
            regs(GLOBAL_CBC_CNFG_SEL+4)(GLOBAL_CBC_ID_OFFSET         + GLOBAL_CBC_ID_WIDTH         - 1 downto GLOBAL_CBC_ID_OFFSET)         <= "00101";
            regs(GLOBAL_CBC_CNFG_SEL+4)(GLOBAL_CBC_FE_ID_OFFSET      + GLOBAL_CBC_FE_ID_WIDTH      - 1 downto GLOBAL_CBC_FE_ID_OFFSET)      <= "001";          
            regs(GLOBAL_CBC_CNFG_SEL+4)(GLOBAL_CBC_I2CADDRESS_OFFSET + GLOBAL_CBC_I2CADDRESS_WIDTH - 1 downto GLOBAL_CBC_I2CADDRESS_OFFSET) <=  "1000101";
            
            regs(GLOBAL_CBC_CNFG_SEL+5)(GLOBAL_CBC_ACTIVE_BIT) <= '1';
            regs(GLOBAL_CBC_CNFG_SEL+5)(GLOBAL_CBC_ID_OFFSET         + GLOBAL_CBC_ID_WIDTH         - 1 downto GLOBAL_CBC_ID_OFFSET)         <= "00110";
            regs(GLOBAL_CBC_CNFG_SEL+5)(GLOBAL_CBC_FE_ID_OFFSET      + GLOBAL_CBC_FE_ID_WIDTH      - 1 downto GLOBAL_CBC_FE_ID_OFFSET)      <= "001";          
            regs(GLOBAL_CBC_CNFG_SEL+5)(GLOBAL_CBC_I2CADDRESS_OFFSET + GLOBAL_CBC_I2CADDRESS_WIDTH - 1 downto GLOBAL_CBC_I2CADDRESS_OFFSET) <=  "1000110";            

            regs(GLOBAL_CBC_CNFG_SEL+6)(GLOBAL_CBC_ACTIVE_BIT) <= '1';
            regs(GLOBAL_CBC_CNFG_SEL+6)(GLOBAL_CBC_ID_OFFSET         + GLOBAL_CBC_ID_WIDTH         - 1 downto GLOBAL_CBC_ID_OFFSET)         <= "00111";
            regs(GLOBAL_CBC_CNFG_SEL+6)(GLOBAL_CBC_FE_ID_OFFSET      + GLOBAL_CBC_FE_ID_WIDTH      - 1 downto GLOBAL_CBC_FE_ID_OFFSET)      <= "001";          
            regs(GLOBAL_CBC_CNFG_SEL+6)(GLOBAL_CBC_I2CADDRESS_OFFSET + GLOBAL_CBC_I2CADDRESS_WIDTH - 1 downto GLOBAL_CBC_I2CADDRESS_OFFSET) <=  "1000111";                      

            regs(GLOBAL_CBC_CNFG_SEL+7)(GLOBAL_CBC_ACTIVE_BIT) <= '1';
            regs(GLOBAL_CBC_CNFG_SEL+7)(GLOBAL_CBC_ID_OFFSET         + GLOBAL_CBC_ID_WIDTH         - 1 downto GLOBAL_CBC_ID_OFFSET)         <= "01000";
            regs(GLOBAL_CBC_CNFG_SEL+7)(GLOBAL_CBC_FE_ID_OFFSET      + GLOBAL_CBC_FE_ID_WIDTH      - 1 downto GLOBAL_CBC_FE_ID_OFFSET)      <= "001";          
            regs(GLOBAL_CBC_CNFG_SEL+7)(GLOBAL_CBC_I2CADDRESS_OFFSET + GLOBAL_CBC_I2CADDRESS_WIDTH - 1 downto GLOBAL_CBC_I2CADDRESS_OFFSET) <=  "1001000";                      
                        
             regs(IO_SEL+IO_TRIG_SUBSEL)(IO_TRIG_EXT_ASYNC_L1A_POL_BIT) <= '1';            
            -- misc.  
            -- 0: veto signal is sent to the fast command manager.
            -- 1: veto signal is sent to external port.
            regs(GLOBAL_MISC_SEL)(GLOBAL_MISC_TRIGGER_MASTER_EXTERNAL_BIT) <= '0';
            --================--
            -- clock system settings
            --================--        
            regs(CSC_SEL)(CSC_TYPE_OFFSET + CSC_TYPE_WIDTH - 1 downto CSC_TYPE_OFFSET)                        <= CLK_TYPE_INTCLK;
            --================--
            -- fast command manager settings
            --================--        
            regs(FCM_SEL)        <= x"0000000C"; 
            regs(FCM_FSG_EN_SEL) <= x"0000000F";

            --================--
            -- cbc i2c bus manager settings
            --================--        
            regs(CIBM_SEL)(CIBM_IPBUS_EN_BIT)   <= '1';
            --================--
            -- fscbram_controller settings
            --================--                    
            regs(FSCBC_SEL)(FSCBC_IPBUS_EN_BIT) <= '1';

            --================--
            -- data buffer settings
            --================--        
            regs(DB_SEL+DB_SAFE_NWORD_FREE_SUBSEL)(DATA_BUFFER_ADDR_WIDTH downto 0) <= std_logic_vector(to_unsigned(DEFAULT_SAFE_NWORD_FREE, DATA_BUFFER_ADDR_WIDTH + 1));

            --================--
            -- mmcme2_drp settings
            --================--  
            -- ipbus control is disabled by default.                  
            regs(MMCME2_DRP_SEL)(MMCME2_DRP_IPB_CTRL_EN_BIT) <= '0';
 
             --================--
            -- data clock timing tuning settings
            --================--  
            regs(IO_SEL+IO_DCTT_SUBSEL)(IO_DCTT_MIN_TUNING_IDELAY_TAP_OFFSET+IO_DCTT_MIN_TUNING_IDELAY_TAP_WIDTH-1 downto IO_DCTT_MIN_TUNING_IDELAY_TAP_OFFSET) <= "00101";
            regs(IO_SEL+IO_DCTT_SUBSEL)(IO_DCTT_MAX_TUNING_IDELAY_TAP_OFFSET+IO_DCTT_MAX_TUNING_IDELAY_TAP_WIDTH-1 downto IO_DCTT_MAX_TUNING_IDELAY_TAP_OFFSET) <= "11000";
            regs(IO_SEL+IO_DCTT_SUBSEL)(IO_DCTT_IDELAY_OFFSET_OFFSET+IO_DCTT_IDELAY_OFFSET_WIDTH-1 downto IO_DCTT_IDELAY_OFFSET_OFFSET)                         <= "00010";
          
           --================--
           -- test out setting
           --================--              
            regs(GLOBAL_TEST_OUT_SEL) <= x"00080007";
          
            ack   <= '0';
            timer    := 0;
            wsel     := 0;
           
        elsif rising_edge(clk) then
        
          -- write
          if ipb_mosi_i.ipb_strobe='1' and ipb_mosi_i.ipb_write='1' then
            regs(sel) <= ipb_mosi_i.ipb_wdata;
          end if;
        
          -- read 
          ipb_miso_o.ipb_rdata <= regs(sel);
          -- ack
          ack <= ipb_mosi_i.ipb_strobe and not ack;

          regs(TEST_SEL) <= fe_cnfg_set(0).n_active_cbc & fe_cnfg_set(0).id  & "000" & fe_cnfg_set(0).active_cbcs(0).id & x"00" & '0' & fe_cnfg_set(0).active_cbcs(0).i2c_address;
        
        end if;
        
    end process;
    
    ipb_miso_o.ipb_ack <= ack;
    ipb_miso_o.ipb_err <= '0';
    
    -- config signal assignment 
    cbc_system_cnfg.global.be_id <= regs(GLOBAL_BE_CNFG_SEL)(GLOBAL_BE_ID_OFFSET + GLOBAL_BE_ID_WIDTH - 1 downto GLOBAL_BE_ID_OFFSET);
    cbc_system_cnfg.global.n_active_cbc <= std_logic_vector(n_active_cbc(cbc_cnfg_set));
    cbc_system_cnfg.global.fes <= fe_cnfg_set;
    cbc_system_cnfg.global.trigger_master_external <= regs(GLOBAL_MISC_SEL)(GLOBAL_MISC_TRIGGER_MASTER_EXTERNAL_BIT);
    cbc_system_cnfg.global.test_out_sel0 <= regs(GLOBAL_TEST_OUT_SEL)(15 downto  0);
    cbc_system_cnfg.global.test_out_sel1 <= regs(GLOBAL_TEST_OUT_SEL)(31 downto 16);

        
    fe_cnfg_set <= mk_fe_cnfg_set(cbc_cnfg_set);
        
    g_cbc_cnfg_sig_gen :
    for i in 0 to NCBC-1 generate
        cbc_cnfg_set(i).active      <= regs(GLOBAL_CBC_CNFG_SEL + i)(GLOBAL_CBC_ACTIVE_BIT);
        cbc_cnfg_set(i).id          <= regs(GLOBAL_CBC_CNFG_SEL + i)(GLOBAL_CBC_ID_OFFSET         + GLOBAL_CBC_ID_WIDTH         - 1 downto GLOBAL_CBC_ID_OFFSET);
        cbc_cnfg_set(i).fe_id       <= regs(GLOBAL_CBC_CNFG_SEL + i)(GLOBAL_CBC_FE_ID_OFFSET      + GLOBAL_CBC_FE_ID_WIDTH      - 1 downto GLOBAL_CBC_FE_ID_OFFSET);
        cbc_cnfg_set(i).i2c_address <= regs(GLOBAL_CBC_CNFG_SEL + i)(GLOBAL_CBC_I2CADDRESS_OFFSET + GLOBAL_CBC_I2CADDRESS_WIDTH - 1 downto GLOBAL_CBC_I2CADDRESS_OFFSET);
    end generate;
    cbc_system_cnfg.global.cbcs <= cbc_cnfg_set;
    
    cbc_system_cnfg.cbc_system_clk <= 
    extclk_fmcl8_clk_a when regs(CSC_SEL)(CSC_TYPE_OFFSET + CSC_TYPE_WIDTH - 1 downto CSC_TYPE_OFFSET) = CLK_TYPE_EXT_CLK_A else
    intclk;

    cbc_system_cnfg.fast_signal_manager.ext_async_l1a_en <= regs(FCM_SEL)(FCM_FS_EN_EXT_ASYNC_L1A_BIT);
    cbc_system_cnfg.fast_signal_manager.fast_signal_fmc_en <= regs(FCM_SEL)(FCM_FS_EN_FMC_BIT);
    cbc_system_cnfg.fast_signal_manager.fast_signal_ipbus_en <= regs(FCM_SEL)(FCM_FS_EN_IPBUS_BIT);
    cbc_system_cnfg.fast_signal_manager.fast_signal_internal_en <= regs(FCM_SEL)(FCM_FS_EN_INTERNAL_BIT);

    cbc_system_cnfg.fast_signal_manager.fast_signal_generator.fast_reset_en  <= regs(FCM_FSG_EN_SEL)(FCM_FSG_EN_FAST_RESET_BIT);
    cbc_system_cnfg.fast_signal_manager.fast_signal_generator.trigger_en     <= regs(FCM_FSG_EN_SEL)(FCM_FSG_EN_TRIGGER_BIT);
    cbc_system_cnfg.fast_signal_manager.fast_signal_generator.test_pulse_en  <= regs(FCM_FSG_EN_SEL)(FCM_FSG_EN_TEST_PULSE_BIT);    
    cbc_system_cnfg.fast_signal_manager.fast_signal_generator.orbit_reset_en <= regs(FCM_FSG_EN_SEL)(FCM_FSG_EN_ORBIT_RESET_BIT);    
    cbc_system_cnfg.fast_signal_manager.fast_signal_generator.Ncycle         <= regs(FCM_FSG_NCYCLE_SEL);
    cbc_system_cnfg.fast_signal_manager.fast_signal_generator.cycle_T        <= regs(FCM_FSG_CYCLE_PERIOD_SEL);
    cbc_system_cnfg.fast_signal_manager.fast_signal_generator.trigger_t      <= regs(FCM_FSG_TRIG_TIMING_SEL);
    cbc_system_cnfg.fast_signal_manager.fast_signal_generator.test_pulse_t   <= regs(FCM_FSG_TPREQ_TIMING_SEL);
    cbc_system_cnfg.fast_signal_manager.fast_signal_generator.orbit_reset_t  <= regs(FCM_FSG_OR_TIMING_SEL);

    cbc_system_cnfg.cbc_i2c_bus_managers.ipbus_en <= regs(CIBM_SEL)(CIBM_IPBUS_EN_BIT);

	cbc_system_cnfg.rdb_controller.cbc_id           <= regs(RDB_CONTROLLER_SEL)(RDB_CONTROLLER_CBC_ID_OFFSET+RDB_CONTROLLER_CBC_ID_WIDTH-1 downto RDB_CONTROLLER_CBC_ID_OFFSET);
	cbc_system_cnfg.rdb_controller.latency          <= regs(RDB_CONTROLLER_SEL)(RDB_CONTROLLER_LATENCY_OFFSET+RDB_CONTROLLER_LATENCY_WIDTH-1 downto RDB_CONTROLLER_LATENCY_OFFSET);
	cbc_system_cnfg.rdb_controller.write_block_size <= regs(RDB_CONTROLLER_SEL)(RDB_CONTROLLER_WRITE_BLOCK_SIZE_OFFSET+RDB_CONTROLLER_WRITE_BLOCK_SIZE_WIDTH-1 downto RDB_CONTROLLER_WRITE_BLOCK_SIZE_OFFSET);

    cbc_system_cnfg.fscbram_controller.ipbus_en   <= regs(FSCBC_SEL)(FSCBC_IPBUS_EN_BIT);

    cbc_system_cnfg.data_buffer.safe_n_word_free <= regs(DB_SEL+DB_SAFE_NWORD_FREE_SUBSEL)(DATA_BUFFER_ADDR_WIDTH downto 0);

    ce_sig_gen :
    for i in 0 to 0 generate    
        cbc_system_cnfg.cbc_emulators(i).data_pattern.s1_addr <= regs(CE_SEL + i * CE_NREGS + CE_DP_SLVS1234_SUBSEL)(CE_DP_SLVS1234_S1_ADDR_OFFSET + CE_DP_SLVS1234_S1_ADDR_WIDTH - 1 downto CE_DP_SLVS1234_S1_ADDR_OFFSET);
        cbc_system_cnfg.cbc_emulators(i).data_pattern.s2_addr <= regs(CE_SEL + i * CE_NREGS + CE_DP_SLVS1234_SUBSEL)(CE_DP_SLVS1234_S2_ADDR_OFFSET + CE_DP_SLVS1234_S2_ADDR_WIDTH - 1 downto CE_DP_SLVS1234_S2_ADDR_OFFSET);
        cbc_system_cnfg.cbc_emulators(i).data_pattern.s3_addr <= regs(CE_SEL + i * CE_NREGS + CE_DP_SLVS1234_SUBSEL)(CE_DP_SLVS1234_S3_ADDR_OFFSET + CE_DP_SLVS1234_S3_ADDR_WIDTH - 1 downto CE_DP_SLVS1234_S3_ADDR_OFFSET);     
        cbc_system_cnfg.cbc_emulators(i).data_pattern.s1_bend <= regs(CE_SEL + i * CE_NREGS + CE_DP_SLVS1234_SUBSEL)(CE_DP_SLVS1234_S1_BEND_OFFSET + CE_DP_SLVS1234_S1_BEND_WIDTH - 1 downto CE_DP_SLVS1234_S1_BEND_OFFSET);
        cbc_system_cnfg.cbc_emulators(i).data_pattern.s2_bend <= regs(CE_SEL + i * CE_NREGS + CE_DP_SLVS1234_SUBSEL)(CE_DP_SLVS1234_S2_BEND_OFFSET + CE_DP_SLVS1234_S2_BEND_WIDTH - 1 downto CE_DP_SLVS1234_S2_BEND_OFFSET);
        cbc_system_cnfg.cbc_emulators(i).data_pattern.s3_bend <= regs(CE_SEL + i * CE_NREGS + CE_DP_SLVS5_SUBSEL)(CE_DP_SLVS1234_S3_BEND_OFFSET + CE_DP_SLVS1234_S3_BEND_WIDTH - 1 downto CE_DP_SLVS1234_S3_BEND_OFFSET); 
        cbc_system_cnfg.cbc_emulators(i).data_pattern.sof     <= regs(CE_SEL + i * CE_NREGS + CE_DP_SLVS5_SUBSEL)(CE_DP_SLVS1234_SOF_BIT); 
        cbc_system_cnfg.cbc_emulators(i).data_pattern.or254   <= regs(CE_SEL + i * CE_NREGS + CE_DP_SLVS5_SUBSEL)(CE_DP_SLVS1234_OR254_BIT); 
        cbc_system_cnfg.cbc_emulators(i).data_pattern.error   <= regs(CE_SEL + i * CE_NREGS + CE_DP_SLVS5_SUBSEL)(CE_DP_SLVS1234_ERROR_BIT); 
        cbc_system_cnfg.cbc_emulators(i).data_pattern.hit254(032 downto 001) <= regs(CE_SEL + i * CE_NREGS + CE_DP_HIT001_TO_HIT032_SUBSEL);
        cbc_system_cnfg.cbc_emulators(i).data_pattern.hit254(064 downto 033) <= regs(CE_SEL + i * CE_NREGS + CE_DP_HIT033_TO_HIT064_SUBSEL);    
        cbc_system_cnfg.cbc_emulators(i).data_pattern.hit254(096 downto 065) <= regs(CE_SEL + i * CE_NREGS + CE_DP_HIT065_TO_HIT096_SUBSEL);
        cbc_system_cnfg.cbc_emulators(i).data_pattern.hit254(128 downto 097) <= regs(CE_SEL + i * CE_NREGS + CE_DP_HIT097_TO_HIT128_SUBSEL);
        cbc_system_cnfg.cbc_emulators(i).data_pattern.hit254(160 downto 129) <= regs(CE_SEL + i * CE_NREGS + CE_DP_HIT129_TO_HIT160_SUBSEL);
        cbc_system_cnfg.cbc_emulators(i).data_pattern.hit254(192 downto 161) <= regs(CE_SEL + i * CE_NREGS + CE_DP_HIT161_TO_HIT192_SUBSEL);
        cbc_system_cnfg.cbc_emulators(i).data_pattern.hit254(224 downto 193) <= regs(CE_SEL + i * CE_NREGS + CE_DP_HIT193_TO_HIT224_SUBSEL);
        cbc_system_cnfg.cbc_emulators(i).data_pattern.hit254(254 downto 225) <= regs(CE_SEL + i * CE_NREGS + CE_DP_HIT225_TO_HIT254_SUBSEL)(29 downto 0);        
    end generate;
         
    cdp_sig_gen :
    for i in 0 to NCBC-1 generate
       cbc_system_cnfg.cbc_data_processors(i).l1a_latency <= regs(CDP_SEL + i * CDP_NREGS + CDP_LAT_SEL)(CDP_LAT_L1A_OFFSET + CDP_LAT_L1A_WIDTH - 1 downto CDP_LAT_L1A_OFFSET);
       cbc_system_cnfg.cbc_data_processors(i).trig_data_latency <= regs(CDP_SEL + i * CDP_NREGS + CDP_LAT_SEL)(CDP_LAT_TRIG_DATA_OFFSET + CDP_LAT_TRIG_DATA_WIDTH - 1 downto CDP_LAT_TRIG_DATA_OFFSET);
    end generate;
    cbc_system_cnfg.mmcme2_drp.ipb_ctrl_en <= regs(MMCME2_DRP_SEL)(MMCME2_DRP_IPB_CTRL_EN_BIT);
    cbc_system_cnfg.mmcme2_drp.saddr       <= regs(MMCME2_DRP_SEL)(MMCME2_DRP_SADDR_OFFSET + MMCME2_DRP_SADDR_WIDTH - 1 downto MMCME2_DRP_SADDR_OFFSET);

    cbc_system_cnfg.io.data_clock_timing_tuning.min_tuning_idelay_tap <= regs(IO_SEL+IO_DCTT_SUBSEL)(IO_DCTT_MIN_TUNING_IDELAY_TAP_OFFSET+IO_DCTT_MIN_TUNING_IDELAY_TAP_WIDTH-1 downto IO_DCTT_MIN_TUNING_IDELAY_TAP_OFFSET);
    cbc_system_cnfg.io.data_clock_timing_tuning.max_tuning_idelay_tap <= regs(IO_SEL+IO_DCTT_SUBSEL)(IO_DCTT_MAX_TUNING_IDELAY_TAP_OFFSET+IO_DCTT_MAX_TUNING_IDELAY_TAP_WIDTH-1 downto IO_DCTT_MAX_TUNING_IDELAY_TAP_OFFSET);
    cbc_system_cnfg.io.data_clock_timing_tuning.idelay_offset         <= regs(IO_SEL+IO_DCTT_SUBSEL)(IO_DCTT_IDELAY_OFFSET_OFFSET+IO_DCTT_IDELAY_OFFSET_WIDTH-1 downto IO_DCTT_IDELAY_OFFSET_OFFSET);
    cbc_system_cnfg.io.ext_async_l1a_pol                              <= regs(IO_SEL+IO_TRIG_SUBSEL)(IO_TRIG_EXT_ASYNC_L1A_POL_BIT);
    
    dctt_fe_gen:
    for j in 0 to NFE-1 generate
        cbc_system_cnfg.io.fes(j).clock_timing_tuning_cbc_sel <= regs(IO_SEL+IO_DCTT_FE_SUBSEL+j)(CBCID_WIDTH-1 downto 0);
    end generate;
end Behavioral;
