----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 13.07.2017 12:46:52
-- Design Name: 
-- Module Name: rdb_controller - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

use  work.cbc_system_package.all;

entity rdb_controller is
Port ( 
	reset				: in	std_logic;	
	clk_40MHz			: in	std_logic;	
	rd_clk				: in	std_logic;	
	trigger				: in	std_logic;	
	read_next			: in	std_logic;	
	ipb_cnfg			: in	rdb_controller_cnfg_type;
	cbc_par_data        : in    cbc_par_data_type;	
	rdb_we				: out	std_logic;	
	rdb_waddr_o			: out	std_logic_vector(14 downto 0);	
	rdb_din				: out	std_logic_vector(63 downto 0);	
	rdb_raddr			: out	std_logic_vector(15 downto 0);	
	write_ready 		: out	std_logic;
	ipb_stat			: out	rdb_controller_stat_type
);
end rdb_controller;

architecture Behavioral of rdb_controller is

	signal write_fsm  : rdb_controller_write_fsm_type;
	signal read_done  : boolean;
	signal nrdata     : unsigned(16 downto 0);
	signal read_ready : std_logic;
	signal rdb_waddr  : std_logic_vector(14 downto 0);	
begin
	rdb_waddr_o <= rdb_waddr;
	ipb_stat.write_fsm <= write_fsm;
	ipb_stat.read_ready <= read_ready;
	ipb_stat.waddr <= rdb_waddr;
	
	process (clk_40MHz, reset)								
		variable write_flag       : boolean;					
		variable wait_frame_count : integer range 0 to 255;			
		variable waddr_int        : integer range 0 to 19457;								
	begin								
		if reset = '1' then												
			waddr_int   := 0;
			rdb_waddr <= (others => '0');							
			read_ready  <= '0';							
			write_ready <= '1';	
			write_flag := false;
			write_fsm <= write_process;	
			wait_frame_count := 0;									
		elsif rising_edge( clk_40MHz) then								

		    rdb_we <= '0';
										
			case write_fsm is
											
			when write_process  =>								
											
							
											
				if wait_frame_count /= 0 then							
					if wait_frame_count = to_integer(unsigned(ipb_cnfg.latency)) then						
						write_flag       := true;					
						wait_frame_count := 0;					
					end if;						
					wait_frame_count := wait_frame_count + 1;						
				end if;							
											
				if trigger = '1'  then							
					if ipb_cnfg.latency = x"00" then						
						write_flag := true;					
					else						
						wait_frame_count := 1;					
					end if;						
					write_ready <= '0';						
				end if;							
												
				if write_flag then							
					rdb_we    <= '1';						
					rdb_din   <= x"0000" & cbc_par_data(6) & cbc_par_data(5)& cbc_par_data(4)& cbc_par_data(3)& cbc_par_data(2)& cbc_par_data(1);						
--					rdb_din   <= '0' & std_logic_vector(to_unsigned(waddr_int, 15)) & '0' & std_logic_vector(to_unsigned(waddr_int, 15)) & x"00000000";
					rdb_waddr <= std_logic_vector(to_unsigned(waddr_int, 15));						
					waddr_int := waddr_int + 1;						
				end if;							

				if write_flag and waddr_int = to_integer(unsigned(ipb_cnfg.write_block_size)) then							
					write_flag := false;								
					write_fsm  <= waiting_read_done;										
				end if;	
			
			when waiting_read_done =>								
											
				if read_done then							
					write_ready <= '1';						
					waddr_int   := 0;		
					rdb_waddr <= (others => '0');						
					read_ready  <= '0';
					write_fsm   <= write_process;														
				else
					read_ready <= '1';		
				end if;							
			end case;
		end if;
	end process;
										
	--=============================--								
	process(reset, rd_clk)								
	--=============================--								
	begin								
		if reset='1' then     								
			rdb_raddr  <= (others => '0');								
			nrdata <= (others => '0');	
			read_done <= false;							
		elsif rising_edge(rd_clk) then	
								
			if read_next = '1' then								
				rdb_raddr <= std_logic_vector(nrdata(15 downto 0)+1);								
				nrdata <= nrdata + 1;								
				if nrdata = ( unsigned( ipb_cnfg.write_block_size & '0' ) - 2 ) then							
					read_done  <= true;						
					rdb_raddr  <= (others => '0');						
					nrdata     <= (others => '0');						
				end if;							
			end if;	
			if read_ready = '0' then
				read_done <= false;	
			end if;
		end if;								
	end process;								

	

end Behavioral;
