--===========================--
-- l1a_tdc 
-- the l1a delay would cause the mismatch of the trigger latency set in CBC and the real latency.  To be fixed. 
-- 28.10.2016 Kirika Uchida
--===========================--

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity l1a_tdc is
    Port ( reset          : in  STD_LOGIC;
           clk40          : in  STD_LOGIC;
           clk320         : in  STD_LOGIC;
		   l1a_nosync     : in  STD_LOGIC;
		   l1a_o		  : out STD_LOGIC;
		   l1a_tdccount_o : out STD_LOGIC_VECTOR( 2 downto 0 )
			  );
end l1a_tdc;

architecture Behavioral of l1a_tdc is

--  signal clk40_d                  : std_logic;

	signal clk40_shift_reg 			: std_logic_vector( 2 downto 0 );
	signal flip_40mhz               : std_logic;
	signal clk40_edge             : std_logic;

	signal tdc_count     			: unsigned( 2 downto 0 );
    signal l1a_tdccount             : std_logic_vector(2 downto 0);
    
    signal async_pulse_int: std_logic;	
    signal sync_pulse_pipe: std_logic_vector(3 downto 0);
	signal l1a_edge      			: std_logic;
	signal l1a_level                : std_logic;
	
	signal l1a_ready      	      : std_logic;
	signal l1a							: std_logic;
	
	type fsm_type is ( init, running );
	signal fsm : fsm_type;

    attribute ASYNC_REG                   : string;
    attribute ASYNC_REG of async_stage      : label is "TRUE";

    attribute SHREG_EXTRACT : string;
    attribute SHREG_EXTRACT of ldpe_inst : label is "no";
    attribute SHREG_EXTRACT of async_stage : label is "no";
    
begin

	-- TDC counter
	process ( reset, clk320 )
	begin
	if reset = '1' then
	
		tdc_count <= ( others => '0' );
		clk40_shift_reg 		<= ( others => '0');
		l1a_ready       	<= '0';				
		l1a_tdccount      <= ( others => '0' );
		fsm <= init;
		
	elsif rising_edge( clk320 ) then
	
		tdc_count <= tdc_count + 1;
				
		case fsm is
			when init =>
				clk40_shift_reg <= clk40_shift_reg( 1 downto 0 ) & flip_40mhz;

				if clk40_edge = '1' then
					tdc_count <= "001";
					fsm <= running;
				end if;
			when running =>

				if l1a_edge = '1' then
					l1a_ready    <= '1';
					l1a_tdccount <= std_logic_vector(tdc_count);
--				elsif l1a_level = '1' then
--					l1a_ready <= '1';

				else
					l1a_ready <= '0';
				end if;

		end case;
	end if;
	end process;

   clk40_edge <= not clk40_shift_reg(2) and clk40_shift_reg(1);
   async_pulse_int <= l1a_nosync;
   l1a_edge <= not sync_pulse_pipe(3) and sync_pulse_pipe(2);
   l1a_level <= sync_pulse_pipe(2);
--   l1a_edge <= not sync_pulse_pipe(2) and sync_pulse_pipe(1);
--   l1a_ready can be removed, sync for 25ns and use sync_pulse_pipe(2) and l1a_tdccount for l1a and tdc
   
   LDPE_inst : LDPE
   generic map (
      INIT => '0') -- Initial value of latch ('0' or '1')  
   port map (
      Q => sync_pulse_pipe(0),      -- Data output
      PRE => async_pulse_int,  -- Asynchronous preset/set input
      D => async_pulse_int,      -- Data input
      G => '1',      -- Gate input
      GE => sync_pulse_pipe(3)     -- Gate enable input
   );

    async_stage: FD
        generic map (
                INIT => '0')
        port map (
                C    => clk320,
                D    => sync_pulse_pipe(0),
                Q    => sync_pulse_pipe(1)
        );

   
    fd_gen: for i in 1 to 2 generate
                sync_stage : FD
                generic map (
                  INIT => '0'
                )
                port map (
                  C    => clk320,
                  D    => sync_pulse_pipe(i),
                  Q    => sync_pulse_pipe(i+1)
                );
        end generate;

	
	process ( reset, clk40 )
	begin
	if reset = '1' then	
		l1a         	   <= '0';
        flip_40mhz         <= '0';
	else
	   if rising_edge( clk40 ) then

         flip_40mhz <= not flip_40mhz;

         if l1a_ready = '1' then
            l1a <= '1';
            l1a_tdccount_o <= l1a_tdccount;
         else
            l1a <= '0';
            l1a_tdccount_o <= (others=> '0');
         end if;

	   end if;
	end if;
	end process;

	l1a_o       <= l1a;

end Behavioral;

