--===========================--
-- cbc_i2c_single_transaction 
-- 28.10.2016 Kirika Uchida
--===========================--

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.user_package.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity cbc_i2c_single_transaction is
	generic ( I2C_SCL_PRESCALE : integer := 400
	);
    Port ( 
--				i2c_data_fifo_we_o		: out std_logic;
--				i2c_wdata_req_o			: out std_logic;	 
				clk0					: in  std_logic;
				reset 					: in  STD_LOGIC;
				cbc_i2c_input_ready 	: in  STD_LOGIC;	-- command data is ready in the i2c command fifo.
				cbc_i2c_addr_i 		    : in  STD_LOGIC_VECTOR (6 downto 0);
				cbc_i2c_page_en_i 	    : in  STD_LOGIC;
				cbc_i2c_page_data_i 	: in  STD_LOGIC_VECTOR (7 downto 0);
				cbc_i2c_rw_i 			: in  STD_LOGIC;
				cbc_i2c_regaddr_i 	    : in  STD_LOGIC_VECTOR (7 downto 0);
				cbc_i2c_regdata_i		: in  STD_LOGIC_VECTOR (7 downto 0);
				cbc_i2c_regdata_o		: out STD_LOGIC_VECTOR (7 downto 0);
				done 					: out STD_LOGIC;
				err 					: out STD_LOGIC;
				scl_o 					: out STD_LOGIC;
				sda_i 					: in  STD_LOGIC;
				sda_o 					: out STD_LOGIC
			  );
end cbc_i2c_single_transaction;

architecture Behavioral of cbc_i2c_single_transaction is

    constant I2C_DATA_COUNTER_WIDTH              : positive := 2;
    
    type cbc_i2c_fsm_type is ( idle, page_w, data_rw );
    signal cbc_i2c_fsm		: cbc_i2c_fsm_type;
    
    signal i2c_data_fifo_we					: std_logic;
    signal i2c_wdata_req					: std_logic;
    
    signal cbc_i2c_addr						: std_logic_vector(6 downto 0);
    signal cbc_i2c_page_data				: std_logic_vector(7 downto 0);
    signal cbc_i2c_rw						: std_logic;
    signal cbc_i2c_regaddr					: std_logic_vector(7 downto 0);
    signal cbc_i2c_regdata_in				: std_logic_vector(7 downto 0);
    signal cbc_i2c_regdata_out				: std_logic_vector(7 downto 0);
    
    signal next_i2c_input_ready 			: std_logic;
    signal next_i2c_rw						: std_logic;
    signal next_i2c_ndata          		    : std_logic_vector( I2C_DATA_COUNTER_WIDTH - 1 downto 0 );
    signal next_i2c_data					: std_logic_vector( 7 downto 0 );
    
    signal i2c_data_fifo_din				: std_logic_vector (7 downto 0);
    
    signal i2c_done							: std_logic;
    signal cbc_i2c_err						: std_logic;
    
    signal cbc_i2c_single_trans_done		: std_logic;

begin

	process ( clk0, reset )
		variable i2c_data_fifo_cnt 			: integer;
	begin
	if ( reset = '1' ) then

		i2c_data_fifo_cnt 		:= 0;
		next_i2c_input_ready 	<= '0';
		cbc_i2c_fsm 				<= idle;
		i2c_data_fifo_we 			<= '0';

	elsif rising_edge( clk0 ) then
		i2c_data_fifo_we <= '0';
		case cbc_i2c_fsm is
			when idle =>
				cbc_i2c_single_trans_done <= '0';

				-- latch next command
				if cbc_i2c_input_ready = '1' then
	
					cbc_i2c_addr			<= cbc_i2c_addr_i;
					cbc_i2c_page_data		<= cbc_i2c_page_data_i;
					cbc_i2c_rw				<= cbc_i2c_rw_i;
					cbc_i2c_regaddr		<= cbc_i2c_regaddr_i;
					cbc_i2c_regdata_in	<= cbc_i2c_regdata_i;
					i2c_data_fifo_cnt		:= 0;
					
					if cbc_i2c_page_en_i = '1' then
						cbc_i2c_fsm <= page_w;
					else
						cbc_i2c_fsm <= data_rw;
					end if;
				end if;
			
			when page_w =>

				next_i2c_rw <= '0';
				
				if i2c_data_fifo_cnt = 0 then
					i2c_data_fifo_we <= '1';
					i2c_data_fifo_din <= ( others => '0' );
					i2c_data_fifo_cnt := i2c_data_fifo_cnt + 1;
				elsif i2c_data_fifo_cnt = 1 then
					i2c_data_fifo_we <= '1';
					i2c_data_fifo_din <= cbc_i2c_page_data;
					i2c_data_fifo_cnt := i2c_data_fifo_cnt + 1;
				elsif i2c_data_fifo_cnt = 2 then
					next_i2c_ndata <= std_logic_vector(to_unsigned(i2c_data_fifo_cnt,I2C_DATA_COUNTER_WIDTH));
					next_i2c_input_ready <= '1';
				end if;
			
				if  i2c_done = '1' then
					i2c_data_fifo_cnt := 0;
					next_i2c_input_ready <= '0';
					cbc_i2c_fsm <= data_rw;
				end if;			

			when data_rw =>
				
				if cbc_i2c_rw = '0' then
					next_i2c_rw <= '0';
					if i2c_data_fifo_cnt = 0 then
						i2c_data_fifo_we <= '1';
						i2c_data_fifo_din <= cbc_i2c_regaddr;
						i2c_data_fifo_cnt := i2c_data_fifo_cnt + 1;
					elsif i2c_data_fifo_cnt = 1 then
						i2c_data_fifo_we <= '1';
						i2c_data_fifo_din <= cbc_i2c_regdata_in;
						i2c_data_fifo_cnt := i2c_data_fifo_cnt + 1;
					elsif i2c_data_fifo_cnt = 2 then
						next_i2c_ndata <= std_logic_vector(to_unsigned(i2c_data_fifo_cnt,I2C_DATA_COUNTER_WIDTH));
						next_i2c_input_ready <= '1'; 
					end if;
					
					if  i2c_done = '1' then
						i2c_data_fifo_cnt := 0;
						next_i2c_input_ready <= '0';
						cbc_i2c_single_trans_done <= '1';
						cbc_i2c_fsm <= idle;
					end if;
				else
					
					if i2c_data_fifo_cnt = 0 then
						next_i2c_rw <= '0';
						i2c_data_fifo_we <= '1';
						i2c_data_fifo_din <= cbc_i2c_regaddr;
						i2c_data_fifo_cnt := i2c_data_fifo_cnt + 1;						
					elsif i2c_data_fifo_cnt = 1 then
						next_i2c_ndata <= std_logic_vector(to_unsigned(i2c_data_fifo_cnt,I2C_DATA_COUNTER_WIDTH));
						next_i2c_input_ready <= '1';				
					end if;
					
					if  i2c_done = '1' then
						next_i2c_input_ready <= '0';
						if next_i2c_rw = '1' then
							i2c_data_fifo_cnt := 0;
							cbc_i2c_single_trans_done <= '1';
							cbc_i2c_fsm <= idle;
						else
							next_i2c_rw <= '1';
						end if;
					end if;
				end if;								
			
			when others =>
		end case;
	end if;
	end process;
	
	cbc_i2c_master : entity work.i2c_master_simple
		generic map( PRESCALE => I2C_SCL_PRESCALE, DATA_COUNTER_WIDTH => I2C_DATA_COUNTER_WIDTH )
		Port map( 
			clk		 			=> clk0,
			rst					=> reset,
			input_ready			=> next_i2c_input_ready,
			saddr     			=> cbc_i2c_addr,
			rw			 		=> next_i2c_rw,
			ndata     			=> next_i2c_ndata,
			data_i      		=> next_i2c_data,
			data_o				=> cbc_i2c_regdata_out,
			wdata_req  			=> i2c_wdata_req,
			done_o 				=> i2c_done,
			err       			=> cbc_i2c_err,
			sda_i 				=> sda_i,		  
			sda_o 				=> sda_o,
			scl_o 				=> scl_o
	);
	
	i2c_data_fifo : entity work.small_fifo
	generic map (DATA_WIDTH => 8, FIFO_DEPTH => 2)
    Port map( 	clk 	=> clk0,
                rst 	=> reset,
                we  	=> i2c_data_fifo_we,
                din  	=> i2c_data_fifo_din,
                re 	=> i2c_wdata_req,
                dout	=> next_i2c_data,
                empty	=> open,
                full	=> open,
                werr	=> open,
                rerr	=> open
			  );

	cbc_i2c_regdata_o <= cbc_i2c_regdata_out;
	done 					<= cbc_i2c_single_trans_done;
	err					<= cbc_i2c_err;
	
--	i2c_data_fifo_we_o <= i2c_data_fifo_we;
--	i2c_wdata_req_o    <= i2c_wdata_req;
end Behavioral;

