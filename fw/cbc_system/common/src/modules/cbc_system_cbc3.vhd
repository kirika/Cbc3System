--===========================--
-- cbc_system_single_cbc3 
-- 28.10.2016 Kirika Uchida
--===========================--

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;
use work.ipbus.all;
use work.system_package.all;
--! user packages
use work.user_package.all;
use work.cbc_system_package.all;

entity cbc_system is
port ( 
	fmc_l8_la_p						: inout	std_logic_vector(33 downto 0);
    fmc_l8_la_n                     : inout std_logic_vector(33 downto 0);
    fmc_l12_la_p                    : inout std_logic_vector(33 downto 0);
    fmc_l12_la_n                    : inout std_logic_vector(33 downto 0);
    fmc_l12_clk0                    : in    std_logic;
    fmc_l12_clk1                    : in    std_logic;
    fabric_clk                      : in    std_logic;
    ipb_clk                         : in    std_logic;
	ipb_rst_i				        : in	std_logic;
    ipb_miso_o                      : out   ipb_rbus_array(0 to nbr_usr_slaves-1);
    ipb_mosi_i                      : in    ipb_wbus_array(0 to nbr_usr_slaves-1);
    usrled1_r                       : out   std_logic;
    usrled1_g                       : out   std_logic;
    usrled1_b                       : out   std_logic;
    usrled2_r                       : out   std_logic;
    usrled2_g                       : out   std_logic;
    usrled2_b                       : out   std_logic
);
end cbc_system;

architecture cbc3 of cbc_system is

    -- clocks
    -- external clock
    signal fmc_ext_clk_40MHz              : std_logic;

	signal clk_320MHz                     : std_logic;
    signal clk_40MHz                      : std_logic;
    signal clk_200MHz                     : std_logic;
    signal clk_10MHz                      : std_logic;
    signal clk_40MHz_iserdes_ctrl         : std_logic_vector(NFE-1 downto 0);
    signal clk_320MHz_iserdes_ctrl        : std_logic_vector(NFE-1 downto 0);

	-- signals relevant to mmcms
    signal clk_select                     : std_logic;
   	signal mmcm_clk_in                    : std_logic;
	signal cbc_system_clocks_0_locked     : std_logic;
    signal mmcm_for_iserdes_sig_set       : mmcm_for_iserdes_sig_set_type;
    signal sigs_to_mmcme2_drp_from_io_set : sigs_to_mmcme2_drp_set_type;
    signal sigs_to_mmcme2_drp_set         : sigs_to_mmcme2_drp_set_type;
    signal sigs_from_mmcme2_drp_set       : sigs_from_mmcme2_drp_set_type;
    signal sigs_from_mmcme2_drp_to_io_set : sigs_from_mmcme2_drp_set_type;        
    signal mmcme2_drp_sclk                : std_logic;
   
    -- external input signals
    signal ext_async_l1a                  : std_logic;
    signal cbc_par_data_set               : cbc_par_data_set_type;    

	-- io test out signals
    signal io_test_out                    : std_logic;

    -- fast signals to oserdes 
    signal cbc_fast_signal_par            : std_logic_vector(7 downto 0);  
	signal cbc_fast_signal_40MHz          : std_logic_vector(3 downto 0);
	
    -- trigger signals
    signal l1a_count                      : std_logic_vector(28 downto 0); 
    signal l1a_tdc_count                  : std_logic_vector(2 downto 0);
    signal l1a_320MHz                     : std_logic;      
    signal internal_l1a_veto              : std_logic;
    signal external_l1a_veto              : std_logic;   
    signal l1a_veto_from_db               : std_logic;
    signal l1a_veto_from_db_320MHz        : std_logic;
    
    -- ipbus signals
    signal ipb_cbc_system_stat            : ipb_cbc_system_stat_type;     
    signal ipb_cbc_system_cnfg            : ipb_cbc_system_cnfg_type;
    signal ipb_cbc_system_ctrl            : ipb_cbc_system_ctrl_type;
    signal ipb_test_sig                   : std_logic;
    
    -- data buffer ipb interface
    signal db_to_ipbif                    : db_to_ipbif_type;
    signal ipbif_to_db                    : ipbif_to_db_type;      

    -- signals btw cbc processors & event builder
    signal dsoebi_set                     : dsoebi_set_type;
    signal dsiebo_set                     : dsiebo_set_type;

    -- signals from event builder to data buffer
    signal ebodbi                         : ebodbi_type;
    signal ebidbo                         : ebidbo_type;

    -- free space from data buffer
    signal n_word_free                    : std_logic_vector(DATA_BUFFER_ADDR_WIDTH downto 0);
    signal safe_n_word_free               : std_logic_vector(DATA_BUFFER_ADDR_WIDTH downto 0);

    -- i2c bus manager signals
    signal ci2cbmgr_to_ipbif_set          : ci2cbmgr_out_set_type;
    signal ci2cbmgr_in_set                : ci2cbmgr_in_set_type;
    signal ipbif_to_ci2cbmgr_set          : ci2cbmgr_in_set_type;
    signal fscbram_ctrl_to_ci2cbmgr_set   : ci2cbmgr_in_set_type;
	signal cbc_i2c_bus_miso_set           : cbc_i2c_bus_miso_set_type;
	signal cbc_i2c_bus_mosi_set           : cbc_i2c_bus_mosi_set_type;    
    signal cbci2cbus_waitings             : std_logic_vector( NFE-1 downto 0);
	signal cbci2cbus_readies              : std_logic_vector( NFE-1 downto 0);
    signal cbci2cbus_waiting              : std_logic;
	signal cbci2cbus_ready                : std_logic;    
    signal cibm_test_out                  : std_logic_vector( NFE-1 downto 0);
    
    -- signals relevant to data clock timing scan buffer 
    signal dctsb_dout                     : std_logic_vector(31 downto 0);
    signal dctsb_read_next                : std_logic;
    
    -- signals relevant to fast slow control bram
    signal fscbram_in                     : fscbram_in_type;
    signal ipbif_to_fscbram               : fscbram_in_type;
    signal fscbram_fast_signal_40MHz      : fast_signal_40MHz_in_type;

	-- signals for sync bit generation
    signal flip_40MHz                     : std_logic;
    signal flip_40MHz_1d                  : std_logic;
    signal clk_sync_bit                   : std_logic;

    -- raw data buffer signals
    signal rdb_we                         : std_logic;
    signal rdb_waddr                      : std_logic_vector(14 downto 0);
    signal rdb_din                        : std_logic_vector(63 downto 0);
    signal rdb_raddr                      : std_logic_vector(15 downto 0);
    signal rdb_dout_to_ipbif			  : std_logic_vector(31 downto 0);
    signal ipbif_to_rdb_read_next         : std_logic;
	signal rdb_write_trigger              : std_logic;
	signal rdb_write_ready                : std_logic;	
	signal cbc_par_data_to_rdb_ctrl       : cbc_par_data_type;

    -- test signals to monitor
	signal test_signal_out                : std_logic_vector(1 downto 0);
    
begin

    --===================================--
    -- clocks for cbc_system
    --===================================--

    clk_select <= '0' when ipb_cbc_system_cnfg.cbc_system_clk = intclk else
                  '1';

   fmc_external_clock_inst : if FMC_EXT_CLK = true generate
   BUFGMUX_0_inst : BUFGMUX
      port map (
         O => mmcm_clk_in,   -- 1-bit output: Clock output
         I0 => '0', -- 1-bit input: Clock input (S=0)
         I1 => fmc_ext_clk_40MHz, -- 1-bit input: Clock input (S=1)
         S => clk_select    -- 1-bit input: Clock select
      );
   end generate;
   
   internal_clock_inst : if FMC_EXT_CLK = false generate
   BUFGMUX_0_inst : BUFGMUX
      port map (
         O => mmcm_clk_in,   -- 1-bit output: Clock output
         I0 => fabric_clk, -- 1-bit input: Clock input (S=0)
         I1 => '0', -- 1-bit input: Clock input (S=1)
         S => clk_select    -- 1-bit input: Clock select
      );
   end generate;   


    ipb_cbc_system_stat.cbc_system_clocks_0.locked <= cbc_system_clocks_0_locked; 
    cbc_system_clocks_0_inst : entity work.cbc_system_clocks_0
       port map (    
       -- Clock in ports
       clk_in1 => mmcm_clk_in,
      -- Clock out ports  
       clk_out1 => clk_40MHz,
       clk_out2 => clk_320MHz,
       clk_out3 => clk_200MHz,
       clk_out4 => clk_10MHz,
      -- Status and control signals
      reset => '0',                
--       reset => ipb_cbc_system_ctrl.global.reset,
       locked => cbc_system_clocks_0_locked
     );
   
    cbc_system_clocks_1_gen :
    for i in 0 to NFE-1 generate

        ipb_cbc_system_stat.cbc_system_clocks_1_set(i).locked <= mmcm_for_iserdes_sig_set(i).locked;


        cbc_system_clocks_1_inst: entity work.cbc_system_clocks_1
        port map
         (-- Clock in ports
          clk_in1          => mmcm_clk_in,
          -- Clock out ports
          clk_out1         => clk_40MHz_iserdes_ctrl(i),
          clk_out2         => clk_320MHz_iserdes_ctrl(i),
          -- Dynamic phase shift ports
          daddr            => mmcm_for_iserdes_sig_set(i).daddr,
          dclk             => mmcm_for_iserdes_sig_set(i).dclk,
          den              => mmcm_for_iserdes_sig_set(i).den,
          din              => mmcm_for_iserdes_sig_set(i).din,
          dout             => mmcm_for_iserdes_sig_set(i).dout,
          dwe              => mmcm_for_iserdes_sig_set(i).dwe,
          drdy             => mmcm_for_iserdes_sig_set(i).drdy,
          -- Status and control signals
          reset            => mmcm_for_iserdes_sig_set(i).rst_mmcm,
          locked           => mmcm_for_iserdes_sig_set(i).locked
         );

        mmcme2_drp_for_cbc_system_inst : entity work.mmcme2_drp_for_cbc_system
        port map(
        SADDR    => sigs_to_mmcme2_drp_set(i).saddr,
        SEN      => sigs_to_mmcme2_drp_set(i).sen,
        SCLK     => sigs_to_mmcme2_drp_set(i).sclk,
        --    RST      => mmcme2_drp_sigs_in.rst or ipb_cbc_system_ctrl.global.reset,
        RST      => sigs_to_mmcme2_drp_set(i).rst,
        SRDY_O   => sigs_from_mmcme2_drp_set(i).srdy,
        -- These signals are to be connected to the MMCM_ADV by port name.
        -- Their use matches the MMCM port description in the Device User Guide.
        DO       => mmcm_for_iserdes_sig_set(i).dout,
        DRDY     => mmcm_for_iserdes_sig_set(i).drdy,
        LOCKED   => mmcm_for_iserdes_sig_set(i).locked,
        DWE      => mmcm_for_iserdes_sig_set(i).dwe,
        DEN      => mmcm_for_iserdes_sig_set(i).den,
        DADDR    => mmcm_for_iserdes_sig_set(i).daddr,
        DI       => mmcm_for_iserdes_sig_set(i).din,
        DCLK     => mmcm_for_iserdes_sig_set(i).dclk,
        RST_MMCM => mmcm_for_iserdes_sig_set(i).rst_mmcm,
        SADDR_O  => sigs_from_mmcme2_drp_set(i).saddr
        );

--    mmcme2_drp_sclk <= mmcm_clk_in1;


        sigs_to_mmcme2_drp_set(i).sclk  <= mmcme2_drp_sclk when ipb_cbc_system_cnfg.mmcme2_drp.ipb_ctrl_en = '1' else sigs_to_mmcme2_drp_from_io_set(i).sclk;
        sigs_to_mmcme2_drp_set(i).saddr <= ipb_cbc_system_cnfg.mmcme2_drp.saddr when ipb_cbc_system_cnfg.mmcme2_drp.ipb_ctrl_en = '1' else sigs_to_mmcme2_drp_from_io_set(i).saddr;
        sigs_to_mmcme2_drp_set(i).rst   <= ipb_cbc_system_ctrl.mmcme2_drp.rst   when ipb_cbc_system_cnfg.mmcme2_drp.ipb_ctrl_en = '1' else sigs_to_mmcme2_drp_from_io_set(i).rst;
        sigs_to_mmcme2_drp_set(i).sen   <= ipb_cbc_system_ctrl.mmcme2_drp.sen   when ipb_cbc_system_cnfg.mmcme2_drp.ipb_ctrl_en = '1' else sigs_to_mmcme2_drp_from_io_set(i).sen;    

        ipb_cbc_system_stat.mmcme2_drps(i).srdy                  <= sigs_from_mmcme2_drp_set(i).srdy when ipb_cbc_system_cnfg.mmcme2_drp.ipb_ctrl_en = '1' else '0';
        ipb_cbc_system_stat.cbc_system_clocks_1_set(i).saddr     <= sigs_from_mmcme2_drp_set(i).saddr;
        sigs_from_mmcme2_drp_to_io_set(i).srdy                   <= sigs_from_mmcme2_drp_set(i).srdy when ipb_cbc_system_cnfg.mmcme2_drp.ipb_ctrl_en = '0' else '0';
        sigs_from_mmcme2_drp_to_io_set(i).saddr                  <= sigs_from_mmcme2_drp_set(i).saddr;
         
    end generate;

    mmcme2_drp_sclk         <= clk_40MHz;      


    --===================================--
    -- LED and test signal settings
    --===================================--    
    usrled1_g <= '0' when cbc_system_clocks_0_locked = '1' else '1';
    usrled1_r <= '1';
    usrled1_b <= '1';
    usrled2_r <= '0' when (internal_l1a_veto = '1' or external_l1a_veto = '1' ) else '1';
    usrled2_g <= '0' when ipb_cbc_system_stat.fast_signal_manager.trigger_fsm = running else '1';
    usrled2_b <= '0' when cbc_fast_signal_40MHz(fast_signal_40MHz_indices.trigger) = '1' else '1';
    
    test_signal_out(0) <= clk_320MHz                                                   when ipb_cbc_system_cnfg.global.test_out_sel0 = x"0001" else
                          clk_40MHz                                                    when ipb_cbc_system_cnfg.global.test_out_sel0 = x"0002" else
                          cbc_fast_signal_40MHz(fast_signal_40MHz_indices.fast_reset)  when ipb_cbc_system_cnfg.global.test_out_sel0 = x"0003" else
                          cbc_fast_signal_40MHz(fast_signal_40MHz_indices.trigger)     when ipb_cbc_system_cnfg.global.test_out_sel0 = x"0004" else
                          cbc_fast_signal_40MHz(fast_signal_40MHz_indices.test_pulse)  when ipb_cbc_system_cnfg.global.test_out_sel0 = x"0005" else                          
                          cbc_fast_signal_40MHz(fast_signal_40MHz_indices.orbit_reset) when ipb_cbc_system_cnfg.global.test_out_sel0 = x"0006" else  
                          cbc_i2c_bus_mosi_set(0).scl                                  when ipb_cbc_system_cnfg.global.test_out_sel0 = x"0007" else
                          cbc_i2c_bus_mosi_set(0).sda                                  when ipb_cbc_system_cnfg.global.test_out_sel0 = x"0008" else
                          cbc_i2c_bus_miso_set(0).sda                                  when ipb_cbc_system_cnfg.global.test_out_sel0 = x"0009" else
                          rdb_write_trigger                                            when ipb_cbc_system_cnfg.global.test_out_sel0 = x"000a" else
                          ipb_cbc_system_ctrl.global.cbc_hard_reset                    when ipb_cbc_system_cnfg.global.test_out_sel0 = x"0100" else
                          sigs_to_mmcme2_drp_set(0).rst                                when ipb_cbc_system_cnfg.global.test_out_sel0 = x"0101" else
                          io_test_out                                                  when ipb_cbc_system_cnfg.global.test_out_sel0 = x"0110" else
                          ipb_cbc_system_ctrl.io.data_clock_timing_tunings(0).reset    when ipb_cbc_system_cnfg.global.test_out_sel0 = x"0111" else
                          sigs_from_mmcme2_drp_set(0).saddr(0)                         when ipb_cbc_system_cnfg.global.test_out_sel0 = x"1000" else
                          sigs_from_mmcme2_drp_set(0).srdy                             when ipb_cbc_system_cnfg.global.test_out_sel0 = x"1001" else
                          ipb_cbc_system_ctrl.io.data_clock_timing_tunings(0).scan_pulse when ipb_cbc_system_cnfg.global.test_out_sel0 = x"0109" else
--                          ipbif_to_ci2cbmgr_set(1).reply_fifo_read_next                  when ipb_cbc_system_cnfg.global.test_out_sel0 = x"010a" else
                          ipbif_to_rdb_read_next                     when ipb_cbc_system_cnfg.global.test_out_sel0 = x"0000";
                                                                              
    test_signal_out(1) <= clk_320MHz                                                   when ipb_cbc_system_cnfg.global.test_out_sel1 = x"0001" else
                          clk_40MHz                                                    when ipb_cbc_system_cnfg.global.test_out_sel1 = x"0002" else
                          cbc_fast_signal_40MHz(fast_signal_40MHz_indices.fast_reset)  when ipb_cbc_system_cnfg.global.test_out_sel1 = x"0003" else
                          cbc_fast_signal_40MHz(fast_signal_40MHz_indices.trigger)     when ipb_cbc_system_cnfg.global.test_out_sel1 = x"0004" else
                          cbc_fast_signal_40MHz(fast_signal_40MHz_indices.test_pulse)  when ipb_cbc_system_cnfg.global.test_out_sel1 = x"0005" else                          
                          cbc_fast_signal_40MHz(fast_signal_40MHz_indices.orbit_reset) when ipb_cbc_system_cnfg.global.test_out_sel1 = x"0006" else  
                          cbc_i2c_bus_mosi_set(0).scl                                  when ipb_cbc_system_cnfg.global.test_out_sel1 = x"0007" else
                          cbc_i2c_bus_mosi_set(0).sda                                  when ipb_cbc_system_cnfg.global.test_out_sel1 = x"0008" else
                          cbc_i2c_bus_miso_set(0).sda                                  when ipb_cbc_system_cnfg.global.test_out_sel1 = x"0009" else
                          rdb_write_trigger                                            when ipb_cbc_system_cnfg.global.test_out_sel1 = x"000a" else                          
                          ipb_cbc_system_ctrl.global.cbc_hard_reset                    when ipb_cbc_system_cnfg.global.test_out_sel1 = x"0100" else
                          clk_40MHz_iserdes_ctrl(0)                                    when ipb_cbc_system_cnfg.global.test_out_sel1 = x"0101" else
                          sigs_to_mmcme2_drp_set(0).sclk                               when ipb_cbc_system_cnfg.global.test_out_sel1 = x"0102" else
                          sigs_to_mmcme2_drp_set(0).rst                                when ipb_cbc_system_cnfg.global.test_out_sel1 = x"0103" else
                          sigs_to_mmcme2_drp_set(0).sen                                when ipb_cbc_system_cnfg.global.test_out_sel1 = x"0104" else
                          sigs_from_mmcme2_drp_set(0).srdy                             when ipb_cbc_system_cnfg.global.test_out_sel1 = x"0105" else
                          io_test_out                                                  when ipb_cbc_system_cnfg.global.test_out_sel1 = x"0106" else  
                          ipb_cbc_system_ctrl.io.data_clock_timing_tunings(0).scan_pulse   when ipb_cbc_system_cnfg.global.test_out_sel1 = x"0107" else                                                                 
                          sigs_from_mmcme2_drp_set(0).saddr(1)                                when ipb_cbc_system_cnfg.global.test_out_sel1 = x"0108" else
                          ci2cbmgr_to_ipbif_set(0).reply_fifo_dout(3)                  when ipb_cbc_system_cnfg.global.test_out_sel1 = x"0109" else
 --                         ci2cbmgr_to_ipbif_set(1).reply_fifo_dout(3)                  when ipb_cbc_system_cnfg.global.test_out_sel1 = x"010a" else
                          ipb_cbc_system_ctrl.rdb_controller.reset  when ipb_cbc_system_cnfg.global.test_out_sel1 = x"0000";

    --===================================--
    io_map : entity work.io
    --===================================--
    port map(
        ipb_cnfg_cbc_cnfg_set          => ipb_cbc_system_cnfg.global.cbcs,
        ipb_ctrl                       => ipb_cbc_system_ctrl.io,
        ipb_cnfg                       => ipb_cbc_system_cnfg.io,
        ipb_stat                       => ipb_cbc_system_stat.io,
        fmc_l8_la_n                    => fmc_l8_la_n,
        fmc_l8_la_p                    => fmc_l8_la_p,
        fmc_l12_la_n                   => fmc_l12_la_n,
        fmc_l12_la_p                   => fmc_l12_la_p,
        fmc_l12_clk0                   => fmc_l12_clk0,
        fmc_l12_clk1                   => fmc_l12_clk1,
        clk_200MHz                     => clk_200MHz,
        clk_40MHz_cbc_ctrl             => clk_40MHz,
        clk_320MHz_cbc_ctrl            => clk_320MHz,
        clk_40MHz_iserdes_ctrl         => clk_40MHz_iserdes_ctrl,
        clk_320MHz_iserdes_ctrl        => clk_320MHz_iserdes_ctrl,
        mmcme2_drp_sclk                => mmcme2_drp_sclk,
        ipb_clk                        => ipb_clk,
        cbc_fast_signal_par            => cbc_fast_signal_par,
        cbc_i2c_bus_mosi_set           => cbc_i2c_bus_mosi_set,
        ext_async_l1a                  => ext_async_l1a,
        cbc_par_data_set_o             => cbc_par_data_set,
        cbc_i2c_bus_miso_set           => cbc_i2c_bus_miso_set,
        cbc_hard_reset                 => ipb_cbc_system_ctrl.global.cbc_hard_reset,
        external_l1a_veto              => external_l1a_veto,
        fmc_ext_clk_40MHz              => fmc_ext_clk_40MHz,                
        test_signal_out                => test_signal_out,
        sigs_to_mmcme2_drp_set         => sigs_to_mmcme2_drp_from_io_set,
        sigs_from_mmcme2_drp_set       => sigs_from_mmcme2_drp_to_io_set,
        io_test_out                    => io_test_out,
        dctsb_dout                     => dctsb_dout,
        dctsb_read_next                => dctsb_read_next
);



    process (clk_40MHz)
    begin
        if rising_edge( clk_40MHz) then
            flip_40MHz <= not flip_40MHz;
        end if;
    end process;
    
    process (clk_320MHz)
    variable timer : integer range 0 to 7;
    begin
        if rising_edge( clk_320MHz ) then        
            flip_40MHz_1d <= flip_40MHz;
            timer := timer + 1;
            if flip_40MHz_1d /= flip_40MHz then
                timer := 2;
            end if;
            if timer = 0 then
                clk_sync_bit <= '1';
            else
                clk_sync_bit <= '0';
            end if;                
        end if;
    end process;
    
	--===========================================--
	ipb_cbc_system_inst: entity work.ipb_cbc_system
	--===========================================--
	port map
	(
	   ipb_rst_i              => ipb_rst_i,
	   ipb_clk                => ipb_clk,
	   clk_40MHz_iserdes_ctrl => clk_40MHz_iserdes_ctrl,
	   clk_320MHz_iserdes_ctrl=> clk_320MHz_iserdes_ctrl,
	   clk_40MHz_cbc_ctrl     => clk_40MHz,
	   clk_320MHz_cbc_ctrl    => clk_320MHz,
       clk_40MHz_daq          => clk_40MHz,
       clk_320MHz_daq         => clk_320MHz,       
	   mmcme2_drp_sclk        => mmcme2_drp_sclk,
	   ipb_mosi_i             => ipb_mosi_i,
	   ipb_miso_o             => ipb_miso_o,
	   cbc_system_stat        => ipb_cbc_system_stat,
	   cbc_system_cnfg        => ipb_cbc_system_cnfg,
	   cbc_system_ctrl        => ipb_cbc_system_ctrl,
	   db_to_ipbif            => db_to_ipbif,
	   ipbif_to_db            => ipbif_to_db,
       ci2cbmgr_to_ipbif_set  => ci2cbmgr_to_ipbif_set,
       ipbif_to_ci2cbmgr_set  => ipbif_to_ci2cbmgr_set,
       dctsb_dout             => dctsb_dout,
       dctsb_read_next        => dctsb_read_next,
       ipbif_to_fscbram       => ipbif_to_fscbram,
       rdb_dout               => rdb_dout_to_ipbif,
       rdb_read_next          => ipbif_to_rdb_read_next,
       test_sig               => ipb_test_sig
	);    
    ipb_cbc_system_stat.global.n_active_cbc <= ipb_cbc_system_cnfg.global.n_active_cbc;

	--===========================================--
    fast_signal_manager : entity work.fast_signal_manager
	--===========================================--
    port map (
        clk_320MHz                                  => clk_320MHz,
        clk_40MHz                                   => clk_40MHz,
        ext_async_l1a                               => ext_async_l1a,
        l1a_veto                                    => internal_l1a_veto,
        fmc_cbc_fast_signal_40MHz                   => "0000",
        fscbram_fast_signal_40MHz                   => fscbram_fast_signal_40MHz,
        ipb_ctrl                                    => ipb_cbc_system_ctrl.fast_signal_manager,
        ipb_cnfg                                    => ipb_cbc_system_cnfg.fast_signal_manager,
        ipb_stat                                    => ipb_cbc_system_stat.fast_signal_manager,
        l1a_320MHz                                  => l1a_320MHz,
        l1a_count_o                                 => l1a_count,
        l1a_tdccount                                => l1a_tdc_count,
 --       int_cbc_fast_signal_40MHz_o                 => int_cbc_fast_signal_40MHz,
        cbc_fast_signal_40MHz_o                     => cbc_fast_signal_40MHz,
        cbc_fast_signal_par                         => cbc_fast_signal_par,
        clk_sync_bit                                => clk_sync_bit
    );
    internal_l1a_veto <= l1a_veto_from_db when ipb_cbc_system_cnfg.global.trigger_master_external = '0' else '0';
    external_l1a_veto <= l1a_veto_from_db when ipb_cbc_system_cnfg.global.trigger_master_external = '1' else '0';    
    ipb_cbc_system_stat.trig_veto.internal_l1a_veto <= internal_l1a_veto;
    ipb_cbc_system_stat.trig_veto.external_l1a_veto <= external_l1a_veto;
    
	--===========================================--    
    cbc_i2c_gen :
    --===========================================--
    for i in 0 to NFE-1 generate 

        ci2cbmgr_in_set(i).command_fifo_we      <= ipbif_to_ci2cbmgr_set(i).command_fifo_we  or fscbram_ctrl_to_ci2cbmgr_set(i).command_fifo_we;
        ci2cbmgr_in_set(i).command_fifo_din     <= ipbif_to_ci2cbmgr_set(i).command_fifo_din when ipbif_to_ci2cbmgr_set(i).command_fifo_we = '1' else
        										   fscbram_ctrl_to_ci2cbmgr_set(i).command_fifo_din when fscbram_ctrl_to_ci2cbmgr_set(i).command_fifo_we = '1' else
        										   (others => '0');
        ci2cbmgr_in_set(i).command_fifo_reset   <= ipb_cbc_system_ctrl.cbc_i2c_bus_managers(i).reset_fifos;
        ci2cbmgr_in_set(i).ipb_clk              <= ipbif_to_ci2cbmgr_set(i).ipb_clk;
        ci2cbmgr_in_set(i).reply_fifo_read_next <= ipbif_to_ci2cbmgr_set(i).reply_fifo_read_next;
        ci2cbmgr_in_set(i).reply_fifo_reset     <= ipb_cbc_system_ctrl.cbc_i2c_bus_managers(i).reset_fifos or fscbram_ctrl_to_ci2cbmgr_set(i).reply_fifo_reset;
        
        cbc_i2c_bus_manager_inst : entity work.cbc_i2c_bus_manager
		port map(
                clk_40MHz               => clk_40MHz,
                ipb_ctrl                => ipb_cbc_system_ctrl.cbc_i2c_bus_managers(i),        
                fe_cnfg                 => ipb_cbc_system_cnfg.global.fes(i),
                i2c_bus_miso            => cbc_i2c_bus_miso_set(i),
                i2c_bus_mosi            => cbc_i2c_bus_mosi_set(i),    
                ipb_stat                => ipb_cbc_system_stat.cbc_i2c_bus_managers(i),        
                ci2cbmgr_in             => ci2cbmgr_in_set(i),
                ci2cbmgr_to_ipbif       => ci2cbmgr_to_ipbif_set(i),
                test_out                => cibm_test_out(i)
                );     
        cbci2cbus_waitings(i)   <=  ipb_cbc_system_stat.cbc_i2c_bus_managers(i).bus_waiting;       
        cbci2cbus_readies(i)    <=  ipb_cbc_system_stat.cbc_i2c_bus_managers(i).bus_ready;                                     
    end generate cbc_i2c_gen;
    

    cbci2cbus_waiting <= get_fe_active_and(cbci2cbus_waitings, ipb_cbc_system_cnfg.global.fes);
    cbci2cbus_ready   <= get_fe_active_and(cbci2cbus_readies, ipb_cbc_system_cnfg.global.fes);    

    fscbram_in.wen  <= ipbif_to_fscbram.wen  when  ipb_cbc_system_cnfg.fscbram_controller.ipbus_en = '1' else '0';
    fscbram_in.din  <= ipbif_to_fscbram.din  when  ipb_cbc_system_cnfg.fscbram_controller.ipbus_en = '1' else (others => '0');
--    fscbram_in.count <= ipbif_to_fscbram.count;
    fscbram_in.addr  <= ipbif_to_fscbram.addr;
    --===========================================--    
    fscbram_controller_inst : entity work.fscbram_controller
    --===========================================--    
    port map(		
        clk_320MHz              => clk_320MHz,
        clk_40MHz               => clk_40MHz,
        reset                   => ipb_cbc_system_ctrl.fscbram_controller.reset,
        start                   => ipb_cbc_system_ctrl.fscbram_controller.start,	
        resume                  => ipb_cbc_system_ctrl.fscbram_controller.resume,
        rdb_write_ready         => rdb_write_ready,
        cbci2cbus_waiting       => cbci2cbus_waiting,	
        cbci2cbus_ready         => cbci2cbus_ready,	
        bram_in                 => fscbram_in,
        ci2cbmgr_in_set         => fscbram_ctrl_to_ci2cbmgr_set,	
        fast_signal_40MHz_in    => fscbram_fast_signal_40MHz,
        ipb_stat                => ipb_cbc_system_stat.fscbram_controller,
        ipb_cnfg_cbc_cnfg_set   => ipb_cbc_system_cnfg.global.cbcs,
        clk_sync_bit            => clk_sync_bit,
        rdb_write_trigger_o     => rdb_write_trigger
    );		

    --===========================================--    
	-- raw data frame buffer and the control
    --===========================================--       							
	-- 64bit x 19456 deep write
	-- 32bit x 38912 deep readout
	rdb_inst : entity work.rdb
	  PORT map(
		clka 	=> clk_40MHz,
		wea(0)  => rdb_we,
		addra	=> rdb_waddr,
		dina    => rdb_din,
		clkb 	=> ipb_clk,	
		addrb   => rdb_raddr,	
		doutb   => rdb_dout_to_ipbif
	  );	
--	rdb_dout_to_ipbif <= x"0000" & rdb_raddr;
    cbc_par_data_to_rdb_ctrl <= cbc_par_data_set( get_cbc_index( a => ipb_cbc_system_cnfg.global.cbcs, cbc_id => ipb_cbc_system_cnfg.rdb_controller.cbc_id ) );	
--	cbc_par_data_to_rdb_ctrl <= cbc_par_data_set(0);
	rdb_ctrl_inst : entity work.rdb_controller
	Port map( 
		reset				=> ipb_cbc_system_ctrl.rdb_controller.reset,
		clk_40MHz			=> clk_40MHz,
		rd_clk				=> ipb_clk,	
		trigger				=> rdb_write_trigger,
		read_next			=> ipbif_to_rdb_read_next,
		ipb_cnfg			=> ipb_cbc_system_cnfg.rdb_controller,
		cbc_par_data        => cbc_par_data_to_rdb_ctrl,	
		rdb_we				=> rdb_we,	
		rdb_waddr_o			=> rdb_waddr,
		rdb_din				=> rdb_din,
		rdb_raddr			=> rdb_raddr,	
		write_ready			=> rdb_write_ready,
		ipb_stat            => ipb_cbc_system_stat.rdb_controller
	);
      
    --===========================================--    
    cbc_data_processor_gen :
	--===========================================--
    for i in 0 to NCBC-1 generate 
        cbc_data_processor_inst : entity work.cbc_data_processor(fc7)
        port map ( 
            be_id                    => ipb_cbc_system_cnfg.global.be_id,
            fe_id                    => ipb_cbc_system_cnfg.global.cbcs(i).fe_id,
            cbc_id                   => ipb_cbc_system_cnfg.global.cbcs(i).id,
            cbc_active               => ipb_cbc_system_cnfg.global.cbcs(i).active,
            clk_320MHz               => clk_320MHz,
            clk_40MHz                => clk_40MHz,
            cbc_par_data             => cbc_par_data_set(i),
--            cbc_par_data             => cbc_par_data_set(0),
            ipb_ctrl                 => ipb_cbc_system_ctrl.cbc_data_processors(i),
            ipb_cnfg                 => ipb_cbc_system_cnfg.cbc_data_processors(i),
            ipb_stat                 => ipb_cbc_system_stat.cbc_data_processors(i),
            l1a_320MHz               => l1a_320MHz,
            dsoebi                   => dsoebi_set(i),
            dsiebo                   => dsiebo_set(i)
        ); 
    end generate cbc_data_processor_gen;
                                                           
	--===========================================--
    event_builder_inst : entity work.event_builder
	--===========================================--
    generic map(NDATASECTIONS => NCBC, MAX_SECTION_PACKET_SIZE => max_section_packet_size.cbc_data_raw)    
    port map(
        be_id                     => ipb_cbc_system_cnfg.global.be_id,
        cbc_cnfg_set              => ipb_cbc_system_cnfg.global.cbcs,
        clk_320MHz                => clk_320MHz,
        l1a_320MHz                => l1a_320MHz,
        l1a_tdc_count             => l1a_tdc_count,
        l1a_count                 => l1a_count,
        dsoebi_set                => dsoebi_set,
        dsiebo_set                => dsiebo_set,
        ebodbi                    => ebodbi,
        ebidbo                    => ebidbo,
        ipb_cnfg                  => ipb_cbc_system_cnfg.event_builder,
        ipb_ctrl                  => ipb_cbc_system_ctrl.event_builder,
        ipb_stat                  => ipb_cbc_system_stat.event_builder
    );

	--===========================================--
    data_buffer_inst : entity work.data_buffer(fc7_ring_buffer)
	--===========================================--
    port map(
        clk_320MHz                => clk_320MHz,
        clk_slow                  => clk_10MHz,
        ebodbi                    => ebodbi,
        ebidbo                    => ebidbo,
        ipbif_to_db               => ipbif_to_db,
        db_to_ipbif               => db_to_ipbif,
        ipb_cnfg                  => ipb_cbc_system_cnfg.data_buffer,
        ipb_ctrl                  => ipb_cbc_system_ctrl.data_buffer,
        ipb_stat                  => ipb_cbc_system_stat.data_buffer,
        n_word_free_o             => n_word_free,
        l1a_veto                  => l1a_veto_from_db_320MHz
    );

    l1a_veto_from_data_buffer_inst : process (  ipb_cbc_system_ctrl.veto.reset, clk_40MHz )
    begin
        if ipb_cbc_system_ctrl.veto.reset = '1' then
            l1a_veto_from_db <= '0';
        elsif rising_edge( clk_40MHz ) then
            l1a_veto_from_db <= l1a_veto_from_db_320MHz;
        end if;
    end process;

--    --===========================================--    
--    cbc_emulators_gen :
--    for i in 0 to NCBC-1 generate 
--   --===========================================--    
--	cbc_emulator_inst : entity work.cbc_emulator
--    port map
--    (
--       clk_320MHz                    => cbc_emulator_clk_320MHz,
--       serial_command                => cbc_emulator_in_serial_command,
--       slvs                          => cbc_emulator_out_ser_data_set(i),
--       i2c_bus_mosi_set              => cbc_emulator_i2c_bus_mosi_set(i),
--       i2c_bus_miso_set              => cbc_emulator_i2c_bus_miso_set(i),
--       hard_reset                    => cbc_emulator_hard_reset,
--       ipb_cnfg                      => ipb_cbc_system_cnfg.cbc_emulators(i),
--       ipb_ctrl                      => ipb_cbc_system_ctrl.cbc_emulators(i),
--       ipb_stat                      => ipb_cbc_system_stat.cbc_emulators(i)
--    );
--    end generate;
end cbc3;
