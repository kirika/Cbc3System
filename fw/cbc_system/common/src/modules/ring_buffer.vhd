--===========================--
-- ring_buffer 
-- 28.10.2016 Kirika Uchida
--===========================--

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ring_buffer is
generic( constant ADDR_WIDTH : positive := 16 ); 
port(
    rst                      : in  std_logic;
    clk                      : in  std_logic; --320MHz
    din                      : in  std_logic_vector(31 downto 0);
    we                       : in  std_logic;
    end_of_event             : in  std_logic;
    trig_readall             : in  std_logic;
    raddr                    : in  std_logic_vector(ADDR_WIDTH-1 downto 0);    
    next_raddr_o             : out std_logic_vector(ADDR_WIDTH-1 downto 0);
    next_rdata               : out std_logic_vector(31 downto 0);
    load_next                : out std_logic;
    waddr_o                  : out std_logic_vector(ADDR_WIDTH-1 downto 0);
    n_word_all_o             : out std_logic_vector(ADDR_WIDTH downto 0);
    n_word_events            : out std_logic_vector(ADDR_WIDTH downto 0);    
    n_word_free              : out std_logic_vector(ADDR_WIDTH downto 0);
    werr                     : out std_logic;
    bram_clka                : out std_logic;
    bram_wea                 : out std_logic;
    bram_addra               : out std_logic_vector(ADDR_WIDTH-1 downto 0);
    bram_dina                : out std_logic_vector(31 downto 0);
    bram_clkb                : out std_logic;
    bram_addrb               : out std_logic_vector(ADDR_WIDTH-1 downto 0);
    bram_doutb               : in  std_logic_vector(31 downto 0)
);
end ring_buffer;

architecture Behavioral of ring_buffer is

    constant FIFO_DEPTH       : integer := 2**ADDR_WIDTH;
    signal waddr              : std_logic_vector(ADDR_WIDTH-1 downto 0);
    signal rerr               : std_logic;
    signal n_word_all         : std_logic_vector(ADDR_WIDTH downto 0);
    signal next_raddr         : std_logic_vector(ADDR_WIDTH-1 downto 0);
    signal next_raddr_1d      : std_logic_vector(ADDR_WIDTH-1 downto 0);
    signal end_of_event_raddr : std_logic_vector(ADDR_WIDTH-1 downto 0);
    signal raddr_1d           : std_logic_vector(ADDR_WIDTH-1 downto 0);  
    signal diff_wraddr        : unsigned(ADDR_WIDTH downto 0);
    signal diff_rwaddr        : unsigned(ADDR_WIDTH downto 0); 
    signal diff_wraddr_event  : unsigned(ADDR_WIDTH downto 0);    
    signal diff_rwaddr_event  : unsigned(ADDR_WIDTH downto 0);   
    signal end_of_event_1d    : std_logic;   
    signal update_next        : std_logic;
    signal waddr_event        : std_logic_vector(ADDR_WIDTH-1 downto 0);
begin

	process ( clk, rst, trig_readall )
		variable timer : natural range 0 to 20;
		variable looped : boolean;
		variable looped_event : boolean;
	begin

		if rst = '1' then
	        waddr <= (others =>'0');	
	        waddr_event <= 	(others =>'0');	
            looped := false;
            looped_event := false;
            werr <= '0'; 
            next_raddr <= (others=>'0');
            rerr <= '0';
            timer := 1;
            end_of_event_raddr <= (others=>'0');
            n_word_events <= (others => '0');
            update_next <= '0';   
            bram_wea <= '0';
            bram_addra <= (others =>'0');	         
	    elsif trig_readall = '1' then
            waddr <= (others =>'0');
            waddr_event <= (others =>'0');
            next_raddr <= (others => '0');
            update_next <= '0';
            bram_wea <= '0';
            bram_addra <= (others =>'0');
            looped := true;
            looped_event := true;
            timer := 1;

		elsif rising_edge( clk ) then
            load_next <= '0';
            update_next <= '0';
            raddr_1d <= raddr;
            next_raddr_1d <= next_raddr;
            bram_wea <= '0';
            if update_next = '1' then
                next_raddr <= std_logic_vector(unsigned(next_raddr) + 1);
                if ( to_integer(unsigned(raddr)) = FIFO_DEPTH - 1 ) then
                    next_raddr <= (others => '0');
                    looped := false;
                    looped_event := false;
                end if;
            end if;
                
            if timer /= 0 then
--                if timer = 3 then
--                    load_next <= '1';
--                    timer := 0;
--                else
--                    timer := timer + 1;
--                end if;

                if timer = 5 then
                    timer := 0;
                else
                    if timer = 4 then
                        load_next <= '1';
                    end if;
                    timer := timer + 1;
                end if;
            else
                -- read operation
                if ( ( looped = true ) or ( waddr /= next_raddr ) ) then
--                    if raddr_1d = raddr then
--                        next_raddr <= std_logic_vector(unsigned(raddr) + 1);
--                        if ( to_integer(unsigned(raddr)) = FIFO_DEPTH - 1 ) then
--                           next_raddr <= (others => '0');
--                           looped := false;
--                         end if;
--                    end if;
                    if update_next = '0' and raddr_1d = next_raddr then
                        update_next <= '1';
                    end if;
 
                    rerr <= '0';
                else
                    rerr <= '1';
                end if;               
                -- read & write address relations, number of data and free space
                if looped = false then
                        diff_wraddr <= unsigned('0'&waddr) - unsigned('0'&raddr);                       
                        diff_rwaddr <= (others => '0');
                else
                    diff_wraddr <= (others => '0');
                    diff_rwaddr <= unsigned('0'&raddr) - unsigned('0'&waddr);
                end if;
                if diff_rwaddr = 0 then -- not looped or no error
                    n_word_all  <= std_logic_vector( diff_wraddr );
                    n_word_free <= std_logic_vector(FIFO_DEPTH - diff_wraddr);
                else
                    n_word_all  <= std_logic_vector(FIFO_DEPTH - diff_rwaddr );
                    n_word_free <= std_logic_vector( diff_rwaddr );
                end if; 
                -- number of data until the latest end of event.               
                if looped_event = false then
                    diff_wraddr_event <= unsigned('0'&waddr_event) - unsigned('0'&raddr);
                    diff_rwaddr_event <= (others => '0');
                else
                    diff_wraddr_event <= (others => '0');
                    diff_rwaddr_event   <= unsigned('0'&raddr) - unsigned('0'&waddr_event);
                end if;        
                if diff_rwaddr_event = 0 then -- not looped or no error                
                    n_word_events <= std_logic_vector( diff_wraddr_event );
                else
                    n_word_events <= std_logic_vector(FIFO_DEPTH - diff_rwaddr_event );                
                end if;                                    
            end if;
			-- write operation
			if( we = '1' ) then
                if( ( looped = false ) or ( waddr /= next_raddr ) ) then
                    waddr <= std_logic_vector(unsigned(waddr)+1);
                    if( to_integer(unsigned(waddr)) = FIFO_DEPTH - 1 ) then
                        waddr <= (others=>'0');
                        looped := true;
                        
                    end if;
                    if ( waddr = next_raddr ) then
                        timer := 1;
                    end if;
                    werr <= '0';
                else
                    werr <= '1';
                end if;
                if looped = true and end_of_event = '1' then
                    looped_event := true;
                end if;                
                
                bram_wea   <= we;
                bram_addra <= waddr;
                bram_dina  <= din;
                if end_of_event = '1' then
                    waddr_event <= std_logic_vector(unsigned(waddr)+1);
                end if;
            end if;
        end if;
    end process;

    waddr_o      <= waddr;
    n_word_all_o <= n_word_all;
    
    next_raddr_o <= next_raddr;
    
    bram_clka  <= clk;
--    bram_wea   <= we;
--    bram_addra <= waddr;
--    bram_dina  <= din;
    bram_clkb  <= clk;
    bram_addrb <= next_raddr_1d;
    next_rdata <= bram_doutb;
--	next_rdata <= x"0000" & next_raddr_1d; 
end Behavioral;
