----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 14.11.2016 14:56:04
-- Design Name: 
-- Module Name: cbc_system_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

use work.ipbus.all;
use work.system_package.all;
--! user packages
use work.user_package.all;

use work.cbc_system_package.all;

entity cbc_system_tb is
--  Port ( );
end cbc_system_tb;

architecture Behavioral of cbc_system_tb is

	signal clk_320MHz                     : std_logic:= '1';
    signal clk_40MHz                      : std_logic:= '1';
    
    signal cbc_ser_data_set               : cbc_data_set_type;    
    -- ipbus signals
    signal ipb_cbc_system_stat            : ipb_cbc_system_stat_type;     
    signal ipb_cbc_system_cnfg            : ipb_cbc_system_cnfg_type;
    signal ipb_cbc_system_ctrl            : ipb_cbc_system_ctrl_type; 
    signal ipb_cbc_system_ctrl_dummy            : ipb_cbc_system_ctrl_type;        
    signal l1a_320MHz                     : std_logic;         
    signal l1a_count                      : std_logic_vector(28 downto 0) := "00000" & x"000000";        
    signal l1a_tdc_count                  : std_logic_vector(2 downto 0);
    signal l1a_count_8bit                 : unsigned(7 downto 0) := x"00";
    -- signals btw cbc processors & event builder
    signal dsoebi_set                     : dsoebi_set_type;
    signal dsiebo_set                     : dsiebo_set_type;
    -- signals from event builder to data buffer
    signal ebodbi                         : ebodbi_type;
    signal ebidbo                         : ebidbo_type;
    -- data buffer ipb interface
    signal db_to_ipbif                    : db_to_ipbif_type;
    signal ipbif_to_db                    : ipbif_to_db_type;      
        
    constant clk_320MHz_period : time :=  3.125 ns;     
    constant clk_40MHz_period  : time := 25.000 ns;
    constant clk_ipb_period    : time := 31.250 ns;
    constant sdelay            : time :=  1.22 ns;      
    constant n_trigs                     : integer := 2;    
             
    signal   index   : unsigned( 2 downto 0) := (others => '1');
    signal   data_index : unsigned( 5 downto 0 ) := to_unsigned(40, 6);

    type slvs6_pattern_type  is array ( 0 to 34 ) of std_logic_vector( 7 downto 0 );
    constant SLVS6_HITS  : slvs6_pattern_type  := (  x"04",x"04",                                                                                                     
                                                     x"28",x"24",x"20",x"1c",x"18",x"14",x"10",x"0c",x"08",x"04", 
                                                     x"28",x"24",x"20",x"1c",x"18",x"14",x"10",x"0c",x"08",x"04", 
                                                     x"28",x"24",x"20",x"1c",x"18",x"14",x"10",x"0c",x"08",x"04",                                                     
                                                     "000000" & "00",
                                                     "00000" & "100",
                                                     "1100" & "1000"
                                                     );
    type slvs15_pattern_type is array ( 1 to 5 ) of std_logic_vector( 7 downto 0 );
    constant SLVS15_STUB : slvs15_pattern_type := (x"01", x"02", x"03", x"04", "10000000" );
    
    signal slvs6_data_tmp : slvs6_pattern_type;
    signal slvs6_data : slvs6_pattern_type; 
    
    signal ipb_rst_i                      : std_logic := '0';   
    signal ipb_clk                        : std_logic := '1';
    signal ipb_miso_o : ipb_rbus_array(0 to nbr_usr_slaves-1);
    signal ipb_mosi_i : ipb_wbus_array(0 to nbr_usr_slaves-1);


    signal ci2cbmgr_to_ipbif_set          : ci2cbmgr_out_set_type;
    signal ipbif_to_ci2cbmgr_set          : ci2cbmgr_in_set_type;

    signal reset_done                    : boolean := false;
    signal ipb_rst_done                  : boolean := false;
    signal idelay_tuned                  : boolean := false;
    signal read_data                     : boolean := false;
    signal trig_count                    : integer range 0 to 100 := 0;
    signal read_count                    : integer range 0 to 30 := 0;
    


    
begin

--    slvs6_data_tmp <= SLVS6_HITS;
    
    clk_320MHz <= not clk_320MHz after clk_40MHz_period / 16;
    clk_40MHz  <= not clk_40MHz after clk_40MHz_period / 2;
    process
    begin
    wait for 0.1 ns;
    ipb_clk    <= not ipb_clk;
    wait for clk_ipb_period / 2;
    ipb_clk <= not ipb_clk;
    wait for clk_ipb_period / 2 - 0.1 ns;
    end process;

    ipb_cbc_system_cnfg.global.be_id <= "0000001";
    ipb_cbc_system_cnfg.global.cbcs(0).fe_id <= "001";
    ipb_cbc_system_cnfg.global.cbcs(0).id    <= "00001";
    ipb_cbc_system_cnfg.global.cbcs(0).active <= '1';
    
    l1a_tdc_count <= (others => '0' );
    ipb_cbc_system_ctrl.data_buffer.trig_readall <= '0';





    process (ipb_clk) 

    begin
        if rising_edge( ipb_clk ) then
            ipb_mosi_i(ipb_cbc_system_stat_sel).ipb_strobe <= '0';
            ipb_mosi_i(ipb_cbc_system_cnfg_sel).ipb_strobe <= '0';
            ipb_mosi_i(ipb_cbc_system_ctrl_sel).ipb_strobe <= '0';
            ipb_mosi_i(ipb_cbc_system_event_data_sel).ipb_strobe <= '0';
            ipb_mosi_i(ipb_cbc_system_cbc_i2c_regs_sel).ipb_strobe <= '0';  
            ipb_mosi_i(ipb_cbc_system_event_data_sel).ipb_addr <= "01000000000000000000010000000000";
            if to_integer(unsigned(ipb_cbc_system_stat.data_buffer.n_word_events)) >= 28 then
                read_data <= true;
            end if;
            if read_data = true then 
                if read_count < 28 then
                    ipb_mosi_i(ipb_cbc_system_event_data_sel).ipb_strobe <= '1';
                    ipb_mosi_i(ipb_cbc_system_event_data_sel).ipb_write <= '0';
                    read_count <= read_count + 1;
                    if  read_count = 27 then
                        read_data <= false;
                    end if;
                end if;              
            end if;
     
        end if;      
    end process;

    
    cbc_data_processors_ctrl_signals:
    for i in 0 to NCBC-1 generate     
        process begin
        
            ipb_cbc_system_ctrl.cbc_data_processors(i).reset <= '0';
            ipb_cbc_system_ctrl.cbc_data_processors(i).cbc_ser_data_delay_start_tuning <= '0';
            wait for 73 ns;
            ipb_cbc_system_ctrl.cbc_data_processors(i).reset <= '1';
            wait for 25 ns;
            ipb_cbc_system_ctrl.cbc_data_processors(i).reset <= '0';    
            wait for 100 ns;
            ipb_cbc_system_ctrl.cbc_data_processors(i).cbc_ser_data_delay_start_tuning <= '1';
            wait for  clk_320MHz_period;
            ipb_cbc_system_ctrl.cbc_data_processors(i).cbc_ser_data_delay_start_tuning <= '0';

            wait;
        end process;    
    end generate;
    
    process (clk_320MHz) 
    variable idelay_tuned_timer : integer range 0 to 1000 := 0;
    begin
        if rising_edge(clk_320MHz) then

            if reset_done = false then
                ipb_cbc_system_ctrl.event_builder.reset <= '1';
                ipb_cbc_system_ctrl.data_buffer.reset <= '1';   
                reset_done <= true;   
            else      
                ipb_cbc_system_ctrl.event_builder.reset <= '0';    
                ipb_cbc_system_ctrl.data_buffer.reset <= '0';  
            end if;

            if idelay_tuned = false and ipb_cbc_system_stat.cbc_data_processors(0).cbc_ser_data_delay.idelay_tuning_fsm = tuned then

                if idelay_tuned_timer = 500 then
                    idelay_tuned <= true;
                    for i in 0 to NCBC-1 loop       
                        ipb_cbc_system_cnfg.cbc_data_processors(i).l1a_latency <= std_logic_vector(to_unsigned(10, 9));
                        ipb_cbc_system_cnfg.cbc_data_processors(i).trig_data_latency <= std_logic_vector(to_unsigned(1, 9));                                      
                    end loop;
                    idelay_tuned_timer := 0;
                end if;
                idelay_tuned_timer := idelay_tuned_timer + 1;
            end if;

            index <= index - 1;
            l1a_320MHz <= '0';    

            for i in 0 to NCBC-1 loop        
                cbc_ser_data_set(i)(6) <= '0' after sdelay;
            end loop;

            for i in 0 to NCBC-1 loop
                for j in 1 to 5 loop
                    cbc_ser_data_set(i)(j) <= SLVS15_STUB(j)(to_integer(index)) after sdelay;
                end loop;
            end loop;

            if idelay_tuned = true and ipb_rst_done = true and reset_done = true and to_integer(unsigned(l1a_count))  <= n_trigs then
                if data_index = 39 then
                    for i in 0 to 34 loop
                        if i < 32 then
                            slvs6_data_tmp(i) <= std_logic_vector(unsigned(SLVS6_HITS(i))+l1a_count_8bit-1);
                        end if;
                    end loop;
                elsif data_index = 38 then
                    for i in 0 to 34 loop
                        if i < 32 then
                            slvs6_data(i) <= bitswap(slvs6_data_tmp(i));
                        else                    
                            slvs6_data(i) <= SLVS6_HITS(i);
                        end if;
                    end loop;
                elsif data_index <= 34 then
                        for i in 0 to NCBC-1 loop
                            cbc_ser_data_set(i)(6) <= slvs6_data(to_integer(data_index))(to_integer(index)) after sdelay; 
                        end loop;              
                elsif data_index = 10 then
                    data_index <= to_unsigned(70, 6);
                end if;
 
                if data_index = 39 then
                    if index = 7 then
                        l1a_320MHz <= '1';
                        l1a_count <= std_logic_vector(unsigned(l1a_count) + 1);
                        l1a_count_8bit <= l1a_count_8bit + 1;
                    end if;
                end if;

                if index = 0 then           
                    data_index <= data_index - 1;
                    if data_index = 0 then
                        data_index <= to_unsigned(40,6);
                    end if;
                end if;

           end if;
       end if;
   end process;    

    process begin
        ipb_rst_i <= '1';
        wait for 50 ns;    
        ipb_rst_i <= '0'; 
        ipb_rst_done <= true; 
        wait;
    end process;

	--===========================================--
	ipb_cbc_system_inst: entity work.ipb_cbc_system
	--===========================================--
	port map
	(
	   ipb_rst_i              => ipb_rst_i,
	   ipb_clk                => ipb_clk,
	   clk_320MHz             => clk_320MHz,
	   clk_40MHz              => clk_40MHz,
	   ipb_mosi_i             => ipb_mosi_i,
	   ipb_miso_o             => ipb_miso_o,
	   cbc_system_stat        => ipb_cbc_system_stat,
	   cbc_system_cnfg        => ipb_cbc_system_cnfg,
	   cbc_system_ctrl        => ipb_cbc_system_ctrl_dummy,
	   db_to_ipbif            => db_to_ipbif,
	   ipbif_to_db            => ipbif_to_db,
       ci2cbmgr_to_ipbif_set  => ci2cbmgr_to_ipbif_set,
       ipbif_to_ci2cbmgr_set  => ipbif_to_ci2cbmgr_set
	);    


    --===========================================--    
    cbc_data_processor_gen :
	--===========================================--
    for i in 0 to NCBC-1 generate 
        cbc_data_processor_inst : entity work.cbc_data_processor(fc7)
        port map ( 
            be_id                    => ipb_cbc_system_cnfg.global.be_id,
            fe_id                    => ipb_cbc_system_cnfg.global.cbcs(i).fe_id,
            cbc_id                   => ipb_cbc_system_cnfg.global.cbcs(i).id,
            clk_320MHz               => clk_320MHz,
            clk_40MHz                => clk_40MHz,
            cbc_ser_data             => cbc_ser_data_set(i),
            ipb_ctrl                 => ipb_cbc_system_ctrl.cbc_data_processors(i),
            ipb_cnfg                 => ipb_cbc_system_cnfg.cbc_data_processors(i),
            ipb_stat_o               => ipb_cbc_system_stat.cbc_data_processors(i),
            l1a_320MHz               => l1a_320MHz,
            dsoebi                   => dsoebi_set(i),
            dsiebo                   => dsiebo_set(i)
        ); 
    end generate cbc_data_processor_gen;
                                                           
	--===========================================--
    event_builder_inst : entity work.event_builder
    --===========================================--
    generic map(NDATASECTIONS => NCBC, MAX_SECTION_PACKET_SIZE => max_section_packet_size.cbc_data_raw)    
    port map(
        be_id                     => ipb_cbc_system_cnfg.global.be_id,
        cbc_cnfg_set              => ipb_cbc_system_cnfg.global.cbcs,
        clk_320MHz                => clk_320MHz,
        l1a_320MHz                => l1a_320MHz,
        l1a_count                 => l1a_count,
        l1a_tdc_count             => l1a_tdc_count,
        dsoebi_set                => dsoebi_set,
        dsiebo_set                => dsiebo_set,
        ebodbi                    => ebodbi,
        ebidbo                    => ebidbo,
        ipb_cnfg                  => ipb_cbc_system_cnfg.event_builder,
        ipb_ctrl                  => ipb_cbc_system_ctrl.event_builder,
        ipb_stat                  => ipb_cbc_system_stat.event_builder
    );
    
	--===========================================--
    data_buffer_inst : entity work.data_buffer(fc7_ring_buffer)
	--===========================================--
    port map(
        clk_320MHz                => clk_320MHz,
        ebodbi                    => ebodbi,
        ebidbo                    => ebidbo,
        ipbif_to_db               => ipbif_to_db,
        db_to_ipbif               => db_to_ipbif,
        ipb_ctrl                  => ipb_cbc_system_ctrl.data_buffer,
        ipb_stat                  => ipb_cbc_system_stat.data_buffer
    );


end Behavioral;
