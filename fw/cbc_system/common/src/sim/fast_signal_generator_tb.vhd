----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03.11.2016 11:41:42
-- Design Name: 
-- Module Name: fast_signal_generator_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

use work.cbc_system_package.all;

entity fast_signal_generator_tb is
--  Port ( );
end fast_signal_generator_tb;

architecture Behavioral of fast_signal_generator_tb is

    constant COUNTER_WIDTH    : integer :=32;
    signal clk                : std_logic := '0';
    signal reset              : std_logic := '0';
    signal load_cnfg          : std_logic := '0';
    signal start              : std_logic := '0';
    signal stop               : std_logic := '0';
    signal Ncycle             : std_logic_vector( COUNTER_WIDTH-1 downto 0 );
    signal fast_reset_en      : std_logic := '1';
    signal test_pulse_en      : std_logic := '1';
    signal trigger_en         : std_logic := '1';
    signal orbit_reset_en     : std_logic := '1';
    signal cycle_T		      : std_logic_vector( COUNTER_WIDTH-1 downto 0 );
    signal test_pulse_t       : std_logic_vector( COUNTER_WIDTH-1 downto 0 );
    signal trigger_t          : std_logic_vector( COUNTER_WIDTH-1 downto 0 );
    signal orbit_reset_t      : std_logic_vector( COUNTER_WIDTH-1 downto 0 );
    signal fast_signal_40MHz  : std_logic_vector( 3 downto 0 );
    signal fsm                : fast_signal_generator_fsm_type;

    constant clk_period : time := 25 ns;
    signal   tb_count   : unsigned( COUNTER_WIDTH-1 downto 0) := (others => '0');
begin

    clk <= not clk after clk_period / 2;

    process (clk)
    begin
        if rising_edge(clk) then
            reset     <= '0';
            load_cnfg <= '0';
            start     <= '0';
            stop      <= '0';
            tb_count <= tb_count + 1;
            if ( tb_count = 1 ) then
                reset <= '1';
            elsif ( tb_count = 3 ) then
                Ncycle           <= (others => '0');
                cycle_T          <= std_logic_vector(to_unsigned(5, COUNTER_WIDTH));
                test_pulse_t     <= std_logic_vector(to_unsigned(2, COUNTER_WIDTH));
                trigger_t        <= std_logic_vector(to_unsigned(3, COUNTER_WIDTH));                
                orbit_reset_t    <= std_logic_vector(to_unsigned(4, COUNTER_WIDTH));  
            elsif ( tb_count = 5 ) then
                load_cnfg <= '1';
            elsif ( tb_count = 10 ) then
                start <= '1';
            elsif ( tb_count = 100 ) then
                tb_count <= (others =>'0');
            end if;
        end if;
    end process;
    
    fast_signal_generator_inst : entity work.fast_signal_generator
    generic map(  COUNTER_WIDTH => COUNTER_WIDTH )
    port map(
        clk                      => clk,
        reset                    => reset,
        load_cnfg                => load_cnfg,            
        start                    => start,
        stop                     => stop,
        Ncycle_i                 => Ncycle,
        fast_reset_en_i          => fast_reset_en,
        test_pulse_en_i          => test_pulse_en,
        trigger_en_i             => trigger_en,
        orbit_reset_en_i         => orbit_reset_en,
        cycle_T_i                => cycle_T,
        test_pulse_t_i           => test_pulse_t,
        trigger_t_i              => trigger_t,
        orbit_reset_t_i          => orbit_reset_t,
        fast_signal_40MHz        => fast_signal_40MHz,
        fsm_o                    => fsm
    );    


end Behavioral;
