----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03.11.2016 17:25:33
-- Design Name: 
-- Module Name: serial_command_generator_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;
use work.cbc_system_package.all;

entity serial_command_generator_tb is
--  Port ( );
end serial_command_generator_tb;

architecture Behavioral of serial_command_generator_tb is

    signal clk_320MHz                : std_logic := '1';
    signal clk_40MHz                 : std_logic := '1';
    signal ext_async_l1a             : std_logic := '0';
    signal fmc_cbc_fast_signal_40MHz : std_logic_vector(3 downto 0) := (others => '0');
    signal ipb_ctrl                  :  serial_command_generator_ctrl_type;
    signal ipb_cnfg                  :  serial_command_generator_cnfg_type;
    signal serial_command            :  std_logic;      
    signal ipb_stat                  :  serial_command_generator_stat_type;
    signal l1a_320MHz                :  std_logic;
    signal l1a_tdc_count             :  std_logic_vector(2 downto 0);

    constant clk_320MHz_period : time :=  3.125 ns;                    
    constant clk_40MHz_period  : time := 25.000 ns;
    signal   tb_count   : unsigned( 31 downto 0) := (others => '0');

begin

    ipb_cnfg.ext_async_l1a_en        <= '1';
    ipb_cnfg.fast_signal_fmc_en      <= '0';
    ipb_cnfg.fast_signal_ipbus_en    <= '0';
    ipb_cnfg.fast_signal_internal_en <= '0';
    ipb_cnfg.fast_signal_generator.fast_reset_en  <= '1';
    ipb_cnfg.fast_signal_generator.trigger_en     <= '1';
    ipb_cnfg.fast_signal_generator.test_pulse_en  <= '1';
    ipb_cnfg.fast_signal_generator.orbit_reset_en <= '1';
    ipb_cnfg.fast_signal_generator.Ncycle         <= (others => '0');        
    ipb_cnfg.fast_signal_generator.cycle_T        <= x"00000002";
    ipb_cnfg.fast_signal_generator.trigger_t      <= x"00000001";
    ipb_cnfg.fast_signal_generator.test_pulse_t   <= x"00000001";
    ipb_cnfg.fast_signal_generator.orbit_reset_t  <= x"00000001";


    clk_320MHz <= not clk_320MHz after clk_40MHz_period / 16;
    clk_40MHz  <= not clk_40MHz after clk_40MHz_period / 2;

    process begin
        ext_async_l1a <= '0';
        wait for 73 ns;
        ext_async_l1a <= '1';
        wait for 25 ns;
        ext_async_l1a <= '0';    
    end process;
 
    process (clk_320MHz)
    begin
        if rising_edge(clk_320MHz) then
            ipb_ctrl.reset           <= '0';
            ipb_ctrl.start_trigger   <= '0';
            ipb_ctrl.stop_trigger    <= '0';
            ipb_ctrl.cbc_fast_signal <= (others => '0');
            ipb_ctrl.fast_signal_generator.reset     <= '0';
            ipb_ctrl.fast_signal_generator.load_cnfg <= '0';
            ipb_ctrl.fast_signal_generator.start     <= '0';
            ipb_ctrl.fast_signal_generator.stop      <= '0';
            tb_count <= tb_count + 1;
            if ( tb_count = 1 ) then
                ipb_ctrl.reset <= '1';
            elsif ( tb_count = 5 ) then
                ipb_ctrl.fast_signal_generator.load_cnfg <= '1';
            elsif ( tb_count = 8 ) then
                ipb_ctrl.start_trigger <= '1';
            elsif ( tb_count = 10 ) then
                 ipb_ctrl.fast_signal_generator.start     <= '1';
            elsif ( tb_count = 100 ) then
                ipb_ctrl.stop_trigger <= '1';
                tb_count <= (others =>'0');
            end if;
        end if;
    end process;
    

                    
	--===========================================--
    serial_command_generator : entity work.serial_command_generator
    --===========================================--
    port map (
        clk_320MHz                                  => clk_320MHz,
        clk_40MHz                                   => clk_40MHz,
        ext_async_l1a                               => ext_async_l1a,
        fmc_cbc_fast_signal_40MHz                   => fmc_cbc_fast_signal_40MHz,
        ipb_ctrl                                    => ipb_ctrl,
        ipb_cnfg                                    => ipb_cnfg,
        serial_command                              => serial_command,          
        ipb_stat                                    => ipb_stat,
        l1a_320MHz                                  => l1a_320MHz,
        l1a_tdccount                                => l1a_tdc_count
    );


end Behavioral;
