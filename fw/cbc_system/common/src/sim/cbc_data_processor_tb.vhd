----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 14.11.2016 12:58:57
-- Design Name: 
-- Module Name: cbc_data_processor_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;
use work.cbc_system_package.all;
entity cbc_data_processor_tb is
--  Port ( );
end cbc_data_processor_tb;

architecture Behavioral of cbc_data_processor_tb is

	signal clk_320MHz                     : std_logic:= '1';
	signal clk_40MHz                      : std_logic:= '1';

    signal cbc_ser_data_set               : cbc_data_set_type;    
    -- ipbus signals
    signal ipb_cbc_system_stat            : ipb_cbc_system_stat_type;     
    signal ipb_cbc_system_cnfg            : ipb_cbc_system_cnfg_type;
    signal ipb_cbc_system_ctrl            : ipb_cbc_system_ctrl_type;    
    signal l1a_320MHz                     : std_logic;         
   -- signals btw cbc processors & event builder
    signal dsoebi_set                     : dsoebi_set_type;
    signal dsiebo_set                     : dsiebo_set_type;

    constant clk_320MHz_period : time :=  3.125 ns;     
    constant clk_40MHz_period  : time := 25.000 ns;

--    constant sdelay              : time := 0 ns;                      -- tuning type 1,      11 us to be tuned. delay = 13    
--    constant sdelay            : time := 0.7 ns;                     -- tuning_type = 1        4 us to be tuned. delay =  4
--    constant sdelay            : time := 0.91 ns;                    -- tuning type = 1     1.74 us to be tuned. delay =  1
--    constant sdelay            : time := 1.22 ns;                    -- tuning type = 2,     14  us to be tuned. delay =  0
--    constant sdelay            : time := clk_40MHz_period / 16;      -- tuning_type = 2,      11 us to be tuned. delay =  0
--      constant sdelay            : time := 2.40 ns;                    -- tuning type = 2,    2.51 us to be tuned. delay = 22
    constant sdelay            : time := 2.50 ns;                    -- tuning_type = 2 +20 1.74 us to be tuned. delay = 21
--    constant sdelay            : time := 2.525 ns;                   -- tuning type = 2     1.72 us to be tuned. delay = 21
--    constant sdelay              : time := 2.6ns;                      -- tuning type = 1        16 us to be tuned. delay = 20 
        
    signal   index   : unsigned( 2 downto 0) := (others => '0');
    signal   data_index : unsigned( 5 downto 0 ) := (others => '0');

begin

    clk_320MHz <= not clk_320MHz after clk_40MHz_period / 16;
    clk_40MHz  <= not clk_40MHz after clk_40MHz_period / 2;
    
    ipb_cbc_system_cnfg.global.cbcs(0).fe_id <= "001";
    ipb_cbc_system_cnfg.global.cbcs(0).id    <= "00001";
    
    
    
    process begin
        ipb_cbc_system_ctrl.cbc_data_processors(0).reset <= '0';
        ipb_cbc_system_ctrl.cbc_data_processors(0).cbc_ser_data_delay_reset <= '0';
        ipb_cbc_system_ctrl.cbc_data_processors(0).cbc_ser_data_delay_start_tuning <= '0';
        wait for 73 ns;
        ipb_cbc_system_ctrl.cbc_data_processors(0).reset <= '1';
        wait for 25 ns;
        ipb_cbc_system_ctrl.cbc_data_processors(0).reset <= '0';    
        wait for 100 ns;
        ipb_cbc_system_ctrl.cbc_data_processors(0).cbc_ser_data_delay_start_tuning <= '1';
        wait for  clk_320MHz_period;
        ipb_cbc_system_ctrl.cbc_data_processors(0).cbc_ser_data_delay_start_tuning <= '0';
        wait;
    end process;    

    process (clk_320MHz) 
    begin
        if rising_edge(clk_320MHz) then
            index <= index - 1;
 --           read_index <= read_index + 1;
            l1a_320MHz <= '0';            
            if index = 0 then
                cbc_ser_data_set(0)(5) <= '1' after sdelay;
                if data_index = 5 then
                    l1a_320MHz <= '1';
                end if;
                if data_index = 10 then
                    cbc_ser_data_set(0)(6) <= '1' after sdelay;
                end if;
            elsif index = 7 then
                cbc_ser_data_set(0)(5) <= '0' after sdelay;
               if data_index = 10 then
                     cbc_ser_data_set(0)(6) <= '1' after sdelay;
               elsif data_index = 44 then
                     cbc_ser_data_set(0)(6) <= '0' after sdelay;
               end if;
            elsif index = 6 then
                cbc_ser_data_set(0)(5) <= '0' after sdelay;                
               if data_index = 10 then
                     cbc_ser_data_set(0)(6) <= '0' after sdelay;
               elsif data_index = 44 then
                     cbc_ser_data_set(0)(6) <= '0' after sdelay;
               end if;
            elsif index = 5 then
                cbc_ser_data_set(0)(5) <= '0' after sdelay;
                if data_index = 10 then
                     cbc_ser_data_set(0)(6) <= '0' after sdelay;
                elsif data_index = 44 then
                     cbc_ser_data_set(0)(6) <= '0' after sdelay;
                end if;
            elsif index = 4 then
                cbc_ser_data_set(0)(5) <= '0' after sdelay;
                if data_index = 44 then
                     cbc_ser_data_set(0)(6) <= '0' after sdelay;
                end if;
            elsif index = 3 then
                cbc_ser_data_set(0)(5) <= '0' after sdelay;
            elsif index = 2 then
                cbc_ser_data_set(0)(5) <= '0' after sdelay;
            elsif index = 1 then
                cbc_ser_data_set(0)(5) <= '0' after sdelay;    
                data_index <= data_index + 1;
            end if;
       end if;
   end process;    



    cbc_data_processor_inst : entity work.cbc_data_processor(fc7)
    port map ( 
        be_id                    => ipb_cbc_system_cnfg.global.be_id,
        fe_id                    => ipb_cbc_system_cnfg.global.cbcs(0).fe_id,
        cbc_id                   => ipb_cbc_system_cnfg.global.cbcs(0).id,
        clk_320MHz               => clk_320MHz,
        clk_40MHz                => clk_40MHz,
        cbc_ser_data             => cbc_ser_data_set(0),
        ipb_ctrl                 => ipb_cbc_system_ctrl.cbc_data_processors(0),
        ipb_cnfg                 => ipb_cbc_system_cnfg.cbc_data_processors(0),
        ipb_stat_o               => ipb_cbc_system_stat.cbc_data_processors(0),
        l1a_320MHz               => l1a_320MHz,
        dsoebi                   => dsoebi_set(0),
        dsiebo                   => dsiebo_set(0)
    ); 

end Behavioral;
