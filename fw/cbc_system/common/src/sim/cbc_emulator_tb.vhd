----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04.11.2016 20:51:41
-- Design Name: 
-- Module Name: cbc_emulator_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;
use work.cbc_system_package.all;

entity cbc_emulator_tb is
--  Port ( );
end cbc_emulator_tb;

architecture Behavioral of cbc_emulator_tb is

    signal cbc_emulator_clk_320MHz        : std_logic := '1';
    signal cbc_emulator_in_serial_command : std_logic;
    signal cbc_emulator_out_ser_data      : std_logic_vector(6 downto 1);
    signal cbc_emulator_i2c_bus_mosi      : cbc_i2c_bus_mosi_type;
    signal cbc_emulator_i2c_bus_miso      : cbc_i2c_bus_miso_type;
    signal cbc_emulator_hard_reset        : std_logic;
    signal ipb_cnfg            : cbc_emulator_cnfg_type;
    signal ipb_ctrl            : cbc_emulator_ctrl_type;
    signal ipb_stat            : cbc_emulator_stat_type;

    constant clk_320MHz_period : time :=  3.125 ns;     
    constant sdelay            : time :=  1.22 ns;              
    constant trig_latency      : integer := 10;
    constant stub_latency      : integer := 2;
     
    signal   index               : unsigned( 2 downto 0) := (others => '0');    
    signal   trig_timer          : unsigned(7 downto 0) := (others => '0');
    signal   test_pulse_timer    : unsigned(7 downto 0) := (others => '0');   
    signal   waiting_for_trigger : std_logic:='0'; 
    signal   trigger_counter     : unsigned(3 downto 0) := (others => '0');
    signal   stop_trigger        : std_logic := '0';
begin

    cbc_emulator_i2c_bus_mosi.scl <= '1';
    cbc_emulator_i2c_bus_mosi.sda <= '1';    

    process (cbc_emulator_clk_320MHz, trigger_counter) 
    begin
        if trigger_counter = 5 then
            stop_trigger <= '1';  
            trigger_counter <= (others => '0');      
        elsif rising_edge(cbc_emulator_clk_320MHz) then
            

            index <= index - 1;
            if index = 0 then
                trig_timer <= trig_timer + 1;
                test_pulse_timer <= test_pulse_timer + 1;
                cbc_emulator_in_serial_command <= '1' after sdelay;
            elsif index = 7 then
                cbc_emulator_in_serial_command <= '1' after sdelay;
            elsif index = 6 then
                cbc_emulator_in_serial_command <= '0' after sdelay;                
            elsif index = 5 then
                cbc_emulator_in_serial_command <= '0' after sdelay;--fast reset
            elsif index = 4 then
                if  stop_trigger = '0' and waiting_for_trigger = '1' and trig_timer = trig_latency then
                    cbc_emulator_in_serial_command <= '1' after sdelay;--trigger
                    waiting_for_trigger <= '0';
                    trigger_counter <= trigger_counter + 1;
                else
                    cbc_emulator_in_serial_command <= '0' after sdelay;--trigger
                end if;
            elsif index = 3 then
                if test_pulse_timer = 20 then
                    cbc_emulator_in_serial_command <= '1' after sdelay;--test pulse
                    waiting_for_trigger <= '1';
                    trig_timer <= (others => '0');   
                    test_pulse_timer <= (others => '0');     
                else
                    cbc_emulator_in_serial_command <= '0' after sdelay;--test pulse   
                end if;
            elsif index = 2 then
                cbc_emulator_in_serial_command <= '0' after sdelay;--orbit reset
            elsif index = 1 then
                cbc_emulator_in_serial_command <= '1' after sdelay;    
            end if;

       end if;
   end process;

    cbc_emulator_clk_320MHz <= not cbc_emulator_clk_320MHz after clk_320MHz_period / 2;

    process begin
        ipb_ctrl.reset <= '0';
        wait for 73 ns;
        ipb_ctrl.reset <= '1';
        wait for 25 ns;
        ipb_ctrl.reset <= '0';    
        wait;
    end process;
 

	cbc_emulator_inst : entity work.cbc_emulator
    generic map( TRIG_LATENCY => trig_latency, STUB_LATENCY => stub_latency )
    port map
    (
       clk_320MHz                    => cbc_emulator_clk_320MHz,
       serial_command                => cbc_emulator_in_serial_command,
       slvs                          => cbc_emulator_out_ser_data,
       i2c_bus_mosi_set              => cbc_emulator_i2c_bus_mosi,
       i2c_bus_miso_set              => cbc_emulator_i2c_bus_miso,
       hard_reset                    => cbc_emulator_hard_reset,
       ipb_cnfg                      => ipb_cnfg,
       ipb_ctrl                      => ipb_ctrl,
       ipb_stat                      => ipb_stat
    );
    
end Behavioral;
