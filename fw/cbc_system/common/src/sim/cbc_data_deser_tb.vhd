----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 14.11.2016 12:21:21
-- Design Name: 
-- Module Name: cbc_data_deser_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;
use work.cbc_system_package.all;

entity cbc_data_deser_tb is
--  Port ( );
end cbc_data_deser_tb;

architecture Behavioral of cbc_data_deser_tb is

    signal reset                : std_logic;
    signal clk_320MHz           : std_logic := '1';
    signal delayed_cbc_ser_data : std_logic_vector(6 downto 1) := (others => '0');
    signal cbc_par_data         : cbc_par_data_type := (others => (others => '0'));
    signal cbc_par_data_update  : std_logic;
    signal fsm                  : cbc_data_deser_fsm_type;

    constant clk_320MHz_period : time :=  3.125 ns;     
    constant sdelay            : time :=  1.22 ns;               
    signal   index   : unsigned( 2 downto 0) := (others => '0');
    signal   data_index : unsigned( 5 downto 0 ) := (others => '0');
begin

    clk_320MHz <= not clk_320MHz after clk_320MHz_period / 2;

    process begin
        reset <= '0';
        wait for 73 ns;
        reset <= '1';
        wait for 25 ns;
        reset <= '0';    
        wait;
    end process;    

    process (clk_320MHz) 
    begin
        if rising_edge(clk_320MHz) then
            index <= index - 1;
            if index = 0 then
                delayed_cbc_ser_data(5) <= '1' after sdelay;
                if data_index = 10 then
                    delayed_cbc_ser_data(6) <= '1' after sdelay;
                end if;
            elsif index = 7 then
                delayed_cbc_ser_data(5) <= '0' after sdelay;
               if data_index = 10 then
                     delayed_cbc_ser_data(6) <= '1' after sdelay;
                 end if;
            elsif index = 6 then
                delayed_cbc_ser_data(5) <= '0' after sdelay;                
               if data_index = 10 then
                     delayed_cbc_ser_data(6) <= '0' after sdelay;
                 end if;
            elsif index = 5 then
                delayed_cbc_ser_data(5) <= '0' after sdelay;
                if data_index = 10 then
                     delayed_cbc_ser_data(6) <= '0' after sdelay;
                 end if;
            elsif index = 4 then
                delayed_cbc_ser_data(5) <= '0' after sdelay;
            elsif index = 3 then
                delayed_cbc_ser_data(5) <= '0' after sdelay;
            elsif index = 2 then
                delayed_cbc_ser_data(5) <= '0' after sdelay;
            elsif index = 1 then
                delayed_cbc_ser_data(5) <= '0' after sdelay;    
                data_index <= data_index + 1;
            end if;
       end if;
   end process;    

    cbc_data_deser_inst : entity work.cbc_data_deser
    port map(
        reset           => reset,
        clk_320MHz      => clk_320MHz,
        ser_data        => delayed_cbc_ser_data,
        par_data_o      => cbc_par_data,
        par_data_update => cbc_par_data_update,
        fsm_o           => fsm
    );
    
end Behavioral;
