#===========================--
# Temporary IOs. To be updated.  
#
# 28.10.2016 Kirika Uchida
#===========================--

#set_property PACKAGE_PIN AP29    [get_ports {fmc_l8_la_p[7]}]
#set_property IOSTANDARD LVDS_25 [get_ports {fmc_l8_la_p[7]}]
set_property PACKAGE_PIN AN24   [get_ports {fmc_l8_la_p[4]}]
set_property IOSTANDARD LVDS_25 [get_ports {fmc_l8_la_p[4]}]
set_property PACKAGE_PIN AK24   [get_ports {fmc_l8_la_p[2]}]
set_property IOSTANDARD LVDS_25 [get_ports {fmc_l8_la_p[2]}]
#set_property PACKAGE_PIN AP30    [get_ports {fmc_l8_la_n[7]}]
#set_property IOSTANDARD LVDS_25 [get_ports {fmc_l8_la_n[7]}]
set_property PACKAGE_PIN AP24    [get_ports {fmc_l8_la_n[4]}]
set_property IOSTANDARD LVDS_25 [get_ports {fmc_l8_la_n[4]}]
set_property PACKAGE_PIN AL24    [get_ports {fmc_l8_la_n[2]}]
set_property IOSTANDARD LVDS_25 [get_ports {fmc_l8_la_n[2]}]

set_property PACKAGE_PIN B30    [get_ports {fmc_l12_la_p[31]}]
set_property IOSTANDARD LVDS_25 [get_ports {fmc_l12_la_p[31]}]
set_property PACKAGE_PIN D29    [get_ports {fmc_l12_la_p[30]}]
set_property IOSTANDARD LVDS_25 [get_ports {fmc_l12_la_p[30]}]
set_property PACKAGE_PIN F29    [get_ports {fmc_l12_la_p[28]}]
set_property IOSTANDARD LVDS_25 [get_ports {fmc_l12_la_p[28]}]
set_property PACKAGE_PIN H32     [get_ports {fmc_l12_la_p[27]}]
set_property IOSTANDARD LVDS_25 [get_ports {fmc_l12_la_p[27]}]
set_property PACKAGE_PIN H29     [get_ports {fmc_l12_la_p[26]}]
set_property IOSTANDARD LVDS_25 [get_ports {fmc_l12_la_p[26]}]
set_property PACKAGE_PIN L33     [get_ports {fmc_l12_la_p[23]}]
set_property IOSTANDARD LVDS_25 [get_ports {fmc_l12_la_p[23]}]
set_property PACKAGE_PIN D34     [get_ports {fmc_l12_la_p[20]}]
set_property IOSTANDARD LVDS_25 [get_ports {fmc_l12_la_p[20]}]
set_property PACKAGE_PIN B33     [get_ports {fmc_l12_la_p[19]}]
set_property IOSTANDARD LVDS_25 [get_ports {fmc_l12_la_p[19]}]
set_property PACKAGE_PIN E31     [get_ports {fmc_l12_la_p[18]}]
set_property IOSTANDARD LVDS_25 [get_ports {fmc_l12_la_p[18]}]
set_property PACKAGE_PIN E32     [get_ports {fmc_l12_la_p[17]}]
set_property IOSTANDARD LVDS_25 [get_ports {fmc_l12_la_p[17]}]
set_property PACKAGE_PIN H33     [get_ports {fmc_l12_la_p[16]}]
set_property IOSTANDARD LVDS_25 [get_ports {fmc_l12_la_p[16]}]
set_property PACKAGE_PIN J34     [get_ports {fmc_l12_la_p[15]}]
set_property IOSTANDARD LVDS_25 [get_ports {fmc_l12_la_p[15]}]
set_property PACKAGE_PIN F33     [get_ports {fmc_l12_la_p[14]}]
set_property IOSTANDARD LVDS_25 [get_ports {fmc_l12_la_p[14]}]
set_property PACKAGE_PIN F34     [get_ports {fmc_l12_la_p[13]}]
set_property IOSTANDARD LVDS_25 [get_ports {fmc_l12_la_p[13]}]
set_property PACKAGE_PIN L34     [get_ports {fmc_l12_la_p[11]}]
set_property IOSTANDARD LVDS_25 [get_ports {fmc_l12_la_p[11]}]
set_property PACKAGE_PIN P30     [get_ports {fmc_l12_la_p[10]}]
set_property IOSTANDARD LVDS_25 [get_ports {fmc_l12_la_p[10]}]
set_property PACKAGE_PIN M30     [get_ports {fmc_l12_la_p[9]}]
set_property IOSTANDARD LVDS_25 [get_ports {fmc_l12_la_p[9]}]
#set_property PACKAGE_PIN T33     [get_ports {fmc_l12_la_p[8]}]
#set_property IOSTANDARD LVDS_25 [get_ports {fmc_l12_la_p[8]}]
#set_property PACKAGE_PIN P34     [get_ports {fmc_l12_la_p[7]}]
#set_property IOSTANDARD LVDS_25 [get_ports {fmc_l12_la_p[7]}]
#set_property PACKAGE_PIN R33     [get_ports {fmc_l12_la_p[6]}]
#set_property IOSTANDARD LVDS_25 [get_ports {fmc_l12_la_p[6]}]
#set_property PACKAGE_PIN N33     [get_ports {fmc_l12_la_p[5]}]
#set_property IOSTANDARD LVDS_25 [get_ports {fmc_l12_la_p[5]}]
set_property PACKAGE_PIN U32    [get_ports {fmc_l12_la_p[4]}]
set_property IOSTANDARD LVDS_25 [get_ports {fmc_l12_la_p[4]}]
set_property PACKAGE_PIN T30    [get_ports {fmc_l12_la_p[2]}]
set_property IOSTANDARD LVDS_25 [get_ports {fmc_l12_la_p[2]}]
set_property PACKAGE_PIN T28    [get_ports {fmc_l12_la_p[1]}]
set_property IOSTANDARD LVDS_25 [get_ports {fmc_l12_la_p[1]}]
#set_property PACKAGE_PIN R28     [get_ports {fmc_l12_la_p[0]}]
#set_property IOSTANDARD LVDS_25 [get_ports {fmc_l12_la_p[0]}]
#
set_property PACKAGE_PIN A31     [get_ports {fmc_l12_la_n[31]}]
set_property IOSTANDARD LVDS_25 [get_ports {fmc_l12_la_n[31]}]
set_property PACKAGE_PIN D30     [get_ports {fmc_l12_la_n[30]}]
set_property IOSTANDARD LVDS_25 [get_ports {fmc_l12_la_n[30]}]
set_property PACKAGE_PIN E29     [get_ports {fmc_l12_la_n[28]}]
set_property IOSTANDARD LVDS_25 [get_ports {fmc_l12_la_n[28]}]
set_property PACKAGE_PIN G32     [get_ports {fmc_l12_la_n[27]}]
set_property IOSTANDARD LVDS_25 [get_ports {fmc_l12_la_n[27]}]
set_property PACKAGE_PIN H30     [get_ports {fmc_l12_la_n[26]}]
set_property IOSTANDARD LVDS_25 [get_ports {fmc_l12_la_n[26]}]
set_property PACKAGE_PIN E29     [get_ports {fmc_l12_la_n[28]}]
set_property PACKAGE_PIN K33     [get_ports {fmc_l12_la_n[23]}]
set_property IOSTANDARD LVDS_25 [get_ports {fmc_l12_la_n[23]}]
set_property PACKAGE_PIN C34     [get_ports {fmc_l12_la_n[20]}]
set_property IOSTANDARD LVDS_25 [get_ports {fmc_l12_la_n[20]}]
set_property PACKAGE_PIN A33     [get_ports {fmc_l12_la_n[19]}]
set_property IOSTANDARD LVDS_25 [get_ports {fmc_l12_la_n[19]}]
set_property PACKAGE_PIN D31     [get_ports {fmc_l12_la_n[18]}]
set_property IOSTANDARD LVDS_25 [get_ports {fmc_l12_la_n[18]}]
set_property PACKAGE_PIN D32     [get_ports {fmc_l12_la_n[17]}]
set_property IOSTANDARD LVDS_25 [get_ports {fmc_l12_la_n[17]}]
set_property PACKAGE_PIN G33     [get_ports {fmc_l12_la_n[16]}]
set_property IOSTANDARD LVDS_25 [get_ports {fmc_l12_la_n[16]}]
set_property PACKAGE_PIN H34     [get_ports {fmc_l12_la_n[15]}]
set_property IOSTANDARD LVDS_25 [get_ports {fmc_l12_la_n[15]}]
set_property PACKAGE_PIN E33     [get_ports {fmc_l12_la_n[14]}]
set_property IOSTANDARD LVDS_25 [get_ports {fmc_l12_la_n[14]}]
set_property PACKAGE_PIN E34     [get_ports {fmc_l12_la_n[13]}]
set_property IOSTANDARD LVDS_25 [get_ports {fmc_l12_la_n[13]}]
set_property PACKAGE_PIN K34     [get_ports {fmc_l12_la_n[11]}]
set_property IOSTANDARD LVDS_25 [get_ports {fmc_l12_la_n[11]}]
set_property PACKAGE_PIN N30     [get_ports {fmc_l12_la_n[10]}]
set_property IOSTANDARD LVDS_25 [get_ports {fmc_l12_la_n[10]}]
set_property PACKAGE_PIN M31     [get_ports {fmc_l12_la_n[9]}]
set_property IOSTANDARD LVDS_25 [get_ports {fmc_l12_la_n[9]}]

#set_property PACKAGE_PIN T34     [get_ports {fmc_l12_la_n[8]}]
#set_property IOSTANDARD LVDS_25 [get_ports {fmc_l12_la_n[8]}]
#set_property PACKAGE_PIN N34     [get_ports {fmc_l12_la_n[7]}]
#set_property IOSTANDARD LVDS_25 [get_ports {fmc_l12_la_n[7]}]
#set_property PACKAGE_PIN R34     [get_ports {fmc_l12_la_n[6]}]
#set_property IOSTANDARD LVDS_25 [get_ports {fmc_l12_la_n[6]}]
#set_property PACKAGE_PIN M33     [get_ports {fmc_l12_la_n[5]}]
#set_property IOSTANDARD LVDS_25 [get_ports {fmc_l12_la_n[5]}]
set_property PACKAGE_PIN U33     [get_ports {fmc_l12_la_n[4]}]
set_property IOSTANDARD LVDS_25 [get_ports {fmc_l12_la_n[4]}]
set_property PACKAGE_PIN T31     [get_ports {fmc_l12_la_n[2]}]
set_property IOSTANDARD LVDS_25 [get_ports {fmc_l12_la_n[2]}]
set_property PACKAGE_PIN T29     [get_ports {fmc_l12_la_n[1]}]
set_property IOSTANDARD LVDS_25 [get_ports {fmc_l12_la_n[1]}]
#set_property PACKAGE_PIN R29     [get_ports {fmc_l12_la_n[0]}]
#set_property IOSTANDARD LVDS_25 [get_ports {fmc_l12_la_n[0]}]

set_property PACKAGE_PIN R31   [get_ports {fmc_l12_clk0}]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l12_clk0}]

set_property PACKAGE_PIN G30   [get_ports {fmc_l12_clk1}]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l12_clk1}]

set_property DIFF_TERM TRUE [get_ports {fmc_l12_la_p[9]}]
set_property DIFF_TERM TRUE [get_ports {fmc_l12_la_p[10]}]
set_property DIFF_TERM TRUE [get_ports {fmc_l12_la_p[15]}]
set_property DIFF_TERM TRUE [get_ports {fmc_l12_la_p[20]}]
set_property DIFF_TERM TRUE [get_ports {fmc_l12_la_p[11]}]
set_property DIFF_TERM TRUE [get_ports {fmc_l12_la_p[16]}]
set_property DIFF_TERM TRUE [get_ports {fmc_l12_la_p[19]}]
set_property DIFF_TERM TRUE [get_ports {fmc_l12_la_p[23]}]
set_property DIFF_TERM TRUE [get_ports {fmc_l12_la_p[26]}]
set_property DIFF_TERM TRUE [get_ports {fmc_l12_la_p[27]}]
set_property DIFF_TERM TRUE [get_ports {fmc_l12_la_p[13]}]
set_property DIFF_TERM TRUE [get_ports {fmc_l12_la_p[14]}]

#set_property DIFF_TERM TRUE [get_ports {fmc_l12_la_p[0]}]

set_clock_groups -asynchronous \
    -group [get_clocks -include_generated_clocks eth_txoutclk] \
    -group [get_clocks -include_generated_clocks osc125_a] \
    -group [get_clocks -include_generated_clocks osc125_b] \
    -group [get_clocks -include_generated_clocks fabric_clk]

set_clock_groups -asynchronous -group {clkout0 clk_out1_ipb_cllk_mmcm} \
                               -group {clk_out1_cbc_system_clocks_0 clk_out2_cbc_system_clocks_0 clk_out4_cbc_system_clocks_0} \
                               -group {clk_out3_cbc_system_clocks_0} \ 
                               -group {clk_out1_cbc_system_clocks_1 clk_out2_cbc_system_clocks_1} 
                                                              
set_property IODELAY_GROUP IODELAY_GROUP_0 [get_cells usr/cbc_system_inst/io_map/data_iserdes_gen[0].data_iserdes_inst/data_iserdes_inst/inst/*delay*]
set_property IODELAY_GROUP IODELAY_GROUP_0 [get_cells usr/cbc_system_inst/io_map/data_iserdes_gen[1].data_iserdes_inst/data_iserdes_inst/inst/*delay*]
