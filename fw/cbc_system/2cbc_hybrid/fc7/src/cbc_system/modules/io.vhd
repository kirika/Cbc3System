--===========================--
-- io 
-- Temporary IOs.  To be updated.
-- 28.10.2016 Kirika Uchida
--===========================--

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

use work.cbc_system_package.all;

entity io is
port ( 
    ipb_cnfg_cbc_cnfg_set               : in       cbc_cnfg_set_type;
    ipb_ctrl                            : in       io_ctrl_type;
    ipb_cnfg                            : in       io_cnfg_type;
    ipb_stat                            : out      io_stat_type;
    fmc_l8_la_p			        : inout	   std_logic_vector(33 downto 0);
    fmc_l8_la_n                         : inout    std_logic_vector(33 downto 0);
    fmc_l12_la_p                        : inout    std_logic_vector(33 downto 0);
    fmc_l12_la_n                        : inout    std_logic_vector(33 downto 0);
    fmc_l12_clk0                        : in       std_logic;
    fmc_l12_clk1                        : in       std_logic;
    clk_200MHz                          : in       std_logic;
    clk_40MHz_cbc_ctrl                  : in       std_logic;
    clk_320MHz_cbc_ctrl                 : in       std_logic;
    clk_40MHz_iserdes_ctrl              : in       std_logic_vector(NFE-1 downto 0);
    clk_320MHz_iserdes_ctrl             : in       std_logic_vector(NFE-1 downto 0);
    mmcme2_drp_sclk                     : in       std_logic;
    ipb_clk                             : in       std_logic;
    cbc_fast_signal_par                 : in       std_logic_vector(7 downto 0);
    cbc_i2c_bus_mosi_set                : in       cbc_i2c_bus_mosi_set_type;
    cbc_hard_reset                      : in       std_logic;
    external_l1a_veto                   : in       std_logic;
    ext_async_l1a                       : out      std_logic;
    cbc_par_data_set_o                  : out      cbc_par_data_set_type;
    cbc_i2c_bus_miso_set                : out      cbc_i2c_bus_miso_set_type;
    fmc_ext_clk_40MHz                   : out      std_logic;
    test_signal_out                     : in       std_logic_vector(1 downto 0);
    sigs_to_mmcme2_drp_set              : out     sigs_to_mmcme2_drp_set_type;
    sigs_from_mmcme2_drp_set            : in      sigs_from_mmcme2_drp_set_type;
    io_test_out                         : out      std_logic;
	dctsb_dout                          : out      std_logic_vector(31 downto 0);
	dctsb_read_next                     : in       std_logic
);
end io;

architecture Behavioral of io is

    type fe_index_type is array (0 to NCBC-1) of integer range 0 to NFE-1;
    constant cbc_to_fe_index            : fe_index_type := ( 0, 0 );

    -- fast signal iserdes out      
    signal data_out_to_pins_p           : std_logic_vector(0 to NFE-1);
    signal data_out_to_pins_n           : std_logic_vector(0 to NFE-1);    
    signal clk_to_pins_p                : std_logic_vector(0 to NFE-1);
    signal clk_to_pins_n                : std_logic_vector(0 to NFE-1);    
    
    type data_from_pins_type is array (0 to NCBC-1) of std_logic_vector(6 downto 1); 
    signal data_in_from_pins_p            : data_from_pins_type;
    signal data_in_from_pins_n            : data_from_pins_type;

    type data_in_to_device_set_type is array(0 to NCBC-1) of std_logic_vector(8*6-1 downto 0);
    signal data_in_to_device_set          : data_in_to_device_set_type;

    signal in_delay_tap_out               : in_delay_tap_out_type;
    
    signal in_delay_tap_out_to_dctt       : in_delay_tap_out_to_dctt_type;   
    signal dctt_to_iserdes_set            : dctt_to_iserdes_set_type;       

    signal sigs_to_mmcme2_drp_from_dctt_set  : sigs_to_mmcme2_drp_set_type;

--    signal bitslip_set                    : bitslip_set_type;                
    signal data_clock_timing_tuning_done  : std_logic_vector(NFE-1 downto 0);
    signal data_clock_timing_pulse_scan_done : std_logic_vector(NFE-1 downto 0);
--    signal idelay_locked                   : std_logic_vector(NCBC-1 downto 0);


    signal cbc_par_data_set   : cbc_par_data_set_type;
    
    signal bram_to_dcts_set   : bram_to_dcts_set_type;
    signal dcts_to_bram_set   : dcts_to_bram_set_type;
    signal dcts_to_bram_set_sync : dcts_to_bram_set_type;
    
    signal data_clock_timing_tuning_test_out : std_logic_vector(NCBC-1 downto 0);

    type cbc_par_data_slvs5_for_dctt_type is array(0 to NFE-1) of std_logic_vector(7 downto 0);
    signal cbc_par_data_slvs5_for_dctt : cbc_par_data_slvs5_for_dctt_type;
    
    type fe_par_slvs5_set_type is array (0 to NFE-1) of std_logic_vector(4*8-1 downto 0);
    type fe_in_delay_tap_out_set_type is array (0 to NFE-1) of std_logic_vector(4*5*6-1 downto 0);

    signal fe_par_slvs5_set           : fe_par_slvs5_set_type;
    signal fe_in_delay_tap_out_set    : fe_in_delay_tap_out_set_type;

    attribute IODELAY_GROUP : string;
    attribute IODELAY_GROUP of IDELAYCTRL_inst_0 : label is "IODELAY_GROUP_0";
--    attribute IODELAY_GROUP of IDELAYCTRL_inst_1 : label is "IODELAY_GROUP_1";    
    
begin

--    io_test_out <= idelay_locked(0);
--    idelay_locked_o <= idelay_locked(0);
    io_test_out <= data_clock_timing_tuning_test_out(0);
    
    fast_signal_oserdes_gen:
    for i in 0 to NFE-1 generate
    --===================--       
    fast_signal_oserdes_cbc0_inst : entity work.fast_signal_oserdes
    --===================--   
       port map 
       ( 
       data_out_from_device =>  bitswap(cbc_fast_signal_par),
       data_out_to_pins_p(0) => data_out_to_pins_p(i),
       data_out_to_pins_n(0) => data_out_to_pins_n(i),
       clk_to_pins_p         => clk_to_pins_p(i),
       clk_to_pins_n         => clk_to_pins_n(i),
       clk_in                => clk_320MHz_cbc_ctrl,                            
       clk_div_in            => clk_40MHz_cbc_ctrl,                        
       clk_reset             => ipb_ctrl.fs_oserdes_reset,        
       io_reset              => ipb_ctrl.fs_oserdes_reset
    );
    end generate;

    cbc_par_data_set_o <= cbc_par_data_set;

    fe_par_slvs5_set(0)(8 * 1 - 1 downto     0) <= cbc_par_data_set(0)(5);
    fe_par_slvs5_set(0)(8 * 2 - 1 downto 8 * 1) <= cbc_par_data_set(1)(5);    
             
    fe_in_delay_tap_out_set(0)(5*6*1-1 downto     0) <= in_delay_tap_out(0);
    fe_in_delay_tap_out_set(0)(5*6*2-1 downto 5*6*1) <= in_delay_tap_out(1);
    
    --===========================================--    
    data_iserdes_gen :
	--===========================================--
    for i in 0 to NCBC-1 generate 

        -- data mapping of iserdes output 
        cbc_para_data_set_gen : for j in 1 to 6 generate

            slice_gen : for k in 0 to 7 generate
                cbc_par_data_set(i)(j)(7-k) <= data_in_to_device_set(i)(6*k+j-1);
            end generate;
        end generate;
        ipb_stat.cbcios(i).slvs5 <= cbc_par_data_set(i)(5);
        
        ipb_stat.cbcios(i).in_delay_tap_out <= in_delay_tap_out(i);         
        -- iserdes 
        data_iserdes_inst : entity work.data_iserdes_pre
        port map 
        ( 
        data_in_from_pins_p => data_in_from_pins_p(i),
        data_in_from_pins_n => data_in_from_pins_n(i),
        data_in_to_device   => data_in_to_device_set(i),
        in_delay_reset      => dctt_to_iserdes_set(cbc_to_fe_index(i)).in_delay_reset,                    
        in_delay_data_ce    => dctt_to_iserdes_set(cbc_to_fe_index(i)).in_delay_ce,      
        in_delay_data_inc   => dctt_to_iserdes_set(cbc_to_fe_index(i)).in_delay_data_inc,     
        in_delay_tap_in     => dctt_to_iserdes_set(cbc_to_fe_index(i)).in_delay_tap_in,          
        in_delay_tap_out    => in_delay_tap_out(i),                  
        bitslip             => dctt_to_iserdes_set(cbc_to_fe_index(i)).bitslip,                                  
        clk_in              => clk_320MHz_iserdes_ctrl(cbc_to_fe_index(i)),                            
        clk_div_in          => clk_40MHz_iserdes_ctrl(cbc_to_fe_index(i)),                        
        io_reset            => dctt_to_iserdes_set(cbc_to_fe_index(i)).iserdes_io_reset
        );
       
    end generate;       


   IDELAYCTRL_inst_0 : IDELAYCTRL
   port map (
      RDY => open,       -- 1-bit output: Ready output
      REFCLK => clk_200MHz, -- 1-bit input: Reference clock input
      RST => '0'        -- 1-bit input: Active high reset input
   );
--   IDELAYCTRL_inst_1 : IDELAYCTRL
--   port map (
--      RDY => open,       -- 1-bit output: Ready output
--      REFCLK => clk_200MHz, -- 1-bit input: Reference clock input
--      RST => '0'        -- 1-bit input: Active high reset input
--   );

	

    -- tuning
    dctt0_gen:    
    for j in 0 to NFE-1 generate 
       
        data_clock_timing_tuning_inst : entity work.data_clock_timing_tuning
        generic map( TUNE_CLOCK => true )
            port map
            (
            clk_40MHz_iserdes_ctrl    => clk_40MHz_iserdes_ctrl(j), 
            ipb_cnfg                  => ipb_cnfg.data_clock_timing_tuning,
            reset                     => ipb_ctrl.data_clock_timing_tunings(j).reset,
            tune                      => ipb_ctrl.data_clock_timing_tunings(j).tune,
            scan_pulse                => ipb_ctrl.data_clock_timing_tunings(j).scan_pulse,
            sync_pattern              => "10000000",                
            mmcme2_drp_sclk           => mmcme2_drp_sclk,
            mmcme2_drp_srdy           => sigs_from_mmcme2_drp_set(j).srdy,                
            mmcme2_drp_current_saddr  => sigs_from_mmcme2_drp_set(j).saddr,
--            in_delay_tap_out          => in_delay_tap_out(tune_cbc_index(j)),
--            cbc_par_data_slvs5        => cbc_par_data_set(tune_cbc_index(j))(5),
            in_delay_tap_out          => in_delay_tap_out(get_cbc_index(ipb_cnfg_cbc_cnfg_set, ipb_cnfg.fes(j).clock_timing_tuning_cbc_sel)),
            cbc_par_data_slvs5        => cbc_par_data_set(get_cbc_index(ipb_cnfg_cbc_cnfg_set, ipb_cnfg.fes(j).clock_timing_tuning_cbc_sel))(5),                            
            bram_to_dcts              => bram_to_dcts_set(j),             
            fsm_o                     => ipb_stat.dctts(j).fsm,
            iserdes_idelay_ctrl_fsm_o => ipb_stat.dctts(j).iserdes_idelay_ctrl_fsm,
            dctt_to_iserdes           => dctt_to_iserdes_set(j),
--            in_delay_reset_o          => in_delay_reset(j),
--            in_delay_ce_o             => in_delay_data_ce_set(j),
--            in_delay_tap_in_o         => in_delay_tap_in(j),
--            in_delay_data_inc         => in_delay_data_inc_set(j),
--            iserdes_io_reset_o        => data_iserdes_io_reset(j),                
--            bitslip_o                 => bitslip_set(j),
            mmcme2_drp_rst_o          => sigs_to_mmcme2_drp_from_dctt_set(j).rst,
            mmcme2_drp_sen_o          => sigs_to_mmcme2_drp_from_dctt_set(j).sen,
            mmcme2_drp_saddr_o        => sigs_to_mmcme2_drp_from_dctt_set(j).saddr,
--            pattern_o                 => ipb_stat.cbcios(j).pattern,   
            bitslip_counter_o         => ipb_stat.dctts(j).iserdes_bitslip_counter,
            done                      => data_clock_timing_tuning_done(j),
            pulse_scan_done_o         => data_clock_timing_pulse_scan_done(j),
--            tuning_stat_set           => ipb_stat.dctts(j).tuning_stat_set,
            dcts_to_bram              => dcts_to_bram_set(j),                          
            test_out                  => data_clock_timing_tuning_test_out(j)       
            );     

        sigs_to_mmcme2_drp_set(j).sclk  <= mmcme2_drp_sclk;
        -- this part may not be needed any more.
        process (mmcme2_drp_sclk)
        begin           
            if rising_edge(mmcme2_drp_sclk) then
                sigs_to_mmcme2_drp_set(j).rst <= '0';
                sigs_to_mmcme2_drp_set(j).sen <= '0';
                if sigs_to_mmcme2_drp_from_dctt_set(j).sen = '1' then
                    sigs_to_mmcme2_drp_set(j).saddr <= sigs_to_mmcme2_drp_from_dctt_set(j).saddr;
                    sigs_to_mmcme2_drp_set(j).sen   <= '1';
                end if;
                if sigs_to_mmcme2_drp_from_dctt_set(j).rst = '1' then
                    sigs_to_mmcme2_drp_set(j).rst <= '1';
                end if;
            end if;
        end process;        
    end generate;       

   cbc_slvs5_scan_bram_gen :
   for i in 0 to NFE - 1 generate          

        process (clk_40MHz_cbc_ctrl)
        begin
            if rising_edge(clk_40MHz_cbc_ctrl) then
                dcts_to_bram_set_sync(i).wen <= '0';
                if dcts_to_bram_set(i).wen = '1' then
                    dcts_to_bram_set_sync(i).wen   <= '1';
                    dcts_to_bram_set_sync(i).waddr <= dcts_to_bram_set(i).waddr;
                    dcts_to_bram_set_sync(i).din <= dcts_to_bram_set(i).din;                    
                end if;
            end if;
        end process;
    end generate;        

    cbc_data_clock_timing_scan_bram_ctrl_inst : entity work.cbc_data_clock_timing_scan_bram_ctrl
    port map(   clk_40MHz           => clk_40MHz_cbc_ctrl,
                ipb_clk             => ipb_clk,
                reset               => ipb_ctrl.dctsb_reset,
                bram_to_dcts_set    => bram_to_dcts_set,
                dcts_to_bram_set    => dcts_to_bram_set,
                bram_dout           => dctsb_dout,
    			bram_read_next      => dctsb_read_next
    );
    

    --===================--   
    -- test signal out
    --===================--    
    test_signal_0_l8            : obufds port map ( i=>test_signal_out(0), o=>fmc_l8_la_p(2), ob=>fmc_l8_la_n(2) );
    test_signal_1_l8            : obufds port map ( i=>test_signal_out(1), o=>fmc_l8_la_p(4), ob=>fmc_l8_la_n(4) );
    test_signal_0_l12           : obufds port map ( i=>test_signal_out(0), o=>fmc_l12_la_p(2), ob=>fmc_l12_la_n(2) );
    test_signal_1_l12           : obufds port map ( i=>test_signal_out(1), o=>fmc_l12_la_p(4), ob=>fmc_l12_la_n(4) );
    --===================--   
    -- cbc hard reset
    --===================--      
    fmcl12_la_obuf_hard_reset : obufds port map ( i=>cbc_hard_reset, o=>fmc_l12_la_p(28), ob=>fmc_l12_la_n(28) );    
    --===================--  
    -- data in
    --===================--  

    data_in_from_pins_p(0)(1) <= fmc_l12_la_p(9);
    data_in_from_pins_p(0)(2) <= fmc_l12_la_p(10);
    data_in_from_pins_p(0)(3) <= fmc_l12_la_p(15);
    data_in_from_pins_p(0)(4) <= fmc_l12_la_p(20);
    data_in_from_pins_p(0)(5) <= fmc_l12_la_p(11);
    data_in_from_pins_p(0)(6) <= fmc_l12_la_p(16);
    data_in_from_pins_n(0)(1) <= fmc_l12_la_n(9);
    data_in_from_pins_n(0)(2) <= fmc_l12_la_n(10);
    data_in_from_pins_n(0)(3) <= fmc_l12_la_n(15);
    data_in_from_pins_n(0)(4) <= fmc_l12_la_n(20);
    data_in_from_pins_n(0)(5) <= fmc_l12_la_n(11);
    data_in_from_pins_n(0)(6) <= fmc_l12_la_n(16);     

    data_in_from_pins_p(1)(1) <= fmc_l12_la_p(19);
    data_in_from_pins_p(1)(2) <= fmc_l12_la_p(23);
    data_in_from_pins_p(1)(3) <= fmc_l12_la_p(26);
    data_in_from_pins_p(1)(4) <= fmc_l12_la_p(27);
    data_in_from_pins_p(1)(5) <= fmc_l12_la_p(13);
    data_in_from_pins_p(1)(6) <= fmc_l12_la_p(14);
    data_in_from_pins_n(1)(1) <= fmc_l12_la_n(19);
    data_in_from_pins_n(1)(2) <= fmc_l12_la_n(23);
    data_in_from_pins_n(1)(3) <= fmc_l12_la_n(26);
    data_in_from_pins_n(1)(4) <= fmc_l12_la_n(27);
    data_in_from_pins_n(1)(5) <= fmc_l12_la_n(13);
    data_in_from_pins_n(1)(6) <= fmc_l12_la_n(14);     

    -- 2, 4, 7, 11, 16, 28        
    --===================--                       
    -- i2c
    --===================--  
    fmcl12_la_obuf_cbc_out_sda_set_1         : ibufds generic map( DIFF_TERM => TRUE ) port map (i=>fmc_l12_la_p(31), ib=>fmc_l12_la_n(31), o=> cbc_i2c_bus_miso_set(0).sda ); 	
    fmcl12_la_ibuf_cbc_in_sda_set_1          : obufds port map ( i=>cbc_i2c_bus_mosi_set(0).sda, o=>fmc_l12_la_p(30), ob=>fmc_l12_la_n(30) );
    fmcl12_la_ibuf_cbc_in_scl_set_1          : obufds port map ( i=>cbc_i2c_bus_mosi_set(0).scl, o=>fmc_l12_la_p(1), ob=>fmc_l12_la_n(1) );
    --===================--  
    -- fast signals
    --===================--   
    fmc_l12_la_p(17) <= data_out_to_pins_p(0);
    fmc_l12_la_n(17) <= data_out_to_pins_n(0);
    fmc_l12_la_p(18) <= clk_to_pins_p(0);
    fmc_l12_la_n(18) <= clk_to_pins_n(0); 
    --===================--  
    -- external asynchronous l1a signal
    --===================--  
    fmc12_la_ibuf_async_l1a : ibuf port map (i => fmc_l12_clk0, o => ext_async_l1a);
 	fmc_l12_ext_clk         : ibufg port map(i => fmc_l12_clk1, o => fmc_ext_clk_40MHz);

end Behavioral;
