--===========================--
-- ipb_cbc_system_cnfg_sel
-- ipb_cbc_system_ctrl_sel
-- ipb_cbc_system_stat_sel
-- ipb_cbc_system_event_data_sel
-- ipb_cbc_system_cbc_i2c_regs_sel 
-- are added.
--
-- 28.10.2016 Kirika Uchida
--===========================--

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;
use work.ipbus.all;
use work.user_package.all;

package user_addr_decode is

--	function user_wb_addr_sel (signal addr : in std_logic_vector(31 downto 0)) return integer;
	function user_ipb_addr_sel(signal addr : in std_logic_vector(31 downto 0)) return integer;

end user_addr_decode;

package body user_addr_decode is

	function user_ipb_addr_sel(signal addr : in std_logic_vector(31 downto 0)) return integer is
		variable sel : integer range 0 to 99;
	begin
		--              addr, "00------------------------------" is reserved (system ipbus fabric)
		if    std_match(addr, "01000000000000000000000000------") then  	sel := user_ipb_stat_regs;
		elsif std_match(addr, "01000000000000000000000001------") then  	sel := user_ipb_ctrl_regs;
		elsif std_match(addr, "010000000000000000000001--------") then  	sel := ipb_cbc_system_cnfg_sel;
		elsif std_match(addr, "010000000000000000000010--------") then  	sel := ipb_cbc_system_ctrl_sel;	
		elsif std_match(addr, "010000000000000000000011--------") then  	sel := ipb_cbc_system_stat_sel;			
		elsif std_match(addr, "01000000000000000000010000000000") then  	sel := ipb_cbc_system_event_data_sel;
		elsif std_match(addr, "010000000000000000001000000000--") then  	sel := ipb_cbc_system_cbc_i2c_regs_sel;
		elsif std_match(addr, "01000000000000000000100000000100") then  	sel := ipb_cbc_data_clock_timing_scan_bram_sel;	
		elsif std_match(addr, "01000000000000000000100000001000") then  	sel := ipb_cfscbram_sel;	
		elsif std_match(addr, "01000000000000000000100000010000") then  	sel := ipb_rdb_sel;
--		 elsif std_match(addr, "01000000000000000000000100000000") then		sel := user_ipb_timer; -- xx
		--              addr, "1-------------------------------" is reserved (wishbone fabric)
		else	
			sel := 99;
		end if;
		return sel;
	end user_ipb_addr_sel;

--   function user_wb_addr_sel(signal addr : in std_logic_vector(31 downto 0)) return integer is
--		variable sel : integer;
--   begin
--		--              addr, "00------------------------------" is reserved (system ipbus fabric)
--		--              addr, "01------------------------------" is reserved (user ipbus fabric)
--		if    std_match(addr, "100000000000000000000000--------") then  	sel := user_wb_regs;
--		elsif std_match(addr, "10000000000000000000000100000000") then		sel := user_wb_timer; -- xx
--		else	
--			sel := 99;
--		end if;
--		return sel;
--	end user_wb_addr_sel; 

end user_addr_decode;
