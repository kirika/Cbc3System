#===========================--
# Temporary IOs. To be updated.  
#
# 28.10.2016 Kirika Uchida
#===========================--
set_property IOSTANDARD LVDS_25 [get_ports {fmc_l8_la_p[31]}]
set_property IOSTANDARD LVDS_25 [get_ports {fmc_l8_la_p[30]}]
set_property IOSTANDARD LVDS_25 [get_ports {fmc_l8_la_p[29]}]
set_property IOSTANDARD LVDS_25 [get_ports {fmc_l8_la_p[28]}]
set_property IOSTANDARD LVDS_25 [get_ports {fmc_l8_la_p[25]}]
set_property IOSTANDARD LVDS_25 [get_ports {fmc_l8_la_p[24]}]
set_property IOSTANDARD LVDS_25 [get_ports {fmc_l8_la_p[21]}]
set_property IOSTANDARD LVDS_25 [get_ports {fmc_l8_la_p[20]}]
set_property IOSTANDARD LVDS_25 [get_ports {fmc_l8_la_p[19]}]
set_property IOSTANDARD LVDS_25 [get_ports {fmc_l8_la_p[16]}]
set_property IOSTANDARD LVDS_25 [get_ports {fmc_l8_la_p[15]}]
set_property IOSTANDARD LVDS_25 [get_ports {fmc_l8_la_p[11]}]
set_property IOSTANDARD LVDS_25 [get_ports {fmc_l8_la_p[7]}]
set_property IOSTANDARD LVDS_25 [get_ports {fmc_l8_la_p[4]}]
set_property IOSTANDARD LVDS_25 [get_ports {fmc_l8_la_p[2]}]
set_property IOSTANDARD LVDS_25 [get_ports {fmc_l8_la_p[0]}]


set_property PACKAGE_PIN AE33 [get_ports {fmc_l8_la_p[31]}]
set_property PACKAGE_PIN AF33 [get_ports {fmc_l8_la_n[31]}]
set_property PACKAGE_PIN AC34 [get_ports {fmc_l8_la_p[30]}]
set_property PACKAGE_PIN AD34 [get_ports {fmc_l8_la_n[30]}]
set_property PACKAGE_PIN AJ29 [get_ports {fmc_l8_la_p[29]}]
set_property PACKAGE_PIN AK29 [get_ports {fmc_l8_la_n[29]}]
set_property PACKAGE_PIN AD31 [get_ports {fmc_l8_la_p[28]}]
set_property PACKAGE_PIN AD32 [get_ports {fmc_l8_la_n[28]}]
set_property PACKAGE_PIN AE34 [get_ports {fmc_l8_la_p[25]}]
set_property PACKAGE_PIN AF34 [get_ports {fmc_l8_la_n[25]}]
set_property PACKAGE_PIN AH30 [get_ports {fmc_l8_la_p[24]}]
set_property PACKAGE_PIN AJ30 [get_ports {fmc_l8_la_n[24]}]
set_property PACKAGE_PIN AM33 [get_ports {fmc_l8_la_p[21]}]
set_property PACKAGE_PIN AN34 [get_ports {fmc_l8_la_n[21]}]
set_property PACKAGE_PIN AM28 [get_ports {fmc_l8_la_n[20]}]
set_property PACKAGE_PIN AM27 [get_ports {fmc_l8_la_p[20]}]
set_property PACKAGE_PIN AH33 [get_ports {fmc_l8_la_n[19]}]
set_property PACKAGE_PIN AG33 [get_ports {fmc_l8_la_p[19]}]
set_property PACKAGE_PIN AK32 [get_ports {fmc_l8_la_n[16]}]
set_property PACKAGE_PIN AJ32 [get_ports {fmc_l8_la_p[16]}]
set_property PACKAGE_PIN AJ34 [get_ports {fmc_l8_la_n[15]}]
set_property PACKAGE_PIN AH34 [get_ports {fmc_l8_la_p[15]}]
set_property PACKAGE_PIN AM30 [get_ports {fmc_l8_la_n[11]}]
set_property PACKAGE_PIN AL29 [get_ports {fmc_l8_la_p[11]}]
set_property PACKAGE_PIN AP30 [get_ports {fmc_l8_la_n[7]}]
set_property PACKAGE_PIN AP29 [get_ports {fmc_l8_la_p[7]}]
set_property PACKAGE_PIN AP24 [get_ports {fmc_l8_la_n[4]}]
set_property PACKAGE_PIN AN24 [get_ports {fmc_l8_la_p[4]}]
set_property PACKAGE_PIN AL24 [get_ports {fmc_l8_la_n[2]}]
set_property PACKAGE_PIN AK24 [get_ports {fmc_l8_la_p[2]}]
set_property PACKAGE_PIN AG31 [get_ports {fmc_l8_la_n[0]}]
set_property PACKAGE_PIN AF31 [get_ports {fmc_l8_la_p[0]}]

set_property IOSTANDARD LVDS_25 [get_ports {fmc_l12_la_p[31]}]
set_property IOSTANDARD LVDS_25 [get_ports {fmc_l12_la_p[30]}]
set_property IOSTANDARD LVDS_25 [get_ports {fmc_l12_la_p[29]}]
set_property IOSTANDARD LVDS_25 [get_ports {fmc_l12_la_p[28]}]
set_property IOSTANDARD LVDS_25 [get_ports {fmc_l12_la_p[25]}]
set_property IOSTANDARD LVDS_25 [get_ports {fmc_l12_la_p[24]}]
set_property IOSTANDARD LVDS_25 [get_ports {fmc_l12_la_p[21]}]
set_property IOSTANDARD LVDS_25 [get_ports {fmc_l12_la_p[20]}]
set_property IOSTANDARD LVDS_25 [get_ports {fmc_l12_la_p[19]}]
set_property IOSTANDARD LVDS_25 [get_ports {fmc_l12_la_p[16]}]
set_property IOSTANDARD LVDS_25 [get_ports {fmc_l12_la_p[15]}]
set_property IOSTANDARD LVDS_25 [get_ports {fmc_l12_la_p[11]}]
set_property IOSTANDARD LVDS_25 [get_ports {fmc_l12_la_p[7]}]
set_property IOSTANDARD LVDS_25 [get_ports {fmc_l12_la_p[4]}]
set_property IOSTANDARD LVDS_25 [get_ports {fmc_l12_la_p[2]}]
set_property IOSTANDARD LVDS_25 [get_ports {fmc_l12_la_p[0]}]

set_property PACKAGE_PIN A31 [get_ports {fmc_l12_la_n[31]}]
set_property PACKAGE_PIN B30 [get_ports {fmc_l12_la_p[31]}]
set_property PACKAGE_PIN D30 [get_ports {fmc_l12_la_n[30]}]
set_property PACKAGE_PIN D29 [get_ports {fmc_l12_la_p[30]}]
set_property PACKAGE_PIN B32 [get_ports {fmc_l12_la_n[29]}]
set_property PACKAGE_PIN B31 [get_ports {fmc_l12_la_p[29]}]
set_property PACKAGE_PIN E29 [get_ports {fmc_l12_la_n[28]}]
set_property PACKAGE_PIN F29 [get_ports {fmc_l12_la_p[28]}]
set_property PACKAGE_PIN C33 [get_ports {fmc_l12_la_n[25]}]
set_property PACKAGE_PIN C32 [get_ports {fmc_l12_la_p[25]}]
set_property PACKAGE_PIN G31 [get_ports {fmc_l12_la_p[24]}]
set_property PACKAGE_PIN F31 [get_ports {fmc_l12_la_n[24]}]
set_property PACKAGE_PIN L31 [get_ports {fmc_l12_la_p[21]}]
set_property PACKAGE_PIN K31 [get_ports {fmc_l12_la_n[21]}]
set_property PACKAGE_PIN C34 [get_ports {fmc_l12_la_n[20]}]
set_property PACKAGE_PIN D34 [get_ports {fmc_l12_la_p[20]}]
set_property PACKAGE_PIN A33 [get_ports {fmc_l12_la_n[19]}]
set_property PACKAGE_PIN B33 [get_ports {fmc_l12_la_p[19]}]
set_property PACKAGE_PIN G33 [get_ports {fmc_l12_la_n[16]}]
set_property PACKAGE_PIN H33 [get_ports {fmc_l12_la_p[16]}]
set_property PACKAGE_PIN H34 [get_ports {fmc_l12_la_n[15]}]
set_property PACKAGE_PIN J34 [get_ports {fmc_l12_la_p[15]}]
set_property PACKAGE_PIN K34 [get_ports {fmc_l12_la_n[11]}]
set_property PACKAGE_PIN L34 [get_ports {fmc_l12_la_p[11]}]
set_property PACKAGE_PIN N34 [get_ports {fmc_l12_la_n[7]}]
set_property PACKAGE_PIN P34 [get_ports {fmc_l12_la_p[7]}]
set_property PACKAGE_PIN U33 [get_ports {fmc_l12_la_n[4]}]
set_property PACKAGE_PIN U32 [get_ports {fmc_l12_la_p[4]}]
set_property PACKAGE_PIN T31 [get_ports {fmc_l12_la_n[2]}]
set_property PACKAGE_PIN T30 [get_ports {fmc_l12_la_p[2]}]
set_property PACKAGE_PIN R28 [get_ports {fmc_l12_la_p[0]}]
set_property PACKAGE_PIN R29 [get_ports {fmc_l12_la_n[0]}]

set_property DIFF_TERM TRUE [get_ports {fmc_l8_la_p[16]}]
set_property DIFF_TERM TRUE [get_ports {fmc_l8_la_p[11]}]
set_property DIFF_TERM TRUE [get_ports {fmc_l8_la_p[15]}]
set_property DIFF_TERM TRUE [get_ports {fmc_l8_la_p[20]}]
set_property DIFF_TERM TRUE [get_ports {fmc_l8_la_p[19]}]
set_property DIFF_TERM TRUE [get_ports {fmc_l8_la_p[21]}]
set_property DIFF_TERM TRUE [get_ports {fmc_l8_la_p[25]}]
set_property DIFF_TERM TRUE [get_ports {fmc_l8_la_p[24]}]

########################################################
create_clock -period 25.000 -name fmcl8_clk_a [get_ports {fmc_l8_la_p[0]}]
create_clock -period 3.125 -name fmcl12_clk_a [get_ports {fmc_l12_la_p[0]}]
########################################################

set_clock_groups -asynchronous \
    -group [get_clocks -include_generated_clocks eth_txoutclk] \
    -group [get_clocks -include_generated_clocks osc125_a] \
    -group [get_clocks -include_generated_clocks osc125_b] \
    -group [get_clocks -include_generated_clocks fabric_clk] \
    -group [get_clocks -include_generated_clocks fmcl8_clk_a] \
    -group [get_clocks -include_generated_clocks fmcl12_clk_a] 
    
#set_clock_groups -asynchronous -group [get_clocks fabric_clk] -group [get_clocks fmcl8_clk_a] -group [get_clocks fmcl12_clk_a] -group [get_clocks clkout0] -group {clk_out1_cbc_system_clocks clk_out2_cbc_system_clocks} -group {clk_out3_cbc_system_clocks} -group {clk_out4_cbc_system_clocks clk_out5_cbc_system_clocks} -group {fmcl12_clk_a}
set_clock_groups -asynchronous -group [get_clocks fmcl8_clk_a] -group [get_clocks fmcl12_clk_a] -group [get_clocks clkout0] -group {clk_out1_cbc_system_clocks_0 clk_out2_cbc_system_clocks_0} -group {clk_out3_cbc_system_clocks_0} -group {clk_out1_cbc_system_clocks_1 clk_out2_cbc_system_clocks_1}


#set_false_path -through usr/cbc_daq_cbc_data_delay_impl/delay_gen[1].cbc_data_timing_tuning/IDDR_inst/R

#set_property PACKAGE_PIN AJ26 [get_ports {fmc_l8_clk1}]
#set_property IOSTANDARD LVCMOS25 [get_ports fmc_l8_clk1]
#set_property PACKAGE_PIN G30 [get_ports {fmc_l12_clk1}]
#set_property IOSTANDARD LVCMOS25 [get_ports fmc_l12_clk1]

#create_clock -period 25.000 -name fmc_l8_clk1_bufo   [get_ports fmc_l8_clk1]
#create_clock -period 25.000 -name fmc_l12_clk1_bufo   [get_ports fmc_l12_clk1]




