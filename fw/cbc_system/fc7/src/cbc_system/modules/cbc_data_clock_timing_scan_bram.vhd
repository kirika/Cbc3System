----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 25.01.2017 17:12:07
-- Design Name: 
-- Module Name: cbc_data_clock_timing_scan_bram - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

use work.cbc_system_package.all;

entity cbc_data_clock_timing_scan_bram_ctrl is
generic ( WADDR_WIDTH : positive := 10; RADDR_WIDTH : positive := 9 );
port(   clk_40MHz    : in  std_logic;
        ipb_clk      : in  std_logic;
        reset        : in  std_logic;
        cdcts_to_bram_set  : in cdcts_to_bram_set;
        ipbif_to_cdctsb    : in  ipbif_to_cdctsb;
        cdctsb_to_ipbif    : out cdctsb_to_ipbif
);
end cbc_data_clock_timing_scan_bram_ctrl;

architecture Behavioral of cbc_data_clock_timing_scan_bram_ctrl is

    signal data_ready       : std_logic_vector(NCBC-1 downto 0);
    
    signal we               : std_logic;
    signal waddr            : std_logic_vector(WADDR_WIDTH downto 0);
    signal din              : std_logic_vector(15 downto 0);  
    signal ndata            : unsigned(ADDR_WIDTH downto 0);
    signal nrdata           : unsigned(ADDR_WIDTH downto 0);    
    signal full             : std_logic;
    signal raddr            : std_logic_vector(ADDR_WIDTH downto 0);
    signal empty            : std_logic;
    constant max_events     : integer := 2**ADDR_WIDTH;

    constant data_all_not_ready : std_logic_vector(NCBC-1 downto 0) := (others => '0');

begin

	--=============================--
    process(reset, clk_40MHz)
    --=============================--
    variable next_index : integer range 0 to NCBC-1;
    begin
    if reset='1' then     
        data_ready <= (others => '0');
        next_index := 0;
    elsif rising_edge(clk_40MHz) then 
        
        for i in 0 to NCBC-1 loop
            bram_to_cdcts_set(i).write_completed <= '0';
        end loop;
        we <= '0';
                
        case fsm is
        when wait_data =>
            for i in 0 to NCBC-1 loop
                if cdcts_to_bram_set(i).wen then
                    data_ready(i) <= '1';
                end if;
            end loop;        
            if data_ready /= data_all_not_ready then
                next_index := 0;
                fsm <= write_data;
            end if;
       when write_data =>

           if  data_ready(next_index) = '1' then
                we    <= '1';
                waddr <= std_logic_vector(to_unsigned(to_integer(cdcts_to_bram_set(next_index).waddr) + 60 * i, WADDR_WIDTH));                
                din   <= cdcts_to_bram_set(next_index).din;
                bram_to_cdcts_set(next_index).write_completed <= '1';
            else
                if next_index = NCBC - 1 then
                    fsm <= wait_data;
                else
                    next_index := next_index + 1;
                end if;
            end if;

       end case;
    end if;
    end process;

    -- nrdata is the # of data which was read before read_next = '1'
    -- # of data presented at dout is nrdata - 1
	--=============================--
    process(reset, ipb_clk)
    --=============================--
    begin
    if reset='1' then     
        raddr  <= (others => '0');
        nrdata <= (others => '0');
    elsif rising_edge(ipb_clk) then
        if ipbif_to_cdctsb.read_next = '1' and empty = '0' then
            raddr <= std_logic_vector(nrdata+1);
            nrdata <= nrdata + 1;
        end if;
    end if;
    end process;
    
    empty <= '1' when ndata = to_unsigned( 0, RADDR_WIDTH+1 ) else '0';
    full  <= '1' when ndata = to_unsigned( max_events, RADDR_WIDTH+1 ) else '0';
    
    cbc_data_clock_timing_scan_bram : entity work.cbc_data_clock_timing_scan_bram
      PORT MAP (
        clka    => clk_40MHz,
        wea(0)  => we,
        addra   => waddr(WADDR_WIDTH - 1 downto 0),
        dina    => din,
        clkb    => ipb_clk,
        addrb   => raddr(ADDR_WIDTH - 1 downto 0),
        doutb   => cdctsb_to_ipbif.dout
      );    

end Behavioral;
