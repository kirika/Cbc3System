--===========================--
-- idelay_tuning 
-- 28.10.2016 Kirika Uchida
-- idelay: 0.6 ns delay with delay = 0 according to the vivado behavioral simulation.
--===========================--

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;
use work.cbc_system_package.all;

entity idelay_tuning is
    generic( COUNTER_WIDTH : natural := 5; LSB_TO_CHECK : integer := 1 );
    Port ( clk_320MHz   : in STD_LOGIC;
           delayed_din  : in STD_LOGIC;
           reset        : in STD_LOGIC;
           start        : in STD_LOGIC;
           fsm_o        : out idelay_tuning_fsm_type;
           delay_o      : out STD_LOGIC_VECTOR (4 downto 0);
           ld_to_idelay : out STD_LOGIC );
end idelay_tuning;

architecture fc7 of idelay_tuning is

    constant max_count                : integer := 2**COUNTER_WIDTH - 1;
    constant half_count               : integer := 2**(COUNTER_WIDTH - 1);
    constant transient_count          : integer := max_count - 4;
    signal fsm					    : idelay_tuning_fsm_type;
    signal timer                    : unsigned( 2 downto 0 );
    signal cnt0                     : unsigned( COUNTER_WIDTH - 1 downto 0 );
    signal cnt1_0                     : unsigned( COUNTER_WIDTH - 1 downto 0 );
    signal cnt2_0                     : unsigned( COUNTER_WIDTH - 1 downto 0 );
    signal cnt1_1                     : unsigned( COUNTER_WIDTH - 1 downto 0 );
    signal cnt2_1                     : unsigned( COUNTER_WIDTH - 1 downto 0 );
    signal cnt1_2                     : unsigned( COUNTER_WIDTH - 1 downto 0 );
    signal cnt1_7                     : unsigned( COUNTER_WIDTH - 1 downto 0 );
                  
    signal delay_increment          : std_logic;

    signal delay                    : unsigned( 4 downto 0);
    signal Q1                       : std_logic;
    signal Q2                       : std_logic;
    signal ld                       : std_logic;
    signal go_to_tuned              : std_logic;
--    signal go_to_step4              : std_logic;
    begin

    process ( clk_320MHz, reset )
    variable tuning_type : integer range 0 to 2;
    begin
        if reset = '1' then 
            timer <= ( others => '0' );
            tuning_type := 0;
            go_to_tuned <= '0'; 
            delay_increment <= '0'; 
            ld           <= '0';
           fsm <= init;
        elsif rising_edge( clk_320MHz ) then    
            timer <= timer + 1;
            ld           <= '0';     
            
            if delay_increment = '1' then
                delay <= delay + 1;
                ld           <= '1';   
                delay_increment <= '0';
            end if;
                
            case fsm is
                when init =>
                    delay  <= ( others => '0' );
                    timer  <= ( others => '0' );
                    cnt0   <= ( others => '0' );
                    cnt1_0 <= ( others => '0' );                            
                    cnt2_0 <= ( others => '0' );
                    cnt1_1 <= ( others => '0' );
                    cnt2_1 <= ( others => '0' );    
                    cnt1_2 <= ( others => '0' );
                    cnt1_7 <= ( others => '0' );                                                                                        
                    delay_increment <= '0';          
                    if start = '1' then
                        ld     <= '1';
                    end if;
                    if ld = '1' then
                        fsm <= step1;
                    end if;
                when step1 => -- setting the timer timing wrt the sync bit
                    if delayed_din = '1' then
                        timer <= ( others => '0' );
                        fsm <= step2;
                    end if;

                when step2 => -- checking the timing of sync bit wrt the clock 320 MHz.
                    if timer = 0 then
                        if Q1 = '1' then
                            cnt1_0 <= cnt1_0 + 1;
                        end if;
                        if Q2 = '1' then
                            cnt2_0 <= cnt2_0 + 1;
                        end if;                    
                        cnt0 <= cnt0 + 1;
                    elsif timer = 1 then
                        if cnt0 = max_count then
                            delay_increment <= '1';
                            cnt0   <= ( others => '0' );
                            cnt1_0 <= ( others => '0' );                            
                            cnt2_0 <= ( others => '0' );           
                            if cnt1_0 >= transient_count and cnt2_0 >= transient_count then
                                tuning_type := 1;
                            elsif cnt1_0 >= transient_count and cnt2_0 < transient_count then
                                tuning_type := 2;
                            elsif delay = "0000" then
                                fsm <= init;
                            end if;
                        end if;
                    elsif timer = 2 then
                        if tuning_type /= 0 then
                            cnt0   <= ( others => '0' );                            
                            cnt1_0 <= ( others => '0' );                            
                            cnt2_0 <= ( others => '0' );
                            cnt1_1 <= ( others => '0' );
                            cnt2_1 <= ( others => '0' );    
                            cnt1_2 <= ( others => '0' );
                            cnt1_7 <= ( others => '0' );              
                            fsm <= step3;
                        end if;
                    end if;
                when step3 => -- scan the delay until ddr output changes at timer = 0

                    if timer = 7 then
                        cnt0 <= cnt0 + 1;
                        if Q1 = '1' then
                            cnt1_7 <= cnt1_7 + 1;
                        end if;
                    elsif timer = 0 then
                        if Q1 = '1' then
                            cnt1_0 <= cnt1_0 + 1;
                        end if;
                        if Q2 = '1' then
                            cnt2_0 <= cnt2_0 + 1;
                        end if;                    
                    elsif timer = 1 then
                        if Q1 = '1' then
                            cnt1_1 <= cnt1_1 + 1;
                        end if;
                        if Q2 = '1' then
                            cnt2_1 <= cnt2_1 + 1;
                        end if;
                    elsif timer = 2 then
                        if Q1 = '1' then
                            cnt1_2 <= cnt1_2 + 1;
                        end if;
                    elsif timer = 3 then
                        if cnt0 = max_count then
                            delay_increment <= '1';           
                            cnt0   <= ( others => '0' );
                            cnt1_0 <= ( others => '0' );                            
                            cnt2_0 <= ( others => '0' );
                            cnt1_1 <= ( others => '0' );
                            cnt2_1 <= ( others => '0' );    
                            cnt1_2 <= ( others => '0' );
                            cnt1_7 <= ( others => '0' );                                                     
                            if tuning_type = 1 then
                                if cnt1_0 = max_count and cnt2_0 < transient_count and cnt1_7 = 0 and cnt1_1 = 0 then
                        --                                if cnt1 = cnt0 and cnt2 = 0 and cnt3 = 0 and cnt 4 = 0 then                                
                                    delay_increment <= '0';
                                    fsm <= tuned;
                                end if;
                            elsif tuning_type = 2 then
                        
                                if cnt1_0 = 0 and cnt1_1 = max_count and cnt2_1 < transient_count and cnt1_2 = 0 then
                                    delay_increment <= '0';
                                    fsm <= tuned;
                                elsif delay = "00000" then
                                    delay_increment <= '0';
                                    fsm <= tuned;
                                end if;
                            end if;
                        end if;
                    elsif timer = 4 then
                        if go_to_tuned = '1' then
                            fsm <= tuned;
                        end if;
                    end if;
     
                when tuned =>               
            end case;
        end if;    
    end process;

   IDDR_inst : IDDR 
   generic map (
      DDR_CLK_EDGE => "SAME_EDGE", -- "OPPOSITE_EDGE", "SAME_EDGE" 
                                       -- or "SAME_EDGE_PIPELINED" 
      INIT_Q1 => '0', -- Initial value of Q1: '0' or '1'
      INIT_Q2 => '0', -- Initial value of Q2: '0' or '1'
      SRTYPE => "ASYNC") -- Set/Reset type: "SYNC" or "ASYNC" 
   port map (
      Q1 => Q1, -- 1-bit output for positive edge of clock 
      Q2 => Q2, -- 1-bit output for negative edge of clock
      C => clk_320MHz,   -- 1-bit clock input
      CE => '1', -- 1-bit clock enable input
      D => delayed_din,   -- 1-bit DDR data input
--      R => reset,   -- 1-bit reset
      R => '0',  -- timing for reset fails.  I just disabled the reset.
      S => '0'    -- 1-bit set
      );

    ld_to_idelay <= ld;   
    delay_o <= std_logic_vector( delay );
    fsm_o   <= fsm;
    
end fc7;
