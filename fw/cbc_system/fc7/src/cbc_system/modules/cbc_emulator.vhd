--===========================--
-- cbc_emulator
-- 28.10.2016 Kirika Uchida
--===========================--

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.cbc_system_package.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity cbc_emulator is
generic ( TRIG_LATENCY : integer := 10; STUB_LATENCY : integer := 2 );
port(
	clk_320MHz       : in    std_logic;
    serial_command   : in    std_logic;
    slvs             : out   std_logic_vector( 6 downto 1 );
    i2c_bus_mosi_set : in    cbc_i2c_bus_mosi_type;
    i2c_bus_miso_set : out   cbc_i2c_bus_miso_type;
    hard_reset       : in    std_logic;
    ipb_cnfg         : in    cbc_emulator_cnfg_type;
    ipb_ctrl         : in    cbc_emulator_ctrl_type;
	ipb_stat         : out   cbc_emulator_stat_type  
    );
end cbc_emulator;

architecture Behavioral of cbc_emulator is

    type slvs15_pattern_type is array ( 1 to 5 ) of std_logic_vector( 7 downto 0 );
    type slvs6_pattern_type  is array ( 0 to 34 ) of std_logic_vector( 7 downto 0 );
    constant SLVS6_L1COUNTER_OFFSET : integer := 21;
-- signals for a programmed pattern for test pulse
    signal slvs15_tp_pattern  : slvs15_pattern_type;
    signal slvs6_tp_pattern   : slvs6_pattern_type;
-- no signal pattern 
    constant SLVS15_NOSTUB : slvs15_pattern_type := (x"00", x"00", x"00", x"00", "10000000" );
    constant SLVS6_NOHITS  : slvs6_pattern_type  := (x"00",x"00",                                                                                                     
                                                     x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00",
                                                     x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00",
                                                     x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00",                                                     
                                                     "000001" & "00",
                                                     "00001" & "000",
                                                     "1100" & "0000"
                                                     );
                                                     
-- data to be sent out
    signal slvs15_data     : slvs15_pattern_type;
    signal slvs6_data      : slvs6_pattern_type;
-- register to get sync. pattern in the external fast signal    
    signal sync_reg        : std_logic_vector(2 downto 0);
-- latched fast signal at each signal timing
    signal fast_reset      : std_logic;
    signal trigger         : std_logic;
    signal test_pulse      : std_logic;
    signal orbit_reset     : std_logic;
-- 320MHz counter in a 40MHz cycle.
    signal counter             : unsigned(2 downto 0);
    signal slvs6_frame_counter : unsigned(5 downto 0);

    signal ndata_buf           : unsigned( 4 downto 0 );
    signal test_pulse_pipeline : std_logic_vector(511 downto 0);
    signal data_fifo_wr_en     : std_logic;
    signal data_fifo_rd_en     : std_logic;
    signal data_fifo_empty     : std_logic;
    signal data_fifo_full      : std_logic;
    signal data_fifo_din       : std_logic_vector(279 downto 0);
    signal data_fifo_dout      : std_logic_vector(279 downto 0);
    signal send_frame          : std_logic;
    
    signal trig_counter        : unsigned(8 downto 0);
    signal w_pipeline_address    : unsigned(8 downto 0);
    signal r_pipeline_address    : unsigned(8 downto 0);
    signal r_pipeline_started    : std_logic;
    
    signal fsm                 : cbc_emulator_fsm_type;
begin

    process( clk_320MHz, ipb_ctrl.reset, ipb_ctrl.load_data_pattern )
    variable fifo_timer : integer range 0 to 2;
    begin
        if ipb_ctrl.reset = '1' then
            slvs <= ( others => '0' );
            slvs15_tp_pattern(1) <= x"01";
            slvs15_tp_pattern(2) <= x"02";
            slvs15_tp_pattern(3) <= x"03";
            slvs15_tp_pattern(4) <= x"12";
            slvs15_tp_pattern(5) <= '1' & '0' & '1' & '0' & "1100";
            slvs6_tp_pattern(34)  <= "11" & "00" & "0000";
            slvs6_tp_pattern(33)  <= "00000" & "000";
            slvs6_tp_pattern(32)  <= "000000" & "00";
            trig_counter          <= to_unsigned(1,9);
            w_pipeline_address    <= to_unsigned(0,9);  
            r_pipeline_address    <= to_unsigned(0,9);                        
	        for i in 0 to 31 loop
                slvs6_tp_pattern(i) <= (others=>'0');
            end loop;            
            sync_reg <= ( others => '0' );
            counter <= "000";
            ndata_buf <= ( others => '0' );
            slvs6_frame_counter <= ( others => '0' );
            fast_reset  <= '0';
            trigger     <= '0';
            test_pulse  <= '0';
            orbit_reset <= '0';
            slvs15_data <= SLVS15_NOSTUB;
            slvs6_data  <= (others => (others => '0') );    
            ndata_buf <= ( others => '0' );
            test_pulse_pipeline <= (others =>'0');   
            data_fifo_wr_en <= '0';  
            data_fifo_rd_en <= '0'; 
            r_pipeline_started <= '0';  
            fifo_timer := 0;                         
            fsm <= init;
        elsif ipb_ctrl.load_data_pattern = '1' then
            slvs15_tp_pattern(1) <= ipb_cnfg.data_pattern.s1_addr;
            slvs15_tp_pattern(2) <= ipb_cnfg.data_pattern.s2_addr; 
            slvs15_tp_pattern(3) <= ipb_cnfg.data_pattern.s3_addr;
            slvs15_tp_pattern(4) <= ipb_cnfg.data_pattern.s2_bend & ipb_cnfg.data_pattern.s1_bend;
            slvs15_tp_pattern(5) <= '1' & ipb_cnfg.data_pattern.error & ipb_cnfg.data_pattern.or254 & ipb_cnfg.data_pattern.sof & ipb_cnfg.data_pattern.s3_bend;
            slvs6_tp_pattern(34)  <= "11" & "00" & "1111";
            slvs6_tp_pattern(33)  <= "00000" & "111";
            slvs6_tp_pattern(32)  <= "000000" & ipb_cnfg.data_pattern.hit254(254 downto 253);
            slvs6_tp_pattern(0)   <= ipb_cnfg.data_pattern.hit254( 4 downto 1 ) & "0000";
            for i in 1 to 31 loop
                slvs6_tp_pattern(i) <= ipb_cnfg.data_pattern.hit254( i * 8 + 4 downto i * 8 - 3 );
            end loop;    
            --slvs6_tp_pattern(0) <= (others =>'0');        
            sync_reg <= ( others => '0' );
            counter <= "000";
            ndata_buf <= ( others => '0' );            
            slvs6_frame_counter <= ( others => '0' ); 
            fast_reset  <= '0';
            trigger     <= '0';
            test_pulse  <= '0';
            orbit_reset <= '0';
            slvs15_data <= SLVS15_NOSTUB;
            slvs6_data  <= SLVS6_NOHITS;                   
            ndata_buf <= ( others => '0' );
            test_pulse_pipeline <= (others =>'0');  
            data_fifo_wr_en <= '0';  
            data_fifo_rd_en <= '0';   
            fifo_timer := 0;                      
            fsm <= init;
        elsif rising_edge( clk_320MHz ) then

            sync_reg <= sync_reg(1 downto 0) & serial_command;
            counter <= counter - 1;           
            data_fifo_wr_en <= '0';
            data_fifo_rd_en <= '0';          
            
            case fsm is
                when init =>
                    if sync_reg = "110" then
                        counter <= "100";
                        fsm <= running;
                    end if;
                when running => 

                        
                    -- fast signal latch    
                    if counter = "100" then -- fast_reset
                        fast_reset <= sync_reg(0);
                         if sync_reg(0) = '1' then
                            trig_counter          <= to_unsigned(1,9);
                            w_pipeline_address      <= (others => '0');   
                         end if;
                         
                    elsif counter = "011" then -- trigger
                        trigger <= sync_reg(0);
                        if sync_reg(0) = '1' then
                            if test_pulse_pipeline(TRIG_LATENCY-1) = '1' then
                                for I in 0 to 32 loop
                                    data_fifo_din( (I+1)*8 - 1 downto I*8 ) <= slvs6_tp_pattern(I);
                                end loop;         
                            else 
                                for I in 0 to 31 loop
                                    data_fifo_din( (I+1)*8 - 1 downto I*8 ) <= SLVS6_NOHITS(I);
                                end loop;
                                data_fifo_din(32*8+1 downto 32*8) <= "00";
                            end if;
                            
                            data_fifo_din(33*8+2 downto 32*8+2) <= std_logic_vector(trig_counter);
                            data_fifo_din(34*8+3 downto 33*8+3) <= std_logic_vector(r_pipeline_address);
                            data_fifo_din(34*8+7 downto 34*8+4) <= "1100";
                            trig_counter <= trig_counter + 1;
                            data_fifo_wr_en <= '1';
                        end if;
                    elsif counter = "010" then -- test pulse
                        test_pulse_pipeline <= test_pulse_pipeline(510 downto 0) & sync_reg(0);

--                        if trigger = '1' then

--                        end if; 

                    elsif counter = "001" then -- orbit reset
                        orbit_reset <= sync_reg(0);            

                        if send_frame = '0' and fifo_timer = 0 and data_fifo_empty = '0' then
                             data_fifo_rd_en <= '1';
                             fifo_timer := 1;
                        end if;
                    elsif counter = "000" then
                        
                        w_pipeline_address <= w_pipeline_address + 1;
                        if( r_pipeline_started = '0' and w_pipeline_address >= TRIG_LATENCY ) then
                            r_pipeline_address <= r_pipeline_address + 1;
                            r_pipeline_started <= '1';
                        end if;
                        if test_pulse_pipeline(STUB_LATENCY) = '1' then
                            slvs15_data <= slvs15_tp_pattern;
                        else
                            slvs15_data <= SLVS15_NOSTUB;
                        end if;                  
                                        
                        if slvs6_frame_counter = 0 then
                            
                            if fifo_timer = 2 then

                                for I in 0 to 34 loop
                                    slvs6_data(I) <= data_fifo_dout( (I+1)*8-1 downto I*8 );
                                end loop;                                
                                
                                slvs6_frame_counter <= to_unsigned( 34, 6 );
                                send_frame <= '1';

                                fifo_timer := 0;
                                
                            elsif fifo_timer = 0 then                                                                
                                send_frame <= '0'; 
                            else
                                fifo_timer := fifo_timer + 1;  
                            end if;
                        else
                            if send_frame = '1' then
                                slvs6_frame_counter <= slvs6_frame_counter - 1;
                            end if; 
                        end if;

                    end if;
        
                    -- sending slvs stub data            
                    slvs(1) <= slvs15_data(1)(to_integer(counter));
                    slvs(2) <= slvs15_data(2)(to_integer(counter));
                    slvs(3) <= slvs15_data(3)(to_integer(counter));
                    slvs(4) <= slvs15_data(4)(to_integer(counter));
                    slvs(5) <= slvs15_data(5)(to_integer(counter));
                    if  send_frame = '1' then
                        slvs(6) <= slvs6_data(to_integer(slvs6_frame_counter))(to_integer(counter));

                    else
                        slvs(6) <= '0';
                    end if;
           end case;
        end if;
    end process;
    ipb_stat.fsm <= fsm;
    
    data_fifo : entity work.cbc_emu_data_buffer
      PORT MAP (
        clk => clk_320MHz,
        srst => ipb_ctrl.reset,
        din => data_fifo_din,
        wr_en => data_fifo_wr_en,
        rd_en => data_fifo_rd_en,
        dout => data_fifo_dout,
        full => data_fifo_full,
        empty => data_fifo_empty
      );
		
		ipb_stat.tp_data_pattern.s1_addr <= slvs15_tp_pattern(1);
		ipb_stat.tp_data_pattern.s2_addr <= slvs15_tp_pattern(2);
		ipb_stat.tp_data_pattern.s3_addr <= slvs15_tp_pattern(3);
		ipb_stat.tp_data_pattern.s1_bend <= slvs15_tp_pattern(4)(3 downto 0);
		ipb_stat.tp_data_pattern.s2_bend <= slvs15_tp_pattern(4)(7 downto 4);		
		ipb_stat.tp_data_pattern.s3_bend <= slvs15_tp_pattern(5)(3 downto 0);
		ipb_stat.tp_data_pattern.sof     <= slvs15_tp_pattern(5)(4);
		ipb_stat.tp_data_pattern.or254   <= slvs15_tp_pattern(5)(5);
		ipb_stat.tp_data_pattern.error   <= slvs15_tp_pattern(5)(6);
		ipb_stat.tp_data_pattern.hit254(254 downto 253) <= slvs6_tp_pattern(32)(1 downto 0);
		hit_stat_gen :
		for i in 0 to 30 generate
			ipb_stat.tp_data_pattern.hit254(252 - i * 8 downto 252 - (i+1)*8 + 1 ) <= slvs6_tp_pattern(31-i);
		end generate;
      ipb_stat.tp_data_pattern.hit254(004 downto 001) <= slvs6_tp_pattern(0)(7 downto 4);                           
		
end Behavioral;
