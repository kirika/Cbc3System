--===========================--
-- io 
-- Temporary IOs.  To be updated.
-- 28.10.2016 Kirika Uchida
--===========================--

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

use work.cbc_system_package.all;

entity io is
port ( 
    ipb_ctrl                            : in       io_ctrl_type;
    ipb_cnfg                            : in       io_cnfg_type;
    ipb_stat                            : out      io_stat_type;
	fmc_l8_la_p						    : inout	   std_logic_vector(33 downto 0);
    fmc_l8_la_n                         : inout    std_logic_vector(33 downto 0);
    fmc_l12_la_p                        : inout    std_logic_vector(33 downto 0);
    fmc_l12_la_n                        : inout    std_logic_vector(33 downto 0);
    clk_200MHz                          : in       std_logic;
    clk_40MHz_cbc_ctrl                  : in       std_logic;
    clk_320MHz_cbc_ctrl                 : in       std_logic;
    clk_40MHz_iserdes_ctrl              : in       std_logic_vector(NCBC-1 downto 0);
    clk_320MHz_iserdes_ctrl             : in       std_logic_vector(NCBC-1 downto 0);
    mmcme2_drp_sclk                     : in       std_logic;
    ipb_clk                             : in       std_logic;
    cbc_fast_signal_par                 : in       std_logic_vector(7 downto 0);
    cbc_i2c_bus_mosi_set                : in       cbc_i2c_bus_mosi_set_type;
    cbc_hard_reset                      : in       std_logic;
    external_l1a_veto                   : in       std_logic;
    cbc_emulator_out_ser_data_set       : in       cbc_data_set_type;    
    cbc_emulator_i2c_bus_miso_set       : in       cbc_i2c_bus_miso_set_type;
    ext_async_l1a                       : out      std_logic;
    cbc_par_data_set_o                  : out      cbc_par_data_set_type;
    cbc_i2c_bus_miso_set                : out      cbc_i2c_bus_miso_set_type;
    ext_clk_40MHz                       : out      std_logic;
    fmc_cbc_fast_signal_40MHz           : out      std_logic_vector(3 downto 0);
    cbc_emulator_clk_320MHz             : out      std_logic;
    cbc_emulator_in_serial_command      : out      std_logic;
    cbc_emulator_i2c_bus_mosi_set       : out      cbc_i2c_bus_mosi_set_type;
    cbc_emulator_hard_reset             : out      std_logic;
    test_signal_out                     : in       std_logic_vector(1 downto 0);
    mmcme2_drp_for_cbc_system_sig_in_set  : out    mmcme2_drp_for_cbc_system_sig_in_set_type;
    mmcme2_drp_for_cbc_system_sig_out_set : in     mmcme2_drp_for_cbc_system_sig_out_set_type;
    io_test_out                         : out      std_logic;
    dctsb_to_ipbif                      : out      dctsb_to_ipbif_type;
    ipbif_to_dctsb                      : in       ipbif_to_dctsb_type;
    idelay_locked_o                     : out      std_logic;
    data_clock_timing_tuning_test_out_o   : out      std_logic
    
);
end io;

architecture Behavioral of io is

    -- fast signal iserdes out      
    signal data_out_to_pins_p           : std_logic_vector(0 to NCBC-1);
    signal data_out_to_pins_n           : std_logic_vector(0 to NCBC-1);    
    signal clk_to_pins_p                : std_logic_vector(0 to NCBC-1);
    signal clk_to_pins_n                : std_logic_vector(0 to NCBC-1);    
    
    type data_from_pins_type is array (0 to NCBC-1) of std_logic_vector(6 downto 1); 
    signal data_in_from_pins_p            : data_from_pins_type;
    signal data_in_from_pins_n            : data_from_pins_type;

    type data_in_to_device_set_type is array(0 to NCBC-1) of std_logic_vector(8*6-1 downto 0);
    signal data_in_to_device_set          : data_in_to_device_set_type;
    signal data_iserdes_io_reset          : std_logic_vector(NCBC-1 downto 0);
    

    type in_delay_data_ce_set_type  is array (0 to NCBC-1) of std_logic_vector(5 downto 0);
    type in_delay_data_inc_set_type is array (0 to NCBC-1) of std_logic_vector(5 downto 0);
    type bitslip_set_type           is array (0 to NCBC-1) of std_logic_vector(5 downto 0);
    type data_timing_tuning_mmcme2_drp_saddr_set_type           is array (0 to NCBC-1) of std_logic_vector(1 downto 0);
        
    signal in_delay_reset                 : std_logic_vector(NCBC-1 downto 0);
    signal in_delay_data_ce_set           : in_delay_data_ce_set_type;
    signal in_delay_data_inc_set          : in_delay_data_inc_set_type;    
    signal in_delay_tap_in                : in_delay_tap_type;   
    signal in_delay_tap_out               : in_delay_tap_type;
    signal bitslip_set                    : bitslip_set_type;                
    signal data_clock_timing_tuning_done  : std_logic_vector(NCBC-1 downto 0);
    signal data_clock_timing_pulse_scan_done : std_logic_vector(NCBC-1 downto 0);
    signal idelay_locked                   : std_logic_vector(NCBC-1 downto 0);

    signal mmcme2_drp_for_cbc_system_sig_in_from_dctt_set  : mmcme2_drp_for_cbc_system_sig_in_set_type;

    signal cbc_par_data_set   : cbc_par_data_set_type;
    
    signal bram_to_dcts_set   : bram_to_dcts_set_type;
    signal dcts_to_bram_set   : dcts_to_bram_set_type;
    signal dcts_to_bram_set_sync : dcts_to_bram_set_type;
    
    signal data_clock_timing_tuning_test_out : std_logic_vector(NCBC-1 downto 0);
    
begin

    io_test_out <= idelay_locked(0);
    idelay_locked_o <= idelay_locked(0);
    data_clock_timing_tuning_test_out_o <= data_clock_timing_tuning_test_out(0);
    
    fast_signal_oserdes_gen:
    for i in 0 to NCBC-1 generate
    --===================--       
    fast_signal_oserdes_cbc0_inst : entity work.fast_signal_oserdes
    --===================--   
       port map 
       ( 
       data_out_from_device =>  bitswap(cbc_fast_signal_par),
       data_out_to_pins_p(0) => data_out_to_pins_p(i),
       data_out_to_pins_n(0) => data_out_to_pins_n(i),
       clk_to_pins_p         => clk_to_pins_p(i),
       clk_to_pins_n         => clk_to_pins_n(i),
       clk_in                => clk_320MHz_cbc_ctrl,                            
       clk_div_in            => clk_40MHz_cbc_ctrl,                        
       clk_reset             => ipb_ctrl.fs_oserdes_reset,        
       io_reset              => ipb_ctrl.fs_oserdes_reset
    );
    end generate;

    cbc_par_data_set_o <= cbc_par_data_set;
    --===========================================--    
    data_iserdes_gen :
	--===========================================--
    for i in 0 to NCBC-1 generate 

        -- data mapping of iserdes output 
        cbc_para_data_set_gen : for j in 1 to 6 generate

            slice_gen : for k in 0 to 7 generate
                cbc_par_data_set(i)(j)(7-k) <= data_in_to_device_set(i)(6*k+j-1);
            end generate;
        end generate;
        ipb_stat.cbcios(i).slvs5 <= cbc_par_data_set(i)(5);
                
        -- iserdes 
        data_iserdes_inst : entity work.data_iserdes
        port map 
        ( 
        data_in_from_pins_p => data_in_from_pins_p(i),
        data_in_from_pins_n => data_in_from_pins_n(i),
        data_in_to_device   => data_in_to_device_set(i),
        in_delay_reset      => in_delay_reset(i),                    
        in_delay_data_ce    => in_delay_data_ce_set(i),      
        in_delay_data_inc   => in_delay_data_inc_set(i),     
        in_delay_tap_in     => in_delay_tap_in(i),          
        in_delay_tap_out    => in_delay_tap_out(i),         
        delay_locked        => idelay_locked(i),                      
        ref_clock           => clk_200MHz,                         
        bitslip             => bitslip_set(i),                           
        clk_in              => clk_320MHz_iserdes_ctrl(i),                            
        clk_div_in          => clk_40MHz_iserdes_ctrl(i),                        
        io_reset            => data_iserdes_io_reset(i)
        );
        ipb_stat.cbcios(i).delay_locked <= idelay_locked(i);
        ipb_stat.cbcios(i).in_delay_tap_out <= in_delay_tap_out(i);
    end generate;       

    -- tuning
    dctt0_gen:    
    for j in 0 to NCBC-1 generate 
       
        data_clock_timing_tuning_inst : entity work.data_clock_timing_tuning
        generic map( TUNE_CLOCK => true )
            port map
            (
            clk_40MHz_iserdes_ctrl    => clk_40MHz_iserdes_ctrl(j), 
            ipb_cnfg                  => ipb_cnfg.data_clock_timing_tuning,
            reset                     => ipb_ctrl.data_clock_timing_tunings(j).reset,
            tune                      => ipb_ctrl.data_clock_timing_tunings(j).tune,
            scan_pulse                => ipb_ctrl.data_clock_timing_tunings(j).scan_pulse,
            sync_pattern              => "10000000",                
            mmcme2_drp_sclk           => mmcme2_drp_sclk,
            mmcme2_drp_srdy           => mmcme2_drp_for_cbc_system_sig_out_set(j).srdy,                
            mmcme2_drp_current_saddr  => mmcme2_drp_for_cbc_system_sig_out_set(j).saddr,
            in_delay_tap_out          => in_delay_tap_out(j),
            cbc_par_data              => cbc_par_data_set(j),        
            bram_to_dcts              => bram_to_dcts_set(j),             
            fsm_o                     => ipb_stat.cbcios(j).data_clock_timing_tuning_fsm,
            iserdes_idelay_ctrl_fsm_o => ipb_stat.cbcios(j).iserdes_idelay_ctrl_fsm,
            in_delay_reset_o          => in_delay_reset(j),
            in_delay_ce_o             => in_delay_data_ce_set(j),
            in_delay_tap_in_o         => in_delay_tap_in(j),
            in_delay_data_inc         => in_delay_data_inc_set(j),
            iserdes_io_reset_o        => data_iserdes_io_reset(j),                
            bitslip_o                 => bitslip_set(j),
            mmcme2_drp_rst_o          => mmcme2_drp_for_cbc_system_sig_in_from_dctt_set(j).rst,
            mmcme2_drp_sen_o          => mmcme2_drp_for_cbc_system_sig_in_from_dctt_set(j).sen,
            mmcme2_drp_saddr_o        => mmcme2_drp_for_cbc_system_sig_in_from_dctt_set(j).saddr,
            pattern_o                 => ipb_stat.cbcios(j).pattern,   
            bitslip_counter_o         => ipb_stat.cbcios(j).iserdes_bitslip_counter,
            done                      => data_clock_timing_tuning_done(j),
            pulse_scan_done_o         => data_clock_timing_pulse_scan_done(j),
            tuning_stat_set           => ipb_stat.cbcios(j).tuning_stat_set,
            dcts_to_bram              => dcts_to_bram_set(j),                          
            test_out                  => data_clock_timing_tuning_test_out(j)       
            );     

        mmcme2_drp_for_cbc_system_sig_in_set(j).sclk  <= mmcme2_drp_sclk;
        -- this part may not be needed any more.
        process (mmcme2_drp_sclk)
        begin           
            if rising_edge(mmcme2_drp_sclk) then
                mmcme2_drp_for_cbc_system_sig_in_set(j).rst <= '0';
                mmcme2_drp_for_cbc_system_sig_in_set(j).sen <= '0';
                if mmcme2_drp_for_cbc_system_sig_in_from_dctt_set(j).sen = '1' then
                    mmcme2_drp_for_cbc_system_sig_in_set(j).saddr <= mmcme2_drp_for_cbc_system_sig_in_from_dctt_set(j).saddr;
                    mmcme2_drp_for_cbc_system_sig_in_set(j).sen   <= '1';
                end if;
                if mmcme2_drp_for_cbc_system_sig_in_from_dctt_set(j).rst = '1' then
                    mmcme2_drp_for_cbc_system_sig_in_set(j).rst <= '1';
                end if;
            end if;
        end process;        
    end generate;       

   cbc_slvs5_scan_bram_gen :
   for i in 0 to NCBC - 1 generate          

        process (clk_40MHz_cbc_ctrl)
        begin
            if rising_edge(clk_40MHz_cbc_ctrl) then
                dcts_to_bram_set(i).wen <= '0';
                if dcts_to_bram_set(i).wen = '1' then
                    dcts_to_bram_set_sync(i).wen   <= '1';
                    dcts_to_bram_set_sync(i).waddr <= dcts_to_bram_set(i).waddr;
                    dcts_to_bram_set_sync(i).din <= dcts_to_bram_set(i).din;                    
                end if;
            end if;
        end process;
    end generate;        

    cbc_data_clock_timing_scan_bram_ctrl_inst : entity work.cbc_data_clock_timing_scan_bram_ctrl
    port map(   clk_40MHz           => clk_40MHz_cbc_ctrl,
                ipb_clk             => ipb_clk,
                reset               => ipb_ctrl.dctsb_reset,
                bram_to_dcts_set    => bram_to_dcts_set,
                dcts_to_bram_set    => dcts_to_bram_set_sync,
                ipbif_to_dctsb      => ipbif_to_dctsb,
                dctsb_to_ipbif      => dctsb_to_ipbif
    );
    

    --===================--   
    -- test signal out
    --===================--    
    test_signal_0           : obufds port map ( i=>test_signal_out(0), o=>fmc_l12_la_p(2), ob=>fmc_l12_la_n(2) );
    test_signal_1           : obufds port map ( i=>test_signal_out(1), o=>fmc_l12_la_p(4), ob=>fmc_l12_la_n(4) );
    --===================--   
    -- cbc hard reset
    --===================--      
    fmcl8_la_obuf_hard_reset : obufds port map ( i=>cbc_hard_reset, o=>fmc_l8_la_p(28), ob=>fmc_l8_la_n(28) );
    fmcl12_la_obuf_hard_reset : obufds port map ( i=>cbc_hard_reset, o=>fmc_l12_la_p(28), ob=>fmc_l12_la_n(28) );    
    --===================--  
    -- data in
    --===================--  
    data_in_from_pins_p(0)(1) <= fmc_l8_la_p(15);
    data_in_from_pins_p(0)(2) <= fmc_l8_la_p(20);
    data_in_from_pins_p(0)(3) <= fmc_l8_la_p(19);
    data_in_from_pins_p(0)(4) <= fmc_l8_la_p(21);
    data_in_from_pins_p(0)(5) <= fmc_l8_la_p(25);
    data_in_from_pins_p(0)(6) <= fmc_l8_la_p(24);
    data_in_from_pins_n(0)(1) <= fmc_l8_la_n(15);
    data_in_from_pins_n(0)(2) <= fmc_l8_la_n(20);
    data_in_from_pins_n(0)(3) <= fmc_l8_la_n(19);
    data_in_from_pins_n(0)(4) <= fmc_l8_la_n(21);
    data_in_from_pins_n(0)(5) <= fmc_l8_la_n(25);
    data_in_from_pins_n(0)(6) <= fmc_l8_la_n(24);      
    data_in_from_pins_p(1)(1) <= fmc_l12_la_p(15);
    data_in_from_pins_p(1)(2) <= fmc_l12_la_p(20);
    data_in_from_pins_p(1)(3) <= fmc_l12_la_p(19);
    data_in_from_pins_p(1)(4) <= fmc_l12_la_p(21);
    data_in_from_pins_p(1)(5) <= fmc_l12_la_p(25);
    data_in_from_pins_p(1)(6) <= fmc_l12_la_p(24);
    data_in_from_pins_n(1)(1) <= fmc_l12_la_n(15);
    data_in_from_pins_n(1)(2) <= fmc_l12_la_n(20);
    data_in_from_pins_n(1)(3) <= fmc_l12_la_n(19);
    data_in_from_pins_n(1)(4) <= fmc_l12_la_n(21);
    data_in_from_pins_n(1)(5) <= fmc_l12_la_n(25);
    data_in_from_pins_n(1)(6) <= fmc_l12_la_n(24);                
    --===================--                       
    -- i2c
    --===================--  
    fmcl8_la_obuf_cbc_out_sda_set_0          : ibufds generic map( DIFF_TERM => TRUE ) port map (i=>fmc_l8_la_p(29), ib=>fmc_l8_la_n(29), o=> cbc_i2c_bus_miso_set(0).sda ); 	
    fmcl8_la_ibuf_cbc_in_sda_set_0           : obufds port map ( i=>cbc_i2c_bus_mosi_set(0).sda, o=>fmc_l8_la_p(31), ob=>fmc_l8_la_n(31) );
	fmcl8_la_ibuf_cbc_in_scl_set_0           : obufds port map ( i=>cbc_i2c_bus_mosi_set(0).scl, o=>fmc_l8_la_p(30), ob=>fmc_l8_la_n(30) );
    fmcl12_la_obuf_cbc_out_sda_set_1         : ibufds generic map( DIFF_TERM => TRUE ) port map (i=>fmc_l12_la_p(29), ib=>fmc_l12_la_n(29), o=> cbc_i2c_bus_miso_set(1).sda ); 	
    fmcl12_la_ibuf_cbc_in_sda_set_1          : obufds port map ( i=>cbc_i2c_bus_mosi_set(1).sda, o=>fmc_l12_la_p(31), ob=>fmc_l12_la_n(31) );
    fmcl12_la_ibuf_cbc_in_scl_set_1          : obufds port map ( i=>cbc_i2c_bus_mosi_set(1).scl, o=>fmc_l12_la_p(30), ob=>fmc_l12_la_n(30) );
    --===================--  
    -- fast signals
    --===================--   
    fmc_l8_la_p(16) <= data_out_to_pins_p(0);
    fmc_l8_la_n(16) <= data_out_to_pins_n(0);
    fmc_l8_la_p(11) <= clk_to_pins_p(0);
    fmc_l8_la_n(11) <= clk_to_pins_n(0);
    fmc_l12_la_p(16) <= data_out_to_pins_p(1);
    fmc_l12_la_n(16) <= data_out_to_pins_n(1);
    fmc_l12_la_p(11) <= clk_to_pins_p(1);
    fmc_l12_la_n(11) <= clk_to_pins_n(1); 
    --===================--  
    -- external asynchronous l1a signal
    --===================--  
    fmcl8_la_ibuf_async_l1a : ibufds generic map( DIFF_TERM => TRUE ) port map (i => fmc_l8_la_p(7), ib => fmc_l8_la_n(7), o => ext_async_l1a );
 

end Behavioral;
