--===========================--
-- cbc_i2c_bus_manager 
-- 28.10.2016 Kirika Uchida
--===========================--


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;
--use work.user_package.all;
use work.cbc_system_package.all;

entity cbc_i2c_bus_manager is
	port ( 
    clk_40MHz                       : in  std_logic;
    ipb_ctrl                        : in  cbc_i2c_bus_manager_ctrl_type;
    fe_cnfg                         : in  fe_cnfg_type;
    i2c_bus_miso                    : in  cbc_i2c_bus_miso_type;
    i2c_bus_mosi                    : out cbc_i2c_bus_mosi_type;
    ipb_stat                        : out cbc_i2c_bus_manager_stat_type;
    ci2cbmgr_in                     : in  ci2cbmgr_in_type;
    ci2cbmgr_to_ipbif               : out ci2cbmgr_out_type;
    test_out                 : out std_logic
    );
end cbc_i2c_bus_manager;

architecture Behavioral of cbc_i2c_bus_manager is

    signal command_fifo_re                   : std_logic;
    signal command_fifo_empty                : std_logic;
    signal command_fifo_full                 : std_logic;
    signal command_fifo_dout                 : std_logic_vector(31 downto 0);
    signal reply_ready                       : std_logic;
    signal reply_fifo_din                    : std_logic_vector(31 downto 0);
    signal reply_fifo_empty                  : std_logic;
    
begin
 
    test_out <= reply_fifo_din(3);
 
    cbc_i2c_command_fifo_inst : entity work.cbc_i2c_command_fifo
    PORT map(   
                rst    => ci2cbmgr_in.command_fifo_reset,
                clk    => clk_40MHz,
                din    => ci2cbmgr_in.command_fifo_din,
                wr_en  => ci2cbmgr_in.command_fifo_we,
                rd_en  => command_fifo_re,
                dout   => command_fifo_dout,
                full   => command_fifo_full,
                empty  => command_fifo_empty
    );

 
    cbc_i2c_transaction_inst : entity work.cbc_i2c_transaction
    port map(   clk0                    => clk_40MHz,
                reset                   => ipb_ctrl.reset,
                initialize              => ipb_ctrl.init,        
                fe_cnfg                 => fe_cnfg,
                scl_o                   => i2c_bus_mosi.scl,
                sda_i                   => i2c_bus_miso.sda,    
                sda_o                   => i2c_bus_mosi.sda,   
                command_fifo_empty      => command_fifo_empty,
                command_fifo_dout       => command_fifo_dout,
                command_fifo_re_o       => command_fifo_re,
                reply_ready             => reply_ready,
                reply_fifo_din          => reply_fifo_din,
                initialized             => ipb_stat.bus_ready,
                fsm_state_cmd_wait      => ipb_stat.bus_waiting
    );                             
--    ci2cbmgr_to_ipbif.reply_fifo_dout <= reply_fifo_din;
    cbc_i2c_reply_fifo_inst : entity work.cbc_i2c_reply_fifo
    port map ( ci2c_clk    => clk_40MHz,
               ipb_clk     => ci2cbmgr_in.ipb_clk,
               reset       => ci2cbmgr_in.reply_fifo_reset,
               reply_ready => reply_ready,
               din_i       => reply_fifo_din,
               read_next   => ci2cbmgr_in.reply_fifo_read_next,      
               dout        => ci2cbmgr_to_ipbif.reply_fifo_dout,
               empty_o     => reply_fifo_empty,
               full_o      => ipb_stat.reply_fifo.full,
               ndata_o     => ipb_stat.reply_fifo.ndata,
               nrdata_o    => ipb_stat.reply_fifo.nrdata
               );

    ipb_stat.command_fifo.empty    <= command_fifo_empty;
    ipb_stat.command_fifo.full     <= command_fifo_full;
   
    ci2cbmgr_to_ipbif.reply_fifo_empty <= reply_fifo_empty;
    ipb_stat.reply_fifo.empty          <= reply_fifo_empty;

    ipb_stat.n_active_cbc <= fe_cnfg.n_active_cbc;

end Behavioral;
