----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03.05.2017 22:41:27
-- Design Name: 
-- Module Name: data_iserdes_pre - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity data_iserdes_pre is
generic ( constant SYS_W : positive:= 6; constant DEV_W : positive:= 48 ); 
port 
( 
    data_in_from_pins_p : in std_logic_vector(SYS_W-1 downto 0);
    data_in_from_pins_n : in std_logic_vector(SYS_W-1 downto 0);
    data_in_to_device   : out std_logic_vector(DEV_W-1 downto 0);
    in_delay_reset      : in std_logic;                   
    in_delay_data_ce    : in std_logic_vector(SYS_W-1 downto 0);
    in_delay_data_inc   : in std_logic_vector(SYS_W-1 downto 0);
    in_delay_tap_in     : in std_logic_vector(5*SYS_W-1 downto 0);       
    in_delay_tap_out    : out std_logic_vector(5*SYS_W-1 downto 0);                        
    bitslip             : in std_logic_vector(SYS_W-1 downto 0);                               
    clk_in              : in std_logic;                           
    clk_div_in          : in std_logic;              
    io_reset            : in std_logic
);
end data_iserdes_pre;

architecture Behavioral of data_iserdes_pre is

    signal in_delay_tap_in_reg     : std_logic_vector(5*SYS_W-1 downto 0);           
    signal in_delay_reset_reg      : std_logic;   
begin
    
    process (clk_in)
    begin
        if rising_edge(clk_div_in) then
            in_delay_tap_in_reg <= in_delay_tap_in;
            in_delay_reset_reg  <= in_delay_reset;
        end if;
    end process;
    
    data_iserdes_inst : entity work.data_iserdes
    port map 
    ( 
    data_in_from_pins_p => data_in_from_pins_p,
    data_in_from_pins_n => data_in_from_pins_n,
    data_in_to_device   => data_in_to_device,
    in_delay_reset      => in_delay_reset_reg,                    
    in_delay_data_ce    => in_delay_data_ce,      
    in_delay_data_inc   => in_delay_data_inc,     
    in_delay_tap_in     => in_delay_tap_in_reg,          
    in_delay_tap_out    => in_delay_tap_out,                  
    bitslip             => bitslip,                                  
    clk_in              => clk_in,                            
    clk_div_in          => clk_div_in,                           
    io_reset            => io_reset
    );


end Behavioral;
