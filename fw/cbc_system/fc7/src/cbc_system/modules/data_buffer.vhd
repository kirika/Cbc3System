--===========================--
-- data_buffer 
-- 28.10.2016 Kirika Uchida
--===========================--

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;
use work.cbc_system_package.all;

entity data_buffer is
port (
    clk_320MHz               : in  std_logic; --320MHz
    clk_slow                 : in  std_logic;
    ebodbi                   : in  ebodbi_type;
    ebidbo                   : out ebidbo_type;
    ipbif_to_db              : in  ipbif_to_db_type;
    db_to_ipbif              : out db_to_ipbif_type;
    ipb_cnfg                 : in  data_buffer_cnfg_type;
    ipb_stat                 : out data_buffer_stat_type;
    ipb_ctrl                 : in  data_buffer_ctrl_type;
    n_word_free_o            : out std_logic_vector(DATA_BUFFER_ADDR_WIDTH downto 0);
    l1a_veto                 : out std_logic
    );
end data_buffer;

architecture fc7_ring_buffer of data_buffer is
    signal n_word_all       : std_logic_vector(DATA_BUFFER_ADDR_WIDTH downto 0);
    signal n_word_events    : std_logic_vector(DATA_BUFFER_ADDR_WIDTH downto 0);
    signal n_word_free      : std_logic_vector(DATA_BUFFER_ADDR_WIDTH downto 0);
    signal safe_n_word_free : std_logic_vector(DATA_BUFFER_ADDR_WIDTH downto 0);    
    
--    signal waddr            : std_logic_vector(DATA_BUFFER_ADDR_WIDTH-1 downto 0);
    signal raddr            : std_logic_vector(DATA_BUFFER_ADDR_WIDTH-1 downto 0);    
    
    signal bram_clka        : std_logic;
    signal bram_we          : std_logic;
    signal bram_addra       : std_logic_vector(DATA_BUFFER_ADDR_WIDTH - 1 downto 0);
    signal bram_dina        : std_logic_vector(31 downto 0);
    signal bram_clkb        : std_logic;    
    signal bram_addrb       : std_logic_vector(DATA_BUFFER_ADDR_WIDTH - 1 downto 0);
    signal bram_doutb       : std_logic_vector(31 downto 0);
    
    signal we               : std_logic;
    signal data             : std_logic_vector(31 downto 0);
    signal end_of_event     : std_logic;

begin

    process ( ipb_ctrl.reset, clk_320MHz )
    begin
        if ipb_ctrl.reset = '1' then
            l1a_veto <= '0';
            safe_n_word_free <= ipb_cnfg.safe_n_word_free;               
        
        elsif rising_edge(clk_320MHz) then

            if unsigned(n_word_free) < unsigned(safe_n_word_free) then
                l1a_veto <= '1';
            else
                l1a_veto <= '0';
            end if;
        end if;
    end process;

    process (clk_320MHz)
    begin
        if rising_edge(clk_320MHz) then
            we           <= ebodbi.we;
            data         <= ebodbi.data;
            end_of_event <= ebodbi.end_of_event;
        end if;
    end process;


    ring_buffer_inst : entity work.ring_buffer
    generic map( ADDR_WIDTH => DATA_BUFFER_ADDR_WIDTH )
    port map(
        rst                        => ipb_ctrl.reset,
        clk                        => clk_320MHz,
        clk_slow                   => clk_slow,
        din                        => data,
        we                         => we,
        end_of_event               => end_of_event,
        trig_readall               => ipb_ctrl.trig_readall,
        raddr                      => raddr,  
        next_raddr_o               => db_to_ipbif.next_addr,
        next_rdata                 => db_to_ipbif.next_data,
        load_next                  => db_to_ipbif.load_next,
--        waddr_o                    => ipb_stat.waddr,
        n_word_all                 => n_word_all,
        n_word_events              => n_word_events,
        n_word_free                => n_word_free,
        n_word_all_slow            => ipb_stat.n_word_all,
        n_word_events_slow         => ipb_stat.n_word_events,
        n_word_free_slow           => ipb_stat.n_word_free,
        werr                       => ipb_stat.werr,
        rerr                       => ipb_stat.rerr,
        bram_clka                  => bram_clka,
        bram_wea                   => bram_we,
        bram_addra                 => bram_addra,
        bram_dina                  => bram_dina,
        bram_clkb                  => bram_clkb,
        bram_addrb                 => bram_addrb,
        bram_doutb                 => bram_doutb           
    );

    ebidbo.n_word_free   <= n_word_free;
    n_word_free_o        <= n_word_free;
    raddr <= ipbif_to_db.addr;
    ipb_stat.raddr <= raddr;

	d64k_data_buffer_gen : if DATA_BUFFER_ADDR_WIDTH = 16 generate    
    fc7_bram_inst : ENTITY work.data_buffer_bram
      PORT map(
        clka  => bram_clka,
        wea(0)=> bram_we,
        addra => bram_addra,
        dina  => bram_dina,
        clkb  => bram_clkb,
        addrb => bram_addrb,
        doutb => bram_doutb
      );
      end generate d64k_data_buffer_gen;
     
	d128k_data_buffer_gen : if DATA_BUFFER_ADDR_WIDTH = 17 generate    
    fc7_bram_inst : ENTITY work.data_buffer_bram_d128k
      PORT map(
        clka  => bram_clka,
        wea(0)=> bram_we,
        addra => bram_addra,
        dina  => bram_dina,
        clkb  => bram_clkb,
        addrb => bram_addrb,
        doutb => bram_doutb
      );
      end generate d128k_data_buffer_gen;     

	d256k_data_buffer_gen : if DATA_BUFFER_ADDR_WIDTH = 18 generate    
    fc7_bram_inst : ENTITY work.data_buffer_bram_d256k
      PORT map(
        clka  => bram_clka,
        wea(0)=> bram_we,
        addra => bram_addra,
        dina  => bram_dina,
        clkb  => bram_clkb,
        addrb => bram_addrb,
        doutb => bram_doutb
      );
      end generate d256k_data_buffer_gen;     
      
      
end fc7_ring_buffer;
