----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 13.01.2017 15:58:51
-- Design Name: 
-- Module Name: data_iserdes_tuning - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;
use work.cbc_system_package.all;

entity data_iserdes_tuning is
  Port ( 
    reset         : in  std_logic;
    tune          : in  std_logic;
    clk_320MHz    : in  std_logic;
    clk_40MHz     : in  std_logic;
    idelay_tap_out: in  std_logic_vector(5*6-1 downto 0);    
    par_data      : in  cbc_par_data_type;
    idelay_ce     : out std_logic;
    idelay_reset  : out std_logic;
    idelay_tap_in_o : out std_logic_vector(5*6-1 downto 0);
    bitslip       : out std_logic
  );
end data_iserdes_tuning;

architecture Behavioral of data_iserdes_tuning is

    signal index            : unsigned(2 downto 0);
    signal flip_40MHz       : std_logic;
    signal flip_40MHz_d1    : std_logic;
    signal flip_40MHz_d2    : std_logic;
    signal idelay_tap_found : std_logic;
    signal idelay_tap_in    : std_logic_vector(5*6-1 downto 0);
    signal idelay_tap_5     : unsigned(4 downto 0);
    signal idelay_tap_inc   : std_logic;
    signal idelay_tap_ready : std_logic;
begin

    process  (reset,clk_40MHz)
    begin
        if reset = '1' then        
            index <= (others => '0');
            flip_40MHz <= '0';
        elsif rising_edge(clk_40MHz) then
            flip_40MHz   <= not flip_40MHz;
            idelay_ce    <= '0';
            idelay_reset <= '0';
            if idelay_tap_inc = '1' then
                idelay_ce <= '1';
            end if;
            if idelay_tap_ready = '1' then
                idelay_reset <= '1';
            end if;            
        end if;
    end process;
    
    idelay_tuning : process  (reset, clk_320MHz)
    variable index         : integer range 0 to 7;
    variable bitslip_timer : integer range 0 to 3;
    begin
        if reset = '1' then        
            index := 0;
            flip_40MHz_d1 <= '0';
            flip_40MHz_d2 <= '0';
            idelay_tap_inc   <= '0';  
            idelay_tap_ready <= '0';           
        elsif tune = '1' then
            fsm   <= sync_index;
        elsif rising_edge(clk_320MHz) then
            flip_40MHz_d1 <= flip_40MHz;
            flip_40MHz_d2 <= flip_40MHz_d1;
            if index = 7 then
                index := 0;
            else    
                index := index + 1;
            end if;
            case fsm is
            when sync_index =>
                if flip_40MHz_d1 xor flip_40MHz_d2 then
                    index := 2;
                    fsm <= slvs5_idelay_reset;
                end if;
            when slvs5_idelay_reset =>
                idelay_tap_in <= idelay_tap_out;            
                if index = 3 then
                    idelay_tap_in(5*5-1 downto 5*4) <= "00000";
                    idelay_tap_ready <= '1';
                end if;
                if idelay_tap_ready = '1' then
                    if index = 0 then
                        idelay_tap_ready <= '0';
                        fsm <= slvs5_bitslip_tuning;
                    end if;
                end if;                
            when slvs5_bitslip_tuning =>
                if index = 4 then
                    if bitslip_timer = 0 then
                        if par_data(5) /= "10000000" then
                            bitslip <= '1';
                            bitslip_timer := 1;
                        else
                            idelay_tap_found <= '0';                        
                            fsm <= slvs5_idelay_tuning;
                        end if;
                    elsif bitslip_timer /= 5 then
                        bitslip_timer := bitslip_timer + 1;
                    end if;
                elsif index = 0 then
                    if bitslip_timer = 5 then
                        if par_data(5) = "10000000" then
                            idelay_tap_found <= '0';
                            fsm <= slvs5_idelay_tuning;
                        end if;
                    end if;                            
                end if;     
            when slvs5_idelay_tuning => 
                idelay_tap_in <= idelay_tap_out;
                idelay_tap_5 <= unsigned(idelay_tap_out(5*5-1 downto 5*4));                
                if index = 0 then
                    if par_data(5) = "01000000" then
                        idelay_tap_found <= '1';
                        idelay_tap_inc   <= '0';  
                    else
                        if idelay_tap_5 /= "11110" then
                             idelay_tap_inc   <= '1';
                            fsm <= change_clock_phase;
                        else
                            idelay_tap_inc   <= '1';
                        end if;      
                    end if;
                end if;
                
                if idelay_tap_found = '1' then                   
                    if index = 1 then
                        idelay_tap_5 <= idelay_tap_5 - 20; 
                    elsif index = 2 then
                        idelay_tap_in(5*5-1 downto 5*4) <= std_logic_vector(idelay_tap_5);
                    elsif index = 3 then
                        idelay_tap_ready <= '1';
                        fsm <= slvs5_idelay_tuned;
                    end if; 
                end if;   
            when change_clock_phase =>
                
            when slvs5_idelay_tuned =>
                if index = 0 then
                    idelay_tap_ready <= '0';
                end if;
            end case; 
        end if;     
    end process;


end Behavioral;
