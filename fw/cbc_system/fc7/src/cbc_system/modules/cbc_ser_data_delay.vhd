--===========================--
-- ser_data_delay 
-- 28.10.2016 Kirika Uchida
--===========================--

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.cbc_system_package.all;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity cbc_ser_data_delay is
  Port ( 
  clk_320MHz               : in  std_logic;
  cbc_ser_data             : in  std_logic_vector( 6 downto 1 );
  delayed_cbc_ser_data_o   : out std_logic_vector( 6 downto 1 );
  cbc_ser_data_delay_o     : out std_logic_vector( 4 downto 0 );
  idelay_tuning_reset      : in  std_logic;
  idelay_tuning_start      : in  std_logic;
  idelay_tuning_fsm        : out idelay_tuning_fsm_type
  );
end cbc_ser_data_delay;

architecture fc7 of cbc_ser_data_delay is

    signal idelay_tuning_ld       : std_logic;
    signal cbc_ser_data_delay     : std_logic_vector(4 downto 0);    
    type cbc_ser_data_delay_set_type is array (1 to 6) of std_logic_vector(4 downto 0);
    signal cbc_ser_data_delay_set : cbc_ser_data_delay_set_type;
    signal delayed_cbc_ser_data   : std_logic_vector( 6 downto 1 );

begin

--     -- IDELAY2 78 ps / tap
--    IDELAYCTRL_inst : IDELAYCTRL
--    port map (
--       RDY    => idelayctrl_rdy,       -- 1-bit output: Ready output
--       REFCLK => clk_200MHz, -- 1-bit input: Reference clock input
--       RST    => idelayctrl_reset        -- 1-bit input: Active high reset input
--    );

    cbc_ser_data_delay_gen :
    for i in 1 to 6 generate
    begin
        IDELAYE2_inst : IDELAYE2
        generic map (
          CINVCTRL_SEL => "FALSE",          -- Enable dynamic clock inversion (FALSE, TRUE)
          DELAY_SRC => "IDATAIN",           -- Delay input (IDATAIN, DATAIN)
          HIGH_PERFORMANCE_MODE => "FALSE", -- Reduced jitter ("TRUE"), Reduced power ("FALSE")
          IDELAY_TYPE => "VAR_LOAD",        -- FIXED, VARIABLE, VAR_LOAD, VAR_LOAD_PIPE
          IDELAY_VALUE => 0,                -- Input delay tap setting (0-31)
          PIPE_SEL => "FALSE",              -- Select pipelined mode, FALSE, TRUE
          REFCLK_FREQUENCY => 192.0,        -- IDELAYCTRL clock input frequency in MHz (190.0-210.0, 290.0-310.0).
          SIGNAL_PATTERN => "DATA"          -- DATA, CLOCK input signal
        )
        port map (
          CNTVALUEOUT => cbc_ser_data_delay_set(i),   -- 5-bit output: Counter value output
          DATAOUT     => delayed_cbc_ser_data(i),     -- 1-bit output: Delayed data output
          C           => clk_320MHz,                  -- 1-bit input: Clock input
          CE          => '0',                         -- 1-bit input: Active high enable increment/decrement input
          CINVCTRL    => '0',                         -- 1-bit input: Dynamic clock inversion input
          CNTVALUEIN  => cbc_ser_data_delay,          -- 5-bit input: Counter value input
          DATAIN      => '0',                         -- 1-bit input: Internal delay data input
          IDATAIN     => cbc_ser_data(i),             -- 1-bit input: Data input from the I/O
          INC         => '0',                         -- 1-bit input: Increment / Decrement tap delay input
          LD          => idelay_tuning_ld,            -- 1-bit input: Load IDELAY_VALUE input
          LDPIPEEN    => '0',                         -- 1-bit input: Enable PIPELINE register to load data input
          REGRST      => '0'                          -- 1-bit input: Active-high reset tap-delay input
        );
    end generate;
    
	idelay_tuning_inst : entity work.idelay_tuning(fc7)
	generic map( COUNTER_WIDTH => 5 )
	Port map( clk_320MHz => clk_320MHz,
			delayed_din    => delayed_cbc_ser_data(5),
			reset          => idelay_tuning_reset,
			start          => idelay_tuning_start,
			fsm_o          => idelay_tuning_fsm,
			delay_o        => cbc_ser_data_delay,
			ld_to_idelay   => idelay_tuning_ld );

		 delayed_cbc_ser_data_o   <= delayed_cbc_ser_data;
		 cbc_ser_data_delay_o     <= cbc_ser_data_delay;
end fc7;
