--===========================--
-- cbc_i2c_reply_fifo 
-- 28.10.2016 Kirika Uchida
--===========================--


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity cbc_i2c_reply_fifo is
generic ( ADDR_WIDTH : positive := 16 );
port(   ci2c_clk    : in  std_logic;
        ipb_clk     : in  std_logic;
        reset       : in  std_logic;
        reply_ready : in std_logic;
        din_i       : in  std_logic_vector(31 downto 0);      
        read_next   : in  std_logic; 
        dout        : out std_logic_vector(31 downto 0);
        empty_o     : out std_logic;
        full_o      : out std_logic;
        ndata_o     : out std_logic_vector(ADDR_WIDTH downto 0);
        nrdata_o    : out std_logic_vector(ADDR_WIDTH downto 0)
);
end cbc_i2c_reply_fifo;

architecture Behavioral of cbc_i2c_reply_fifo is

    signal we               : std_logic;
    signal waddr            : std_logic_vector(ADDR_WIDTH downto 0);
    signal din              : std_logic_vector(31 downto 0);  
    signal ndata            : unsigned(ADDR_WIDTH downto 0);
    signal nrdata           : unsigned(ADDR_WIDTH downto 0);    
    signal full             : std_logic;
    signal raddr            : std_logic_vector(ADDR_WIDTH downto 0);
    signal empty            : std_logic;
    constant max_events     : integer := 2**ADDR_WIDTH;

begin

 	--=============================--
    process(reset, ci2c_clk)
    --=============================--
    begin
    if reset='1' then     
        we       <= '0';
        waddr    <= ( others => '0' );
        ndata    <= ( others => '0' );   
    elsif rising_edge(ci2c_clk) then
  
        we         <= '0';
        if reply_ready = '1' then
   
            if full = '0' then
                we        <= '1';
                waddr     <= std_logic_vector(ndata);
                ndata     <= ndata + 1;
                din       <= din_i;
            end if;
        end if;
    end if;
    end process;

    -- nrdata is the # of data which was read before read_next = '1'
    -- # of data presented at dout is nrdata - 1
	--=============================--
    process(reset, ipb_clk)
    --=============================--
    begin
    if reset='1' then     
        raddr  <= (others => '0');
        nrdata <= (others => '0');
    elsif rising_edge(ipb_clk) then
        if read_next = '1' and empty = '0' then
            raddr <= std_logic_vector(nrdata+1);
            nrdata <= nrdata + 1;
        end if;
    end if;
    end process;

    empty <= '1' when ndata = to_unsigned( 0, ADDR_WIDTH+1 ) else '0';
    full  <= '1' when ndata = to_unsigned( max_events, ADDR_WIDTH+1 ) else '0';

    -- 32 x 65536
    cbc_i2c_reply_bram : ENTITY work.cbc_i2c_reply_bram
    PORT map (
        clka => ci2c_clk,
        wea(0)  => we,
        addra   => waddr(ADDR_WIDTH - 1 downto 0),
        dina    => din,
        clkb    => ipb_clk,
        addrb   => raddr(ADDR_WIDTH - 1 downto 0),
        doutb   => dout
    );
 
    ndata_o  <= std_logic_vector(ndata);
    nrdata_o <= std_logic_vector(nrdata);
    full_o   <= full;
    empty_o  <= empty;
end Behavioral;
