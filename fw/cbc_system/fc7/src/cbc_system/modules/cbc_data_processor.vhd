--===========================--
-- cbc_data_processor
--  consists of
--   + cbc_ser_data_delay      :  cbc serial data timing (slvs5) to the 320MHz clock is tuned.
--   + cbc_data_deser          :  cbc serial data are deserialized 320MHz -> 40MHz
--   + cbc_data_frame_receiver :  data frames are identified.
--   + cbc_data_frame_processor:  doing nothing.  Just adding another clock delay.  For further extension.
--   + trig_data_shift_reg     :  trigger data are put into pipeline.
--   + trig_data_fifo          :  with L1A, the corresponding trigger data are put into the fifo buffer to be matched with the data.
--   + cbc_data_fifo           :  the data from cbc_data_frame_processor are put into the fifo buffer. 8bit in 32 bit out 
--   + cbc_data_info_fifo      :  data info are put in fifo. One word per event.  
--   + cbc_data_fifo_write_fsm :  trigger data and triggered data are merged and put into the cbc_data_fifo with the header. 8bit in 32bit out
--   + cbc_data_packet_send_fsm:  data are sent out when the data fifo is not empty.  
-- 28.10.2016 Kirika Uchida
--===========================--

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

use work.cbc_system_package.all;

entity cbc_data_processor is
generic ( CBCDATA : cbcdata_type := raw; SEGMENT_DATA_SIZE_WIDTH : integer :=8 );
port ( 
    be_id                    : in  std_logic_vector(6 downto 0);
    fe_id                    : in  std_logic_vector(2 downto 0);
    cbc_id                   : in  std_logic_vector(4 downto 0);
    cbc_active               : in  std_logic;
    clk_320MHz               : in  std_logic;
    clk_40MHz                : in  std_logic;
    cbc_par_data             : in  cbc_par_data_type;
    ipb_ctrl                 : in  cbc_data_processor_ctrl_type;
    ipb_cnfg                 : in  cbc_data_processor_cnfg_type;
    ipb_stat                 : out cbc_data_processor_stat_type;
    l1a_320MHz               : in  std_logic;
    dsoebi                   : out dsoebi_type;
    dsiebo                   : in  dsiebo_type;
    cbc_data_frame_counter_o : out std_logic_vector(31 downto 0)
);
end cbc_data_processor;

architecture fc7 of cbc_data_processor is

    constant cbc_data_header_size      : unsigned(7 downto 0) := x"01";

    signal delayed_cbc_ser_data        : std_logic_vector(6 downto 1);
    signal cbc_data_delay_idelay_tuned : std_logic;
    -- for cbc_slvs_deser     
    signal cbc_data_deser_reset        : std_logic;
--    signal cbc_par_data                : cbc_par_data_type;
    signal cbc_par_data_update         : std_logic;
    -- data frame shift register & buffer
    type cbc_data_frame_buffer_type is array (0 to 34) of std_logic_vector(7 downto 0);
    signal cbc_par_data_sr             : cbc_data_frame_buffer_type;
    signal cbc_data_frame              : cbc_data_frame_buffer_type;
    signal cbc_data_frame_l1_count     : std_logic_vector(8 downto 0);
    signal cbc_data_frame_ready        : std_logic;
    
    signal cbc_data_frame_counter      : unsigned(31 downto 0);
    -- triggered data (result of data frame processing)

    signal cbc_triggered_data          : cbc_triggered_data_buffer_type;
    signal cbc_triggered_data_ready    : std_logic;
    
    -- trigger data pipeline    
    signal trig_data_pipe_l1a_lat      : unsigned(8 downto 0);
    signal trig_data_shift_out         : std_logic_vector(39 downto 0);
    -- trigger data buffer
    signal trig_data_fifo_we           : std_logic;
    signal trig_data_fifo_re           : std_logic;
    signal trig_data_fifo_empty        : std_logic;
    signal trig_data_fifo_full         : std_logic;
    signal trig_data_fifo_werr         : std_logic;
    signal trig_data_fifo_rerr         : std_logic;
    signal trig_data_fifo_din          : std_logic_vector(39 downto 0);
    signal trig_data_fifo_dout         : std_logic_vector(39 downto 0);
       
    -- in byte
    constant cbc_trig_data_size        : unsigned(11 downto 0) := x"005"; -- 5
    constant cbc_data_frame_size       : unsigned(11 downto 0) := x"023"; --35
    signal cbc_triggered_data_size     : unsigned(11 downto 0);
    signal cbc_data_size               : unsigned(11 downto 0);

    signal cbc_data_l1_count_latched   : std_logic_vector(8 downto 0);

    signal cbc_data_packet_header      : std_logic_vector(31 downto 0);
    signal cbc_data_packet_size        : unsigned(11 downto 0);

    signal cbc_data_fifo_din         : std_logic_vector(7 downto 0);
    signal cbc_data_fifo_wr_en       : std_logic;
    signal cbc_data_fifo_rd_en       : std_logic;
    signal cbc_data_fifo_dout        : std_logic_vector(31 downto 0);
    signal cbc_data_fifo_full        : std_logic;
    signal cbc_data_fifo_empty       : std_logic;   
    signal cbc_data_fifo_rd_count    : STD_LOGIC_VECTOR(10 DOWNTO 0);
    signal cbc_data_fifo_wr_count    : STD_LOGIC_VECTOR(12 DOWNTO 0);        
    signal cbc_data_fifo_wr_index    : unsigned(SEGMENT_DATA_SIZE_WIDTH-1 downto 0);
        
    signal cbc_data_info_fifo_din         : std_logic_vector(63 downto 0);
    signal cbc_data_info_fifo_wr_en       : std_logic;
    signal cbc_data_info_fifo_rd_en       : std_logic;
    signal cbc_data_info_fifo_dout        : std_logic_vector(63 downto 0);
    signal cbc_data_info_fifo_full        : std_logic;
    signal cbc_data_info_fifo_empty       : std_logic;   
    signal cbc_data_info_fifo_data_count  : std_logic_vector(8 downto 0);
--    signal ipb_stat                       : cbc_data_processor_stat_type;
    
--    type cbc_data_fifo_write_fsm_type is (idle, write_trig_data, wait_triggered_data, write_triggered_data, write_data_paddings, write_data_info);
    signal cbc_data_fifo_write_fsm        : cbc_data_fifo_write_fsm_type;
--    type cbc_data_packet_send_fsm_type is (idle, send_data_ready, send_packet);
    signal cbc_data_packet_send_fsm       : cbc_data_packet_send_fsm_type;  

    signal data_packet_nword_pre          : unsigned(DATA_SECTION_DATA_SIZE_WIDTH-1 downto 0);    
    signal data_packet_nword              : unsigned(DATA_SECTION_DATA_SIZE_WIDTH-1 downto 0);
    signal data_packet_ready              : std_logic;
    signal data_packet_nword_sent         : unsigned(DATA_SECTION_DATA_SIZE_WIDTH-1 downto 0);
    
    signal read_data_fifo_en              : std_logic;
	signal reset_320MHz                   : std_logic;
	
	type trig_data_fifo_data_set_type is array (0 to to_integer(cbc_trig_data_size) - 1) of std_logic_vector(7 downto 0);
	signal trig_data_fifo_data_set        : trig_data_fifo_data_set_type;

	signal cbc_data_fifo_din_tmp    : std_logic_vector(7 downto 0); 

begin

--		reset_inst : process (clk_320MHz)
--		begin
--			if rising_edge(clk_320MHz) then
--				reset_320MHz <= ipb_ctrl.reset;
--			end if;
--		end process;


-- cbc_data_frame_receiver
    cbc_data_frame_receiver : process (ipb_ctrl.reset_40MHz, clk_40MHz)
    variable frame_ready : boolean;
    begin
        if ipb_ctrl.reset_40MHz = '1' then
            cbc_par_data_sr         <= (others => (others => '0'));
            cbc_data_frame          <= (others => (others => '0'));
            cbc_data_frame_l1_count <= (others => '0');            
            cbc_data_frame_ready    <= '0';   
            cbc_data_frame_counter <= (others=>'0');
            frame_ready := false;         
        elsif ipb_ctrl.frame_counter_reset = '1' then
                cbc_data_frame_counter <= (others=>'0');
        elsif rising_edge(clk_40MHz) then
            cbc_par_data_sr(0 to 33) <= cbc_par_data_sr(1 to 34);
            cbc_par_data_sr(34)      <= bitswap(cbc_par_data(6));
            cbc_data_frame_ready     <= '0';        
            if frame_ready then
                cbc_data_frame_ready <= '1';
                frame_ready := false;
            end if;
            if cbc_par_data_sr(0)(1 downto 0) = "11" and cbc_par_data_sr(34)(7 downto 4) = "0000" then
                for i in 0 to 33 loop 
                    cbc_data_frame(i) <= cbc_par_data_sr(i+1)(1 downto 0) & cbc_par_data_sr(i)(7 downto 2);
                end loop;
                cbc_data_frame(34) <= "00" & cbc_par_data_sr(34)(7 downto 2);
                for i in 0 to 2 loop
                    cbc_data_frame_l1_count(8-i) <= cbc_par_data_sr(1)(5+i);
                end loop;
                for i in 0 to 5 loop
                    cbc_data_frame_l1_count(5-i) <= cbc_par_data_sr(2)(i);
                end loop;
                frame_ready := true;
                cbc_data_frame_counter <= cbc_data_frame_counter + 1;
                cbc_par_data_sr <= (others=>(others => '0'));
            end if;

        end if;
    end process;

-- cbc_data_frame_processor  
    cbc_data_frame_processor : process (ipb_ctrl.reset, clk_320MHz)
	variable cbc_data_frame_ready_320MHz : boolean;
    begin
        if ipb_ctrl.reset = '1' then
            cbc_triggered_data <= (others=>(others => '0'));
            cbc_triggered_data_ready <= '0';
			cbc_data_frame_ready_320MHz := false;
        elsif rising_edge(clk_320MHz) then
            cbc_triggered_data_ready <= '0';

            if cbc_data_frame_ready_320MHz then
               for i in 0 to 34 loop
                    cbc_triggered_data(i) <= cbc_data_frame(i);
                end loop;
                -- have to be 1 only one cycle. cbc_data_frame_ready is 1 for 25 ns.  fix me later.
                cbc_triggered_data_ready <= '1';
            	cbc_data_frame_ready_320MHz := false;
            end if;

            if cbc_data_frame_ready = '1' then
                cbc_data_frame_ready_320MHz := true; 
            end if;
            
 
        end if;            
    end process;
    cbc_triggered_data_size <= cbc_data_frame_size;
    ipb_stat.triggered_data <= cbc_triggered_data;

-- trigger data pipeline - 40MHz   
    trig_data_shift_reg_inst : ENTITY work.trig_data_shift_reg
    PORT map(
        A   => std_logic_vector(trig_data_pipe_l1a_lat),
        D   => cbc_par_data(5) & cbc_par_data(4) & cbc_par_data(3) & cbc_par_data(2) & cbc_par_data(1),
        CLK => clk_40MHz,
        SCLR=> ipb_ctrl.reset_40MHz,
        Q   => trig_data_shift_out
    );
    trig_data_pipe_l1a_lat <= unsigned(ipb_cnfg.l1a_latency) - unsigned(ipb_cnfg.trig_data_latency);     


-- trigger data buffer from l1a to cbc_data_frame_ready - 320MHz
-- 40 x 32
    trig_data_fifo_inst : entity work.trig_data_fifo
      PORT map(
        clk   => clk_320MHz,
        srst  => ipb_ctrl.reset,
        din   => trig_data_fifo_din,
        wr_en => trig_data_fifo_we,
        rd_en => trig_data_fifo_re,
        dout  => trig_data_fifo_dout,
        full  => trig_data_fifo_full,
        empty => trig_data_fifo_empty
      );

--    generic map(DATA_WIDTH=>40, FIFO_DEPTH=>32)
--    port map(     
--        clk   => clk_320MHz,
--        rst   => ipb_ctrl.reset,
--        we    => trig_data_fifo_we,
--        din   => trig_data_fifo_din,
--        re    => trig_data_fifo_re,
--        dout  => trig_data_fifo_dout,
--        empty => trig_data_fifo_empty,
--        full  => trig_data_fifo_full,
--        werr  => trig_data_fifo_werr,
--        rerr  => trig_data_fifo_rerr
--    );
    ipb_stat.trig_data_fifo_empty <= trig_data_fifo_empty;
    ipb_stat.trig_data_fifo_full  <= trig_data_fifo_full;
    
--    trig_data_fifo_dout_gen :  
--    for i in 0 to to_integer(cbc_trig_data_size)-1 generate 
--    	trig_data_fifo_data_set(i) <= trig_data_fifo_dout((i+1)*8-1 downto i*8);
--    end generate trig_data_fifo_dout_gen;
    
    process (ipb_ctrl.reset, clk_320MHz)
    variable din_ready : boolean;
    begin
        if ipb_ctrl.reset = '1' then
            trig_data_fifo_we <= '0';
--            din_ready := false;
        elsif rising_edge(clk_320MHz) then             
            trig_data_fifo_we <= '0';
--            if din_ready then
--                 trig_data_fifo_we  <= '1';                              
--            	 din_ready := false;
--            elsif l1a_320MHz = '1' then
--           		 din_ready := true;
--                 trig_data_fifo_din <= trig_data_shift_out;
--            end if;
			if l1a_320MHz = '1' then
				trig_data_fifo_we  <= '1';     
				trig_data_fifo_din <= trig_data_shift_out;
			end if;
       end if;
    end process; 

-- cbc_data_fifo in : width = 8, out : width = 32 
    cbc_data_fifo_inst: entity work.cbc_data_fifo
    PORT map(
        clk   => clk_320MHz,
        srst  => ipb_ctrl.reset,
        din   => cbc_data_fifo_din,
        wr_en => cbc_data_fifo_wr_en,
        rd_en => cbc_data_fifo_rd_en,
        dout  => cbc_data_fifo_dout,
        full  => cbc_data_fifo_full,
        empty => cbc_data_fifo_empty,
        rd_data_count => cbc_data_fifo_rd_count,
        wr_data_count => cbc_data_fifo_wr_count
    );
    
    ipb_stat.data_fifo_full  <= cbc_data_fifo_full;
    ipb_stat.data_fifo_empty <= cbc_data_fifo_empty;
    
-- cbc_data_info_fifo in : width 64, out : width = 64
-- one word infor per event
    cbc_data_info_fifo_inst : entity work.cbc_data_info_fifo
    port map(     
        clk   => clk_320MHz,
        srst  => ipb_ctrl.reset,
        din   => cbc_data_info_fifo_din,
        wr_en => cbc_data_info_fifo_wr_en,
        rd_en => cbc_data_info_fifo_rd_en,
        dout  => cbc_data_info_fifo_dout,
        full  => cbc_data_info_fifo_full,
        empty => cbc_data_info_fifo_empty,
        data_count => cbc_data_info_fifo_data_count
    );    

    ipb_stat.data_info_fifo_full  <= cbc_data_info_fifo_full;
    ipb_stat.data_info_fifo_empty <= cbc_data_info_fifo_empty;

    cbc_data_fifo_write : process (ipb_ctrl.reset, ipb_ctrl.frame_counter_reset, clk_320MHz)
        variable fifo_re_timer            : integer range 0 to 3;
        variable trig_data_fifo_out_ready : boolean;
--		variable cbc_data_fifo_din_pre    : std_logic_vector(7 downto 0);    
    begin
        if ipb_ctrl.reset = '1' then
            cbc_data_fifo_wr_index <= (others => '0');
            fifo_re_timer := 0;   
            cbc_data_size <= (others => '0');
            trig_data_fifo_re <= '0';        
            cbc_data_fifo_wr_en <= '0';
            cbc_data_info_fifo_wr_en <= '0'; 
            cbc_data_packet_header <= (others=>'0');                
            cbc_data_fifo_write_fsm <= idle;
			trig_data_fifo_out_ready := false;
        elsif rising_edge(clk_320MHz) then
            trig_data_fifo_re <= '0';                         
            cbc_data_fifo_wr_en <= '0';
            cbc_data_info_fifo_wr_en <= '0';
     
            case cbc_data_fifo_write_fsm is
            when idle =>
                cbc_data_fifo_wr_index <= (others => '0');            
                cbc_data_size <= (others => '0');                                                        
                cbc_data_fifo_wr_en <= '0';
                cbc_data_info_fifo_wr_en <= '0';
                cbc_data_l1_count_latched <= (others=>'0');                     
                cbc_data_packet_header <= (others=>'0');
                cbc_data_packet_size <= (others=>'0');

				if trig_data_fifo_out_ready then					
					for i in 0 to to_integer(cbc_trig_data_size)-1 loop
						trig_data_fifo_data_set(i) <= trig_data_fifo_dout((i+1)*8-1 downto i*8);
					end loop;  
--					cbc_data_fifo_din_pre := trig_data_fifo_dout(7 downto 0);               
                	trig_data_fifo_out_ready := false;
                	fifo_re_timer := 0;
                	cbc_data_fifo_write_fsm <= write_trig_data;
--                	cbc_data_fifo_din_tmp <= trig_data_fifo_data_set(0);
                	cbc_data_fifo_din_tmp <= trig_data_fifo_dout(7 downto 0);
				elsif fifo_re_timer = 2 then
					trig_data_fifo_out_ready := true; 
                    fifo_re_timer := fifo_re_timer + 1; 					
                elsif fifo_re_timer = 1 then                   
                    fifo_re_timer := fifo_re_timer + 1;                   
                elsif fifo_re_timer = 0 then 
                    if trig_data_fifo_empty = '0' then
                        trig_data_fifo_re <= '1';
                        fifo_re_timer := fifo_re_timer + 1;
                    end if; 
                end if;
            when write_trig_data =>          

            	if(cbc_data_fifo_wr_en = '0') then
					cbc_data_fifo_wr_en <= '1';
					cbc_data_fifo_din <= cbc_data_fifo_din_tmp;
--					cbc_data_fifo_din <= (others => '1');
					cbc_data_fifo_wr_index <= cbc_data_fifo_wr_index + 1;
					cbc_data_size <= cbc_data_size + 1;	
			    	if cbc_data_fifo_wr_index = cbc_trig_data_size - 1 then
						cbc_data_fifo_write_fsm <= wait_triggered_data;
					end if;	
				else
					cbc_data_fifo_din_tmp <= trig_data_fifo_data_set(to_integer(cbc_data_fifo_wr_index));
				end if;				
--				cbc_data_fifo_din_pre := trig_data_fifo_data_set(to_integer(cbc_data_fifo_wr_index));			

            when wait_triggered_data =>
				cbc_data_fifo_wr_index <= (others => '0');
                if cbc_triggered_data_ready = '1' then
                    cbc_data_l1_count_latched  <= std_logic_vector(cbc_data_frame_l1_count); 
                    cbc_data_fifo_write_fsm <= write_triggered_data;
                	cbc_data_fifo_din_tmp <= cbc_triggered_data(0);
                end if;
            when write_triggered_data =>
				
				if(cbc_data_fifo_wr_en = '0') then
					cbc_data_fifo_wr_en  <= '1';
					cbc_data_fifo_din <= cbc_data_fifo_din_tmp;                    
					cbc_data_fifo_wr_index <= cbc_data_fifo_wr_index + 1;
					cbc_data_size <= cbc_data_size + 1;
                	if cbc_data_fifo_wr_index = cbc_triggered_data_size - 1 then
						cbc_data_fifo_write_fsm <= write_data_paddings;
					end if;
				else
					cbc_data_fifo_din_tmp <= cbc_triggered_data(to_integer(cbc_data_fifo_wr_index));	
				end if;
--				cbc_data_fifo_din_pre := cbc_triggered_data(to_integer(cbc_data_fifo_wr_index));	 
                
                 
            when write_data_paddings =>
                if cbc_data_size(2 downto 0) /= 0 then
                    cbc_data_fifo_wr_en  <= '1';
                    cbc_data_fifo_din <= (others => '0');
                    cbc_data_size <= cbc_data_size + 1;                    
                else
                    cbc_data_fifo_wr_index <= ( others => '0' );
                    cbc_data_fifo_write_fsm <= write_data_info;    
                end if;                    
            when write_data_info =>
                if cbc_data_fifo_wr_index = 1 then
                    cbc_data_info_fifo_wr_en <= '1';
                    cbc_data_info_fifo_din <= cbc_data_packet_header & x"00" & "000" &"00" & std_logic_vector(cbc_data_size(11 downto 2)) & cbc_data_l1_count_latched; 
                    cbc_data_fifo_write_fsm <= idle;  
                else 
                    if cbc_data_fifo_wr_index = 0 then
                        cbc_data_packet_header <= "00000" & be_id & fe_id & cbc_id & "00" & std_logic_vector(cbc_data_size(11 downto 2));
                        cbc_data_packet_size <= ("00" & cbc_data_size(11 downto 2) ) + 1;
                    end if;
                    cbc_data_fifo_wr_index <= cbc_data_fifo_wr_index + 1;                      
                end if;                
            end case;
        end if;
    end process;            
    ipb_stat.fifo_write_fsm         <= cbc_data_fifo_write_fsm;
    ipb_stat.cbc_data_frame_counter <= std_logic_vector(cbc_data_frame_counter) when cbc_active = '1' else (others => '1');                   
    

    cbc_data_packet_send : process (ipb_ctrl.reset, clk_320MHz)
        variable index : integer range 1 to 80;
        variable fifo_re_timer : integer range 0 to 2;
        variable fifo_ready : boolean;
        variable cbc_data_l1_count_pre : std_logic_vector(8 downto 0);
    begin
        if ipb_ctrl.reset = '1' then
            fifo_re_timer := 0;
            fifo_ready := false;
            cbc_data_info_fifo_rd_en <= '0';
            data_packet_ready <= '0';                         
            cbc_data_packet_send_fsm <= idle;
            data_packet_nword_pre <= (others => '0');
            data_packet_nword_sent <= (others => '0');
            read_data_fifo_en <= '0';
        elsif rising_edge(clk_320MHz) then             
            dsoebi.data_packet_valid      <= '0';
            cbc_data_info_fifo_rd_en <= '0';
            read_data_fifo_en <= '0';
            case cbc_data_packet_send_fsm is
            when idle =>
                data_packet_ready <= '0';  
                data_packet_nword_sent <= (others => '0');                                           

				data_packet_nword_pre <= unsigned(cbc_data_info_fifo_dout(DATA_SECTION_DATA_SIZE_WIDTH + 8 downto 9)) + 1;
				cbc_data_l1_count_pre := cbc_data_info_fifo_dout(8 downto 0); 

                if fifo_ready then
                    cbc_data_packet_send_fsm <= send_data_ready;
                    fifo_re_timer := 0;
                    fifo_ready := false;
                end if;
                if fifo_re_timer = 1 then
                    fifo_re_timer := fifo_re_timer + 1;
                    fifo_ready := true;
                elsif fifo_re_timer = 0 then
                    if cbc_data_info_fifo_empty = '0' then
                        fifo_re_timer := fifo_re_timer + 1;
                        cbc_data_info_fifo_rd_en <= '1';                       
                    end if;    
                end if;
               
            when send_data_ready =>                                             
                data_packet_nword <= data_packet_nword_pre;
                dsoebi.cbc_data_l1_count <= cbc_data_l1_count_pre;
                data_packet_ready <= '1';               
                cbc_data_packet_send_fsm <= send_packet;
                read_data_fifo_en <= '1';
            when send_packet =>
            
                read_data_fifo_en <= '1';
                if data_packet_nword_sent = data_packet_nword - 1 then
                    read_data_fifo_en <= '0';
                end if;                
                if dsiebo.read_en = '1' then                   

                    if data_packet_ready = '1' then
                        data_packet_ready <= '0'; 
                    end if;
                    if data_packet_nword_sent = 0 then
                        dsoebi.data_packet_valid      <= '1';
                        dsoebi.data_packet_out <= cbc_data_info_fifo_dout(63 downto 32); -- cbc packet header 
                    else
                        dsoebi.data_packet_valid      <= '1';
                        dsoebi.data_packet_out <= cbc_data_fifo_dout(7 downto 0) & cbc_data_fifo_dout(15 downto 8) & cbc_data_fifo_dout(23 downto 16) & cbc_data_fifo_dout(31 downto 24); 

                    end if;
                    data_packet_nword_sent <= data_packet_nword_sent + 1;   
                                                
                else
                    if data_packet_nword_sent = data_packet_nword then
                        cbc_data_packet_send_fsm <= idle;
                    end if;            
                end if;
             end case;
        end if;
    end process; 
    ipb_stat.packet_send_fsm <= cbc_data_packet_send_fsm;
       
    cbc_data_fifo_rd_en      <= dsiebo.read_en and read_data_fifo_en;
--    dsoebi.data_packet_out   <= cbc_data_info_fifo_dout(63 downto 32) when data_packet_nword_sent = 0 else cbc_data_fifo_dout; 
    dsoebi.data_packet_nword <= std_logic_vector(data_packet_nword);
--    ipb_stat_o <= ipb_stat;
    dsoebi.data_packet_ready <= data_packet_ready;



end fc7;
