--===========================--
-- cbc_data_clock_timing_scan_bram_ctrl
-- This module just enable a segment(CBC) which requested the write access.
-- If multiple segments has requests, a segment with the smallest index is enabled.
-- 01.26.2017 Kirika Uchida
--===========================--

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

use work.cbc_system_package.all;

entity cbc_data_clock_timing_scan_bram_ctrl is
generic ( WADDR_WIDTH : positive := 10; RADDR_WIDTH : positive := 9 );
port(   clk_40MHz         : in  std_logic;
        ipb_clk           : in  std_logic;
        reset             : in  std_logic;
        dcts_to_bram_set  : in dcts_to_bram_set_type;
        bram_to_dcts_set  : out bram_to_dcts_set_type;
        bram_dout         : out std_logic_vector(31 downto 0);
        bram_read_next    : in  std_logic
);
end cbc_data_clock_timing_scan_bram_ctrl;

architecture Behavioral of cbc_data_clock_timing_scan_bram_ctrl is

    signal we               : std_logic;
    signal waddr            : std_logic_vector(WADDR_WIDTH-1 downto 0);
    signal din              : std_logic_vector(15 downto 0);  
    signal nrdata           : unsigned(RADDR_WIDTH downto 0);    
    signal raddr            : std_logic_vector(RADDR_WIDTH-1 downto 0);
	signal tmp_dout         : std_logic_vector(31 downto 0);
	signal wcount           : unsigned(WADDR_WIDTH-1 downto 0);

    type fsm_type is (wait_req, wait_data);
    signal fsm : fsm_type;
begin

	--=============================--
    process(reset, clk_40MHz)
    --=============================--
    variable feid : integer range 0 to NFE-1;
    begin
    if reset='1' then     
        feid := 0;
        for i in 0 to NFE-1 loop
            bram_to_dcts_set(i).ready <= '0';           
        end loop;
        we <= '0';     
        wcount <= (others => '0');       
        fsm <= wait_req;
    elsif rising_edge(clk_40MHz) then 
        we <= '0';              
        case fsm is
        when wait_req =>
            for i in 0 to NFE-1 loop
                if dcts_to_bram_set(NFE-1-i).req = '1' then
                    feid := NFE-1-i;
                    bram_to_dcts_set(feid).ready <= '1';
                    fsm <= wait_data;
                end if;
            end loop;              
        when wait_data =>
            we    <= dcts_to_bram_set(feid).wen;
            waddr <= std_logic_vector(unsigned(dcts_to_bram_set(feid).waddr) + 60 * feid);                
--			waddr <= std_logic_vector(wcount);
--			if we = '1' then
--				wcount <= wcount + 1;
--			end if;
--			din <= (others => '1');			
            din   <= dcts_to_bram_set(feid).din;
            if dcts_to_bram_set(feid).req = '0' then
                bram_to_dcts_set(feid).ready <= '0';
                fsm <= wait_req;
            end if;
       end case;
    end if;
    end process;

	--=============================--
    process(reset, ipb_clk)
    --=============================--
    begin
    if reset='1' then     
        raddr  <= (others => '0');
        nrdata <= to_unsigned(1, RADDR_WIDTH+1);
    elsif rising_edge(ipb_clk) then
        if bram_read_next = '1'  then                
            raddr <= std_logic_vector(nrdata(RADDR_WIDTH-1 downto 0));
--			bram_dout <= x"00000" & "00" & std_logic_vector(nrdata);
            nrdata <= nrdata + 1;
--           if nrdata = to_unsigned(33 * NCBC - 1, RADDR_WIDTH + 1) then
--                nrdata <= (others => '0');
--           end if; 
        end if;
    end if;
	
    end process;
--    dctsb_to_ipbif.dout <= bram_out(15 downto 0) & bram_out(31 downto 16);   
--    dctsb_to_ipbif.dout <= bram_out; 
--    dctsb_to_ipbif.dout <= raddr;
    cbc_data_clock_timing_scan_bram : entity work.cbc_data_clock_timing_scan_bram
      PORT MAP (
        clka    => clk_40MHz,
        wea(0)  => we,
        addra   => waddr,
        dina    => din,
        clkb    => ipb_clk,
        addrb   => raddr,
        doutb   => bram_dout
      );    

end Behavioral;
