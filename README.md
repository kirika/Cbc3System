# Firmware information
The project file is cbc_system/fc7/prj/2fmc8cbcs/fpga_fc7_cbc_system.xpr for VIVADO

my memo
fixed fast signal control indices in io.vhd
fixed i2c bus index in fscbram_controller.vhd

## IPBUS and FC7 firmware installation 
```
cd Cbc3System/fw
svn co svn+ssh://username@svn.cern.ch/reps/cactus/tags/ipbus_fw/ipbus_2_0_v1
svn co svn+ssh://username@svn.cern.ch/reps/ph-ese/be/fc7/tags/fc7_5.0.1
```

# Software information
## Main library and software for Cbc3System
set the environment and build cbc3hal.  Please set the environment variable CACTUS_ROOT to an appropriate value in sw/cbc3hal/setup.sh beforehand. 
```
cd Cbc3System
```
### fc7 software is required to be build. user image can be stored in the SD card and load that with the command at PC 
```
cd fc7_5.0.1/sw/fc7
source setup.sh
```
#### add `#include <arpa/inet.h>` to fc7/src/common/MmcPipeInterface.cpp 
```
vim fc7/src/common/MmcPipeInterface.cpp 
make
cd fc7/tests
```
#### to write an user image on SD card
```
./bin/fc7-userimage-reprogram.exe -i 192.168.0.80 -f [bit file name]
```
### cbc3hal software build
uhal and root are required

for uhal, follow the instruction at http://ipbus.web.cern.ch/ipbus/doc/user/html/software/installation.html

for root, follow the guid at https://root.cern.ch/root/htmldoc/guides/users-guide/ROOTUsersGuide.html#installing-root
```
export CBC3SYSTEM=${PWD}
cd sw
cd cbc3hal;source setup.sh;make
```

## For KiNOKO
download kinoko from http://www.awa.tohoku.ac.jp/~sanshiro/kinoko-download/download.cgi?file=kinoko-2017-02-21.tar.gz&name=Kinoko+Version+2.6.2 

install xterm

cbc3hal library need to be build before KiNOKO interface software under local-kinoko to be build.
```
cd Cbc3System
export CBC3SYSTEM=${PWD}
cd ${CBC3SYSTEM}/sw
tar zxvf kinoko-2017-02-21.tar.gz
cd local-kinoko
./build.sh
```
kinoko scripts for Cbc3System are located under local-kinoko/daq.  Please run kinoko there.

This is not maintained currently.

```
cd ${CBC3SYSTEM}/sw/local-kinoko/daq
kinoko Cbc3BeFc7.kcom
```
## How to use the softwares
### CASE 1: cbc3hal only
Please set the environment variable CBC3SYSTEM and do source ${CBC3SYSTEM}/sw/cbc3hal/setup.sh before using the software there. 

### CASE 2: cbc3hal & kinoko 
Please set the environment variable CBC3SYSTEM and do source ${CBC3SYSTEM}/sw/setup.sh before using the software there. 

