/* Cbc3BeFc7_tiny.kts */
/* created by Kirika Uchida on Feb. 24 2017 */ 

string DeviceName = "Cbc3BeFc7";

datasource Cbc3BeFc7
{

    DataRecord run_header_record;
    DataRecord condition_record;

    int min_block_read_size        = 56000;
    string cbc3hal_config_dir      = "../../cbc3hal/etc";
    string connection_file         = cbc3hal_config_dir + "/connections.xml"; 
    string board_id                = "FC7_SINGLE_CBC3_V1_CH"; 
    string be_config_file          = cbc3hal_config_dir + "/cbc3_system_config.txt"; 
    string run_config_file         = cbc3hal_config_dir + "/param_tuning_config.txt"; 
    string cbc_i2c_reg_config_file = cbc3hal_config_dir + "/CBC_I2CREG_CNFG.txt";

    SoftwareModule cbc3be1(DeviceName);	

    cbc3be1.connectIpbus( connection_file, board_id );
    cbc3be1.writeBeBoardConfig( be_config_file );
    cbc3be1.writeBeBoardConfig( run_config_file );
    cbc3be1.writeCbcRegs( cbc_i2c_reg_config_file );

    cbc3be1.setTimeoutUsec("1");
    cbc3be1.setDataReadyFlagPollingInterval_usec(1);

    on run_begin{
	cbc3be1.setMinBlockReadSize(min_block_read_size);
	cbc3be1.startFastSignalGenerator();
    }

    on trigger(cbc3be1) {
	cbc3be1.blockRead(0);
    }
}
