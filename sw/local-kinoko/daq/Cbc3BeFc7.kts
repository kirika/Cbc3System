/* Cbc3BeFc7.kts */
/* created by Kirika Uchida on 5th Dec. 2016 */ 

string DeviceName = "Cbc3BeFc7";

datasource Cbc3BeFc7
{

    DataRecord run_header_record;
    DataRecord condition_record;

    Register min_block_read_size;
    Register timeout_usec;
    Register polling_interval_usec;

    SoftwareModule cbc3be1(DeviceName);	

    string connection_file = getRegistry("control/connection_file");
    string board_id      = getRegistry("control/board_id_be1");
    cbc3be1.connectIpbus( connection_file, board_id );
    println( "Connection file: " + connection_file + ", board id: " + board_id );
    cbc3be1.setDebug(0x000);

/*
    string run_configuration_file = getRegistry("control/run_configuration_file");
    cbc3be1.writeBeBoardConfig( run_configuration_file );
*/

    /* called after InitializeRun() */
    on run_begin{

	readRegistry("control/min_block_read_size", min_block_read_size);  
	cbc3be1.setMinBlockReadSize(min_block_read_size);
	echo("min_block_read_size: ",min_block_read_size);
	readRegistry("control/timeout_usec", timeout_usec);  
	cbc3be1.setTimeoutUsec( timeout_usec );
	readRegistry("control/polling_interval_usec", polling_interval_usec);  
	cbc3be1.setDataReadyFlagPollingInterval_usec(polling_interval_usec);

	cbc3be1.startFastSignalGenerator();

/*
	invoke checkCbcSystemConfig();
	invoke synchronously cbci2creg_vcth_set();
	invoke  cbci2creg_vcth_set();
*/
    }

    on trigger(cbc3be1) {
	cbc3be1.blockRead(0);
    }

    on command("resume"){
	cbc3be1.startFastSignalGenerator();
    }
}
