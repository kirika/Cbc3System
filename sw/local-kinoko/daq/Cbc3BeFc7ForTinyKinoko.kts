/* Cbc3BeFc7ForTinyKinoko.kts */
/* created by Kirika Uchida on 18.02.2017 */ 

string DeviceName = "Cbc3BeFc7";

datasource Cbc3BeFc7
{

    Register timeout_sec;
    Register min_block_read_size;
    Register vcth;

    SoftwareModule cbc3be1(DeviceName);	

    string connection_file = shell( "printenv IPB_CONNECTION_FILE" );
    chop( connection_file );
    string board_id      = shell( "printenv IPB_BOARD_ID" ); 
    chop( board_id );
    cbc3be1.connectIpbus( connection_file, board_id );
    int min_block_read_size = 1500;  
    int timeout_usec = 0;
    int polling_interval_usec = 1;

    on run_begin{
	cbc3be1.setMinBlockReadSize(min_block_read_size);
	cbc3be1.setTimeoutUsec( timeout_usec );
	cbc3be1.setDataReadyFlagPollingInterval_usec(polling_interval_usec);
    }

    on trigger(cbc3be1) {
	cbc3be1.blockRead(0);
    }
}
