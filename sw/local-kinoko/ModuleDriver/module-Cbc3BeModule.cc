/* module-Cbc3BeModule.cc */
/* Author Kirika Uchida */

#include "RoomSoftwareDevice.hh"
#include "RoomDeviceFactory.hh"
#include "module-Cbc3BeModule.hh"
#include "Cbc3BeFc7.h"
#include "SimCbc3BeFc7.h"
#include "CbcI2cRegIpbusInterface.h"
#include <cstdlib>

using namespace std;
using namespace cbc3hal;

static TRoomSoftwareModuleCreator Creator01(
	"Cbc3BeFc7", new TSoftwareModule_Cbc3BeModule("Cbc3BeFc7")
	);


static TRoomSoftwareModuleCreator Creator02(
	"SimCbc3BeFc7", new TSoftwareModule_Cbc3BeModule("SimCbc3BeFc7")
	);

TSoftwareModule_Cbc3BeModule::TSoftwareModule_Cbc3BeModule(string name)
    : TRoomSoftwareModule(name, "Software_Cbc3BeModule"), fName(name), fDebug(0), fMinBlockReadSize(0), fBlockDataConditionSize(0), fTimeoutUsec(false)
{
    if( !fName.compare("Cbc3BeFc7") ){	
	fCbc3BeInterface = new Cbc3BeFc7();
    }	
    else if( !fName.compare("SimCbc3BeFc7") ){
	fCbc3BeInterface = new SimCbc3BeFc7();
    }
}

TSoftwareModule_Cbc3BeModule::~TSoftwareModule_Cbc3BeModule()
{}

TRoomSoftwareModule* TSoftwareModule_Cbc3BeModule::Clone()
{
    return new TSoftwareModule_Cbc3BeModule(fName);
}

bool TSoftwareModule_Cbc3BeModule::ProcessCommand(const string& Command, const vector<string>& ArgumentList) throw(THardwareException)
{
    if(Command == "connectIpbus") {
	if(ArgumentList.size() != 2){
	    throw THardwareException( "TSoftwareModule_Cbc3BeModule::connect(string ConnectionFileName, string BoardId)", "# of arguments is wrong" );
	}
	fCbc3BeInterface->Initialize( ArgumentList[0], ArgumentList[1] );
    }
    else if(Command =="writeBeBoardConfig"){
	fCbc3BeInterface->WriteBeBoardConfig( ArgumentList[0] );
    }
    else if(Command =="writeCbcRegs"){
	fCbc3BeInterface->rwCbcI2cRegs( 1, 1, ArgumentList[0] );
    }
    else if(Command =="setDebug"){
	fDebug = strtol( ArgumentList[0].c_str(), 0, 0 );
	fCbc3BeInterface->SetDebug(fDebug);
    }
    else {
	return TRoomSoftwareModule::ProcessCommand(Command, ArgumentList);
    }
    return true;
}

int TSoftwareModule_Cbc3BeModule::Initialize(int InitialState) throw(THardwareException)
{
    fCbc3BeInterface->InitializeRun();
    return 0;
}

int TSoftwareModule_Cbc3BeModule::Finalize(int FinalState) throw(THardwareException)
{
    fCbc3BeInterface->FinalizeRun();
    cout << "Cbc3BeModule::Finalize() finished" << endl;
    return 0;
}

bool TSoftwareModule_Cbc3BeModule::HasData(int Address) throw(THardwareException)
{
    bool retval = fCbc3BeInterface->WaitData( 1, fTimeoutUsec );
    if(retval) return true;
    else return false;
//   return fCbc3BeInterface->HasData( fCbc3BeInterface->GetMinNumWordToWait() );
//    return false;
}
bool TSoftwareModule_Cbc3BeModule::WaitData(unsigned TimeOut_sec) throw(THardwareException) 
{
    /*
    static bool done(false);
    if(!done)  cout << "TimeOut_sec " << TimeOut_sec << endl; 
    done = true; 
    */
    bool retval = fCbc3BeInterface->WaitData( TimeOut_sec, fTimeoutUsec );
    //cout << "retval=" << retval << endl;
    return retval;
}
int TSoftwareModule_Cbc3BeModule::BlockRead(int Address, void *Data, int MaxSize) throw(THardwareException)
{
//    cout << "TSoftwareModule_Cbc3BeModule::BlockRead() Reading next data" << endl;
    uint32_t *data = (uint32_t *) Data;
    int tmp = fCbc3BeInterface->ReadData( data );
 //   cout << tmp << " byte was read." << endl;
    return tmp * 4;
}
int TSoftwareModule_Cbc3BeModule::NextDataBlockSize(int Address) throw(THardwareException)
{
    return fCbc3BeInterface->DataBufferSize() * 4;
}

int TSoftwareModule_Cbc3BeModule::Clear(int Address) throw(THardwareException)
{
    return 0;
}
int TSoftwareModule_Cbc3BeModule::MiscControlIdOf(const string& CommandName) throw (THardwareException)
{
 //   cout << CommandName << endl;
    int ControlId = -1;
    if (CommandName == "setMinBlockReadSize") {
	return ControlId_SetMinBlockReadSize;
    }
    else if(CommandName =="setDataReadyFlagPollingInterval_usec"){
	return ControlId_SetDataReadyFlagPollingInterval_usec;
    }
    else if(CommandName =="setTimeoutUsec"){
	return ControlId_SetTimeout_usec;
    }
    else if(CommandName == "setVCTH"){
	return ControlId_SetVCTH;
    }
    else if(CommandName == "startFastSignalGenerator"){
	return ControlId_StartFastSignalGenerator;
    }
    else if(CommandName == "resume"){
	return ControlId_Resume;
    }
    else {
	throw THardwareException(
		_ModelName, "unknown command: " + CommandName
		);
    }

    return ControlId;
}
int TSoftwareModule_Cbc3BeModule::MiscControl(int ControlId, int* ArgumentList, int NumberOfArguments) throw(THardwareException)
{
    if (ControlId == ControlId_SetMinBlockReadSize) {
	if (NumberOfArguments < 1) {
	    throw THardwareException(
		    _ModelName + "::setMinBlockReadSize()",
		    "too few argument[s]"
		    );
	}
	SetMinBlockReadSize( ArgumentList[0] );
    }
    else if(ControlId == ControlId_SetDataReadyFlagPollingInterval_usec){
	if(fDebug) cout << "data ready flag polling interval in usec = " << ArgumentList[0] << endl;
	fCbc3BeInterface->DataReadyFlagPollingInterval_usec( ArgumentList[0] );
    }
    else if(ControlId == ControlId_SetTimeout_usec){
	fTimeoutUsec = (ArgumentList[0] != 0 ) ? true : false;
    }
    else if(ControlId == ControlId_SetVCTH){
	unsigned value = ArgumentList[0];
	if(fDebug) cout << "Setting VCTH = " << value << endl;
	CBCI2C_ITEM_DATA_MAP item_data;
	item_data["vcth"] = value; 
	fCbc3BeInterface->writeAllCbcI2cRegsByType( "VCTH", item_data ); 
    }
    else if(ControlId == ControlId_StartFastSignalGenerator){
	if(fDebug) cout << "Starting Fast signal generator" << endl;
	fCbc3BeInterface->StartFastSignalGenerator();
    }
    else if(ControlId == ControlId_Resume){
    }
    else {
	return 0;
    }
    return 1;
}

void TSoftwareModule_Cbc3BeModule::SetMinBlockReadSize( unsigned MinBlockReadSize ) throw(THardwareException)
{
    if( fDebug ) cout << "[TSoftwareModule_Cbc3BeModule] Setting the min size of block read to " << std::dec << MinBlockReadSize << "." << endl;
    fCbc3BeInterface ->MinNumWordToWait( MinBlockReadSize );
}
