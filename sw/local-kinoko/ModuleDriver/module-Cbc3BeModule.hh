/* module-Cbc3BeModule.hh */
/* Author Kirika Uchida */

#ifndef __module_Cbc3BeModule_hh__
#define __module_Cbc3BeModule_hh__

#include "RoomSoftwareDevice.hh"

namespace cbc3hal{

    class Cbc3BeInterface;
}

class Cbc3BeInterface;

class TSoftwareModule_Cbc3BeModule: public TRoomSoftwareModule {

    public:
	TSoftwareModule_Cbc3BeModule(std::string name );
	virtual ~TSoftwareModule_Cbc3BeModule();
	virtual TRoomSoftwareModule* Clone(void);
	virtual bool ProcessCommand(const std::string& Command, const std::vector<std::string>& ArgumentList) throw(THardwareException);
	virtual int Initialize(int InitialState = 0) throw(THardwareException);
	virtual int Finalize(int FinalState = 0) throw(THardwareException);
	virtual bool HasData(int Address = -1) throw(THardwareException);
	virtual bool WaitData(unsigned TimeOut_sec = 1) throw(THardwareException); 
	virtual int BlockRead(int Address, void *Data, int MaxSize) throw(THardwareException);
	virtual int NextDataBlockSize(int Address = -1) throw(THardwareException);
	virtual int Clear(int Address = -1) throw(THardwareException);
	virtual int MiscControlIdOf(const std::string& CommandName) throw (THardwareException);
	virtual int MiscControl(int ControlId, int* ArgumentList = 0, int NumberOfArguments = 0) throw(THardwareException);
	void SetMinBlockReadSize( unsigned MinBlockReadSize ) throw(THardwareException);
    protected:
	enum TControlId {
	    ControlId_SetMinBlockReadSize,
	    ControlId_SetDataReadyFlagPollingInterval_usec,
	    ControlId_SetTimeout_usec,
	    ControlId_SetVCTH,
	    ControlId_StartFastSignalGenerator,
	    ControlId_Resume,
	    _NumberOfControls
	};
	cbc3hal::Cbc3BeInterface *fCbc3BeInterface;
	std::string fName;
	int      fDebug;
	unsigned fMinBlockReadSize;
	unsigned fBlockDataConditionSize;
	bool     fTimeoutUsec;
};


#endif
