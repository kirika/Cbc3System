#!/bin/bash

if [[ -z ${CBC3SYSTEM} ]]; then
    echo "set CBC3SYSTEM environment variable"
    exit 0
fi
source ${CBC3SYSTEM}/sw/cbc3hal/setup.sh

cp src/configure.in ../kinoko/src/
cp src/configure-for ../kinoko/src/
cd ../kinoko/src
autoconf 
./configure-for LINUX
make
source kinoko-bashrc 

cd ${CBC3SYSTEM}/sw/local-kinoko
for dir in bin
do
	if [ ! -d $dir ]; then
		mkdir $dir
	fi   
done

cd ModuleDriver
make rebuild-kinoko
cd ../Components
make
cd ../

