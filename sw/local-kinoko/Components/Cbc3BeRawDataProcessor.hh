/* KinokoCbc3BeRawDataProcessor.hh */
/* Kirika Uchida 07 Dec 2016 */

#ifndef __CBC3BERAWDATAPROCESSOR_HH__
#define __CBC3BERAWDATAPROCESSOR_HH__

#include "KinokoSystemComponent.hh"
#include "KinokoDataProcessor.hh"
#include "KinokoTaggedDataSection.hh"
#include "KinokoNestedDataSection.hh"
#include "KinokoIndexedDataSection.hh"
#include "KinokoBlockDataSection.hh"
#include <Cbc3BeData.h>

class CbcDataSection{

    public:
	CbcDataSection(){;}
	~CbcDataSection(){}
	TKinokoNestedDataSection* _CbcDataSection;
	TKinokoNestedDataSectionFormatter* _CbcDataFormatter;
	TKinokoIndexedDataSection* _CbcStubDataSection;
	TKinokoIndexedDataSectionFormatter* _CbcStubDataFormatter;
	TKinokoTaggedDataSection* _CbcTrigFlagDataSection;
	TKinokoTaggedDataSectionFormatter* _CbcTrigFlagDataFormatter;
	int _CbcTrigFlagData_Sof_Index;
	int _CbcTrigFlagData_Or254_Index;
	int _CbcTrigFlagData_Err_Index;
	TKinokoTaggedDataSection* _CbcStatusDataSection;
	TKinokoTaggedDataSectionFormatter* _CbcStatusDataFormatter;
	int _CbcStatusData_Error0_Index;
	int _CbcStatusData_Error1_Index;
	int _CbcStatusData_PipelineAddress_Index;
	int _CbcStatusData_L1ACounter_Index;
	TKinokoIndexedDataSection* _CbcHitDataSection;
	TKinokoIndexedDataSectionFormatter* _CbcHitDataFormatter;
};


class Cbc3BeRawDataProcessor: public TKinokoDataProcessor {
	public:
		Cbc3BeRawDataProcessor(void);
		virtual ~Cbc3BeRawDataProcessor();
	protected:
		virtual void BuildDataSource(TKinokoDataSource* DataSource);
		virtual void OnConstruct(void) throw(TKinokoException);
		virtual void OnRunBegin(void) throw(TKinokoException);
		virtual void OnRunEnd(void) throw(TKinokoException);
		//virtual void OnReceivePacket(void* Packet, long PacketSize) throw(TKinokoException);
		virtual void OnReceiveDataPacket(void* Packet, long PacketSize) throw(TKinokoException);
		virtual void OnReceiveTrailerPacket(void* Packet, long PacketSize) throw(TKinokoException);
		virtual void OnReceiveRunBeginPacket(void* Packet, long PacketSize) throw(TKinokoException);
		virtual void OnReceiveRunEndPacket(void* Packet, long PacketSize) throw(TKinokoException);
		/*
		virtual void BuildDescriptor(TKcomComponentDescriptor& Descriptor);
		virtual int ProcessEvent(int EventId, TKcomEvent& Event);
		*/
		void SendReportPacket() throw(TKinokoException);
		void WriteBeDataPacket() throw(TKinokoException);
		void WriteCbcDataPacket() throw(TKinokoException);
	protected:
		int _DataSourceId;
		/* BE block data (raw data) section */
		int _BeBlockDataSectionId;
		TKinokoBlockDataSection* _BeBlockDataSection;
		TKinokoBlockDataSectionScanner* _BeBlockDataScanner;
		/* Event data flags */
//		TKinokoTaggedDataSection *
		/* BE data section */
		TKinokoTaggedDataSection* _BeDataSection;
		TKinokoTaggedDataSectionFormatter* _BeDataFormatter;
		int _BeData_Status_Index;
		int _BeData_L1ACounter_Index;
		/* CBC data section */ 
		std::map<unsigned, CbcDataSection*>  _CbcDataSections;
		TKinokoTaggedDataSection* _EventDataSection;
		TKinokoTaggedDataSectionFormatter* _EventDataFormatter;
		long _EventCount, _DataAmount;
		int _EventCountIndex, _EventRateIndex, _DataFlowIndex;
		int _LastReportTime, _ReportInterval;
		int _EventRate;
		int _DataFlow;

		long _Prescale;
		long _TotalEventCount;

		cbc3hal::BeDataDecoder _BeDataDecoder;

		enum TEventId{
			EventId_SetPrescale,
			_NumberOfEvents
		};
		int fVCTH;
};

#endif

