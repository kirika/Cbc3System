#include <iostream>
#include "KcomProcess.hh"
#include "Cbc3DataProcessorCom.hh"

#include "Cbc3ParamTuningDataProcessor.hh"


using namespace std;

int main(int argc, char** argv)
{
	TMushArgumentList ArgumentList(argc, argv);

	TKinokoDataProcessor *MyDataProcessor = new Cbc3ParamTuningDataProcessor();
	TKcomComponent *Component = new Cbc3DataProcessorCom(MyDataProcessor);
	TKcomProcess *ComProcess = new TKcomProcess(Component);

	try{
			ComProcess->Start(ArgumentList);
	}
	catch( TKcomException &e ){
		cerr << "ERROR: " << argv[0] << ": " << e << endl;
	}
	delete ComProcess;
	delete Component;
	delete MyDataProcessor;

	return 0;
}
