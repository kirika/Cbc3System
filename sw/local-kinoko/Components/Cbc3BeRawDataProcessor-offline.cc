#include <iostream>
#include "KcomProcess.hh"
#include "KinokoStandaloneComponent.hh"

#include "Cbc3BeRawDataProcessor.hh"

using namespace std;

int main(int argc, char** argv) { 
    TMushArgumentList ArgumentList(argc, argv); 
    TKinokoDataProcessor *MyDataProcessor = new Cbc3BeRawDataProcessor();
    TKinokoStandaloneDataProcessor StandaloneDataProcessor(MyDataProcessor, "MyDataProcessor"); 
    try { 
	StandaloneDataProcessor.Start(ArgumentList); 
    } catch (TKinokoException &e) { 
	cerr << "ERROR: " << e << endl; 
    } 
    return 0; 
}
