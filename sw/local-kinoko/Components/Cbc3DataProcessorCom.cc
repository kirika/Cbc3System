#include "Cbc3DataProcessorCom.hh"
#include "Cbc3DataProcessorBase.hh"

void Cbc3DataProcessorCom::BuildDescriptor(TKcomComponentDescriptor& Descriptor)
{
    TKinokoDataProcessorCom::BuildDescriptor(Descriptor);
    Descriptor.AddComment("Cbc3DataDataProcessorCom: derived from TKinokoDataProcessorCom");
    TKcomEventDeclaration ChangeConfig("changeConfig");
    Descriptor.RegisterEventSource(EventId_ChangeConfig, ChangeConfig);
}
int Cbc3DataProcessorCom::DoTransaction(void) throw(TKcomException)
{
    if( _Cbc3DataProcessor->ChangeConfigEvent() ){

	_Registry->SetValue( "cbc3dataprocessor/config_value", _Cbc3DataProcessor->GetConfigValue() );
	_Cbc3DataProcessor->ResetConfigValue();
	const std::map<int,std::string> &olm = _Cbc3DataProcessor->GetOffsetListMap();
	std::map<int,std::string>::const_iterator it = olm.begin();
	for(; it != olm.end(); it++){
	    char regname[256];
	    sprintf( regname, "cbc3dataprocessor/offsets_g%1d", it->first );
	   // cout << it->second << endl;
	    _Registry->SetValue( regname, it->second );
	}    
	_Cbc3DataProcessor->ResetOffsetListMap();
	TKcomEvent Event;
	/*
	   const vector<std::string> list = _Cbc3DataProcessor->EventArgumentList(); 
	   cout << "Com : " << list.at(0) << endl;
	   for( unsigned i=0; i< list.size(); i++ )
	   Event.ArgumentList().push_back( list.at(i) );
	   */
	EmitEventOneWay(EventId_ChangeConfig, Event);
	_Cbc3DataProcessor->SetChangeConfigEvent(false);
    }
    return TKinokoDataProcessorCom::DoTransaction();
}
