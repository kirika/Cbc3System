/* Cbc3ParamTuningDataProcessor.hh */
/* Kirika Uchida 18.02.2017 */

#ifndef __CBC3PARAMTUNINGDATAPROCESSOR_HH__
#define __CBC3PARAMTUNINGDATAPROCESSOR_HH__

//#include "KinokoSystemComponent.hh"
#include "Cbc3DataProcessorBase.hh"
#include "KinokoBlockDataSection.hh"
#include "KinokoNestedDataSection.hh"
#include "KinokoIndexedDataSection.hh"
#include "KinokoTaggedDataSection.hh"
#include <Cbc3BeData.h>
#include <string>

struct param_tuning_item{
    int value;
    int nhit;
    int le50p_value;
    int gt50p_value;
    int le50p_nhit;
    int gt50p_nhit;
};

class Cbc3ParamTuningDataProcessor : public Cbc3DataProcessorBase {

    public:
	enum TUNING_PROC_ID { VcthTuning, OffsetTuning, VcthScan }; 
	enum ParamNbit      { VcthNbit=10, OffsetNbit=8 };
    public:
	Cbc3ParamTuningDataProcessor(void);
	virtual ~Cbc3ParamTuningDataProcessor();
    protected:
	virtual void BuildDataSource(TKinokoDataSource* DataSource);
	virtual void OnConstruct(void) throw(TKinokoException);
	virtual void OnRunBegin(void) throw(TKinokoException);
	virtual void OnReceivePacket(void* Packet, long PacketSize) throw(TKinokoException);
	virtual void OnReceiveDataPacket(void* Packet, long PacketSize) throw(TKinokoException);
	virtual void OnReceiveTrailerPacket(void* Packet, long PacketSize) throw(TKinokoException);
	virtual void OnReceiveRunBeginPacket(void* Packet, long PacketSize) throw(TKinokoException);
	virtual void OnReceiveRunEndPacket(void* Packet, long PacketSize) throw(TKinokoException);

	void ProcessBeBlockDataSection( void *Packet );
	void ProcessConditionRecordDataSection( void *Packet );
	void WriteCbcDataPacket() throw(TKinokoException);
	void SendReportPacket(int EventRate, int DataFlow) throw(TKinokoException);
    protected:
	int _DataSourceId;
	/* BE block data (raw data) section */
	int _BeBlockDataSectionId;
	TKinokoBlockDataSection* _BeBlockDataSection;
	TKinokoBlockDataSectionScanner* _BeBlockDataScanner;
	/* condition data section */
	int _ConditionRecordDataSectionId;
	TKinokoTaggedDataSection *_ConditionRecordDataSection;
	TKinokoTaggedDataSectionScanner *_ConditionRecordDataScanner;
	int _ConditionRecordDataVcthIndex;
	/* CBC data section */ 
	TKinokoNestedDataSection* _CbcDataSection;
	TKinokoNestedDataSectionFormatter* _CbcDataFormatter;
	TKinokoIndexedDataSection* _CbcVcthDataSection;
	TKinokoIndexedDataSectionFormatter* _CbcVcthDataFormatter;
	/* Event data section */
	TKinokoTaggedDataSection* _EventDataSection;
	TKinokoTaggedDataSectionFormatter* _EventDataFormatter;
	long _EventCount, _DataAmount;
	int _EventRateIndex, _DataFlowIndex;
	int _LastReportTime, _ReportInterval;
	int EventRate;
	int DataFlow;

	long _Prescale;
	long _TotalEventCount;

	cbc3hal::BeDataDecoder _BeDataDecoder;
	std::map<int,int>    fGroupMap;//channel,group

	int _SampleCount; 
	int _SampleN0;
	int fVcthStep;
	struct param_tuning_item                fParamTuningVcthData;  
	std::map<int, struct param_tuning_item> fParamTuningOffsetData;  

	TUNING_PROC_ID _TuningProcess;
	int fHitSum;
	int fVcth;

};

#endif

