#ifndef __CBC3DATAPROCESSOR_H__
#define __CBC3DATAPROCESSOR_H__ 

#include "KinokoDataProcessor.hh"
#include <vector>
#include <string>
#include <map>

class Cbc3DataProcessorBase : public TKinokoDataProcessor {

    public:
	Cbc3DataProcessorBase(void):_ChangeConfigEvent(false){}
	virtual ~Cbc3DataProcessorBase(){}
	bool ChangeConfigEvent()const{ return _ChangeConfigEvent; }
	const std::vector<std::string>& EventArgumentList()const{ return _EventArgumentList; }
	void SetChangeConfigEvent(bool arg){ _ChangeConfigEvent = arg; }
	const std::string &GetConfigValue()const{ return _ConfigValue; }
	const std::map<int,std::string> &GetOffsetListMap()const{ return fOffsetListMap; }
	void ResetConfigValue(){ _ConfigValue = ""; }
	void ResetOffsetListMap(){ fOffsetListMap.clear(); }
    protected:
	bool _ChangeConfigEvent;
	std::string _ConfigValue;
	std::map<int,std::string> fOffsetListMap;//group,list
	std::vector<std::string> _EventArgumentList;
};

#endif
