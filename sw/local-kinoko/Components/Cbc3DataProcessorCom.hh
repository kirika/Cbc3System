#ifndef __CBC3DATAPROCESSORCOM_H__
#define __CBC3DATAPROCESSORCOM_H__
#include "KinokoDataProcessorCom.hh"

class Cbc3DataProcessorBase;

class Cbc3DataProcessorCom : public TKinokoDataProcessorCom{

    public:
	Cbc3DataProcessorCom(TKinokoDataProcessor* DataProcessor): TKinokoDataProcessorCom(DataProcessor),_Cbc3DataProcessor((Cbc3DataProcessorBase*) DataProcessor){}
	virtual ~Cbc3DataProcessorCom(){}
	virtual void BuildDescriptor(TKcomComponentDescriptor& Descriptor);
	virtual int  DoTransaction(void) throw(TKcomException);

    protected:

	Cbc3DataProcessorBase *_Cbc3DataProcessor;

	enum TEventId{
	    EventId_ChangeConfig,
	    _NumberOfEvents
	};
};

#endif

