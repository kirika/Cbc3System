/* Cbc3ParamTuningDataProcessor.cc */
/* Kirika Uchida 18.02.2017 */

#include "Cbc3DataProcessorBase.hh"
#include "Cbc3ParamTuningDataProcessor.hh"
#include <sstream>
#include <iomanip>

using namespace std;
using namespace cbc3hal;



static void make_offset_lists( std::map<int, struct param_tuning_item> &pt_map, std::map<int,int> &group_map, std::map<int,string> &list_map ){

    std::map<int, struct param_tuning_item>::iterator it = pt_map.begin();
    for(;it!=pt_map.end();it++){
	int ch = it->first;
	int g = group_map[ch];
	struct param_tuning_item& item = it->second; 
	ostringstream oss; 
	oss << "{" 
	    << std::hex << std::setw(2) << std::setfill('0') << ch << ":"
	    << std::hex << std::setw(2) << std::setfill('0') << item.value 
	    << "},"; 
	list_map[g] = list_map[g] + oss.str(); 
    }
    return;
}
static void param_tuning_data_update( struct param_tuning_item &item, int bit, int nhit50, bool prop = 1 ){

    if( bit >= 0 ){
	if( ( prop && item.nhit <= nhit50 ) || ( !prop && item.nhit > nhit50) ){
	    item.le50p_value = item.value; 
	    item.le50p_nhit = item.nhit; 
	    item.value &= ~( 1 << bit );
	}
	else{
	    item.gt50p_value = item.value; 
	    item.gt50p_nhit = item.nhit; 
	}
	if( bit != 0 ){
	    item.value |= ( 1 << (bit - 1));
	}
	item.nhit = 0;
    }
    else if( bit == -1 ){
	float dl =  nhit50 - item.le50p_nhit;
	float dg =  item.le50p_nhit - nhit50;
	if( dl < dg ){
	    item.value = item.le50p_value;
	    item.nhit = item.le50p_nhit;
	}
	else{
	    item.value = item.gt50p_value;
	    item.nhit = item.gt50p_nhit;
	}
    } 
}

Cbc3ParamTuningDataProcessor::Cbc3ParamTuningDataProcessor(void):
    _BeDataDecoder(), _SampleCount(0), _SampleN0(100), fParamTuningOffsetData(), fVcth(0)
{
    //    cout << "Cbc3ParamTuningDataProcessor group map = {";
    for(int i=1; i <= 254; i++){
	fGroupMap[i] = ( (i-1) % 16 )/2; 
	//	cout << fGroupMap[i];
    }
    //   cout << endl;
}

Cbc3ParamTuningDataProcessor::~Cbc3ParamTuningDataProcessor(void)
{
}

void Cbc3ParamTuningDataProcessor::BuildDataSource(TKinokoDataSource* DataSource)
{
    _CbcDataSection = new TKinokoNestedDataSection(DataSource, "cbc1"); 
    DataSource->AddDataSection(_CbcDataSection);
    _CbcDataFormatter = _CbcDataSection->Formatter();

    _CbcVcthDataSection = new TKinokoIndexedDataSection(DataSource, "vcth");
    _CbcVcthDataSection->SetWidth( 8, 10 );
    _CbcVcthDataSection->SetMaxElementDepth(254);
    _CbcDataSection->AddDataSection(_CbcVcthDataSection);
    _CbcVcthDataFormatter = _CbcVcthDataSection->Formatter(); 

    int DataWidth(32);
    _EventDataSection = new TKinokoTaggedDataSection(DataSource, "data_flow");
    _EventRateIndex = _EventDataSection->AddField("event_rate", DataWidth);
    _DataFlowIndex = _EventDataSection->AddField("data_flow", DataWidth);
    DataSource->AddDataSection(_EventDataSection);
    _EventDataFormatter = _EventDataSection->Formatter();
}

void Cbc3ParamTuningDataProcessor::OnConstruct(void) throw(TKinokoException)
{
    _EventCount = 0;
    _DataAmount = 0;
    _LastReportTime = TMushDateTime().AsLong();
    _ReportInterval = 5;
    EventRate = 0;
    DataFlow = 0;
}
void Cbc3ParamTuningDataProcessor::OnRunBegin(void) throw(TKinokoException)
{
    _Prescale = 0;
    _TotalEventCount = 0;
    string DataSourceName;
    string SectionName;
    TKinokoDataSource* DataSource;
    TKinokoDataSection* DataSection;
    _TuningProcess = VcthTuning;
    fHitSum = 0;
    fVcthStep = 10;

    DataSourceName = "Cbc3BeFc7";
    DataSource = _InputDataDescriptor->DataSource(DataSourceName);
    if (DataSource == 0) {
	throw TKinokoException("datasouce not found: " + DataSourceName);
    }
    _DataSourceId = DataSource->DataSourceId();

    SectionName = "cbc3be1";
    DataSection = DataSource->DataSection(SectionName);
    if (DataSection == 0) {
	throw TKinokoException("section not found: " + SectionName);
    }
    _BeBlockDataSectionId = DataSection->SectionId();
    _BeBlockDataSection = (TKinokoBlockDataSection*) DataSection;
    _BeBlockDataScanner = _BeBlockDataSection->Scanner();
    //cout << "I am here OnRunbegin" << endl;
    SectionName = "condition_record";
    DataSection = DataSource->DataSection(SectionName);
    if (DataSection == 0) {
	throw TKinokoException("section not found: " + SectionName);
    }
    _ConditionRecordDataSectionId = DataSection->SectionId();
    _ConditionRecordDataSection   = (TKinokoTaggedDataSection*) DataSection;
    _ConditionRecordDataScanner   = _ConditionRecordDataSection->Scanner();
    _ConditionRecordDataVcthIndex = _ConditionRecordDataSection->FieldIndexOf("vcth");
    cout << "I am here OnRunbegin end" << endl;
    _SampleCount = 0;
    fOffsetListMap.clear();
    /*
       _EventArgumentList.clear();
       _EventArgumentList.push_back(oss.str()); 
       */
    //_EventArgumentList.push_back("OFFSET!!!"); 
    //    cout << _EventArgumentList.at(0) << endl;
    fParamTuningOffsetData.clear();
    for( unsigned i=1; i <= 254; i++ ){
	fParamTuningOffsetData[i].value = 0x80;
	fParamTuningOffsetData[i].nhit = 0; 
	fParamTuningOffsetData[i].le50p_value = 0xFF;
	fParamTuningOffsetData[i].le50p_nhit = 0;
	fParamTuningOffsetData[i].gt50p_value = 0x00;
	fParamTuningOffsetData[i].gt50p_nhit = 0;

    }
    fParamTuningVcthData.value = 0x200;
    fParamTuningVcthData.nhit = 0; 
    fParamTuningVcthData.le50p_value = 0x3FF;
    fParamTuningVcthData.le50p_nhit = 0;
    fParamTuningVcthData.gt50p_value = 0x000;
    fParamTuningVcthData.gt50p_nhit = 0;

    ostringstream oss; 
    oss << "offsets:[{00:80}]"; 
    oss << " vcth:" << std::dec << 0x200;
    oss << " resume";
    _ConfigValue = oss.str(); 
    _ChangeConfigEvent = true;
}

void Cbc3ParamTuningDataProcessor::OnReceivePacket(void* Packet, long PacketSize) throw(TKinokoException)
{
    SendPacket(Packet, PacketSize);
}

void Cbc3ParamTuningDataProcessor::OnReceiveDataPacket(void* Packet, long PacketSize) throw(TKinokoException)
{
    _DataAmount += PacketSize;

    int DataSourceId = TKinokoDataStreamScanner::DataSourceIdOf(Packet);
    if (DataSourceId != _DataSourceId) {
	cout << "### Unexpected DataSource: ID=" << DataSourceId << endl;
	return;
    }
    int SectionId = TKinokoDataSectionScanner::SectionIdOf(Packet);
    if( SectionId ==  _BeBlockDataSectionId ){

	ProcessBeBlockDataSection(Packet);

	if( _SampleCount != 0 && _SampleCount % _SampleN0 == 0 ){ 

	    if( _TuningProcess == VcthTuning ){

		int bit = VcthNbit - _SampleCount / _SampleN0;
		param_tuning_data_update( fParamTuningVcthData, bit,  _SampleN0 * 254 / 2., false );
		ostringstream oss; 
		oss << "vcth:" << fParamTuningVcthData.value;
		oss << " resume"; 
		_ConfigValue = oss.str(); 
		_ChangeConfigEvent = true;
		if( bit == -1 ){
		    _TuningProcess = OffsetTuning;
		    _SampleCount = 0;
		}
	    }
	    else if( _TuningProcess == OffsetTuning ){

		int bit = OffsetNbit - _SampleCount / _SampleN0;
		cout << "bit = " << bit << endl;

		for( int i=1; i <= 254; i++ ){
		    struct param_tuning_item& item = fParamTuningOffsetData[i]; 
		    param_tuning_data_update( item, bit, _SampleN0/2. );
		    if( bit == -1 ){
			cout << "2 "
			    << "0x" << std::hex << std::setfill('0') << i << " " 
			    << "0x" << std::hex << std::setfill('0') << item.value << endl; 
		    }
		}
		make_offset_lists(fParamTuningOffsetData, fGroupMap, fOffsetListMap); 
		if( bit == -1 ){
		    _TuningProcess = VcthScan;
		    _SampleCount = 0;
		    _ConfigValue = "vcth:0 "; 
		    fVcth = 0;
		}
		_ConfigValue = "resume";
		_ChangeConfigEvent = true;
	    }
	    else if( _TuningProcess == VcthScan ){
		int NextVcth(0);
		if( fHitSum == 0 ){
		    NextVcth = fVcth + fVcthStep;
		} 
		else if( fHitSum != _SampleCount * 254 ){
		    if( fVcthStep != 1 ){
			NextVcth = fVcth - fVcthStep; 
			fVcthStep = ( fVcthStep/2 == 0 ) ? 1 : fVcthStep/2;
		    }
		    else{
			NextVcth = fVcth + fVcthStep;
		    }
		}
		else if( fHitSum == _SampleCount * 254 ){
		    fVcthStep = 10;
		    NextVcth = fVcth + fVcthStep;
		}
		ostringstream oss;
		if( NextVcth > 0x2FF ){
		    oss << "vcth:" << std::dec << fParamTuningVcthData.value;
		    oss << " stop";
		}
		else{
		    oss << "vcth:" << NextVcth;
		    oss << " resume"; 
		}
		_ConfigValue = oss.str(); 
		_ChangeConfigEvent = true;
		fHitSum = 0;
		_SampleCount = 0;
	    }
	}
    }
    else if( SectionId ==  _ConditionRecordDataSectionId ){
	ProcessConditionRecordDataSection(Packet);
	cout << "I am here reading condition record" << endl;
    }
    else{
	cout << "### Unexpected DataSection: ID=" << SectionId << endl;
	return;
    }
}
void Cbc3ParamTuningDataProcessor::ProcessBeBlockDataSection( void *Packet ){

    void *BlockData = _BeBlockDataScanner->DataAreaOf(Packet);
    int BlockDataSize = TKinokoDataSectionScanner::DataSizeOf(Packet);
    //cout << "BlockDataSize = " << std::dec << BlockDataSize << endl;
    uint32_t *data = (uint32_t *)BlockData;
    _BeDataDecoder.SetData( data, BlockDataSize );
    while( _BeDataDecoder.SetNextEvent() ){ 
	//	_BeDataDecoder.DumpEventBeData(cout);
	_EventCount++;
	_TotalEventCount++;
	_SampleCount++;
	if( _Prescale &&  ((_TotalEventCount) % _Prescale) != 0 ) continue;
	while( _BeDataDecoder.SetNextCbc() ){ 
	    WriteCbcDataPacket();
	}
	long PassedTime = TMushDateTime().AsLong() - _LastReportTime;
	if (PassedTime > _ReportInterval) {
	    EventRate = _EventCount / PassedTime;
	    DataFlow = _DataAmount / PassedTime;
	    _EventCount -= EventRate * PassedTime;
	    _DataAmount -= DataFlow * PassedTime;
	    _LastReportTime += PassedTime;
	}
	SendReportPacket(EventRate, DataFlow);
	SendEventTrailerPacket();
    }
}
void Cbc3ParamTuningDataProcessor::ProcessConditionRecordDataSection( void *Packet ){

    _ConditionRecordDataScanner->ReadFrom(Packet, _ConditionRecordDataVcthIndex, fVcth);
}


void Cbc3ParamTuningDataProcessor::OnReceiveTrailerPacket(void* Packet, long PacketSize) throw(TKinokoException)
{
}

void Cbc3ParamTuningDataProcessor::WriteCbcDataPacket() throw(TKinokoException)
{
    int CbcDataSize(0);
    int CbcDataPacketSize(0);
    int VcthDataSize(0);
    int VcthDataPacketSize(0);

    const CbcDataPacketDecoder * cbc_data_packet_decoder = _BeDataDecoder.GetCbcDataPacketDecoder();

    vector<unsigned> CbcHitChannelList = cbc_data_packet_decoder->HitChannels();
    int NElements = CbcHitChannelList.size();
    if(NElements !=0 ){
	VcthDataSize = _CbcVcthDataFormatter->DataSizeFor(NElements);
	VcthDataPacketSize = _CbcVcthDataFormatter->PacketSizeFor(VcthDataSize);
	CbcDataSize += VcthDataPacketSize;
	CbcDataPacketSize = _CbcDataFormatter->PacketSizeFor(CbcDataSize);
    }
    for( unsigned i=0; i < CbcHitChannelList.size(); i++ ){
	if( _TuningProcess == VcthTuning ){
	    fParamTuningVcthData.nhit += 1;
	} 
	if( _TuningProcess == OffsetTuning ){
	    fParamTuningOffsetData[CbcHitChannelList.at(i)].nhit += 1;
	}
	fHitSum ++;
    }

    void* Buffer;
    unsigned char* CbcDataBuffer;
    if( CbcDataSize != 0 ){
	do {
	    _OutputStream->NextEntry(Buffer, CbcDataPacketSize);
	} while (Buffer == 0);
	_CbcDataFormatter->WriteHeaderTo(Buffer, CbcDataSize);
	CbcDataBuffer = (unsigned char*)_CbcDataFormatter->DataAreaOf(Buffer);
    }

    int offset(0);
    if( VcthDataSize != 0 ){
	_CbcVcthDataFormatter->WriteHeaderTo( &CbcDataBuffer[offset], VcthDataSize );
	for( unsigned i=0; i < CbcHitChannelList.size(); i++ ){ 
	    _CbcVcthDataFormatter->WriteTo( &CbcDataBuffer[offset], i, CbcHitChannelList.at(i), fVcth );
	}
	//	cout << "Cbc3ParamTuningDataProcessor : # of hit = " << CbcHitChannelList.size() << " at Vcth = " << fVcth << endl;
    }	

    _OutputStream->Flush(Buffer, CbcDataPacketSize);

}

void Cbc3ParamTuningDataProcessor::SendReportPacket(int EventRate, int DataFlow) throw(TKinokoException)
{
    int DataSize = _EventDataFormatter->DataSize();
    int PacketSize = _EventDataFormatter->PacketSizeFor(DataSize);

    void* Buffer;
    do {
	_OutputStream->NextEntry(Buffer, PacketSize);
    } while (Buffer == 0);

    _EventDataFormatter->WriteHeaderTo(Buffer, DataSize);
    _EventDataFormatter->WriteTo(Buffer, _EventRateIndex, EventRate);
    _EventDataFormatter->WriteTo(Buffer, _DataFlowIndex, DataFlow);

    _OutputStream->Flush(Buffer, PacketSize);

}

void Cbc3ParamTuningDataProcessor::OnReceiveRunBeginPacket(void* Packet, long PacketSize) throw(TKinokoException)
{
    //	SendPacket(Packet, PacketSize);
}

void Cbc3ParamTuningDataProcessor::OnReceiveRunEndPacket(void* Packet, long PacketSize) throw(TKinokoException)
{
    //	SendPacket(Packet, PacketSize);
}
