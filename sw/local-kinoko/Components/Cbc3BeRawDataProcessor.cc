/* KinokoCbc3BeRawDataProcessor.cc */
/* Kirika Uchida 07 Dec 2016 */

#include "KinokoDataProcessor.hh"
#include "KinokoTaggedDataSection.hh"
#include "Cbc3BeRawDataProcessor.hh"

using namespace std;
using namespace cbc3hal;

Cbc3BeRawDataProcessor::Cbc3BeRawDataProcessor(void):_BeDataDecoder()
{
}

Cbc3BeRawDataProcessor::~Cbc3BeRawDataProcessor(void)
{
}

void Cbc3BeRawDataProcessor::BuildDataSource(TKinokoDataSource* DataSource)
{
    int DataWidth = 32;
    //data produced by BE board
    _BeDataSection = new TKinokoTaggedDataSection(DataSource, "be_data");
    _BeData_Status_Index = _BeDataSection->AddField("Status", DataWidth);
    _BeData_L1ACounter_Index = _BeDataSection->AddField("L1A_counter", DataWidth);
    DataSource->AddDataSection(_BeDataSection);
    _BeDataFormatter = _BeDataSection->Formatter();

    _CbcDataSections[1] = new CbcDataSection();
    _CbcDataSections[5] = new CbcDataSection();

    map<unsigned, CbcDataSection *>::iterator it = _CbcDataSections.begin();
    char str[256];
    for(; it != _CbcDataSections.end(); it++ ){ 
	unsigned cbc_id = it->first;
	CbcDataSection *sec = it->second;
	sprintf( str, "cbc%d", cbc_id );
	sec->_CbcDataSection = new TKinokoNestedDataSection(DataSource, str); 
	DataSource->AddDataSection(sec->_CbcDataSection);
	sec->_CbcDataFormatter = sec->_CbcDataSection->Formatter();

	sec->_CbcStubDataSection = new TKinokoIndexedDataSection(DataSource, "stub_data" );
	sec->_CbcStubDataSection->SetWidth( 8, 4 );
	sec->_CbcStubDataSection->SetMaxElementDepth(3);
	sec->_CbcDataSection->AddDataSection(sec->_CbcStubDataSection);
	sec->_CbcStubDataFormatter = sec->_CbcStubDataSection->Formatter();

	sec->_CbcTrigFlagDataSection = new TKinokoTaggedDataSection(DataSource, "trig_flag_data" );
	sec->_CbcTrigFlagData_Sof_Index = sec->_CbcTrigFlagDataSection->AddField("Sof", 1 );
	sec->_CbcTrigFlagData_Or254_Index = sec->_CbcTrigFlagDataSection->AddField("Or254", 1 );
	sec->_CbcTrigFlagData_Err_Index = sec->_CbcTrigFlagDataSection->AddField("Err", 1 );
	sec->_CbcDataSection->AddDataSection(sec->_CbcTrigFlagDataSection);
	sec->_CbcTrigFlagDataFormatter = sec->_CbcTrigFlagDataSection->Formatter();

	sec->_CbcStatusDataSection = new TKinokoTaggedDataSection(DataSource, "status_data");
	sec->_CbcStatusData_Error0_Index = sec->_CbcStatusDataSection->AddField("err0", 1);
	sec->_CbcStatusData_Error1_Index = sec->_CbcStatusDataSection->AddField("err1", 1);
	sec->_CbcStatusData_PipelineAddress_Index = sec->_CbcStatusDataSection->AddField("pipeline_address", 9);
	sec->_CbcStatusData_L1ACounter_Index = sec->_CbcStatusDataSection->AddField("L1A_counter", 9);
	sec->_CbcDataSection->AddDataSection(sec->_CbcStatusDataSection);
	sec->_CbcStatusDataFormatter = sec->_CbcStatusDataSection->Formatter();

	sec->_CbcHitDataSection = new TKinokoIndexedDataSection(DataSource, "hit_data");
	sec->_CbcHitDataSection->SetWidth( 8, 1 );
	sec->_CbcHitDataSection->SetMaxElementDepth(254);
	sec->_CbcDataSection->AddDataSection(sec->_CbcHitDataSection);
	sec->_CbcHitDataFormatter = sec->_CbcHitDataSection->Formatter(); 
    }

    _EventDataSection = new TKinokoTaggedDataSection(DataSource, "data_flow");
    _EventCountIndex = _EventDataSection->AddField("event_count", DataWidth );
    _EventRateIndex = _EventDataSection->AddField("event_rate", DataWidth);
    _DataFlowIndex = _EventDataSection->AddField("data_flow", DataWidth);
    DataSource->AddDataSection(_EventDataSection);
    _EventDataFormatter = _EventDataSection->Formatter();

}

void Cbc3BeRawDataProcessor::OnConstruct(void) throw(TKinokoException)
{
    _EventCount = 0;
    _DataAmount = 0;
    _LastReportTime = TMushDateTime().AsLong();
    _ReportInterval = 1;
    _EventRate = 0;
    _DataFlow = 0;
}

void Cbc3BeRawDataProcessor::OnRunBegin(void) throw(TKinokoException)
{
    _Prescale = 0;
    _TotalEventCount = 0;
    string DataSourceName;
    string SectionName;
    TKinokoDataSource* DataSource;
    TKinokoDataSection* DataSection;

    DataSourceName = "Cbc3BeFc7";
    DataSource = _InputDataDescriptor->DataSource(DataSourceName);
    if (DataSource == 0) {
	throw TKinokoException("datasouce not found: " + DataSourceName);
    }
    _DataSourceId = DataSource->DataSourceId();

    SectionName = "cbc3be1";
    DataSection = DataSource->DataSection(SectionName);
    if (DataSection == 0) {
	throw TKinokoException("section not found: " + SectionName);
    }
    _BeBlockDataSectionId = DataSection->SectionId();
    _BeBlockDataSection = (TKinokoBlockDataSection*) DataSection;
    _BeBlockDataScanner = _BeBlockDataSection->Scanner();
    cout << "OnRunBegin finished." << endl;
}
void Cbc3BeRawDataProcessor::OnRunEnd(void) throw(TKinokoException)
{
	cout << "Total event count = " << _TotalEventCount << endl;
	SendReportPacket(); 
}
/*
void Cbc3BeRawDataProcessor::OnReceivePacket(void* Packet, long PacketSize) throw(TKinokoException)
{
    SendPacket(Packet, PacketSize);
}
*/

void Cbc3BeRawDataProcessor::OnReceiveDataPacket(void* Packet, long PacketSize) throw(TKinokoException)
{
    _DataAmount += PacketSize;

    int DataSourceId = TKinokoDataStreamScanner::DataSourceIdOf(Packet);
    if (DataSourceId != _DataSourceId) {
	cout << "### Unexpected DataSource: ID=" << DataSourceId << endl;
	return;
    }
    int SectionId = TKinokoDataSectionScanner::SectionIdOf(Packet);
    if (SectionId != _BeBlockDataSectionId) {
	cout << "### Unexpected DataSection: ID=" << SectionId << endl;
	return;
    }
    void *BlockData = _BeBlockDataScanner->DataAreaOf(Packet);
    int BlockDataSize = TKinokoDataSectionScanner::DataSizeOf(Packet);
    //cout << "BlockDataSize = " << std::dec << BlockDataSize << endl;
    fVCTH = -1;
    _BeDataDecoder.SetData( BlockData, BlockDataSize );
//    _BeDataDecoder.DumpEventBeData(cout);
    while( _BeDataDecoder.SetNextEvent() ){ 
	_EventCount++;
	_TotalEventCount++;
	bool store(false);
	while( _BeDataDecoder.SetNextCbc() ){ 
	    const CbcDataPacketDecoder * cbc_data_packet_decoder = _BeDataDecoder.GetCbcDataPacketDecoder();
	    vector<unsigned> CbcHitChannelList = cbc_data_packet_decoder->HitChannels();
	    if( CbcHitChannelList.size() != 0 ){
		store = true;
		break;
	    }
	}
	long PassedTime = TMushDateTime().AsLong() - _LastReportTime;
	if (PassedTime > _ReportInterval) {
	    _EventRate = _EventCount / PassedTime;
	    _DataFlow = _DataAmount / PassedTime;
	    _EventCount -= _EventRate * PassedTime;
	    _DataAmount -= _DataFlow * PassedTime;
	    cout << "EventRate = " << _EventRate << " DataFlow = " << _DataFlow << endl; 
	    cout << "TotalEventCount = " << _TotalEventCount << " DataAmount = " << _DataAmount << endl;
	    _LastReportTime += PassedTime;
	}
	if(store){
	    _BeDataDecoder.ResetEvent(); 

	    WriteBeDataPacket();
	    while( _BeDataDecoder.SetNextCbc() ){ 
		WriteCbcDataPacket();
	    }
	    SendReportPacket();
	    SendEventTrailerPacket();
	}
    }
}

void Cbc3BeRawDataProcessor::OnReceiveTrailerPacket(void* Packet, long PacketSize) throw(TKinokoException)
{
}
/*
   void Cbc3BeRawDataProcessor::BuildDescriptor(TKcomComponentDescriptor& Descriptor)
   {
   TKinokoDataProcessor::BuildDescriptor(Descriptor);

   TKcomEventDeclaration SetPrescaleEvent("setPrescale");
   SetPrescaleEvent.AddArgument(TKcomPropertyDeclaration(
   "prescale", TKcomPropertyDeclaration::Type_Long
   ));
   Descriptor.RegisterEventSlot(EventId_SetPrescale, SetPrescaleEvent);
   }
   int Cbc3BeRawDataProcessor::ProcessEvent(int EventId, TKcomEvent& Event)
   {
   int Result = 0;

   switch(EventId){
   case EventId_SetPrescale:
   if( istrstream(Event.ArgumentList()[0].c_str()) >> _Prescale ){
   Result = 1;
   }
   break;
   default:
   Result = TKinokoDataProcessor::ProcessEvent(EventId, Event);
   }
   return Result;
   }
   */

void Cbc3BeRawDataProcessor::WriteBeDataPacket() throw(TKinokoException)
{
    int DataSize = _BeDataFormatter->DataSize();
    int PacketSize = _BeDataFormatter->PacketSizeFor(DataSize);

    void* Buffer;
    do {
	_OutputStream->NextEntry(Buffer, PacketSize);
    } while (Buffer == 0);

    _BeDataFormatter->WriteHeaderTo(Buffer, DataSize);
    _BeDataFormatter->WriteTo(Buffer, _BeData_Status_Index, _BeDataDecoder.BeStatus() );
    _BeDataFormatter->WriteTo(Buffer, _BeData_L1ACounter_Index, _BeDataDecoder.BeL1ACounter() );
    _OutputStream->Flush(Buffer, PacketSize);
}

void Cbc3BeRawDataProcessor::WriteCbcDataPacket() throw(TKinokoException)
{
    int CbcDataSize(0);
    int CbcDataPacketSize(0);
    int StubDataSize(0);
    int StubDataPacketSize(0);
    int HitDataSize(0);
    int HitDataPacketSize(0);

    const CbcDataPacketDecoder * cbc_data_packet_decoder = _BeDataDecoder.GetCbcDataPacketDecoder();
    cbc_data_packet_decoder->DumpData(cout);
    unsigned cbc_id = cbc_data_packet_decoder->Id();
    CbcDataSection *sec = _CbcDataSections[cbc_id];
    //cout << "CBC ID = " << cbc_id << endl;

    vector<unsigned> StubPosList = cbc_data_packet_decoder->StubPositions();
    vector<unsigned> StubBendList = cbc_data_packet_decoder->StubBends();
    int NElements(0);

    NElements = StubPosList.size();
    if(NElements!=0){
	StubDataSize = sec->_CbcStubDataFormatter->DataSizeFor(NElements);
	StubDataPacketSize = sec->_CbcStubDataFormatter->PacketSizeFor(StubDataSize);
    }
    CbcDataSize += StubDataPacketSize;
    //cout << "StubDataSize " << StubDataSize << endl;
    //	cout << "StubDataPacketSize " << StubDataPacketSize << endl;

    int TrigFlagDataSize = sec->_CbcTrigFlagDataFormatter->DataSize();
    int TrigFlagDataPacketSize = sec->_CbcTrigFlagDataFormatter->PacketSizeFor(TrigFlagDataSize);
    CbcDataSize += TrigFlagDataPacketSize;
    //cout << "TrigFlagDataSize " << TrigFlagDataSize << endl;
    //	cout << "TrigFlagDataPacketSize " << TrigFlagDataPacketSize << endl;

    int StatusDataSize = sec->_CbcStatusDataFormatter->DataSize();
    int StatusDataPacketSize = sec->_CbcStatusDataFormatter->PacketSizeFor(StatusDataSize);
    CbcDataSize += StatusDataPacketSize;
    //cout << "StatusDataSize " << StatusDataSize << endl;
    //cout << "StatusDataPacketSize " << StatusDataPacketSize << endl;

    vector<unsigned> CbcHitChannelList = cbc_data_packet_decoder->HitChannels();
    NElements = CbcHitChannelList.size();
    if(NElements!=0){ 
	HitDataSize = sec->_CbcHitDataFormatter->DataSizeFor(NElements);
	HitDataPacketSize = sec->_CbcHitDataFormatter->PacketSizeFor(HitDataSize);
    }
    CbcDataSize += HitDataPacketSize;
    //cout << "HitDataSize " << HitDataSize << endl;
    //cout << "HitDataPacketSize " << HitDataPacketSize << endl;

    CbcDataPacketSize = sec->_CbcDataFormatter->PacketSizeFor(CbcDataSize);
    //cout << "CbcDataSize " << CbcDataSize << endl;
    //cout << "CbcPacketDataSize " << CbcDataPacketSize << endl;
    void* Buffer;
    do {
	_OutputStream->NextEntry(Buffer, CbcDataPacketSize);
    } while (Buffer == 0);
    sec->_CbcDataFormatter->WriteHeaderTo(Buffer, CbcDataSize);
    unsigned char* CbcDataBuffer = (unsigned char*)(sec->_CbcDataFormatter->DataAreaOf(Buffer));

    int offset(0);
    if( StubPosList.size() != 0 ){
	sec->_CbcStubDataFormatter->WriteHeaderTo( &CbcDataBuffer[offset], StubDataSize ); 
	for( unsigned i=0; i < StubPosList.size(); i++ ){ 
	    cout << "Stub found " << StubPosList.at(i) << endl;
	    sec->_CbcStubDataFormatter->WriteTo( &CbcDataBuffer[offset], i, StubPosList.at(i), StubBendList.at(i) );
	}
    }

    offset += StubDataPacketSize;
    sec->_CbcTrigFlagDataFormatter->WriteHeaderTo( &(CbcDataBuffer[offset]), TrigFlagDataSize ); 
    sec->_CbcTrigFlagDataFormatter->WriteTo( &(CbcDataBuffer[offset]), sec->_CbcTrigFlagData_Sof_Index, cbc_data_packet_decoder->TrigSof() ); 
    sec->_CbcTrigFlagDataFormatter->WriteTo( &(CbcDataBuffer[offset]), sec->_CbcTrigFlagData_Or254_Index, cbc_data_packet_decoder->TrigOr254() ); 
    sec->_CbcTrigFlagDataFormatter->WriteTo( &(CbcDataBuffer[offset]), sec->_CbcTrigFlagData_Err_Index, cbc_data_packet_decoder->TrigErr() ); 

    offset += TrigFlagDataPacketSize;
    sec->_CbcStatusDataFormatter->WriteHeaderTo( &CbcDataBuffer[offset], StatusDataSize );
    sec->_CbcStatusDataFormatter->WriteTo( &CbcDataBuffer[offset], sec->_CbcStatusData_Error0_Index, cbc_data_packet_decoder->Error(0) );
    sec->_CbcStatusDataFormatter->WriteTo( &CbcDataBuffer[offset], sec->_CbcStatusData_Error1_Index, cbc_data_packet_decoder->Error(1) );
    sec->_CbcStatusDataFormatter->WriteTo( &CbcDataBuffer[offset], sec->_CbcStatusData_PipelineAddress_Index, cbc_data_packet_decoder->PipelineAddress() );
    sec->_CbcStatusDataFormatter->WriteTo( &CbcDataBuffer[offset], sec->_CbcStatusData_L1ACounter_Index, cbc_data_packet_decoder->L1ACounter() );

    offset += StatusDataPacketSize;
    if( CbcHitChannelList.size() != 0 ){ 
	sec->_CbcHitDataFormatter->WriteHeaderTo( &CbcDataBuffer[offset], HitDataSize );
	cout << "# of hit = " << CbcHitChannelList.size() << endl;
	for( unsigned i=0; i < CbcHitChannelList.size(); i++ ){ 
	    sec->_CbcHitDataFormatter->WriteTo( &CbcDataBuffer[offset], i, CbcHitChannelList.at(i), 1 );
	}
    }

    /*
       virtual int                   Id()const{ return (fData[0] & 0x1F000) >> 12; } 
       virtual int                   FeId()const{ return (fData[0] & 0xE0000) >> 17; } 
       virtual int                   BeId()const{ return (fData[0] & 0xF00000) >> 20; } 
       */

    _OutputStream->Flush(Buffer, CbcDataPacketSize);

}

void Cbc3BeRawDataProcessor::SendReportPacket() throw(TKinokoException)
{
    int DataSize = _EventDataFormatter->DataSize();
    int PacketSize = _EventDataFormatter->PacketSizeFor(DataSize);

    void* Buffer;
    do {
	_OutputStream->NextEntry(Buffer, PacketSize);
    } while (Buffer == 0);

    _EventDataFormatter->WriteHeaderTo(Buffer, DataSize);
//    _EventDataFormatter->WriteTo(Buffer, _EventCountIndex, 1);
    cout << "TotalEventCount = " << _TotalEventCount << endl;
    _EventDataFormatter->WriteTo(Buffer, _EventRateIndex, _EventRate);
    _EventDataFormatter->WriteTo(Buffer, _DataFlowIndex, _DataFlow);
    _EventDataFormatter->WriteTo(Buffer, _EventCountIndex, _TotalEventCount);

    _OutputStream->Flush(Buffer, PacketSize);

}

void Cbc3BeRawDataProcessor::OnReceiveRunBeginPacket(void* Packet, long PacketSize) throw(TKinokoException)
{
    //SendPacket(Packet, PacketSize);
}

void Cbc3BeRawDataProcessor::OnReceiveRunEndPacket(void* Packet, long PacketSize) throw(TKinokoException)
{
    /*
    SendReportPacket(); 
    SendPacket(Packet, PacketSize);
    */
}
