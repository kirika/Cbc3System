#include <iostream>
#include "KcomProcess.hh"
#include "KinokoDataProcessorCom.hh"

#include "Cbc3BeRawDataProcessor.hh"

using namespace std;

int main(int argc, char** argv)
{
	TMushArgumentList ArgumentList(argc, argv);

	TKinokoDataProcessor *MyDataProcessor = new Cbc3BeRawDataProcessor();
	TKcomComponent *Component = new TKinokoDataProcessorCom(MyDataProcessor);
	TKcomProcess *ComProcess = new TKcomProcess(Component);

	try{
			ComProcess->Start(ArgumentList);
	}
	catch( TKcomException &e ){
		cerr << "ERROR: " << argv[0] << ": " << e << endl;
	}

	delete ComProcess;
	delete Component;
	delete MyDataProcessor;

	/*

	TKinokoStandaloneDataProcessor StandaloneDataProcessor(MyBlockDataProcessor, "MyDataProcessor");

	try {
		StandaloneDataProcessor.Start(ArgumentList);
	}
	catch (TKinokoException &e) {
		cerr << "ERROR: " << e << endl;
	}
	*/

	return 0;
}
