CURRENT_DIR=${PWD}
source ${CBC3SYSTEM}/sw/cbc3hal/setup.sh

cd ${CBC3SYSTEM}/sw/kinoko
for dir in include lib bin
do
	if [ ! -d $dir ]; then
		mkdir $dir
	fi   
done
source ${CBC3SYSTEM}/sw/kinoko/src/kinoko-bashrc
export KCOM_PATH=.:${CBC3SYSTEM}/sw/local-kinoko/bin:${CBC3SYSTEM}/sw/kinoko/bin
export PATH=.:${CBC3SYSTEM}/sw/local-kinoko/bin:${PATH}

cd ${CURRENT_DIR}
#export TABREE_ROOT=${PWD}/Tabree-2013-0815

