#include <getopt.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <readline/readline.h>
#include <readline/history.h>
#include <cstdio>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <signal.h>
#include <uhal/uhal.hpp>
#include "ipbus_utils.h"
#include "CbcI2cRegIpbusInterface.h"
#include "Fc7SystemRegInterface.h" 
#include "Cbc3BeInterface.h"
#include "Cbc3BeFc7.h"
#include "fc7/MmcPipeInterface.hpp"
#include <TH1F.h>
#include <TPad.h>
#include <TStyle.h>
#include <TH2F.h>
#include <TFile.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 
#include <sys/types.h>
#include <sys/wait.h>


using namespace std;
using namespace uhal;
using namespace cbc3hal;
using namespace ipbutl;

class KinokoCanvasInterface;
class DaqController;
//
// default variables
string connection_file( "connections.xml" );
string id("board");

DaqController *daqc(0);


// replace s2 to s3 in s1 and the result is returned
std::string rstr( std::string s1, std::string s2, std::string s3 )
{
    std::string::size_type  pos( s1.find( s2 ) );

    while( pos != std::string::npos )
    {
	s1.replace( pos, s2.length(), s3 );
	pos = s1.find( s2, pos + s3.length() );
    }

    return s1;
}
string time_string(){

    time_t rawtime;
    struct tm * timeinfo;
    time (&rawtime);
    timeinfo = localtime (&rawtime);

    string TimeFormat = "%d %b %Y %H:%M:%S %Z";

    char Buffer[256];
    Buffer[0] = '[';
    strftime(&Buffer[1], sizeof(Buffer)-2, TimeFormat.c_str(), timeinfo);
    string stime(Buffer);
    stime += "]";
    return stime;
}

class KinokoCanvasInterface{

    public:
	KinokoCanvasInterface(const string &hostname, int portno);
	virtual ~KinokoCanvasInterface();
	void WriteCommand(const std::string &com);
	static std::string CreatePlot( const string  &pname, int xofst, int yofst, int width, int height );
	static std::string PlotSetFrame( const string  &pname, float xmin, float xmax, float ymin, float ymax,
	       const string &xtitle, const string &ytitle, const string &title, int linewidth);

	template<class T>
	    static std::string PlotHistCommand( const string  &pname, float xmin, float xbin_width, vector<T> &hist, int offset = 0 );
	template<class T>
	    static std::string PlotPlotCommand( const string  &pname, vector<T> &x,  vector<T> &y, vector<T> &ex, vector<T> &ey, int offset = 0 );

    protected:
    string _hostname;
    int _portno;
    int _sockfd;
    struct sockaddr_in _serv_addr;
    struct hostent *_server;
    pid_t _pid;

};

KinokoCanvasInterface::KinokoCanvasInterface(const string &hostname, int portno): _hostname(hostname), _portno(portno)
{
    char command[256];
    sprintf( command, "kinoko-canvas %s %d", _hostname.c_str(), _portno);

    _sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (_sockfd < 0) 
	cerr << "ERROR opening socket" << endl;
    _server = gethostbyname(hostname.c_str());
    if (_server == NULL) {
	fprintf(stderr,"ERROR, no such host\n");
	exit(0);
    }
    bzero((char *) &_serv_addr, sizeof(_serv_addr));
    _serv_addr.sin_family = AF_INET;
    bcopy((char *)_server->h_addr, 
	    (char *)&_serv_addr.sin_addr.s_addr,
	    _server->h_length);
    _serv_addr.sin_port = htons(_portno);
    if (connect(_sockfd,(struct sockaddr *) &_serv_addr,sizeof(_serv_addr)) < 0){ 
	cerr << "ERROR connecting" << endl;
	exit(0);
    }
}
void KinokoCanvasInterface::WriteCommand(const std::string &com){

    write(_sockfd, com.c_str(), strlen(com.c_str()) );
}
std::string KinokoCanvasInterface::CreatePlot( const string  &pname, int xofst, int yofst, int width, int height )
{
    ostringstream oss;
    oss << ".create plot "
	<< pname << " " 
	<< xofst << " "
	<< yofst << " "
	<< width  << " "
	<< height << ";"; 
    return oss.str();
}
std::string KinokoCanvasInterface::PlotSetFrame( const string  &pname, float xmin, float xmax, float ymin, float ymax,
       const string &xtitle, const string &ytitle, const string &title, int linewidth )
{
    ostringstream oss;
    oss << pname <<  " frame " << xmin << " " << xmax << " " << ymin << " " << ymax << ";";
    oss << pname <<  " set xtitle " << xtitle << ";"; 
    oss << pname <<  " set ytitle " << ytitle << ";";
    oss << pname <<  " set title "  << title << ";"; 
    oss << pname <<  " set linewidth " << linewidth << ";"; 
    return oss.str();
}
template <class T>
std::string KinokoCanvasInterface::PlotHistCommand( const string  &pname, float xmin, float xbin_width, vector<T> &hist, int offset )
{
    ostringstream oss;
    oss << pname << " hist " << xmin << " " << xbin_width << " ";
    for(unsigned i=offset; i < hist.size() + offset; i++){
	oss << hist.at(i%hist.size()) << " ";
    }
    oss << ";";
    return oss.str();
}
template <class T>
std::string KinokoCanvasInterface::PlotPlotCommand( const string  &pname, vector<T> &x,  vector<T> &y, vector<T> &ex, vector<T> &ey, int offset )
{
    ostringstream oss;
    oss << pname << " errorplot ";
    for(unsigned i=0; i < y.size(); i++){
	oss << x.at(i) << " " << y.at((i+offset)%y.size()) << " ";
	oss << ex.at(i) << " " << ey.at((i+offset)%y.size()) << " ";
    }
    oss << ";";
    return oss.str();
}

KinokoCanvasInterface::~KinokoCanvasInterface(){

    close(_sockfd);
}

class DaqControllerException : public std::exception{
    public:
	DaqControllerException()throw(){}
	DaqControllerException( const std::string &estr )throw(){err_str = estr; }
	DaqControllerException(const DaqControllerException &e)throw(){ err_str = e.what(); }
	DaqControllerException& operator= (const DaqControllerException &e) throw(){ err_str = e.what(); return *this; }
	virtual ~DaqControllerException()throw(){}
	virtual const char * what() const throw(){ return err_str.c_str(); }
    private:
	std::string err_str;
};

class DaqController{

    public:
	enum CBC_FLAG_IDS { CBC_LAT_ERR, CBC_BOF_ERR, CBC_TRG_ERR, CBC_SOF_ERR, CBC_OR254, N_CBC_FLAGS };
	static const int EVENT_RATE_BUF_SIZE;
	static const int MAX_CBC_NHITS;
	static const string RED;
	static const string YELLOW;
	static const string GREEN;
	static const string BLUE;
	static const string NC;

    public:
	DaqController(Cbc3BeFc7 *cbc3be, int data_buffer_size, const std::string &rawfname = "data.raw", KinokoCanvasInterface *kcif=0);
	virtual ~DaqController(){}
	void SetDebug(bool debug = true){ _debug = debug; }
	void SetWaitDataSec(int w_data_sec){ _wait_data_sec = w_data_sec; }
	void SetWaitDataUsecEn(bool w_data_usec_en) { _wait_data_usec_en = w_data_usec_en; } 
	void SetFastSginalGenerator(bool fsg_en = true){ _fsg_en = fsg_en; }
	void SetRunEndFlag(bool run_end = true){ _run_end = run_end; }
	void SetWriteTestFlag(bool write_test = true){ _write_test = write_test; }
	void SetMaxEventCount(unsigned long n){ _max_event_count = n; }
	int  GetErrorFlag(){ return _error_flag; }
	void SetHistoPrescale(int pres ){ _histo_prescale = pres; }

	void StartRun();
	void EndRun();
	void InitializeBeHistograms();
	void InitializeCbcHistograms(int cbc_id);
	void CreateCbcPlotsOnKinokoCanvas(int cbc_id);
	void CreateBePlotsOnKinokoCanvas();
	void WriteTrendHistsToKinokoCanvas();
	void WriteCbcHistsToKinokoCanvas();

    protected:
	bool _run_end;
	int _ievt;
	int _rawdata_fd;
	const uint32_t *_evt_buf;
	ssize_t   _evt_size;
	unsigned long _max_event_count;
	bool _debug;
	bool _write_test;
	bool _fsg_en;
	Cbc3BeFc7 *_cbc3be;
	int _data_buffer_size;
	KinokoCanvasInterface *_kcif;
	int _error_flag;
	string _pname_ua9_trig_rate;
	string _ptitle_ua9_trig_rate;
	string _pname_event_rate;
	string _ptitle_event_rate;
	string _pname_tdc;
	string _ptitle_tdc;
	vector<float> _ua9_trig_rate;
	vector<float> _ua9_trig_rate_t;
	vector<float> _e_ua9_trig_rate;
	vector<float> _e_ua9_trig_rate_t;
	vector<float> _event_rate;
	vector<float> _event_rate_t;
	vector<float> _e_event_rate;
	vector<float> _e_event_rate_t;
	vector<int> _tdc;
	int _p_rate;
	int _p_ua9_trig_rate;
	int _max_event_rate;
	int _max_ua9_trig_rate;
	int _max_tdc;

	set<int> _cbc_ids;
	map<unsigned, string> _pname_cbc_flag;
	map<unsigned, string> _pname_cbc_ch_hits;
	map<unsigned, string> _pname_cbc_n_hits;
	map<unsigned, string> _pname_cbc_stub_addr;
	map<unsigned, string> _pname_cbc_stub_bend;
	map<unsigned, string> _ptitle_cbc_flag;
	map<unsigned, string> _ptitle_cbc_ch_hits;
	map<unsigned, string> _ptitle_cbc_n_hits;
	map<unsigned, string> _ptitle_cbc_stub_addr;
	map<unsigned, string> _ptitle_cbc_stub_bend;
	map<unsigned, vector<int> > _cbc_flag;
	map<unsigned, vector<int> > _cbc_ch_hits;
	map<unsigned, vector<int> > _cbc_n_hits;
	map<unsigned, vector<int> > _cbc_stub_addr;
	map<unsigned, vector<int> > _cbc_stub_bend;
	int _max_cbc_flag;
	int _max_cbc_ch_hits;
	int _max_cbc_n_hits;
	int _max_cbc_stub_addr;
	int _max_cbc_stub_bend;
	string _cbc_img_fname;
	int _histo_prescale;
	int _wait_data_sec;//second when _wait_data_usec_en is 0, usec when _wait_data_usec_en is 1
	bool _wait_data_usec_en;
};

const int DaqController::EVENT_RATE_BUF_SIZE = 90;
const int DaqController::MAX_CBC_NHITS = 10;
const string DaqController::RED="\033[0;31m";
const string DaqController::YELLOW="\033[1;33m";
const string DaqController::GREEN="\033[0;32m";
const string DaqController::BLUE="\033[0;34m";
const string DaqController::NC="\033[0m";

DaqController::DaqController(Cbc3BeFc7 *cbc3be, int data_buffer_size, const std::string &rawfname, KinokoCanvasInterface *kcif):
    _run_end(false), _ievt(0), _rawdata_fd(-1),_max_event_count(0), _debug(false),_write_test(false),_fsg_en(false), _cbc3be(cbc3be),
    _data_buffer_size(data_buffer_size), _kcif(kcif), _error_flag(0), _p_ua9_trig_rate(0), _p_rate(0),_max_ua9_trig_rate(1),_max_event_rate(1),_max_tdc(0), _max_cbc_ch_hits(0), _max_cbc_stub_addr(0), _max_cbc_stub_bend(0),
	_histo_prescale(1000){

    _rawdata_fd = open( rawfname.c_str(), O_WRONLY | O_CREAT, 0444);
    if(_rawdata_fd == -1){
	perror("open");
	throw DaqControllerException( rawfname + " could not be opened." );
    }
    _cbc_img_fname = rstr( rawfname, ".raw", "-cbc.eps" );  
}

void DaqController::EndRun(){

    cout << GREEN <<time_string() << " RUNEND. TOTAL NUMBER OF EVENTS IS TAKEN " << _ievt << NC << endl;

    if( _rawdata_fd != -1 ){
	close (_rawdata_fd);
    }
}

void DaqController::StartRun(){

    ostringstream oss;

    _cbc3be->InitializeRun();
    if(_fsg_en) _cbc3be->StartFastSignalGenerator();

    uint32_t *data_buffer;
    data_buffer = new uint32_t[_data_buffer_size];
    BeDataDecoder be_data_decoder;

    struct timeval t0_trend0, t_trend0, dt_trend0;
    struct timeval t0_trend, t_trend, dt_trend;
    struct timeval t0_rd, t_rd, dt_rd, Dt_rd,Dt_rd_last;
    gettimeofday(&t0_trend0,0);
    gettimeofday(&t0_trend,0);

    int NB(0);
    float rd_write_rate(0);

    int total_nread(0);
    int last_ievt(0);
    int ua9_last_time(0);
    int ua9_time(0);
    int ua9_count(0);
    int ua9_last_count(0);

    InitializeBeHistograms();
    if(_kcif) CreateBePlotsOnKinokoCanvas();
    while( true ){

	    gettimeofday(&t_trend0,0);
	    timersub(&t_trend0,&t0_trend0,&dt_trend0);
	    if( dt_trend0.tv_sec > 5 ){
		/*
		    unsigned ua9_time = _cbc3be->ReadBeBoardRegister("ua9_tdc.time");
		    ua9_time = _cbc3be->ReadBeBoardRegister("ua9_tdc.time");
		    unsigned ua9_count = _cbc3be->ReadBeBoardRegister("ua9_tdc.ua9_count");
		    unsigned total_count = _cbc3be->ReadBeBoardRegister("ua9_tdc.total_count");
		    cout << GREEN << time_string() << "[UA9 TIME] : " << ua9_time << " [UA9 COUNT] : " << ua9_count << " [UA9 TOTAL COUNT] : " << total_count << endl;
		    */
		    t0_trend0 = t_trend0;
	    }
	    cout << "wait for data" << endl;
	if( _cbc3be->WaitData( _wait_data_sec, _wait_data_usec_en ) ){

	    cout << "data is here" << endl;

	    total_nread++;

	    int nwords = _cbc3be->ReadData( data_buffer );
	    be_data_decoder.SetData( data_buffer, nwords * 4 );

	    gettimeofday(&t0_rd,0);
	    write (_rawdata_fd, data_buffer, nwords * 4 );
	    gettimeofday(&t_rd,0);
	    timersub(&t_rd,&t0_rd,&dt_rd);
	    //timeradd(&dt_rd,&Dt_rd_last, &Dt_rd);
	    NB = nwords * 4;
	    Dt_rd = dt_rd;

	    if(!_write_test){ 
		int this_evti(0);
		while( be_data_decoder.SetNextEvent() ){ 

		    if(_kcif && _ievt==0){
			_kcif->WriteCommand(".set redraw manual;.createPage Other;.selectPage 0; .set pagetitle CBC;");
		    }

		    string evt_time = time_string();

		    _ievt++;
		    this_evti++;

		    int tdc = be_data_decoder.TDC(); 
		    _tdc.at(tdc)++;
		    if(_max_tdc < _tdc.at(tdc) ) _max_tdc = _tdc.at(tdc); 
			
		    //		int be_l1c = be_data_decoder.BeL1ACounter();
		    /*
		     * analysis on TDC
		     */

		    vector<int> pa, cntr;

		    //if( !(_histo_prescale == 0 || _ievt % _histo_prescale == 0) ) continue;

		    while( be_data_decoder.SetNextCbc() ){

			const CbcDataPacketDecoder *cbc_data_packet_decoder = be_data_decoder.GetCbcDataPacketDecoder();
			if(_debug)cbc_data_packet_decoder->DumpData(cout);

			int cbc_id = cbc_data_packet_decoder->Id(); 

			//Initializing the histograms and canvas at the first event for the CBC.
			if(_cbc_ids.find(cbc_id)==_cbc_ids.end()){
			    _cbc_ids.insert(cbc_id);
			    InitializeCbcHistograms(cbc_id);
			    if(_kcif){
				CreateCbcPlotsOnKinokoCanvas(cbc_id);
			    }
			}

			/*
			 * Error log
			 */
			if( cbc_data_packet_decoder->Error(0) ){
			    _cbc_flag[cbc_id][CBC_LAT_ERR]++;
			    if(_max_cbc_flag < _cbc_flag[cbc_id][CBC_LAT_ERR]) _max_cbc_flag = _cbc_flag[cbc_id][CBC_LAT_ERR];
			    if(_cbc_flag[cbc_id][CBC_LAT_ERR]<10) cout << YELLOW << evt_time << " Latency error found at CBC ID = " << cbc_id << NC << endl;
			    _error_flag = true;
			}	
			if( cbc_data_packet_decoder->Error(1) ){
			    _cbc_flag[cbc_id][CBC_BOF_ERR]++;
			    if(_max_cbc_flag < _cbc_flag[cbc_id][CBC_BOF_ERR]) _max_cbc_flag = _cbc_flag[cbc_id][CBC_BOF_ERR];
			    if(_cbc_flag[cbc_id][CBC_BOF_ERR]<10) cout << YELLOW << evt_time << " Buffer overflow error found at CBC ID = " << cbc_id << NC << endl;
			    _error_flag = true;
			}
			bool trig_err = cbc_data_packet_decoder->TrigErr();
			if( trig_err ){
			    _cbc_flag[cbc_id][CBC_TRG_ERR]++;
			    if(_max_cbc_flag < _cbc_flag[cbc_id][CBC_TRG_ERR]) _max_cbc_flag = _cbc_flag[cbc_id][CBC_TRG_ERR];
			    if(_cbc_flag[cbc_id][CBC_TRG_ERR]<10) cout << YELLOW << evt_time << " Trigger Error is reported  at CB ID = " << cbc_id << NC << endl;
			    _error_flag = true;
			}
			bool sof = cbc_data_packet_decoder->TrigSof();
			if( sof ){
			    _cbc_flag[cbc_id][CBC_SOF_ERR]++;
			    if(_max_cbc_flag < _cbc_flag[cbc_id][CBC_SOF_ERR]) _max_cbc_flag = _cbc_flag[cbc_id][CBC_SOF_ERR];
			    if(_cbc_flag[cbc_id][CBC_SOF_ERR]<10) cout << YELLOW << evt_time << " Stub overflow is reported  at CB ID = " << cbc_id << NC << endl;
			    _error_flag = true;
			}
			bool or254 = cbc_data_packet_decoder->TrigOr254();
			if( or254 ){
			    _cbc_flag[cbc_id][CBC_OR254]++;
			    if(_max_cbc_flag < _cbc_flag[cbc_id][CBC_OR254]) _max_cbc_flag = _cbc_flag[cbc_id][CBC_OR254];
			}	
			cout << "finished or254" << endl;

			int nhits = cbc_data_packet_decoder->HitChannels().size();
			cout << "I am here" << endl;
			if(nhits> MAX_CBC_NHITS) nhits = MAX_CBC_NHITS; 
			cout << "I am here" << endl;
			_cbc_n_hits[cbc_id][nhits]++;
			cout << "I am here" << endl;
			if(_max_cbc_n_hits < _cbc_n_hits[cbc_id][nhits]) _max_cbc_n_hits = _cbc_n_hits[cbc_id][nhits];
			//
			cout << "the cbc " << cbc_id << " filling the histograms" << endl;
			//		    filling the histograms
			std::vector<unsigned> hits = cbc_data_packet_decoder->HitChannels();
			for(unsigned hit_i=0; hit_i < hits.size(); hit_i++){
			    _cbc_ch_hits[cbc_id][hits.at(hit_i)]++;
			    if(_max_cbc_ch_hits < _cbc_ch_hits[cbc_id][hits.at(hit_i)] ) _max_cbc_ch_hits = _cbc_ch_hits[cbc_id][hits.at(hit_i)];
			}	

			vector<unsigned> sps = cbc_data_packet_decoder->StubPositions();
			vector<unsigned> sbs = cbc_data_packet_decoder->StubBends();
			for(unsigned s_i=0; s_i < sps.size(); s_i++){
			    if(sps.at(s_i) == 0) continue;
			    _cbc_stub_addr[cbc_id][sps.at(s_i)]++;
			    if(_max_cbc_stub_addr < _cbc_stub_addr[cbc_id][sps.at(s_i)] ) _max_cbc_stub_addr = _cbc_stub_addr[cbc_id][sps.at(s_i)];
			    _cbc_stub_addr[cbc_id][sps.at(s_i)]++;
			    if(_max_cbc_stub_bend < _cbc_stub_bend[cbc_id][sbs.at(s_i)] ) _max_cbc_stub_bend = _cbc_stub_bend[cbc_id][sbs.at(s_i)];
			    _cbc_stub_bend[cbc_id][sbs.at(s_i)]++;
			}
			cout << "the cbc " << cbc_id << " finished" << endl;
		    }
		}
	    }
	    gettimeofday(&t_trend,0);
	    timersub(&t_trend,&t0_trend,&dt_trend);
	    if( dt_trend.tv_sec > 2 ){

		    //The last data writing rate
		    /*
		    rd_write_rate = NB / (Dt_rd.tv_sec+Dt_rd.tv_usec * 0.000001);
		    float e = sqrt(NB) / (Dt_rd.tv_sec+Dt_rd.tv_usec * 0.000001);
		    cout << GREEN << time_string() << " RAW DATA WRITE RATE = " << rd_write_rate << "+/-" << e << " B/sec" << endl; 
			*/

		    //event rate
		    int diff = _ievt - last_ievt;
		    float rate = (float) diff / (dt_trend.tv_sec + dt_trend.tv_usec * 0.000001 );
		    if( _event_rate[_p_rate] > _max_event_rate ){ 
			    _max_event_rate = 0;
			    for(unsigned r_i=0; r_i < _event_rate.size(); r_i++){
				    if(r_i == (unsigned)_p_rate) continue;
				    _max_event_rate = (_event_rate.at(r_i) > _max_event_rate) ? _event_rate.at(r_i) : _max_event_rate;
			    }
		    }
		    _event_rate[_p_rate++] = rate; 
		    _max_event_rate = (_event_rate.at(_p_rate-1) > _max_event_rate) ? _event_rate.at(_p_rate-1) : _max_event_rate;
		    _p_rate = _p_rate % EVENT_RATE_BUF_SIZE;
		    //ua9 trigger rate
		    /*
		    ua9_time = _cbc3be->ReadBeBoardRegister("ua9_tdc.time");
		    ua9_time = _cbc3be->ReadBeBoardRegister("ua9_tdc.time");
		    int ua9_count = _cbc3be->ReadBeBoardRegister("ua9_tdc.ua9_count");
		    float ua9_rate = (float) (ua9_count - ua9_last_count) / (ua9_time - ua9_last_time ) / 25 * 1000000000; 	
		    ua9_last_count = ua9_count;
		    ua9_last_time = ua9_time;
		    if( _ua9_trig_rate[_p_ua9_trig_rate] > _max_ua9_trig_rate ){ 
			    _max_ua9_trig_rate = 0;
			    for(unsigned r_i=0; r_i < _ua9_trig_rate.size(); r_i++){
				    if(r_i == (unsigned)_p_ua9_trig_rate) continue;
				    _max_ua9_trig_rate = (_ua9_trig_rate.at(r_i) > _max_ua9_trig_rate) ? _ua9_trig_rate.at(r_i) : _max_ua9_trig_rate;
			    }
		    }
		    _ua9_trig_rate[_p_ua9_trig_rate++] = ua9_rate; 
		    _max_ua9_trig_rate = (_ua9_trig_rate.at(_p_ua9_trig_rate-1) > _max_ua9_trig_rate) ? _ua9_trig_rate.at(_p_ua9_trig_rate-1) : _max_ua9_trig_rate;
		    _p_ua9_trig_rate = _p_ua9_trig_rate % EVENT_RATE_BUF_SIZE;
		    */

		last_ievt = _ievt;
		t0_trend = t_trend;
		NB = 0;

		if(_kcif){
		    WriteTrendHistsToKinokoCanvas();
		    WriteCbcHistsToKinokoCanvas();
		    _kcif->WriteCommand( ".selectPage 0;.redraw;.clear;" );
		}
	    }
	}
	cout << "end of the loop" << endl;
	if( _max_event_count != 0 && _ievt > _max_event_count ) _run_end = true;
	if(_run_end){
	    if(_kcif){
		cout << GREEN << time_string() << " WRITING THE CANVAS IMAGE TO " << _cbc_img_fname << NC << endl;
		WriteTrendHistsToKinokoCanvas();
		WriteCbcHistsToKinokoCanvas();
		_kcif->WriteCommand( ".redraw;" );
		_kcif->WriteCommand( string(".saveImage ") + _cbc_img_fname + ";");
		while(true){
		    sleep(1);
		    //		system("ps -u kirika | grep kinoko");
		    string com("test -f ");
		    com += _cbc_img_fname;
		    if( system(com.c_str()) == 0 ){
			system("ls");
			break;
		    }
		}
		_kcif->WriteCommand( ".quit;");
	    }
	    break;
	}
    }
}
void DaqController::InitializeBeHistograms()
{
	for(unsigned t_i=0; t_i < 256; t_i++){
		_tdc.push_back(0);
	}
	for(unsigned e_i=0; e_i < (unsigned)EVENT_RATE_BUF_SIZE; e_i++){
	_event_rate.push_back(0);
	int time_i = (int)e_i-(int)EVENT_RATE_BUF_SIZE+1;
	_event_rate_t.push_back(time_i*2.);
	_e_event_rate.push_back(0);
	_e_event_rate_t.push_back(0);

	_ua9_trig_rate.push_back(0);
	_ua9_trig_rate_t.push_back(time_i*2.);
	_e_ua9_trig_rate.push_back(0);
	_e_ua9_trig_rate_t.push_back(0);
    }
}
void DaqController::InitializeCbcHistograms(int cbc_id)
{
    for(unsigned f_i=0; f_i < N_CBC_FLAGS; f_i++){
	_cbc_flag[cbc_id].push_back(0);
    }
    for(unsigned h_i=0; h_i <= MAX_CBC_NHITS; h_i++){
	_cbc_n_hits[cbc_id].push_back(0);
    }
    for(unsigned s_i=0; s_i <= 15; s_i++){
	_cbc_stub_bend[cbc_id].push_back(0);
    }
    for(unsigned ch_i=0; ch_i <= 255; ch_i++){
	_cbc_ch_hits[cbc_id].push_back(0);
    }
    for(unsigned s_i=0; s_i <= 255; s_i++){
	_cbc_stub_addr[cbc_id].push_back(0);
    }
}
void DaqController::CreateBePlotsOnKinokoCanvas()
{
    ostringstream oss;
    oss.str() = ".selectPage 0;";

    int xofst(0);
    int yofst(0);
    int width(0);
    int height(20);

    _pname_ua9_trig_rate = "p_ua9_trig_rate";
    _ptitle_ua9_trig_rate = "UA9 trigger rate over the last 2 sec";
    xofst=0;
    width  = 35;
    oss << KinokoCanvasInterface::CreatePlot(_pname_ua9_trig_rate, xofst, yofst, width, height);
    oss << KinokoCanvasInterface::PlotSetFrame(_pname_ua9_trig_rate, -EVENT_RATE_BUF_SIZE*2, 10, 0, 1.2, 
	    "time from current [sec]", "Hz", _ptitle_ua9_trig_rate, 2); 

    _pname_event_rate = "p_event_rate";
    _ptitle_event_rate = "Event rate over the last 2 sec";
    xofst = 35;
    width  = 35;
    oss << KinokoCanvasInterface::CreatePlot(_pname_event_rate, xofst, yofst, width, height);
    oss << KinokoCanvasInterface::PlotSetFrame(_pname_event_rate, -EVENT_RATE_BUF_SIZE*2, 10, 0, 1.2, 
	    "time from current [sec]", "Hz", _ptitle_event_rate, 2); 

    _pname_tdc = "p_tdc";
    _ptitle_tdc = "TDC";
    xofst = 70;
    width = 30;
    oss << KinokoCanvasInterface::CreatePlot(_pname_tdc, xofst, yofst, width, height);
    oss << KinokoCanvasInterface::PlotSetFrame(_pname_tdc, -0.5, 255.5, 0, 1.2, 
	    "TDC count", "N entry", _ptitle_tdc, 2); 
    _kcif->WriteCommand( oss.str() );
}
void DaqController::CreateCbcPlotsOnKinokoCanvas(int cbc_id)
{
    ostringstream oss;

    ostringstream tmp;
    tmp << "pcbc" << cbc_id << "_flag"; 
    _pname_cbc_flag[cbc_id] = tmp.str();
    tmp.str("");
    tmp <<  "Flag stats. on CBC";
    tmp << cbc_id;
    _ptitle_cbc_flag[cbc_id] = tmp.str();

    tmp.str("");
    tmp << "pcbc";
    tmp << cbc_id << "_n_hits"; 
    _pname_cbc_n_hits[cbc_id] = tmp.str();
    tmp.str("");
    tmp <<  "Number of hits on CBC";
    tmp << cbc_id;
    _ptitle_cbc_n_hits[cbc_id] = tmp.str();

    tmp.str("");
    tmp <<  "pcbc";
    tmp << cbc_id << "_stub_bend"; 
    _pname_cbc_stub_bend[cbc_id] = tmp.str();
    tmp.str("");
    tmp << "Stub bend on CBC";
    tmp << cbc_id;
    _ptitle_cbc_stub_bend[cbc_id] = tmp.str();

    tmp.str("");
    tmp << "pcbc";
    tmp << cbc_id << "_ch_hit"; 
    _pname_cbc_ch_hits[cbc_id] = tmp.str();
    tmp.str("");
    tmp << "Channel hits on CBC";
    tmp << cbc_id;
    _ptitle_cbc_ch_hits[cbc_id] = tmp.str();

    tmp.str("");
    tmp << "pcbc";
    tmp << cbc_id << "_stub_addr"; 
    _pname_cbc_stub_addr[cbc_id] = tmp.str();
    tmp.str("");
    tmp << "Stub address on CBC";
    tmp << cbc_id; 
    _ptitle_cbc_stub_addr[cbc_id] = tmp.str();

    oss.str("");
    oss << ".selectPage 0;";
    int xofst(50);
    int yofst(20);
    int width(0);
    int height(0);
    if(cbc_id == 2) xofst = 0;
    //flag 
    width  = 15;
    height = 20;
    oss << KinokoCanvasInterface::CreatePlot(_pname_cbc_flag[cbc_id], xofst, yofst, width, height);
    oss << KinokoCanvasInterface::PlotSetFrame(_pname_cbc_flag[cbc_id], 0, N_CBC_FLAGS, 0, 1.2, 
	    "FLAGS", "N entries", _ptitle_cbc_flag[cbc_id], 2); 
    //n hits
    xofst = xofst + width;
    width  = 15;
    oss << KinokoCanvasInterface::CreatePlot(_pname_cbc_n_hits[cbc_id], xofst, yofst, width, height);
    oss << KinokoCanvasInterface::PlotSetFrame(_pname_cbc_n_hits[cbc_id], 0, MAX_CBC_NHITS+1, 0, 1.2, 
	    "Number of hits", "N entries", _ptitle_cbc_n_hits[cbc_id], 2); 
    //stub bend 
    xofst = xofst + width;
    width  = 20;
    oss << KinokoCanvasInterface::CreatePlot(_pname_cbc_stub_bend[cbc_id], xofst, yofst, width, height);
    oss << KinokoCanvasInterface::PlotSetFrame(_pname_cbc_stub_bend[cbc_id], -0.5, 15.5, 0, 1.2, 
	    "Stub bend", "N entries", _ptitle_cbc_stub_bend[cbc_id], 2); 
    //channel hits
    xofst = 50;
    if(cbc_id == 2) xofst = 0;
    yofst = yofst + height;
    width = 50;
    height = 30;
    oss << KinokoCanvasInterface::CreatePlot(_pname_cbc_ch_hits[cbc_id], xofst, yofst, width, height);
    oss << KinokoCanvasInterface::PlotSetFrame(_pname_cbc_ch_hits[cbc_id], -0.5, 255.5, 0, 1.2, 
	    "Channel", "N entries", _ptitle_cbc_ch_hits[cbc_id], 2); 
    //stub address
    yofst = yofst + height;
    width = 50;
    height = 30;
    oss << KinokoCanvasInterface::CreatePlot(_pname_cbc_stub_addr[cbc_id], xofst, yofst, width, height);
    oss << KinokoCanvasInterface::PlotSetFrame(_pname_cbc_stub_addr[cbc_id], -0.5, 255.5, 0, 1.2, 
	    "Stub address", "N entries", _ptitle_cbc_stub_addr[cbc_id], 2); 

    _kcif->WriteCommand( oss.str() );
}
void DaqController::WriteTrendHistsToKinokoCanvas()
{
    ostringstream oss;
    oss.str() = ".selectPage 0;";

    oss << KinokoCanvasInterface::PlotSetFrame(_pname_event_rate, -EVENT_RATE_BUF_SIZE*2, 10, 0, _max_event_rate * 1.2, 
	    "time from current", "Hz", _ptitle_event_rate, 2); 
    oss << KinokoCanvasInterface::PlotPlotCommand<float>(_pname_event_rate, _event_rate_t, _event_rate, _e_event_rate_t, _e_event_rate, _p_rate); 

    oss << KinokoCanvasInterface::PlotSetFrame(_pname_ua9_trig_rate, -EVENT_RATE_BUF_SIZE*2, 10, 0, _max_ua9_trig_rate * 1.2, 
	    "time from current", "Hz", _ptitle_ua9_trig_rate, 2); 
    oss << KinokoCanvasInterface::PlotPlotCommand<float>(_pname_ua9_trig_rate, _ua9_trig_rate_t, _ua9_trig_rate, _e_ua9_trig_rate_t, _e_ua9_trig_rate, _p_ua9_trig_rate); 

    oss << KinokoCanvasInterface::PlotSetFrame(_pname_tdc, -0.5, 255.5, 0, _max_tdc * 1.2, 
		"TDC count", "N entries", _ptitle_tdc, 2); 
    oss << KinokoCanvasInterface::PlotHistCommand<int>(_pname_tdc, 0, 1, _tdc); 
    //cout << oss.str() << endl;
    _kcif->WriteCommand( oss.str() );
}
void DaqController::WriteCbcHistsToKinokoCanvas()
{
    ostringstream oss;
    oss.str() = ".selectPage 0;";

    set<int>::iterator it_cbc_ids = _cbc_ids.begin();
    for(; it_cbc_ids != _cbc_ids.end(); it_cbc_ids++){

	int cbc_id = *it_cbc_ids;

	oss << KinokoCanvasInterface::PlotSetFrame(_pname_cbc_flag[cbc_id], 0, N_CBC_FLAGS, 0, _max_cbc_flag * 1.2, 
		"FLAGS", "N entries", _ptitle_cbc_flag[cbc_id], 2); 
	oss << KinokoCanvasInterface::PlotHistCommand<int>(_pname_cbc_flag[cbc_id], 0, 1, _cbc_flag[cbc_id]); 

	oss << KinokoCanvasInterface::PlotSetFrame(_pname_cbc_n_hits[cbc_id], 0, MAX_CBC_NHITS+1, 0, _max_cbc_n_hits * 1.2, 
		"Number of hits", "N entries", _ptitle_cbc_n_hits[cbc_id], 2); 
	oss << KinokoCanvasInterface::PlotHistCommand<int>(_pname_cbc_n_hits[cbc_id], 0, 1, _cbc_n_hits[cbc_id]); 

	oss << KinokoCanvasInterface::PlotSetFrame(_pname_cbc_stub_bend[cbc_id], -0.5, 15.5, 0, _max_cbc_stub_bend * 1.2, 
		"Stub bend", "N entries", _ptitle_cbc_stub_bend[cbc_id], 2); 
	oss << KinokoCanvasInterface::PlotHistCommand<int>(_pname_cbc_stub_bend[cbc_id], -0.5, 1, _cbc_stub_bend[cbc_id]); 

	oss << KinokoCanvasInterface::PlotSetFrame(_pname_cbc_ch_hits[cbc_id], -0.5, 255.5, 0, _max_cbc_ch_hits * 1.2, 
		"Channel", "N entries", _ptitle_cbc_ch_hits[cbc_id], 2); 
	oss << KinokoCanvasInterface::PlotHistCommand<int>(_pname_cbc_ch_hits[cbc_id], -0.5, 1, _cbc_ch_hits[cbc_id]); 

	oss << KinokoCanvasInterface::PlotSetFrame(_pname_cbc_stub_addr[cbc_id], -0.5, 255.5, 0, _max_cbc_stub_addr * 1.2, 
		"Stub address", "N entries", _ptitle_cbc_stub_addr[cbc_id], 2); 
	oss << KinokoCanvasInterface::PlotHistCommand<int>(_pname_cbc_stub_addr[cbc_id], -0.5, 1, _cbc_stub_addr[cbc_id]); 

    }
    //	oss << _pname.str() << "setfont default 10;"; 
    _kcif->WriteCommand( oss.str() );
}

void sig_handler(int signo)
{
    if(signo == SIGINT){
	/*
	close_files();
	*/
	if(daqc) daqc->SetRunEndFlag(true);
    }
    if(signo == SIGTERM){
	//close_files();
	if(daqc) daqc->SetRunEndFlag(true);
    }
    if(signo == SIGSEGV){
	//close_files();
	if(daqc) daqc->SetRunEndFlag(true);
    }
}


void printHelp( char *argv[] )
{
    unsigned indent(50);
    cout << setfill(' ');
    cout << "usage: " << argv[0] << "\n" 
	<< setw(indent) << " --help                       "        << ": prints this help.\n" 
	<< setw(indent) << " --loadSD                     "        << ": load user image on SD card.\n" 
	<< setw(indent) << " --conFile  [connection file] "        << ": connection file name to use.\n" 
	<< setw(indent) << " --boardId  [id]              "        << ": board id to use.\n" 
	<< setw(indent) << " --debug    [value]           "        << ": debug value for Cbc3BeInterface.\n" 
	<< setw(indent) << " --fsg_en                     "        << ": fast signal generator enabled.\n" 
	<< setw(indent) << " --kc_en                      "        << ": kinoko-canvas is enabled.\n" 
	<< "environment variables and the values if the environment variables are not set:\n"
	<< setw(indent) << "[connection file] : IPB_CONNECTION_FILE, " << connection_file << "\n"
	<< setw(indent) << "[id]              : IPB_BOARD_ID,        " << id << "\n"
	<< endl;

}

int main( int argc, char *argv[] )
{
    signal(SIGINT, sig_handler); 
    signal(SIGTERM, sig_handler); 
    signal(SIGSEGV, sig_handler); 

    setLogLevelTo( uhal::Error() );
    //getting environment variables
    char *env_value;
    env_value = getenv( "IPB_CONNECTION_FILE" );
    if( env_value != NULL ) connection_file = env_value; 
    env_value = getenv( "IPB_BOARD_ID" );
    if( env_value != NULL ) id = env_value;
    env_value = getenv( "CBC3HAL_ROOT" );
    string cbc3hal_dir( env_value );

    int debug(0);
    bool write_test(false);
    int min_nword(0);
    bool fsg_en(false);
    bool kc_en(false);
    string kinoko_arg;
    int data_buffer_size(131072); //131Kx4 bytes.  524KB
    string rawdatafname("data.raw");
    int polint_usec(0);
    int histo_prescale(-1);
    int wait_time(1);
    int wait_usec_en(1);
    unsigned long max_event_count(0);

    //getting options
    int c;
    while(1)
    {
	static struct option long_options[] = { 
	    { "help"            , no_argument, 0, 'a' },
	    { "conFile"         , required_argument, 0, 'b' },
	    { "boardId"         , required_argument, 0, 'c' },
	    { "debug"           , required_argument, 0, 'd' },
	    { "min_nword"       , required_argument, 0, 'e' },
	    { "fsg_en"          , no_argument,       0, 'f' },
	    { "kc_en"           , required_argument, 0, 'g' },
	    { "data_buf_size"   , required_argument, 0, 'h' },
	    { "data_file"       , required_argument, 0, 'i' },
	    { "write_test"      , no_argument, 0, 'j' },
	    { "polint_usec"     , required_argument, 0, 'k' },
	    { "histo_prescale"  , required_argument, 0, 'l' },
	    { "wait_time"       , required_argument, 0, 'm' },
	    { "wait_usec_en"    , required_argument, 0, 'n' },
	    { "max_event_count" , required_argument, 0, 'o' }
	};
	int option_index = 0;
	c = getopt_long( argc, argv, "ab:c:d:e:fg:h:i:jk:l:m:n:o:", long_options, &option_index );
	if( c == -1 ) {
	    break;
	}

	switch( c )
	{
	    case 'a':
		printHelp( argv );
		return 0;
	    case 'b':
		connection_file = optarg;
		break;
	    case 'c':
		id = optarg;
		break;
	    case 'd':
		debug = strtol(optarg, 0, 0);
		break;
	    case 'e':
		min_nword = strtol(optarg, 0, 0);
		break;
	    case 'f':
		fsg_en = true;
		break;
	    case 'g':
		kc_en = true;
		kinoko_arg = optarg;
		break;
	    case 'h':
		data_buffer_size = strtol(optarg,0,0);
		break;
	    case 'i':
		rawdatafname = optarg;
		break;
	    case 'j':
		write_test = true;
		break;
	    case 'k':
		polint_usec = strtol(optarg,0,0);
		break;
	    case 'l':
		histo_prescale = strtol(optarg,0,0);
		break;
	    case 'm':
		wait_time = strtol(optarg, 0, 0);		
		break;
	    case 'n':
		wait_usec_en = strtol(optarg, 0, 0);		
		break;
	    case 'o':
		max_event_count = strtoul(optarg, 0, 0);
		break;
	    default:
		abort();
	}
    }
    string com("test -f ");
    com += rawdatafname;
    if( system( com.c_str() ) == 0 ){
	cerr << "The file " << rawdatafname << " exists." << endl;
	return -1;
    } 
/*
 * KinokoCanvas initialization
 */
    KinokoCanvasInterface *kcif(0);
    if(kc_en){
	kinoko_arg = rstr( kinoko_arg, ":", " " );
	istringstream iss_arg(kinoko_arg); 
	string hostname;
	int portno;
	iss_arg >> hostname;
	iss_arg >> portno;
	kcif = new KinokoCanvasInterface(hostname, portno);
    }

    try{
	/*
	 * Cbc3BeInterface initialization
	 */
//	HwInterface *hw(0);
	Cbc3BeFc7 *cbc3be(0);

	cbc3be = new Cbc3BeFc7( debug );
	cbc3be->Initialize( connection_file, id );
//	hw = cbc3be->GetHwInterface(); 
	cbc3be->MinNumWordToWait(min_nword);
	cbc3be->DataReadyFlagPollingInterval_usec(polint_usec);
	/*
	 * DaqController initialization
	 */
	daqc = new DaqController( cbc3be, data_buffer_size, rawdatafname, kcif );
	if(debug)daqc->SetDebug(debug);
	if(write_test)daqc->SetWriteTestFlag();
	daqc->SetWaitDataSec(wait_time);
	daqc->SetWaitDataUsecEn(wait_usec_en);
	daqc->SetMaxEventCount(max_event_count);
    }
    catch( const std::exception& e ){
	cerr << e.what() << endl;
	return -1;
    }

/*
 * Data taking 
 */

    if(fsg_en) daqc->SetFastSginalGenerator();
    if(histo_prescale != -1 ){
	    daqc->SetHistoPrescale(histo_prescale);
    cout << DaqController::GREEN <<time_string() << " HISTOGRAM PRESCALE IS SET TO " << histo_prescale << endl;
    }
    daqc->StartRun();
    daqc->EndRun();
    /*
     * Saving the canvas
     */
    delete kcif;

    return daqc->GetErrorFlag(); 
}
