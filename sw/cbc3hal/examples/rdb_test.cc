#include <getopt.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <readline/readline.h>
#include <readline/history.h>
#include <cstdio>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <signal.h>
#include <uhal/uhal.hpp>
#include "ipbus_utils.h"
#include "CbcI2cRegIpbusInterface.h"

using namespace std;
using namespace uhal;
using namespace cbc3hal;
using namespace ipbutl;

#define N_MAX_HIT 16
HwInterface *hw(0);

/*
 * modify here
 */
std::string connection_file("../etc/connections.xml");
std::string id("FC7_SINGLE_CBC3_64_CH");

void channel_mask( std::map<int,int> &msks, std::vector<uint32_t> &seq_data ){ 

    std::map<int,int> regs;
    map<int,int>::iterator it_msks = msks.begin();
    for(; it_msks != msks.end(); it_msks++){ 
	int ch = it_msks->first;
	int msk = it_msks->second;
	if(ch <1 || ch >254) continue;
	int addr = 0x20 + (ch-1) / 8;
	int value = msk << ((ch-1) % 8);
	if( regs.find(addr) == regs.end() )
	    regs[addr] = value;
	else
	    regs[addr] |= value;
    }
    map<int,int>::iterator it_regs = regs.begin();
    for(;it_regs != regs.end(); it_regs++){
	int a = it_regs->first;
	int v = it_regs->second;
	unsigned value = 0x81100000 | (a << 8) | v;
	seq_data.push_back(value);
    }
}

class Cbc3TestException : public std::exception{
    public:
	Cbc3TestException()throw(){}
	Cbc3TestException( const std::string &estr )throw(){err_str = estr; }
	Cbc3TestException(const Cbc3TestException &e)throw(){ err_str = e.what(); }
	Cbc3TestException& operator= (const Cbc3TestException &e) throw(){ err_str = e.what(); return *this; }
	virtual ~Cbc3TestException()throw(){}
	virtual const char * what() const throw(){ return err_str.c_str(); }
    private:
	std::string err_str;
};

void WriteBeBoardConfig( const std::string &node, uint32_t value )
{
	write_ipbus( hw, node, value );
}

void WriteFscBram( const std::vector<uint32_t> &data )
{
    write_ipbus( hw, "cbc_system_ctrl.fscmc.reset", 1 );
    usleep(1);
    writeBlock_ipbus( hw, "fscbram", data ); 
}

void StartFscBramController(int nwords)
{
    WriteBeBoardConfig( "cbc_system_cnfg.rdb_ctrl.write_block_size", nwords/2 );

    struct timeval t0, t, dt;
    gettimeofday(&t0,0);
    int fsm = read_ipbus( hw, "cbc_system_stat.fscbc.fsm" );
    while( fsm != 2 ){  
	gettimeofday(&t,0);
	timersub(&t,&t0,&dt);
	if( dt.tv_sec > 1 ){
	    cerr << "fscbc is not ready.  Timeout with fsm = " << fsm << endl;
	    return;
	}
	usleep(1);
	fsm = read_ipbus( hw, "cbc_system_stat.fscbc.fsm" );
    }
    write_ipbus( hw, "cbc_system_ctrl.fscmc.start", 1 );
}
int ReadRawData( uint32_t*& Data, int nword, bool dcheck=1 )
{
    if( dcheck){
	struct timeval t0, t, dt;
	gettimeofday(&t0,0);
	bool read_ready = read_ipbus( hw, "cbc_system_stat.rdb_ctrl.read_ready" );
	while( !read_ready ){  
	    cout << "raw data is not ready yet!" << endl;
	    gettimeofday(&t,0);
	    timersub(&t,&t0,&dt);
	    if( dt.tv_sec > 1 ){
		cerr << "raw data buffer is not ready to read. Timeout." << endl;
		return -1;
	    }
	    usleep(1);
	    read_ready = read_ipbus( hw, "cbc_system_stat.rdb_ctrl.read_ready" );
	}
    }
    else{
	usleep(nword/80+1);
    }
    string node = "rdb";
    uhal::ValVector<uint32_t> data = hw->getNode(node).readBlock( nword );
    hw->dispatch();
    //usleep(10);
    for( unsigned i=0; i < data.size(); i++ ){
	Data[i] = data.at(i); 
    }
    return data.size();
}

void dump_raw_data( uint32_t *buffer, int n, std::ostream &os ){

    unsigned wpl(2);
    os << setbase(16);
    os << setfill('0');
    os << setw(8);
    os << setbase(16) << setfill('0') << setw(8) << 0 << ' ';
    for(int i=0; i < n; i++){
	os << setbase(16) << setfill('0') << setw(8) << buffer[i] << ' ';
	if( i % wpl == wpl - 1 ){
	    os << endl;
	    if( i != n-1) 
		os << setbase(16) << setfill('0') << setw(8) << i+1 << ' ';
	}
    }
}
void dump_seq_data( std::vector<uint32_t> &data, std::ostream &os ){
    for(unsigned i=0; i < data.size(); i++){
	os << setbase(16) << setfill('0') << setw(8) << data.at(i) << endl; 
    }
}
void dump_ser_data( uint32_t *buffer, int n, std::ostream &os ){
    int slvs[6];
    for(int i=0; i < n; i++){
	if(i%2==0){
	    slvs[0] = buffer[i] & 0x000000ff;
	    slvs[1] = (buffer[i] & 0x0000ff00) >> 8;
	    slvs[2] = (buffer[i] & 0x00ff0000) >> 16;
	    slvs[3] = (buffer[i] & 0xff000000) >> 24;
	}
	if(i%2==1){
	    slvs[4] = buffer[i] & 0x000000ff;
	    slvs[5] = (buffer[i] & 0x0000ff00) >> 8;
	    os << setbase(16) << setfill('0') << setw(2) 
		<< slvs[0] << ' '
		<< setbase(16) << setfill('0') << setw(2) 
		<< slvs[1] << ' '
		<< setbase(16) << setfill('0') << setw(2) 
		<< slvs[2] << ' '
		<< setbase(16) << setfill('0') << setw(2) 
		<< slvs[3] << ' '
		<< setbase(16) << setfill('0') << setw(2) 
		<< slvs[4] << ' '
		<< setbase(16) << setfill('0') << setw(2) 
		<< slvs[5] << ' '
		<< endl;
	}
    }
}
void get_slvs5(const uint32_t *buffer, int *slvs5, int n ){

    for( int i=0; i < n; i++){
	slvs5[i] = (buffer[2*i+1] & 0x000000FF);
    }
}
void get_slvs6(const uint32_t *buffer, int *slvs6, int n ){

    for( int i=0; i < n; i++){
	slvs6[i] = (buffer[2*i+1] & 0x0000FF00) >> 8;
    }
}

void run_rdb_and_fsc( uint32_t *buffer, int nwords ){

    WriteBeBoardConfig( "cbc_system_ctrl.fast_signal_manager.fast_signal_generator_stop", 1 );
    WriteBeBoardConfig( "cbc_system_ctrl.fast_signal_manager.stop_trigger", 1 );
    WriteBeBoardConfig( "cbc_system_ctrl.global.daq_reset", 1 );
    WriteBeBoardConfig( "cbc_system_ctrl.fast_signal_manager.start_trigger", 1 ); 
    WriteBeBoardConfig( "cbc_system_ctrl.rdb_ctrl.reset", 1 );

    StartFscBramController(nwords);

    usleep(1000);
    int read_nwords = ReadRawData( buffer, nwords );
    if( read_nwords != nwords )
	throw Cbc3TestException("ReadRawData() returned unexpected # of words read.");
}
int main( int argc, char *argv[] )
{
    string fname;
    if(argc>1){
	fname = argv[1];
    }
    else fname = "rdb_test.txt";
    ofstream ofile(fname.c_str());
    bool orbit_reset(false);
    if(argc>2){
	if(strcmp(argv[2],"or")==0) orbit_reset = true;
    }
    bool fast_reset(false);
    if(argc>3){
	if(strcmp(argv[3],"fr")==0) fast_reset = true;
    }
    bool noise(false);
    if(argc>4){
	if(strcmp(argv[4],"noise")==0) noise = true;
    }

    ConnectionManager cm( string( "file://" ) + connection_file ); 
    hw = new HwInterface( cm.getDevice( id ) );

//    unsigned N(2000);//5 x 38 x 2 + alpha
    unsigned N(112);
    unsigned n = N/2;

    uint32_t *rawdata(0);
    int *slvs6(0);

    rawdata = new uint32_t[N];
    slvs6   = new int[n];

    int NE(4);
    int  nc[NE];
    int clist[NE][16];
    nc[0] = 2;
    clist[0] = {33,34,253,254,0,0,0,0,0,0,0,0,0,0,0,0};
//    clist[0] = {253,254,33,34,0,0,0,0,0,0,0,0,0,0,0};
    nc[1] = 8;
    clist[1] = {1,2,33,34,221,222,253,254,0,0,0,0,0,0,0,0};
    nc[2] = 12;
    clist[2] = {1,2,33,34,225,226,189,190,221,222,253,254,0,0,0,0};
    nc[3] = 16;
    clist[3] = {1,2,33,34,161,162,225,226,157,158,189,190,221,222,253,254};
    //mask[1] = {1,2,0,0,0,0,0,0,0,0};

//    clist[1] = {33,34};

//    std::map<int,int> msks;
//    for(int i=1; i < 255; i++){
////	if( i == 1 || i == 2 || i == 3 || i == 4 || i == 5 || i == 6 || i == 33 || i == 34 ){
///*
//	if( i == 3 || i == 4 || i == 5 || i == 6 || i == 33 || i == 34 ){
//	    msks[i] = 1;
//	}
//	else msks[i] = 0;
//	*/
//	//msks[i] = 0;
//	if( i == 33 || i == 34 ){
//	    msks[i] = 1;
//	}
//	/*
//	else if( i == 23 || i == 24 ){
//	    msks[i] = 1;
//	}
//	else if( i == 101 || i == 102 ){
//	    msks[i] = 1;
//	}
//	*/
//	else msks[i] = 0;
//    }

    for(int evt=0; evt < NE; evt++){
	std::map<int,int> msks;
	if(noise)
	    for(int i=1; i < 255; i++)msks[i] = 1;
	else
	{
	    for(int i=1; i < 255; i++)msks[i] = 0;
	    for(int i=0; i < nc[evt]; i++){
		int c = clist[evt][i];
		msks[c] = 1;
	    }
	}
	vector<uint32_t> seq_data;

	seq_data.push_back((unsigned) 0x22000000);
	channel_mask( msks, seq_data);
	seq_data.push_back((unsigned) 0xe0000000);

	WriteFscBram(seq_data);
	StartFscBramController(N);
	cout << "masks written" << endl;
	sleep(1);

	seq_data.clear();
	//fast command [31:29]101, [27:16] count, [15:12] fast reset, [11:8] L1, [7:4] test pulse req, [3:0] orbit reset 
	//i2c  command [31:29]100, [28:24] cbc id, [21:20] rw flags, [16] page, [15:8] addr, [7:0] value
	//stub comes after 13 clock cycle from test pulse request.
	seq_data.push_back((unsigned) 0x22000000);
	if(evt==0 && orbit_reset){
	    seq_data.push_back((unsigned) 0xa0010001);// orbit reset 
	}
	else
	    seq_data.push_back((unsigned) 0xa0010000);// puase

	if(evt==0 && fast_reset){
	    seq_data.push_back((unsigned) 0xa0011000);// fast reset
	}
	else
	    seq_data.push_back((unsigned) 0xa0010000);// puase

	seq_data.push_back((unsigned) 0xa00a0000);// puase

	seq_data.push_back((unsigned) 0xa0010210);// test pulse & buffer recording start 
	//	  seq_data.push_back((unsigned) 0xa0010200);// buffer recording start 
	seq_data.push_back((unsigned) 0xa0070000);// puase
	seq_data.push_back((unsigned) 0xa0010100);// T1
	//	  seq_data.push_back((unsigned) 0xaeff0000);// puase
	// seq_data.push_back((unsigned) 0xa0a00000);// puase works but not recommended
	/*
	   seq_data.push_back((unsigned) 0xa0010010);// test pulse 
	   seq_data.push_back((unsigned) 0xa0070000);// puase
	   seq_data.push_back((unsigned) 0xa0010100);// T1
	   */

	seq_data.push_back((unsigned) 0xe0000000);

	WriteFscBram(seq_data);
	run_rdb_and_fsc(rawdata, N);

	ofile << "EVENT " << evt << endl;
	if(noise){
	    ofile << "All channels on" << endl;
	}
	else{
	    for(int i=0; i < nc[evt]; i++){
		int c = clist[evt][i];
		msks[c] = 1;
		ofile << std::dec << setfill(' ') << setw(3) << c << ' ';
	    }
	    ofile << endl;
	}
	ofile << "-----------------" <<endl;
	dump_seq_data(seq_data, ofile);
	ofile << "-----------------" <<endl;
	//	  dump_raw_data(rawdata,N, cout);
	dump_ser_data(rawdata,N-2, ofile);//fix me later.
	//get_slvs6(rawdata, slvs6, n);
	ofile << "=================" <<endl;

    }
    ofile.close();
    return 0;
}
