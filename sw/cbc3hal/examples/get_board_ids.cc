#include <getopt.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <uhal/uhal.hpp>

using namespace std;
using namespace uhal;

string connection_file( "connections.xml" );

void printHelp( char *argv[] )
{
    unsigned indent(50);
    cout << setfill(' ');
    cout << "usage: " << argv[0] << "\n" 
	<< setw(indent) << " --help                      "        << ": prints this help.\n" 
	<< setw(indent) << " --conFile  [connection file]"        << ": connection file name to use.\n" 
	<< setw(indent) << " --delim    [delimiter]"              << ": delimiter for the output.\n" 
	<< endl;
}
int main( int argc, char *argv[] )
{

    setLogLevelTo( uhal::Error() );
    //getting environment variables
    char *env_value;
    env_value = getenv( "IPB_CONNECTION_FILE" );
    if( env_value != NULL ) connection_file = env_value; 

    string delim("\n");

    //getting options
    int c;
    while(1)
    {
	static struct option long_options[] = { 
	    { "help"    , no_argument, 0, 'a' },
	    { "conFile" , required_argument, 0, 'b' },
	    { "delim"   , required_argument, 0, 'c' }
	};
	int option_index = 0;
	c = getopt_long( argc, argv, "ab:c:", long_options, &option_index );
	if( c == -1 ) {
	    break;
	}
	switch( c )
	{
	    case 'a':
		printHelp( argv );
		return 0;
	    case 'b':
		connection_file = optarg;
		break;
	    case 'c':
		delim = optarg;
		break;
	    default:
		abort();
	}
    }
    string arg_command;
    if( optind < argc ){
	arg_command = argv[optind++];
    }	
    vector<string> ids;
    try{
	ConnectionManager cm( string( "file://" ) + connection_file ); 
	ids = cm.getDevices();
    }
    catch( const std::exception& e ){
	cout << e.what() << endl;
	return -1;
    }

    if( delim == "\\t" ) delim = "\t"; 
    for( unsigned i=0; i < ids.size(); i++ ){
	cout << ids.at(i) << delim; 
    }
}
