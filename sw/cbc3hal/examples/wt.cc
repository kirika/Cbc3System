/*
 * Author : Kirika Uchida
 */

#include <getopt.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <readline/readline.h>
#include <readline/history.h>
#include <cstdio>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <exception>

#include <uhal/uhal.hpp>
#include "ipbus_utils.h"
#include "CbcI2cRegIpbusInterface.h"
#include "Fc7SystemRegInterface.h" 
#include "Cbc3BeInterface.h"
#include "Cbc3BeFc7.h"
#include "Cbc3BeData.h"

#include <TH1F.h>
#include <TH2F.h>
#include <TFile.h>

using namespace std;
using namespace uhal;
using namespace cbc3hal;
using namespace ipbutl;

string connection_file( "connections.xml" );
string id("board");
unsigned cbc_id = 1;
HwInterface *hw(0);

//set of wrong stub addresses
set<int> init_waddrs(){
    set<int> wa;
    wa.insert(108);
    wa.insert(226);
    wa.insert(173);
    wa.insert(227);
    wa.insert(239);
    return wa;
}
set<int> waddrs(init_waddrs());


typedef void wt_func(const std::string &test_name, Cbc3BeFc7 *cbc3be, std::ostream &os, 
	std::map<std::string, std::string> &wt_str_configs, 
	std::map<std::string, int> &wt_int_configs, 
	int debug ); 

void print_test( const string &test_name, const string &log,  std::ostream &os){
    os << test_name << " " << log << endl;
}
void print_delta_t( const string &test_name, const struct timeval &dt, std::ostream &os){

    cout << test_name << " Took " << dt.tv_sec + dt.tv_usec / 1000000. << " sec." << endl;
}

void run_test( wt_func func, const std::string &test_name, Cbc3BeFc7 *cbc3be, std::ostream &os, 
	std::map<std::string, std::string> &wt_str_configs, 
	std::map<std::string, int> &wt_int_configs, 
	int debug ){

    struct timeval t0, t, dt;
    gettimeofday(&t0,0);
    func(test_name, cbc3be, os, wt_str_configs, wt_int_configs, debug);
    gettimeofday(&t,0);
    timersub(&t,&t0,&dt);
    print_delta_t( test_name, dt, cout );
}

class WaferTestException : public std::exception{
    public:
	WaferTestException()throw(){}
	WaferTestException( const std::string &estr )throw(){err_str = estr; }
	WaferTestException(const WaferTestException &e)throw(){ err_str = e.what(); }
	WaferTestException& operator= (const WaferTestException &e) throw(){ err_str = e.what(); return *this; }
	virtual ~WaferTestException()throw(){}
	virtual const char * what() const throw(){ return err_str.c_str(); }
    private:
	std::string err_str;
};

void reset_data( uint32_t *rawdata, int *slvs5, unsigned N ){

    for(unsigned i=0; i<N; i++){
	rawdata[i] = 0;
	if(i%2) slvs5[i/2] = 0;
    }
} 

void init_run(Cbc3BeFc7 *cbc3be){

    unsigned trig_lat(0);
    cbc3be->rwCbcI2cRegsByName(1,0,"FcCntrlCompBetaTrigLat.trig_lat", trig_lat, cbc_id );
    cout << "Trigger Latency = " << std::dec << trig_lat << endl;

    char cnode[256];
    sprintf( cnode, "cbc_system_cnfg.cbc_data_processors.cbc%d.latencies.l1a", cbc_id );
    cbc3be->WriteBeBoardConfig( cnode, trig_lat ); 

    cbc3be->SendFastSignal( "reset" ); 
    cbc3be->WriteBeBoardConfig( "cbc_system_ctrl.fast_signal_manager.fast_signal_generator_stop", 1 );
    cbc3be->WriteBeBoardConfig( "cbc_system_ctrl.fast_signal_manager.stop_trigger", 1 );
    cbc3be->WriteBeBoardConfig( "cbc_system_ctrl.global.daq_reset", 1 );
    cbc3be->WriteBeBoardConfig( "cbc_system_ctrl.fast_signal_manager.start_trigger", 1 ); 
    cbc3be->WriteBeBoardConfig( "cbc_system_ctrl.rdb_ctrl.reset", 1 );

}
bool run_fsc_and_take_data( Cbc3BeFc7 *cbc3be, uint32_t *buffer, int nwords ){

    //    cbc3be->InitializeRun();
    //sleep(1);
    cbc3be->StartFscBramController();
    cbc3be->ReadBeBoardRegister( "cbc_system_stat.cbc_data_processors.cbc1.data_frame_counter" );
    cbc3be->MinNumWordToWait(nwords);
    int read_nwords(0);
    if( cbc3be->WaitData( 1, false ) ){
	read_nwords = cbc3be->ReadData( buffer );
    }
    bool ok(true);
    if( read_nwords != nwords )ok = false; 
    return ok;
}

bool run_rdb_and_fsc( Cbc3BeFc7 *cbc3be, uint32_t *buffer, int nwords ){

    cbc3be->WriteBeBoardConfig( "cbc_system_ctrl.fast_signal_manager.fast_signal_generator_stop", 1 );
    cbc3be->WriteBeBoardConfig( "cbc_system_ctrl.fast_signal_manager.stop_trigger", 1 );
    cbc3be->WriteBeBoardConfig( "cbc_system_ctrl.global.daq_reset", 1 );
    cbc3be->WriteBeBoardConfig( "cbc_system_ctrl.fast_signal_manager.start_trigger", 1 ); 
    cbc3be->WriteBeBoardConfig( "cbc_system_ctrl.rdb_ctrl.reset", 1 );

    /*
    unsigned cbc_fc = cbc3be->ReadBeBoardRegister( "cbc_system_stat.cbc_data_processors.cbc1.data_frame_counter" );
    cout << "cbc frame count = " << cbc_fc << endl;
    */

    cbc3be->StartFscBramController();
    /*
       string node = "rdb";
       uhal::ValVector<uint32_t> data = hw->getNode(node).readBlock( nwords );
       hw->dispatch();
       for( unsigned i=0; i < data.size(); i++ ){
       buffer[i] = data.at(i); 
       }
       int read_nwords = data.size();
       */
    int read_nwords = cbc3be->ReadRawData( buffer, nwords );
    bool ok(true);
    if( read_nwords != nwords )ok = false; 
    return ok;
} 
void dump_raw_data( uint32_t *buffer, int n, std::ostream &os ){

    unsigned wpl(2);
    os << setbase(16);
    os << setfill('0');
    os << setw(8);
    os << setbase(16) << setfill('0') << setw(8) << 0 << ' ';
    for(int i=0; i < n; i++){
	os << setbase(16) << setfill('0') << setw(8) << buffer[i] << ' ';
	if( i % wpl == wpl - 1 ){
	    os << endl;
	    if( i != n-1) 
		os << setbase(16) << setfill('0') << setw(8) << i+1 << ' ';
	}
    }
}
void get_slvs5(const uint32_t *buffer, int *slvs5, int n ){

    for( int i=0; i < n; i++){
	slvs5[i] = (buffer[2*i+1] & 0x0000FF00) >> 8;
    }
}
void get_error_bits(int *slvs5, int hp, bool &e1, bool &e2){
    int p(hp);
    e1 = (slvs5[p] & 0x20) ? 1 : 0;
    e2 = (slvs5[p] & 0x10) ? 1 : 0;
}
int get_pipeline_address(int *slvs5, int hp){
    int p(hp);
    int paddr = (slvs5[p] & 0xF) << 5;  
    paddr |= (slvs5[++p] & 0xF8) >> 3; 
    return paddr;
}
int get_l1counter(int *slvs5, int hp){
    int p=hp+1;
    int l1cntr = (slvs5[p] & 0x07) << 6;
    l1cntr |= (slvs5[++p] & 0xFC) >> 2;  
    return l1cntr;
}
int find_data_frame_header_position( int *slvs5, int n, int offset=0 ){

    for(int i=offset; i < n; i++){
	if( (slvs5[i] & 0xC0) == 0xC0 ) return i;
    }
    return -1;
}
bool has_hit(int *slvs5, int ph, int ch ){

    int p_bpos = 7-(ch+5)%8;
    int p_buf = (ch+5)/8 + ph + 2;
    return slvs5[p_buf] & (1 << p_bpos);
}
void dump_chdata( int *slvs5, int ph, std::ostream &os ){

    os << "Channel hit" << endl;
    for(int ch=1; ch < 255; ch++){
	if( ch % 32 == 1 ){
	    int ech(0);
	    if( ch / 32 != 7 ) ech = ch + 31;
	    else ech = ch + 29; 
	    os << endl << "ch[" << std::setw(3) << std::setfill('0') << ch << "-" << std::setw(3) << std::setfill('0') << ech << "]";
	}
	os << has_hit( slvs5, ph, ch );
    }
}
void dump_i2cregs( Cbc3BeFc7 *cbc3be, std::ostream &os ){

    CBCI2C_TYPE_DATA_MAP cbci2c_type_data_map = cbc3be->readCbcAllI2cRegsByType(cbc_id);
    //print_regcnf( cbc3be->GetCbcI2cRegReplies(), cout, cbc_id );
    CBCI2C_TYPE_DATA_MAP::iterator it = cbci2c_type_data_map.begin();
    for(; it != cbci2c_type_data_map.end(); it++){
	CBCI2C_ITEM_DATA_MAP &id = it->second;
	CBCI2C_ITEM_DATA_MAP::iterator item_it = id.begin();
	string item;
	for(; item_it != id.end(); item_it++){
	    item = it->first + "." + item_it->first;
	    os << std::setfill(' ') << setw(50) << std::left << item << " "; 
	    os << setw(3) << std::right << std::dec << item_it->second << endl;
	}
    }
}
void print_buff_hit( bool hit[32][254], std::ostream &os ){

    for(unsigned bi=0; bi<32; bi++){
	os << "Buffer[" << std::setw(2) << std::setfill('0') << std::dec << bi << "] " << endl;
	for(unsigned ch=1; ch<255; ch++){
	    if( ch % 32 == 1 ){
		int ech(0);
		if( ch / 32 != 7 ) ech = ch + 31;
		else ech = ch + 29; 
		os << "ch[" << std::setw(3) << std::setfill('0') << ch << "-" << std::setw(3) << std::setfill('0') << ech << "]";
	    }
	    os << hit[bi][ch-1] << " ";
	    if( ch % 32 == 0 ) os << endl;
	}
	os << endl;
    }

}
void print_buff_pipeline( int paddr_values[32][512], std::ostream &os ){

    for(unsigned bi=0; bi<32; bi++){
	os << "Buffer[" << std::setw(2) << std::setfill('0') << std::dec << bi << "] " << endl;
	for(unsigned i=0; i<512; i++){
	    if( i % 32 == 0 ){
		int ei = i + 31;
		os << "delay[" << std::hex << std::setw(3) << std::setfill('0') << i << "-" << std::hex << std::setw(3) << std::setfill('0') << ei << "]";
	    }
	    os << std::setw(3) << std::setfill('0') << std::hex << paddr_values[bi][i] << " ";
	    if( i%32 == 31 ) os << endl;
	}
    }
}
void print_buff_l1a_count( int l1a_count[32][512], std::ostream &os ){

    for(unsigned bi=0; bi<32; bi++){
	os << "Buffer[" << std::setw(2) << std::setfill('0') << std::dec << bi << "] " << endl;
	for(unsigned i=0; i<512; i++){
	    if( i % 32 == 0 ){
		int ei = i + 31;
		os << "data block[" << std::hex << std::setw(3) << std::setfill('0') << i << "-" << std::hex << std::setw(3) << std::setfill('0') << ei << "]";
	    }
	    os << std::setw(3) << std::setfill('0') << std::hex << l1a_count[bi][i] << " ";
	    if( i%32 == 31 ) os << endl;
	}
    }

}
bool read_after_burn_fuse_values( Cbc3BeFc7 *cbc3be, ostream &os ){

    bool ok(true);
    string name;
    unsigned value(0);
    name = "ChipIdFuse.sel";
    value = 1;
    //The read back value stays 0
    ok = cbc3be->rwCbcI2cRegsByName( 0, 1, name, value, 1 ); 
    name = "ChipIdFuse.id";
    ok &= cbc3be->rwCbcI2cRegsByName( 1, 0, name, value, 1 ); 
    cout << "after-bun fuse register ID value = " << value << endl;
    name = "BandgapFuse.override";
    value = 0;
    ok &= cbc3be->rwCbcI2cRegsByName( 1, 1, name, value, 1 ); 
    name = "BandgapFuse.sel";
    value = 1;
    //The read back value for the sel stays 1, but the band gap value is afer burn fuse value
    ok &= cbc3be->rwCbcI2cRegsByName( 0, 1, name, value, 1 ); 
    name = "BandgapFuse.bg_fuse";
    ok &= cbc3be->rwCbcI2cRegsByName( 1, 0, name, value, 1 ); 
    os << "after-bun fuse register Bandgap value = " << value << endl;
    return ok;
}
bool offset_tuning( Cbc3BeFc7 *cbc3be, ostream &os, string output_dir, int sample_count, int debug ){

    int group_type(8);
    const GROUP_CHANNELSET_MAP &group_map = Cbc3BeInterface::SetGroupChannelSetMap(group_type);

    /*
    cbc3be->ReadAllCbcConfig(cbc_id);
    print_regcnf( cbc3be->GetCbcI2cRegReplies(), os, cbc_id );
    */

    CBCI2C_ITEM_DATA_MAP item_data;
    //Get initial VCTH
    item_data.clear();
    CBCI2C_TYPE_DATA vcth0( "VCTH", item_data );
    cbc3be->rwCbcI2cRegsByType( 1, 0, vcth0, cbc_id );

    //Finding pedestal VCTH
    map<CBC_ID, vector<unsigned> > offsets0_map;
    vector<unsigned> offsets0(254,0x80);
    offsets0_map[cbc_id] = offsets0;
    cbc3be->FindVcth( sample_count, 0.5, &offsets0_map );

    unsigned cbc_id = 1;

    if(debug) os << "Tuning Offsets begins" << endl;
    //Tuning Offsets
    map<CBC_ID, map<CHANNEL_ID,unsigned> > all_offsets_map;

    for( unsigned g=0; g<group_map.size(); g++){

	map<CBC_ID, map<CHANNEL_ID,unsigned> > offsets_map;
	cbc3be->TuneOffsets(cbc_id, g,sample_count, offsets_map[cbc_id]);

	//add the tuned values in the global offset map
	all_offsets_map[cbc_id].insert(offsets_map[cbc_id].begin(), offsets_map[cbc_id].end());
    }

    //saving the results and writing those tuned values to CBC.
    string name;
    char fname[256];
    char fname_root[256];
    sprintf(fname_root, "offset_tuning_be%d.root", cbc3be->GetBeId() );
    if( output_dir == "" ) name = fname_root;
    else name = output_dir + "/" + fname_root;
    TFile *froot = new TFile(name.c_str(), "recreate");

    //offset histogram is filled.
    TH1F *h = new TH1F( Form("hoffsets_be%dcbc%d",cbc3be->GetBeId(), cbc_id), "; offset; count", 256, -0.5, 255.5 );
    map<CHANNEL_ID,unsigned> the_map =  all_offsets_map[cbc_id];
    map<CHANNEL_ID,unsigned>::iterator it_the_map = the_map.begin();
    for(; it_the_map != the_map.end(); it_the_map++){
	h->Fill(it_the_map->second);
    }
    cbc3be->rwCbcI2cRegsByType( 1, 1, vcth0, cbc_id );
    //New configuration file is opened.
    sprintf(fname, "offset_tuning-be%dcbc%d.knt", cbc3be->GetBeId(), cbc_id );
    if( output_dir == "" ) name = fname;
    else name = output_dir + "/" + fname;
    ofstream ofile(name.c_str());

    //Writing the tuned values to the registers
    if(debug) os << "Write tuned offset values to the registers" << endl;
    cbc3be->rwCbcI2c_Offsets(1,1,all_offsets_map[cbc_id],cbc_id);

    //Writing the tuned configuration to a file.
    if(debug) os << "Read all the registers on CBC " << cbc_id << " and write the tuned configuration to a file." << endl;
    cbc3be->ReadAllCbcConfig(cbc_id);
    CBCI2C_REGS regs = cbc3be->GetCbcI2cRegReplies();
    print_regcnf(regs, ofile, cbc_id);
    ofile.close();

    if(debug) os << "The parameter tunings finished for CBC " << cbc_id << "." << endl;

    froot->Write();
    froot->Close();

    return true;
}

void vcth_scan( Cbc3BeFc7 *cbc3be, ostream &os, string output_dir, string fname, int sample_count, bool tp_en, int debug ){

    CBCI2C_ITEM_DATA_MAP item_data;
    //Get initial VCTH
    item_data.clear();
    CBCI2C_TYPE_DATA vcth0( "VCTH", item_data );
    cbc3be->rwCbcI2cRegsByType( 1, 0, vcth0, cbc_id );

    //Get initial offsets
    map<unsigned, vector<unsigned> > offsets0;
    cbc3be->rwCbcI2c_Offsets(1,0,offsets0[cbc_id],cbc_id);
    //Set all offsets to 0xFF
    unsigned offset(0xFF);
    cbc3be->rwCbcI2c_Offset(1,1,0,offset,cbc_id);
    //Opening ROOT file to save histograms.
    string name;
    if( output_dir == "" ) name = fname;
    else name = output_dir + "/" + fname;
    ostringstream oss;
    oss << "A root file " << fname << " is created.";
    print_test( "vcth_scan", oss.str(), os ); 

    TFile fout( name.c_str(), "recreate" ); 

    int group_type(8);
    Cbc3BeInterface::SetGroupChannelSetMap(group_type);
    int ng = Cbc3BeInterface::GetGroupChannelSetMap().size(); 
    //VCTH scan
    for( int g=0; g<ng; g++){

	if( tp_en ){
	    CBCI2C_ITEM_DATA_MAP item_data;
	    item_data["group"] = g;
	    if( !cbc3be->writeAllCbcI2cRegsByType("TestPulseDelayGroup",item_data) )  
		throw WaferTestException( "vcth scan failed." ); 
	    // cout << "group = " << std::dec << g << " delay = " << tp_delay << endl;
	}

	cbc3be->ActivateGroupOffsets( g, offsets0, 0 );

	CbcHistList<TH1F> hlist;
	//	map<string, TH1F *> hmap;

	bool desc(false);
	int min(-1);
	int max(-1);
	cbc3be->ScanVcth(g, sample_count, hlist, desc, min, max );

	map<unsigned, TH1F*> &hmap = hlist.GetHistMap();
	map<unsigned,TH1F *>::iterator it_hmap = hmap.begin();
	for( ; it_hmap != hmap.end(); it_hmap++ ){
	    it_hmap->second->Write();
	}

	cbc3be->DisableGroupOffsets( g, 0 );
    }
    fout.Close();

    cbc3be->rwCbcI2c_Offsets(1,1,offsets0[cbc_id],cbc_id);
    cbc3be->rwCbcI2cRegsByType( 1, 1, vcth0, cbc_id );

    string command = "root -q -b 'root_scripts/scurve_analyzer.C+(\"";
    command = command + name + "\", \"be1cbc1\")'";
    system( command.c_str() );
}


std::string rstr( std::string s1, std::string s2, std::string s3 )
{
    std::string::size_type  pos( s1.find( s2 ) );

    while( pos != std::string::npos )
    {
	s1.replace( pos, s2.length(), s3 );
	pos = s1.find( s2, pos + s3.length() );
    }

    return s1;
}

void printHelp( char *argv[] )
{
    unsigned indent(50);
    cout << setfill(' ');
    cout << "usage: " << argv[0] << "\n" 
	<< setw(indent) << " --help                                            "  << ": prints this help.\n" 
	<< setw(indent) << " --debug     [bit0:CBC3INTERFACE,bit1:CBCI2C,bit2:param tuning]"  << ": debug flag. \n" 
	<< setw(indent) << " --conFile   [connection file]                     "  << ": connection file name to use.\n" 
	<< setw(indent) << " --boardId   [id]                                  "  << ": board id to use.\n" 
	<< setw(indent) << " --fc7Config [file name]                           "  << ": fc7 config file to use.\n" 
	<< setw(indent) << " --outDir    [output directory]                    "  << ": output directory.\n"
	<< setw(indent) << " --dll       [DLL value to test from 0 to 24]      "  << ": .\n"
	<< "environment variables and the values if the environment variables are not set:\n"
	<< setw(indent) << "[connection file] : IPB_CONNECTION_FILE, " << connection_file << "\n"
	<< setw(indent) << "[id]              : IPB_BOARD_ID,        " << id << "\n"
	<< endl;

}

void cbci2c_write_read_test( const std::string &test_name, Cbc3BeFc7 *cbc3be, ostream &os, 
	std::map<std::string,std::string> &wt_str_configs, 
	std::map<std::string,int> &wt_int_configs, 
	int debug ){ 

    print_test( test_name, string("CBC I2C write and readback test with ") + wt_str_configs["cbci2c_file"], cout);
    if( !cbc3be->rwCbcI2cRegs( 1, 1, wt_str_configs["cbci2c_file"], cbc_id ) ){
	throw WaferTestException( test_name + " cbci2c register configuration failed." ); 
    }
}
//valid stubs are in the range of i:[0-127], j:[0-14] for (nss+nsw%2)==0, j:[0-13] for (nss+nsw%2)==1 
int get_seed_address( int i, int nss ){
    int cs = i * 2 + 2;
    if(!(nss%2)) return cs-1;
    else return cs;
}
int get_stub_bend( int i, int j, bool shift ){

    int diff;
//    if(shift) diff = j - i + 1;
    if(shift) diff = j - i - 1;
    else diff = j - i;
    if(diff<0) diff += 16;
    return diff;
}
int get_window_cluster_index( int i, int b, bool shift ){

    int ofst = b;
    if(b>8) ofst = b - 16;
    if(shift) ofst += 1;
    return i + ofst;
}
std::vector<int> get_seed_channels_for_stub( int i, int nss ){
    vector<int> chs;
    int cs = i * 2 - 1;
    if(nss%2) cs += 2;
    chs.push_back(cs);
    if(nss > 1) chs.push_back(cs+2);
    if(nss > 2) chs.push_back(cs-2);
    if(nss > 3) chs.push_back(cs+4);
    return chs;
}
std::vector<int> get_window_channels_for_stub( int j, int nsw ){

    vector<int> chs;
    int cw = j * 2;
    if(nsw%2) cw += 2;
    chs.push_back(cw);
    if( nsw > 1 ) chs.push_back(cw+2); 
    if( nsw > 2 ) chs.push_back(cw-2);
    if( nsw > 3 ) chs.push_back(cw+4);
    return chs;
}
std::vector<int> get_channels_for_stub( int i, int j, int nss, int nsw ){

    std::vector<int> chs_seed = get_seed_channels_for_stub(i, nss); 
    std::vector<int> chs_window = get_window_channels_for_stub(j,nsw); 
    chs_seed.insert(chs_seed.end(), chs_window.begin(), chs_window.end());
    return chs_seed;

}
bool valid_seed( int i, int nss ){
    std::vector<int> seed_chs = get_seed_channels_for_stub(i, nss); 
    for(unsigned s_i=0; s_i < seed_chs.size(); s_i++){
	if(seed_chs.at(s_i) < 1 || seed_chs.at(s_i)>254) return false;
    }	
    return true;
}
bool valid_cluster_on_window( int j, int nsw ){

    std::vector<int> chs = get_window_channels_for_stub(j, nsw); 
    for(unsigned w_i=0; w_i < chs.size(); w_i++){
	if(chs.at(w_i) < 1 || chs.at(w_i)>254) return false;
    }	
    return true;
}
bool valid_stub( int i, int j, int nss, int nsw ){

    if( !valid_seed(i,nss) ) return false;
    if( !valid_cluster_on_window(j,nsw) ) return false;
    return true;
}

bool addr_bend_check(bool addr_ok[127], bool bend_ok[127][16], int nss, int nsw, std::ostream &os){

    ostringstream oss;
    bool stub_ok(true);
    for(int i=0; i<127; i++){
//	cout << "i, nss = " << i << " " << nss << endl;
	int sa = get_seed_address(i,nss);

	for(int b_i=0; b_i<16; b_i++){
	    if(b_i == 8) continue;
	    if((nss+nsw)%2 && b_i == 7) continue;
	    bool shift(false);
	    if( !(nsw % 2 ) && ((nss + nsw)%2) ) shift = true;
	    int j = get_window_cluster_index(i, b_i, shift);

	    if( !valid_stub(i, j, nss, nsw) )continue;

	    if( addr_ok[i] == false ){
		if(waddrs.find(sa) == waddrs.end()){ 
		    stub_ok = false;
		    if(b_i == 0){
			oss.str("");
			oss << "The address " << std::dec << sa << std::hex << "(" << sa << ") was not found in the scan.";
			os << oss.str() << endl;
		    }
		}
		//		throw WaferTestException( test_name + oss.str() ); 
	    }
	    else{
		/*
		if(i==7){
		    cout << "b_i j = " << b_i << " " << j << endl;
		    if(b_i==9) cout << "bend_ok for 9 is " << bend_ok[i][b_i] << endl;
		}
		*/
		if( bend_ok[i][b_i] == false ){
		    stub_ok = false;
		    oss.str("");
		    oss << "The bend " << std::dec << b_i  << "(" << std::hex << j << ") for the seed address " << std::dec << sa << " i = " << i << "(" << std::hex << sa << ")" 
			<< " j = " << std::dec << j << " was not found in the scan.";
		    os << oss.str() << endl;
		}
	    }
	}
    }
    return stub_ok;
}
void frame_11( const std::string &test_name, Cbc3BeFc7 *cbc3be, ostream &os,
	std::map<std::string,std::string> &wt_str_configs, 
	std::map<std::string,int> &wt_int_configs, 
	int debug ){ 

    print_test( test_name, "Mark's frame 4 and 11. Raw data frame check. Not needed practically.", cout); 

    string fscbram_file = wt_str_configs["fscbram_file"];

    unsigned N(80);
    unsigned n = N/2;

    uint32_t *rawdata(0);
    int *slvs5(0);

    cbc3be->WriteBeBoardConfig("cbc_system_cnfg.rdb_ctrl.write_block_size", n); 

    cbc3be->WriteFscBram(fscbram_file);

    rawdata = new uint32_t[N];
    if( !run_rdb_and_fsc( cbc3be, rawdata, N ) ){

	delete rawdata;
	throw WaferTestException( test_name + " failed in run_rdb_and_fsc." );
    }

    if(debug) dump_raw_data(rawdata,N, os);

    n = N/2;
    slvs5 = new int[n];
    get_slvs5(rawdata, slvs5, n);

    int hp = find_data_frame_header_position(slvs5, n);

    ostringstream oss;
    oss << "Header position is " << std::dec << hp; 
    print_test( test_name, oss.str(), os );

    if(hp < 0) {
	delete rawdata;
	delete slvs5;
	throw WaferTestException( test_name + " failed." ); 
    }
    int paddr=-1;
    int l1cntr=-1;
    paddr = get_pipeline_address(slvs5, hp);
    oss.str("");
    oss << "Pipeline address is " << std::dec << paddr;
    print_test( test_name, oss.str(), os );
    l1cntr = get_l1counter(slvs5, hp); 
    oss.str("");
    oss << "L1 counter value is " << std::dec << l1cntr;
    print_test( test_name, oss.str(), os );
    oss.str("");
    oss << "Channel hit";
    for(int ch=1; ch < 255; ch++){
	if( ch % 32 == 1 ){
	    print_test( test_name, oss.str(), os );
	    int ech(0);
	    if( ch / 32 != 7 ) ech = ch + 31;
	    else ech = ch + 29; 
	    oss.str("");
	    oss << "ch[" << std::setw(3) << std::setfill('0') << ch << "-" << std::setw(3) << std::setfill('0') << ech << "]";
	}
	oss << has_hit( slvs5, hp, ch );
    }
    print_test( test_name, oss.str(), os );

    if(debug){
	dump_raw_data(rawdata,N, os);
	dump_chdata(slvs5, hp, os);
    }

    delete rawdata;
    delete slvs5;
}
void frame_15( const std::string &test_name, Cbc3BeFc7 *cbc3be, ostream &os, 
	std::map<std::string,std::string> &wt_str_configs, 
	std::map<std::string,int> &wt_int_configs, 
	int debug ){ 

    print_test(test_name, "After fuse check.", cout);
    cbc3be->CbcReset();
    cbc3be->CbcI2cBusInit();
    if( !read_after_burn_fuse_values(cbc3be,cout) ) throw WaferTestException(test_name + " failed.");

    cbc3be->CbcReset();
    cbc3be->CbcI2cBusInit();
    if( !cbc3be->rwCbcI2cRegs( 1, 1, wt_str_configs["cbci2c_file"], cbc_id ) ){
	throw WaferTestException( test_name + " failed." ); 
    }
    if( !read_after_burn_fuse_values(cbc3be,cout) ) throw WaferTestException(test_name + " failed.");

    cbc3be->CbcReset();
    cbc3be->CbcI2cBusInit();
    if( !read_after_burn_fuse_values(cbc3be,cout) ) throw WaferTestException(test_name + " failed.");
}
void frame_16( const std::string &test_name, Cbc3BeFc7 *cbc3be, ostream &os,
	std::map<std::string,std::string> &wt_str_configs, 
	std::map<std::string,int> &wt_int_configs, 
	int debug ){ 

    print_test(test_name, "Pedestal offsets tuning.", cout);
    cbc3be->WriteBeBoardConfig(wt_str_configs["config_file"]);

    //Get sample count
    int sample_count(100);
    std::map<std::string,uint32_t> regmap;
    cbc3be->ReadBeBoardConfig( wt_str_configs["config_file"], regmap );
    std::map<std::string,uint32_t>::iterator it_regmap = regmap.begin();
    it_regmap = regmap.find( "cbc_system_cnfg.fast_signal_manager.fast_signal_generator.Ncycle" );
    if( it_regmap != regmap.end() ) sample_count = it_regmap->second; 

    cbc3be->CbcReset();//Mark's proceduer
    //CBC I2C BUS INITIALIZATION AND CONFIGURATION
    cbc3be->CbcI2cBusInit();
    if( !cbc3be->rwCbcI2cRegs( 1, 1, wt_str_configs["cbci2c_file"], cbc_id ) ){
	throw WaferTestException( test_name + " failed."); 
    }
    //Offset tuning
    offset_tuning(cbc3be, cout, wt_str_configs["output_dir"], sample_count, debug );
}
void vcth_scan_test( const std::string &test_name, Cbc3BeFc7 *cbc3be, ostream &os,
	std::map<std::string,std::string> &wt_str_configs, 
	std::map<std::string,int> &wt_int_configs, 
	int debug ){ 

    unsigned sample_count = cbc3be->ReadBeBoardRegister( "cbc_system_cnfg.fast_signal_manager.fast_signal_generator.Ncycle" );

    if( wt_str_configs.find("cbci2c_file") != wt_str_configs.end() ){
	if( !cbc3be->rwCbcI2cRegs( 1, 1, wt_str_configs["cbci2c_file"], cbc_id ) ){
	    throw WaferTestException( test_name + " cbc i2c register configuration failed."); 
	}
    }
    if( wt_int_configs.find("TestPulsePolEnAMux.tp_en")!= wt_int_configs.end() ){
	unsigned value = wt_int_configs["TestPulsePolEnAMux.tp_en"];
	cout << "Test pulse enabled? : " << value << endl;
	if( !cbc3be->rwCbcI2cRegsByName( 1, 1, "TestPulsePolEnAMux.tp_en", value, cbc_id ) )
	    throw WaferTestException( test_name + " TestPulsePolEnAMux.tp_en writing failed."); 
	cbc3be->rwCbcI2cRegsByName( 1, 0, "TestPulsePolEnAMux.tp_en", value, cbc_id ); 
	cout << "Test pulse enabled read back value? : " << value << endl;

    }
    bool tp_en(false);
    if( wt_int_configs.find("tp_en")!= wt_int_configs.end() && wt_int_configs["tp_en"] == 1 ) tp_en = true;
    vcth_scan( cbc3be, cout, wt_str_configs["output_dir"], wt_str_configs["output_file"], sample_count, tp_en, debug );

    dump_i2cregs(cbc3be,os);
}

void frame_18a( const std::string &test_name, Cbc3BeFc7 *cbc3be, ostream &os,
	std::map<std::string,std::string> &wt_str_configs, 
	std::map<std::string,int> &wt_int_configs, 
	int debug ){ 

    print_test(test_name, "Pipeline check - check all pipeline address, l1a counter, and all channel data should be 0 delaying the L1A by 1 clock 511 times.", cout);
    if( !cbc3be->rwCbcI2cRegs( 1, 1, wt_str_configs["cbci2c_file"], cbc_id ) )
	throw WaferTestException( test_name + " cbc i2c register configuration failed."); 

    unsigned value = wt_int_configs["VCTH.vcth"];
    if( !cbc3be->rwCbcI2cRegsByName( 1, 1, "VCTH.vcth", value, cbc_id ) )
	throw WaferTestException( test_name + " VCTH.vcth writing failed."); 


    vector<uint32_t> fscbuf_data;
    fscbuf_data.push_back((unsigned) 0xa0200000);
    fscbuf_data.push_back((unsigned) 0xa0011000);//fast reset
    fscbuf_data.push_back((unsigned) 0xa0e00000);
    fscbuf_data.push_back((unsigned) 0xa0010100);//T1
    fscbuf_data.push_back((unsigned) 0xa0060000);
    //fscbuf_data.push_back((unsigned) 0xa0010200);
    fscbuf_data.push_back((unsigned) 0xe0000000);

    int npipe[512];
    for(unsigned i=0; i < 512; i++) npipe[i]=0;
//    unsigned sum_header_bits(0);
    unsigned sum_err_bits(0);
    bool no_hit = true;


    unsigned nwords = 14 * 512;
    uint32_t *buffer = new uint32_t[nwords];
    BeDataDecoder be_data_decoder;
    int ncbcdata(0);

    init_run(cbc3be);

    for(unsigned i=0; i < 512; i++){
	cbc3be->WriteFscBram(fscbuf_data);
	cbc3be->StartFscBramController();
	fscbuf_data[2] = (((i + 0xe1) << 16) | 0xa0000000 );//1 clock delay 

    }
    cbc3be->MinNumWordToWait(nwords);
    int read_nwords(0);
    if( cbc3be->WaitData( 1, false ) ){
	read_nwords = cbc3be->ReadData( buffer );
    }
    if( read_nwords != (int)nwords )
	throw WaferTestException( test_name + " Data read failed."); 

    be_data_decoder.SetData( buffer, nwords * 4 );
    while( be_data_decoder.SetNextEvent() ){ 

	while( be_data_decoder.SetNextCbc() ){

	    ncbcdata++;
	    const CbcDataPacketDecoder *cbc_dpd = be_data_decoder.GetCbcDataPacketDecoder();
	    sum_err_bits += cbc_dpd->Error(0);
	    sum_err_bits += cbc_dpd->Error(1);
	    int paddr = cbc_dpd->PipelineAddress();
	    if(paddr >=0 && paddr <= 511) npipe[paddr]++; 
	    if(cbc_dpd->HitChannels().size() != 0 ){
		no_hit = false;
		cout << "Some channels have hits!" << endl;
	    }
	}
    }
    delete buffer;
    /*
    unsigned N(80);
    uint32_t *rawdata = new uint32_t[N];
    unsigned n = N/2;
    int *slvs5 = new int[n];

    cbc3be->WriteBeBoardConfig("cbc_system_cnfg.rdb_ctrl.write_block_size", n); 

    for(unsigned i=0; i < 512; i++){

	cbc3be->WriteFscBram(fscbuf_data);
	run_rdb_and_fsc( cbc3be, rawdata, N );
	get_slvs5(rawdata, slvs5, n);
	int hp = find_data_frame_header_position(slvs5, n);
	int p = hp;
	sum_header_bits += (slvs5[p] & 0x80) >> 7;
	sum_header_bits += (slvs5[p] & 0x40) >> 6;
	bool e1(false), e2(false);
	get_error_bits(slvs5, hp, e1, e2);
	sum_err_bits += e1; 
	sum_err_bits += e2; 
	int paddr = get_pipeline_address(slvs5, hp);
	npipe[paddr]++;

	fscbuf_data[2] = (((i + 0xe1) << 16) | 0xa0000000 );//1 clock delay 
	for(int ch=1; ch < 255; ch++){
	    if( has_hit(slvs5, hp, ch) ){
		no_hit = false;
		break;
	    }
	}
    }
    delete rawdata;
    delete slvs5;
    cout << "sum of all header bits is " << sum_header_bits << endl;
    */
    ostringstream oss;
    oss << "# of CBC DATA is " << ncbcdata << "."; 
    print_test( test_name, oss.str(), os );

    oss.str("");
    oss << "Sum of all error bits is " << sum_err_bits << ".";

    bool pipe_ok(true);
    oss.str("");
    oss << "Pipeline stats." << endl;
    for(unsigned i=0; i < 512; i++){
	if(i%32==0){
	    print_test( test_name, oss.str(), os );
	    int eaddr = i + 31;
	    oss.str("");
	    oss << "addr[" << std::setw(3) << std::setfill('0') << i << "-" << std::setw(3) << std::setfill('0') << eaddr << "]";
	}
	oss << npipe[i] << " ";
	if(npipe[i] != 1) {
	    pipe_ok = false;
	}
    }
    print_test( test_name, oss.str(), os );
    oss.str("");
    oss << "no hit ? " << no_hit;
    print_test( test_name, oss.str(), os );
    oss.str("");
    oss << "pipeline address seen once only? " << pipe_ok;
    print_test( test_name, oss.str(), os );
}
void frame_18b( const std::string &test_name, Cbc3BeFc7 *cbc3be, ostream &os,
	std::map<std::string,std::string> &wt_str_configs, 
	std::map<std::string,int> &wt_int_configs, 
	int debug ){ 

    print_test(test_name, "Pipeline check - check all pipeline address, l1a counter, and all channel data should be 1 delaying the L1A by 1 clock 511 times.", cout);

    ostringstream oss;

    unsigned value = wt_int_configs["VCTH.vcth"];
    if( !cbc3be->rwCbcI2cRegsByName( 1, 1, "VCTH.vcth", value, cbc_id ) )
	throw WaferTestException( test_name + " VCTH.vcth writing failed."); 

    vector<uint32_t> fscbuf_data;
    fscbuf_data.push_back((unsigned) 0xa0200000);
    fscbuf_data.push_back((unsigned) 0xa0011000);//fast reset
    fscbuf_data.push_back((unsigned) 0xa0e00000);
    fscbuf_data.push_back((unsigned) 0xa0010100);//T1
//    fscbuf_data.push_back((unsigned) 0xa0010200);
    fscbuf_data.push_back((unsigned) 0xe0000000);

    int npipe[512];
    for(unsigned i=0; i < 512; i++) npipe[i]=0;
//    unsigned sum_header_bits(0);
    unsigned sum_err_bits(0);
    bool all_hit = true;


    unsigned nwords = 14 * 512;
    uint32_t *buffer = new uint32_t[nwords];
    BeDataDecoder be_data_decoder;
    int ncbcdata(0);

    init_run(cbc3be);

    for(unsigned i=0; i < 512; i++){
	cbc3be->WriteFscBram(fscbuf_data);
	cbc3be->StartFscBramController();
	fscbuf_data[2] = (((i + 0xe1) << 16) | 0xa0000000 );//1 clock delay 

    }

    cbc3be->MinNumWordToWait(nwords);
    int read_nwords(0);
    if( cbc3be->WaitData( 0, false ) ){
	read_nwords = cbc3be->ReadData( buffer );
    }
    if( read_nwords != (int)nwords ){
	oss.str("");
	oss << " Data read failed. " << read_nwords << " words read.";
	throw WaferTestException( test_name + oss.str() ); 
    }

    be_data_decoder.SetData( buffer, nwords * 4 );
    while( be_data_decoder.SetNextEvent() ){ 

	while( be_data_decoder.SetNextCbc() ){

	    ncbcdata++;
	    const CbcDataPacketDecoder *cbc_dpd = be_data_decoder.GetCbcDataPacketDecoder();
	    sum_err_bits += cbc_dpd->Error(0);
	    sum_err_bits += cbc_dpd->Error(1);
	    int paddr = cbc_dpd->PipelineAddress();
	    if(paddr >=0 && paddr <= 511) npipe[paddr]++; 
	    if(cbc_dpd->HitChannels().size() != 254 ){
		all_hit = false;
		cout << "Some all channels do not have hits!" << endl;
	    }
	}
    }
    delete buffer;

    oss.str("");
    oss << "# of CBC DATA is " << ncbcdata << "."; 
    print_test( test_name, oss.str(), os );

    oss.str("");
    oss << "Sum of all error bits is " << sum_err_bits << ".";

    bool pipe_ok(true);
    oss.str("");
    oss << "Pipeline stats." << endl;
    for(unsigned i=0; i < 512; i++){
	if(i%32==0){
	    print_test( test_name, oss.str(), os );
	    int eaddr = i + 31;
	    oss.str("");
	    oss << "addr[" << std::setw(3) << std::setfill('0') << i << "-" << std::setw(3) << std::setfill('0') << eaddr << "]";
	}
	oss << npipe[i] << " ";
	if(npipe[i] != 1) {
	    pipe_ok = false;
	}
    }
    print_test( test_name, oss.str(), os );
    oss.str("");
    oss << "all hit ? " << all_hit;
    print_test( test_name, oss.str(), os );
    oss.str("");
    oss << "pipeline address seen once only? " << pipe_ok;
    print_test( test_name, oss.str(), os );
}
void frame_19a( const std::string &test_name, Cbc3BeFc7 *cbc3be, ostream &os,
	std::map<std::string,std::string> &wt_str_configs, 
	std::map<std::string,int> &wt_int_configs, 
	int debug ){ 

    print_test(test_name, "Buffer check - check all error bits and data bits are 0.", cout);
    ostringstream oss;

    ofstream ofile(wt_str_configs["output_file"].c_str());

    if( !cbc3be->rwCbcI2cRegs( 1, 1, wt_str_configs["cbci2c_file"], cbc_id ) )
	throw WaferTestException( test_name + " cbc i2c register configuration failed."); 

    unsigned value = wt_int_configs["VCTH.vcth"];
    if( !cbc3be->rwCbcI2cRegsByName( 1, 1, "VCTH.vcth", value, cbc_id ) )
	throw WaferTestException( test_name + " VCTH.vcth writing failed."); 

    value = wt_int_configs["40MHzClockOr254DLL.dll"];
    if( !cbc3be->rwCbcI2cRegsByName( 1, 1, "40MHzClockOr254DLL.dll", value, cbc_id ) )
	throw WaferTestException( test_name + " 40MHzClockOr254DLL.dll writing failed."); 

    int ne1(0);
    int ne2(0);
    bool e1=false;
    bool e2=false;
    bool no_hit = true;

    vector<uint32_t> fscbuf_data;
    fscbuf_data.push_back((unsigned) 0xa0200000);
    fscbuf_data.push_back((unsigned) 0xa0011000);// fast reset
    fscbuf_data.push_back((unsigned) 0xa0df0000);
    fscbuf_data.push_back((unsigned) 0xa0010200);
    fscbuf_data.push_back((unsigned) 0xa0200100);// T1
    fscbuf_data.push_back((unsigned) 0xe0000000);
    cbc3be->WriteFscBram(fscbuf_data);

    int N = 2600;
    int n = N/2;
    uint32_t *rawdata = new uint32_t[N];
    int *slvs5 = new int[n];
    cbc3be->WriteBeBoardConfig("cbc_system_cnfg.rdb_ctrl.write_block_size", n); 

    bool hit[32][254];
    for(unsigned ch=0; ch < 254; ch++){
	for(unsigned bi=0; bi<32; bi++){
	    hit[bi][ch]=false;
	}
    }
    run_rdb_and_fsc( cbc3be, rawdata, N );
    get_slvs5(rawdata, slvs5, n);
    int hp=0;
    int ofst_incr=0;
    for(unsigned bi=0; bi<32; bi++){
	bool this_no_hit(true);
	if(bi!=0) ofst_incr = 35;
	hp = find_data_frame_header_position(slvs5, n, hp + ofst_incr);
	if(hp<0) {
	    dump_raw_data(rawdata,N, os);
	    delete rawdata;
	    delete slvs5;
	    ofile.close();
	    oss.str("");
	    oss << "header_error at Buffer[" << bi << "]";
	    throw WaferTestException( test_name + oss.str() );
	    break;
	}
	get_error_bits(slvs5, hp, e1, e2);
	ne1+=e1;
	ne2+=e2;
	for(int ch=1; ch < 255; ch++){
	    if( has_hit(slvs5, hp, ch) ){
		hit[bi][ch]=true;
		this_no_hit = false;
		break;
	    }
	}
	if(!this_no_hit) no_hit = false; 
    }
    delete rawdata;
    delete slvs5;
    print_buff_hit(hit,ofile);
    ofile.close();

    if( !(ne1 == 0 && ne2 == 0 && no_hit )){

	oss.str("");
	if( !(ne1==0 && ne2 == 0) ){
	    oss << "error bit stats # of e1 = " << std::dec << ne1 << ", # of e2 = " << ne2;
	}
	if(!no_hit){
	    oss << ", some hits are found.";
	}
	throw WaferTestException( test_name + oss.str() ); 
    }
}
void frame_19b( const std::string &test_name, Cbc3BeFc7 *cbc3be, ostream &os,
	std::map<std::string,std::string> &wt_str_configs, 
	std::map<std::string,int> &wt_int_configs, 
	int debug ){ 

    print_test(test_name, "Buffer check - check all error bits and data bits are 1.", cout);

    ostringstream oss;

    ofstream ofile(wt_str_configs["output_file"].c_str());

    unsigned value = wt_int_configs["VCTH.vcth"];
    if( !cbc3be->rwCbcI2cRegsByName( 1, 1, "VCTH.vcth", value, cbc_id ) )
	throw WaferTestException( test_name + " VCTH.vcth writing failed."); 

    value = wt_int_configs["40MHzClockOr254DLL.dll"];
    if( !cbc3be->rwCbcI2cRegsByName( 1, 1, "40MHzClockOr254DLL.dll", value, cbc_id ) )
	throw WaferTestException( test_name + " 40MHzClockOr254DLL.dll writing failed."); 
    cbc3be->SendFastSignal( "orbit_reset" ); 
    cbc3be->SendFastSignal( "reset" ); 

    value = wt_int_configs["FcCntrlCompBetaTrigLat.trig_lat"];
    if( !cbc3be->rwCbcI2cRegsByName( 1, 1, "FcCntrlCompBetaTrigLat.trig_lat", value, cbc_id ) )
	throw WaferTestException( test_name + " FcCntrlCompBetaTrigLat.trig_lat writing failed."); 

    //dump_i2cregs(cbc3be, os);
    //usleep(1000);

    int ne1(0);
    int ne2(0);
    bool e1=false;
    bool e2=false;
    bool all_hit = true;

    vector<uint32_t> fscbuf_data;
    fscbuf_data.push_back((unsigned) 0xa0200000);
//    fscbuf_data.push_back((unsigned) 0xa0011000);// fast reset
    fscbuf_data.push_back((unsigned) 0xa0200100);// T1.  It seems like that the buffer overflow error disappears when the buffer gets free space.
    fscbuf_data.push_back((unsigned) 0xa0010300);
    fscbuf_data.push_back((unsigned) 0xa0200100);// T1
    fscbuf_data.push_back((unsigned) 0xe0000000);

    cbc3be->WriteFscBram(fscbuf_data);


    int N = 2600;
    int n = N/2;
    uint32_t *rawdata = new uint32_t[N];
    int *slvs5 = new int[n];
    cbc3be->WriteBeBoardConfig("cbc_system_cnfg.rdb_ctrl.write_block_size", n); 

    bool hit[32][254];
    for(unsigned ch=0; ch < 254; ch++){
	for(unsigned bi=0; bi<32; bi++){
	    hit[bi][ch]=true;
	}
    }

    if( !run_rdb_and_fsc( cbc3be, rawdata, N ) ){

	delete rawdata;
	throw WaferTestException( test_name + " failed in run_rdb_and_fsc." );
    }
//    dump_raw_data(rawdata,N, os);
    get_slvs5(rawdata, slvs5, n);
    int hp=0;
    int ofst=0;
    for(int i=0; i < n; i++){ 
	if(slvs5[i] == 0){
	    ofst = i;
	    break;
	}
    }
    cout << "Header offset is " << ofst << endl;
    for(unsigned bi=0; bi<32; bi++){
	bool this_all_hit(true);
	if(bi!=0) ofst = 35;
	hp = find_data_frame_header_position(slvs5, n, hp + ofst);
	if(hp<0) {
	    dump_raw_data(rawdata,N, os);
	    delete rawdata;
	    delete slvs5;
	    oss.str("");
	    oss << "header_error at Buffer[" << bi << "]";
	    throw WaferTestException( test_name + oss.str() );
	    break;
	}
	get_error_bits(slvs5, hp, e1, e2);
	ne1+=e1;
	ne2+=e2;
	if( !(e1 && e2 )) cerr << "Buffer["<< bi << "] errors are not both 1 [e1,e2] = " << e1 << "," << e2 << endl;
	for(int ch=1; ch < 255; ch++){
	    if( !has_hit(slvs5, hp, ch) ){
		hit[bi][ch]=false;
		this_all_hit = false;
		break;
	    }
	}
	if(!this_all_hit) all_hit = false; 
    }
    delete rawdata;
    delete slvs5;
    print_buff_hit(hit,ofile);
    ofile.close();

    if( !(ne1 == 32 && ne2 == 32 && all_hit )){

	oss.str("");
	if( !(ne1==32 && ne2 == 32) ){
	    oss << "error bit stats # of e1 = " << std::dec << ne1 << ", # of e2 = " << ne2;
	}
	if(!all_hit){
	    oss << endl << " Some channels do not have hits.";
	    print_buff_hit(hit,os);
	}
	throw WaferTestException( test_name + oss.str() ); 
    }
}

void frame_19c( const std::string &test_name, Cbc3BeFc7 *cbc3be, ostream &os,
	std::map<std::string,std::string> &wt_str_configs, 
	std::map<std::string,int> &wt_int_configs, 
	int debug ){ 

    print_test(test_name, "Buffer check - check all pipeline address exists in all pipeline address buffer ram region.", os);

    ostringstream oss;

    ofstream ofile(wt_str_configs["output_file"].c_str());

    unsigned nwords = 14*32*256;
    uint32_t *buffer = new uint32_t[nwords];
    BeDataDecoder be_data_decoder;


    //pipeline
    int paddr_values[32][512];
    for(unsigned bi=0; bi<32; bi++){
	for(unsigned i=0; i<512; i++){
	    paddr_values[bi][i] = -1;
	}
    }
    vector<uint32_t> fscbuf_data;
    fscbuf_data.push_back((unsigned) 0xa0200000);
    fscbuf_data.push_back((unsigned) 0xa0011000);// fast reset
    fscbuf_data.push_back((unsigned) 0xa0200000);
    //	fscbuf_data.push_back((unsigned) 0xa0df0000);// pause pause cannot be divided??? check firmware
    fscbuf_data.push_back((unsigned) 0xa0010200);// raw data buffer trigger
    fscbuf_data.push_back((unsigned) 0xa0200100);// T1
    fscbuf_data.push_back((unsigned) 0xa5000000);
    fscbuf_data.push_back((unsigned) 0xe0000000);

    init_run(cbc3be);
    cbc3be->MinNumWordToWait(nwords);

    for(unsigned j=0; j<2;j++){
	for(unsigned i=0; i<256; i++){

	    unsigned pause = ((0x16+i+ j*256)<<16) | 0xa0000000;
	    fscbuf_data[2] = pause;
	    cbc3be->WriteFscBram(fscbuf_data);
	    cbc3be->StartFscBramController();
	}
	int read_nwords(0);
	if( cbc3be->WaitData( 1, false ) ){
	    read_nwords = cbc3be->ReadData( buffer );
	}
	if( read_nwords != (int)nwords ){
	    oss.str("");
	    oss << nwords << " words requested and " << read_nwords << " read.";
	    throw WaferTestException( test_name + " Data read failed." + oss.str() ); 
	}

	be_data_decoder.SetData( buffer, nwords * 4 );
	int cd_i(0);
	while( be_data_decoder.SetNextEvent() ){ 

	    while( be_data_decoder.SetNextCbc() ){
		const CbcDataPacketDecoder *cbc_dpd = be_data_decoder.GetCbcDataPacketDecoder();
		int paddr = cbc_dpd->PipelineAddress();
		int bi = cd_i % 32;
		int i = cd_i / 32 + j * 256;
		cd_i++;
		paddr_values[bi][i] = paddr;
	    }
	}
    }
    delete buffer;
    print_buff_pipeline(paddr_values, ofile);
    ofile.close();

    int addr(0);
    for(unsigned j=0; j<2;j++){
	for(unsigned i=0; i<256; i++){
	    addr = i + j * 256;
	    for(unsigned bi=0; bi<32; bi++){
		if(addr == 512) addr = 0;
		if(paddr_values[bi][i+j*256] != addr ) {
		    oss.str("");
		    oss << "Buffer[" << std::dec << std::setw(2) << std::setfill('0') << bi << "] iteration " 
			<< i << "+" << j << "*256 should have address " << addr << ", but " << paddr_values[bi][i+j*256] << " was found.";
		    throw WaferTestException( test_name + " buffer ram test for pipeline region failed. " + oss.str() ); 
		}
		addr ++;
	    }
	}
    }
}


void frame_19d( const std::string &test_name, Cbc3BeFc7 *cbc3be, ostream &os,
	std::map<std::string,std::string> &wt_str_configs, 
	std::map<std::string,int> &wt_int_configs, 
	int debug ){ 

    print_test(test_name, "Buffer check - check all Level 1 counter value exists in the all buffer ram region.", cout);

    ostringstream oss;

    ofstream ofile(wt_str_configs["output_file"].c_str());

    unsigned nwords = 14*32*16;
    uint32_t *buffer = new uint32_t[nwords];
    BeDataDecoder be_data_decoder;

    int l1a_count[32][512];
    for(unsigned bi=0; bi<32; bi++){
	for(unsigned i=0; i<512; i++){
	    l1a_count[bi][i] = -1;
	}
    }
    vector<uint32_t> fscbuf_data;
    fscbuf_data.push_back((unsigned) 0xa0200000);
    fscbuf_data.push_back((unsigned) 0xa0010200);// raw data buffer trigger
    fscbuf_data.push_back((unsigned) 0xa0200100);// T1
    fscbuf_data.push_back((unsigned) 0xa5000000);
    fscbuf_data.push_back((unsigned) 0xe0000000);

    init_run(cbc3be);
    cbc3be->SendFastSignal( "orbit_reset" ); 

    int cd_i(0);
    for(unsigned j=0; j<32;j++){

	for(unsigned i=0; i<16; i++){

	    cbc3be->WriteFscBram(fscbuf_data);
	    cbc3be->StartFscBramController();
	}
	int read_nwords(0);
	cbc3be->MinNumWordToWait(nwords);
	if( cbc3be->WaitData( 1, false ) ){
	    read_nwords = cbc3be->ReadData( buffer );
	}
	if( read_nwords != (int)nwords ){
	    oss.str("");
	    oss << nwords << " words requested and " << read_nwords << " read.";
	    throw WaferTestException( test_name + " Data read failed." + oss.str() ); 
	}

	be_data_decoder.SetData( buffer, nwords * 4 );
	while( be_data_decoder.SetNextEvent() ){ 

	    while( be_data_decoder.SetNextCbc() ){
		const CbcDataPacketDecoder *cbc_dpd = be_data_decoder.GetCbcDataPacketDecoder();
		int l1ac = cbc_dpd->L1ACounter();
		//cout << "CBC data " << cd_i << " counter " << l1ac << endl;
		int bi = cd_i % 32;
		int ci = cd_i / 32;
		l1a_count[bi][ci] = l1ac;
		cd_i++;
	    }
	}
	cbc3be->SendFastSignal( "trigger" ); 
	usleep(1);
	cbc3be->WriteBeBoardConfig( "cbc_system_ctrl.global.daq_reset", 1 );
    }
    delete buffer;
    print_buff_l1a_count(l1a_count, ofile);
    ofile.close();

    bool l1a_count_ok(true);
    int count(1);
    for(unsigned j=0; j<32; j++){
	for(unsigned i=0; i<16; i++){
	    for(unsigned bi=0; bi<32; bi++){
		if(count == 512 ) count = 0;
		if(count++ != l1a_count[bi][i+j*16]){
		    oss.str("");
		    oss << "Buffer[" << std::dec << std::setw(2) << std::setfill('0') << bi << "] iteration " 
		    << i << "+" << j << "*16 should have l1a count " << count-1 << ", but " << l1a_count[bi][i+j*16] << " was found.";
		    throw WaferTestException( test_name + " buffer ram test for l1a counter region failed." + oss.str() ); 
		    l1a_count_ok = false;
		    break;
		}
	    }
	}
	++count;
    }
}
void frame_20( const std::string &test_name, Cbc3BeFc7 *cbc3be, ostream &os,
	std::map<std::string,std::string> &wt_str_configs, 
	std::map<std::string,int> &wt_int_configs, 
	int debug ){ 

    print_test(test_name, "check the channel masking", cout);

    ostringstream oss;

    ofstream ofile(wt_str_configs["output_file"].c_str());

    if( !cbc3be->rwCbcI2cRegs( 1, 1, wt_str_configs["cbci2c_file"], cbc_id ) )
	throw WaferTestException( test_name + " cbc i2c register configuration failed."); 

    unsigned value = wt_int_configs["VCTH.vcth"];
    if( !cbc3be->rwCbcI2cRegsByName( 1, 1, "VCTH.vcth", value, cbc_id ) )
	throw WaferTestException( test_name + " VCTH.vcth writing failed."); 


    int nwords(14*2);
    uint32_t *buffer = new uint32_t[nwords];
    BeDataDecoder be_data_decoder;

    unsigned N(500);
    unsigned n = N/2;
    uint32_t *rawdata(0);
    rawdata = new uint32_t[N];
    cbc3be->WriteBeBoardConfig("cbc_system_cnfg.rdb_ctrl.write_block_size", n); 
    cbc3be->WriteBeBoardConfig( "cbc_system_ctrl.rdb_ctrl.reset", 1 );

    vector<uint32_t> fscbuf_data;
    fscbuf_data.push_back((unsigned) 0xa0200000);
    fscbuf_data.push_back((unsigned) 0xa0011000);//fast reset
    fscbuf_data.push_back((unsigned) 0xa0e00000);
    fscbuf_data.push_back((unsigned) 0xa0010100);//T1
    fscbuf_data.push_back((unsigned) 0xa0260000);
    fscbuf_data.push_back((unsigned) 0xa1000000);
    for(int a=31; a<64;a++){
	unsigned value = 0x81100000 | (a << 8);
	fscbuf_data.push_back(value);
    }
    fscbuf_data.push_back((unsigned) 0x22000000);
    // this much pause is still needed after the i2c command fifo becomes empty.
    // 1ms. -- RAL
    fscbuf_data.push_back((unsigned) 0xaffe0000);
    fscbuf_data.push_back((unsigned) 0xaffe0000);
    fscbuf_data.push_back((unsigned) 0xaffe0000);
    fscbuf_data.push_back((unsigned) 0xaffe0000);
    fscbuf_data.push_back((unsigned) 0xaffe0000);
    fscbuf_data.push_back((unsigned) 0xaffe0000);
    fscbuf_data.push_back((unsigned) 0xaffe0000);
    fscbuf_data.push_back((unsigned) 0xaffe0000);
    fscbuf_data.push_back((unsigned) 0xaffe0000);
    fscbuf_data.push_back((unsigned) 0xaffe0000);
    fscbuf_data.push_back((unsigned) 0xa0011000);//fast reset
    fscbuf_data.push_back((unsigned) 0xa0e00000);
    fscbuf_data.push_back((unsigned) 0xa0010200);
    fscbuf_data.push_back((unsigned) 0xa0010100);//T1
    fscbuf_data.push_back((unsigned) 0xe0000000);

    init_run(cbc3be);
    cbc3be->SendFastSignal( "orbit_reset" ); 
    cbc3be->WriteBeBoardConfig("cbc_system_ctrl.cbc_data_processors.cbc1.frame_counter_reset", 1);
    cbc3be->WriteFscBram(fscbuf_data);
    cbc3be->StartFscBramController();

    int read_nwords(0); 
    cbc3be->MinNumWordToWait(nwords);
    if( cbc3be->WaitData( 1, false ) ){
	read_nwords = cbc3be->ReadData( buffer );
    }
    if( read_nwords != (int)nwords ){
	oss.str("");
	oss << nwords << " words requested and " << read_nwords << " read.";
	throw WaferTestException( test_name + " Data read failed." + oss.str() ); 
    }
    be_data_decoder.SetData( buffer, nwords * 4 );
    while( be_data_decoder.SetNextEvent() ){ 

	while( be_data_decoder.SetNextCbc() ){
	    const CbcDataPacketDecoder *cbc_dpd = be_data_decoder.GetCbcDataPacketDecoder();
	    cbc_dpd->DumpData(os);
	}
    }
    /*
    read_nwords = cbc3be->ReadRawData( rawdata, N );
    dump_raw_data(rawdata,N, os);
    */
    reset_cbc_i2c_reply_fifos( hw );
    delete rawdata;
    delete buffer;
    //This is necessary after using fscbram to do i2c communication.
}

void stub_scan_test( const std::string &test_name, Cbc3BeFc7 *cbc3be, ostream &os,
	std::map<std::string,std::string> &wt_str_configs, 
	std::map<std::string,int> &wt_int_configs, 
	int debug ){ 

    string stub_test_type = wt_str_configs["stub_test_type"];
    print_test(test_name, string("stub address and bend check : ") + stub_test_type, cout);

    int nss = atoi(stub_test_type.substr( 0, 1 ).c_str());
    int nsw = atoi(stub_test_type.substr( 4, 1 ).c_str());

    ostringstream oss;

    if( wt_str_configs.find("cbci2c_file") != wt_str_configs.end() ){
	if( !cbc3be->rwCbcI2cRegs( 1, 1, wt_str_configs["cbci2c_file"], cbc_id ) )
	throw WaferTestException( test_name + " cbc i2c register configuration failed."); 
    }
    string name("stub_check.root");
    TFile *froot = new TFile(name.c_str(), "update");
    string hname = "h_addr_bend_";
    hname += stub_test_type; 
    TH2F *h_addr_bend = new TH2F( hname.c_str(), "; address from cbc; bend from cbc", 254, 1, 255, 16, 0, 16); 
    hname = "h_addr_";
    hname += stub_test_type; 
    TH2F *h_addr = new TH2F( hname.c_str(), "; address generated; address from cbc;", 254, 1, 255, 254, 1, 255); 
    hname = "h_bend_";
    hname += stub_test_type; 
    TH2F *h_bend = new TH2F( hname.c_str(), "; bend generated; bend from cbc;", 16, -0.5, 15.5, 16, -0.5, 15.5); 

    bool addr_ok[127];
    bool bend_ok[127][16];
    for(unsigned i=0; i<127; i++){
	addr_ok[i] = false;
	for(unsigned bi=0; bi<16; bi++){
	    bend_ok[i][bi] = false;
	}
    }

    int nwords(14);
    uint32_t *buffer = new uint32_t[nwords];
    BeDataDecoder be_data_decoder;

    unsigned N(500);
    unsigned n = N/2;
    uint32_t *rawdata(0);
    rawdata = new uint32_t[N];
    cbc3be->WriteBeBoardConfig("cbc_system_cnfg.rdb_ctrl.write_block_size", n); 
    usleep(1);
    cbc3be->WriteBeBoardConfig( "cbc_system_ctrl.rdb_ctrl.reset", 1 );

    init_run(cbc3be);


    int ni0(43);
    int nm(3);

    map<int,int> regs;
    for(int a=31; a<64;a++)regs[a] = 0;

    for(int i0 = 0; i0 < ni0; i0++){

	bool ok(true);
	for(int j0=-7; j0 < 8; j0++){

	    int stub_id[nm];
	    for( int im = 0; im < nm; im++)stub_id[im] = -1;

	    if( !(nsw%2) && (nss+nsw)%2 && j0 == -7 ) continue;

	    int nth_stub(0);
	    for( int im = 0; im < nm; im++){
//	    for( int im = 0; im < 1; im++){

		int i = i0 + ni0 * im;
		int j = i + j0;
		//if( im == 1 ) cout << "i, j=" << i << "," << j << endl;
		std::vector<int> chs = get_channels_for_stub(i,j,nss,nsw);
		/*
		for( unsigned ch_i=0; ch_i < chs.size(); ch_i++){
		    cout << chs.at(ch_i) << " ";
		}
		*/
		if( !valid_stub( i, j, nss, nsw ) ) continue;
		unsigned saddr = get_seed_address(i,nss);
		bool shift(false);
		if( !(nsw % 2 ) && (nss+nsw)%2 ) shift = true;
		unsigned sbend = get_stub_bend(i,j, shift);
//		cout << nth_stub << "shift=" << shift << " th stub is i,j=" << std::dec << i << "," << j << " addr,bend=0x" << std::hex << saddr << ",0x" << sbend << " [ch " << std::dec;
		stub_id[nth_stub++] = im;
		//modify the masks
		for( unsigned ch_i=0; ch_i < chs.size(); ch_i++){
		    //		    cout << chs.at(ch_i) << ",";

		    if(chs.at(ch_i)<1 || chs.at(ch_i)>254) continue;
		    int addr = 0x20 + (chs.at(ch_i)-1) / 8;
		    int value = 1 << ((chs.at(ch_i)-1) % 8);
		    if( regs.find(addr) == regs.end() )
			regs[addr] = value;
		    else
			regs[addr] |= value;
		}
		//		cout << "] ";
	    }
	    //	    cout << endl;
	    vector<uint32_t> fscbuf_data;
	    fscbuf_data.push_back((unsigned) 0xa0200000);
	    map<int,int>::iterator it_regs = regs.begin();
	    for(;it_regs != regs.end(); it_regs++){
		int a = it_regs->first;
		int v = it_regs->second;
		unsigned value = 0x81100000 | (a << 8) | v;
		fscbuf_data.push_back(value);
	    }
	    it_regs = regs.begin();
	    for(;it_regs != regs.end(); it_regs++){
		int a = it_regs->first;
		int v = it_regs->second;
		if( v == 0 ) regs.erase(a);
	    }
	    it_regs = regs.begin();
	    for(;it_regs != regs.end(); it_regs++){
		it_regs->second = 0;
	    }

	    //test position
	    fscbuf_data.push_back((unsigned) 0x22000000);
	    fscbuf_data.push_back((unsigned) 0xaffe0000);
	    fscbuf_data.push_back((unsigned) 0xaffe0000);
	    fscbuf_data.push_back((unsigned) 0xaffe0000);
	    fscbuf_data.push_back((unsigned) 0xaffe0000);
	    fscbuf_data.push_back((unsigned) 0xaffe0000);
	    fscbuf_data.push_back((unsigned) 0xaffe0000);
	    fscbuf_data.push_back((unsigned) 0xaffe0000);
	    fscbuf_data.push_back((unsigned) 0xaffe0000);
	    fscbuf_data.push_back((unsigned) 0xaffe0000);
	    fscbuf_data.push_back((unsigned) 0xaffe0000);
	    //fscbuf_data.push_back((unsigned) 0xa0010200);
	    fscbuf_data.push_back((unsigned) 0xa0011000);//fast reset
	    fscbuf_data.push_back((unsigned) 0xa0e00000);
	    fscbuf_data.push_back((unsigned) 0xa0010300);//T1
	    fscbuf_data.push_back((unsigned) 0xa0010000);
	    fscbuf_data.push_back((unsigned) 0xa0010000);
	    fscbuf_data.push_back((unsigned) 0xa0010000);
	    fscbuf_data.push_back((unsigned) 0xa0010000);
	    fscbuf_data.push_back((unsigned) 0xa0010000);
	    fscbuf_data.push_back((unsigned) 0xa0010000);
	    fscbuf_data.push_back((unsigned) 0xa0010000);
	    fscbuf_data.push_back((unsigned) 0xe0000000);

	    /*
	       cbc3be->WriteFscBram(fscbuf_data);
	       cbc3be->StartFscBramController();
	       */

	    cbc3be->WriteBeBoardConfig( "cbc_system_ctrl.rdb_ctrl.reset", 1 );
	    cbc3be->WriteFscBram(fscbuf_data);
	    cbc3be->StartFscBramController();
	    cbc3be->ReadRawData( rawdata, N, true );

	    int read_nwords(0); 
	    cbc3be->MinNumWordToWait(nwords);
	    if( cbc3be->WaitData( 1, false ) ){
		read_nwords = cbc3be->ReadData( buffer );
	    }
	    if( read_nwords != (int)nwords ){
		oss.str("");
		oss << nwords << " words requested and " << read_nwords << " read.";
		throw WaferTestException( test_name + " Data read failed." + oss.str() ); 
	    }
	    be_data_decoder.SetData( buffer, nwords * 4 );
	    while( be_data_decoder.SetNextEvent() ){ 

		while( be_data_decoder.SetNextCbc() ){
		    const CbcDataPacketDecoder *cbc_dpd = be_data_decoder.GetCbcDataPacketDecoder();
		    if(debug)cbc_dpd->DumpData(os);
		    //		    cbc_dpd->DumpData(os);
		    vector<unsigned> sp = cbc_dpd->StubPositions();
		    vector<unsigned> sb = cbc_dpd->StubBends();
		    for(unsigned s_i=0; s_i < sp.size(); s_i++){
			if(sp.at(s_i) == 0) continue;
			h_addr_bend->Fill( sp.at(s_i), sb.at(s_i) ); 
			//			for( int im = 0; im < 1; im++){
			int im = stub_id[s_i];
			int i = i0 + ni0 * im;
			int j = i + j0; 
			unsigned addr = get_seed_address(i,nss);
			bool shift(false);
			if( !(nsw % 2 ) && ((nss+nsw)%2) ) shift = true;
			unsigned bend = get_stub_bend(i,j, shift);
			//		cout << "stub found at addr=0x" << std::hex << sp.at(im) << "(" << addr << ")" << " bend=0x" << sb.at(s_i) << "(" << bend << ") for " << im << "th stub" << endl;
			if(sp.at(s_i) == addr){ 
			    addr_ok[i] = true;
			}
			else if(waddrs.find(addr) == waddrs.end() ){
			    cout << "stub[" << s_i << "]" << " found at a wrong place addr=0x" << std::hex << sp.at(s_i) << " bend=0x" << sb.at(s_i) << " for addr=0x" << addr << " bend=0x" << bend << endl;
			    ok = false;
			}
			if(sb.at(s_i) == bend){ 
			    if(sb.at(s_i)<0 || sb.at(s_i)>15){
				cout << "stub bend invalid value " <<  sb.at(s_i) << endl;
			    }
			    bend_ok[i][sb.at(s_i)] = true;
			}
			else if(waddrs.find(addr) == waddrs.end() ){
			    if( addr_ok[i] == false ) cout << "\t";
			    cout << "stub[" << s_i << "]" << " found with a wrong bend addr=0x" << std::hex << sp.at(s_i) << " bend=0x" << sb.at(s_i) << " for addr=0x" << addr << " bend=0x" << bend << endl;
			}
			h_addr->Fill( addr, sp.at(s_i));
			if(addr_ok[i])h_bend->Fill( bend, sb.at(s_i) );
		    }
		//    if(!ok)cbc_dpd->DumpData(os);
		}
	    }
//	    if(!ok)dump_raw_data(rawdata,N, os);
	 }
    }
    delete buffer;

    reset_cbc_i2c_reply_fifos( hw );

    h_addr_bend->Write(h_addr_bend->GetName(), TObject::kOverwrite);
    h_addr->Write(h_addr->GetName(), TObject::kOverwrite);
    h_bend->Write(h_bend->GetName(), TObject::kOverwrite);
    froot->Close();

    //check the result.
    bool stub_ok(true);
    stub_ok = addr_bend_check(addr_ok, bend_ok, nss, nsw, os);
//    if(!stub_ok) throw WaferTestException( test_name + " stub test failed." );
}


int main( int argc, char *argv[] )
{
    setLogLevelTo( uhal::Error() );
    //getting environment variables
    char *env_value;
    env_value = getenv( "IPB_CONNECTION_FILE" );
    if( env_value != NULL ) connection_file = env_value; 
    env_value = getenv( "IPB_BOARD_ID" );
    if( env_value != NULL ) id = env_value;
    env_value = getenv( "CBC3HAL_ROOT" );
    string cbc3hal_dir( env_value );

    string cbci2c_file;

    string output_dir = "";
    int dll(1);
    int debug(0);
    bool quick(false);
    string be_config_file("");
    //getting options
    int c;
    while(1)
    {
	static struct option long_options[] = { 
	    { "help"     , no_argument, 0, 'a' },
	    { "conFile"  , required_argument, 0, 'b' },
	    { "boardId"  , required_argument, 0, 'c' },
	    { "fc7Config", required_argument, 0, 'h' },
	    { "outDir"   , required_argument, 0, 'd' },
	    { "debug"    , required_argument, 0, 'e' },
	    { "dll"      , required_argument, 0, 'f' },
	    { "quick"    , no_argument, 0, 'g' },
	};
	int option_index = 0;
	c = getopt_long( argc, argv, "ab:c:d:e:f:g:h", long_options, &option_index );
	if( c == -1 ) {
	    break;
	}

	switch( c )
	{
	    case 'a':
		printHelp( argv );
		return 0;
	    case 'b':
		connection_file = optarg;
		break;
	    case 'c':
		id = optarg;
		break;
	    case 'd':
		output_dir  = optarg;
		break;
	    case 'e':
		debug = atoi(optarg);
		break;
	    case 'f':
		dll = atoi(optarg);
		break;
	    case 'g':
		quick = true;
		break;
	    case 'h':
		be_config_file = optarg;
		break;
	    default:
		abort();
	}
    }

    Cbc3BeFc7   *cbc3be(0);
    //create ipbus connection and read the node value
    try{
	ConnectionManager cm( string( "file://" ) + connection_file ); 
	hw = new HwInterface( cm.getDevice( id ) );
	cbc3be = new Cbc3BeFc7(debug);
	cbc3be->Initialize( connection_file, id );
	read_ipbus( hw, "cbc_system_stat.system.id" );
    }
    catch( const std::exception& e ){
	cout << e.what() << endl;
	cout << "IPBUS is not ready" << endl;
	return -1;
    }

    string test_name;
    map<string,string> wt_str_configs;
    map<string,int>    wt_int_configs;
    struct timeval t0, t, dt;
    unsigned value(0);
    char ofname[256];

    bool ok(true);
    CBCI2C_ITEM_DATA_MAP item_data;

    unsigned N(0);
    unsigned n(0);
    uint32_t *rawdata(0);
    int *slvs5(0);
    int hp(0), p(0), ofst(0);
    vector<uint32_t> fscbuf_data;

    uint32_t *buffer(0); 

    bool header_error=false;
    bool no_hit(true);
    bool all_hit(true);
    int l1cntr(-1);
    int paddr(-1);

    map<unsigned,TH1F*> hpipe_buf_map;
    map<unsigned,TH1F*> hl1c_buf_map;

    string name;
    char fname[256];
    char fname_root[256];
    TFile *froot(0);

    wt_func cbci2c_write_read_test; 

    try{
	//Frame 0 - initialization, file creation, CSV to VME DAQ board, ...
	//Frame 1 - no operation
	//Frame 2 turn HMP4040 on
	//Frame 3 - hard reset, FC7 configuration, CBC I2C communication initialization, data clock timing tuning with FCI delay 0. 
	if (be_config_file == "" ){
	    be_config_file = cbc3hal_dir + "/etc/wt/cbc3_system_config_wt.txt";
	}
	test_name = "FRAME[03 ]";
	print_test(test_name, string("FC7 configuration with ") + be_config_file, cout );
	cbc3be->ConfigureBeBoard( be_config_file, 0 );
	print_test(test_name, "END-----------------------------", cout);

	//Frame 4 - moved to Frame 11
	//Frame 5
	//stuck bits test - main bias settings 
	test_name = "FRAME[05 ]";

	wt_str_configs.clear();
	wt_str_configs["cbci2c_file"] = cbc3hal_dir + "/etc/wt/CBC_I2CREGS_CNFG_WT_FRAME5.txt";

	print_test(test_name, "Stuck bits test - main bias settings", cout);
	run_test( cbci2c_write_read_test, test_name, cbc3be, cout, wt_str_configs, wt_int_configs, debug );
	print_test(test_name, "END-----------------------------", cout);

	//Frame 6
	//JOHAN
	//set up external MUX for Keithley DMM to read VDDA
	test_name = "FRAME[06 ]";
	print_test( test_name, string( "TO BE IMPLEMENTED"), cout );
	print_test(test_name, "END-----------------------------", cout);

	//Frame 7
	//stuck bits test - main bias settings
	test_name = "FRAME[07 ]";

	wt_str_configs.clear();
	wt_str_configs["cbci2c_file"] = cbc3hal_dir + "/etc/wt/CBC_I2CREGS_CNFG_WT_FRAME7.txt";

	print_test(test_name, "Stuck bits test - main bias settings", cout);
	run_test( cbci2c_write_read_test, test_name, cbc3be, cout, wt_str_configs, wt_int_configs, debug );
	print_test(test_name, "END-----------------------------", cout);

	//Frame 8 - load default values for main bias settings
	test_name = "FRAME[08 ]";

	wt_str_configs.clear();
	wt_str_configs["cbci2c_file"] = cbc3hal_dir + "/etc/wt/CBC_I2CREGS_CNFG_WT_FRAME8.txt";

	print_test(test_name, "Stuck bits test - main bias settings", cout);
	run_test( cbci2c_write_read_test, test_name, cbc3be, cout, wt_str_configs, wt_int_configs, debug );
	print_test(test_name, "END-----------------------------", cout);

	//Frame 9 - no operation
	//Frame 10 - check VDDD and VLDOI currents (<50mA & <200mA)
	//JOHAN
	test_name = "FRAME[10 ]";
	print_test( test_name, string( "check VDDD(<52mA) and VLDOI(<200mA) currents. TO BE IMPLEMENTED"), cout );
	print_test(test_name, "END-----------------------------", cout);

	//Frame 11 (Mark's Frame 4, and Frame 11 are merged here. 
	//Just to see if the CBC data frame comes and header is found.
	wt_func frame_11;
	test_name = "FRAME[11 ]";

	wt_str_configs.clear();
	wt_str_configs["fscbram_file"] = cbc3hal_dir + "/etc/wt/FSC_CNFG_WT_FRAME4.txt"; 

	run_test(frame_11, test_name, cbc3be, cout, wt_str_configs, wt_int_configs, debug );
	print_test(test_name, "END-----------------------------", cout);

	//Frame 12 - no operation
	
	//Frame 13 - more stuck bits tests
	test_name = "FRAME[13a]";

	wt_str_configs.clear();
	wt_str_configs["cbci2c_file"] = cbc3hal_dir + "/etc/wt/CBC_I2CREGS_CNFG_WT_FRAME13_1.txt";
	print_test(test_name, "Stuck bits test - bend LUTs, channel offsets, channel mask 1", cout);
	run_test( cbci2c_write_read_test, test_name, cbc3be, cout, wt_str_configs, wt_int_configs, debug );
	print_test(test_name, "END-----------------------------", cout);

	test_name = "FRAME[13b]";
	wt_str_configs["cbci2c_file"] = cbc3hal_dir + "/etc/wt/CBC_I2CREGS_CNFG_WT_FRAME13_0.txt";
	print_test(test_name, "Stuck bits test - bend LUTs, channel offsets, channel mask 0", cout);
	run_test( cbci2c_write_read_test, test_name, cbc3be, cout, wt_str_configs, wt_int_configs, debug );
	print_test(test_name, "END-----------------------------", cout);

	//Frame 14
	//TO BE IMPLEMENTED
	//band gap tune
	test_name = "FRAME[14 ]";
	print_test( test_name, string( "band gap tune. TO BE IMPLEMENTED"), cout );
	print_test(test_name, "END-----------------------------", cout);
	
	//Frame 15
	//Repeat the after fuse checks
	wt_func frame_15;
	test_name = "FRAME[15 ]";
	wt_str_configs.clear();
	wt_str_configs["cbci2c_file"] = cbc3hal_dir + "/etc/wt/CBC_I2CREGS_CNFG_WT_FRAME15.txt";

	run_test(frame_15, test_name, cbc3be, cout, wt_str_configs, wt_int_configs, debug );
	print_test(test_name, "END-----------------------------", cout);

	//Frame 16
	//Pedestal offsets tuning
	wt_func frame_16;

	test_name = "FRAME[16 ]";
	wt_str_configs.clear();
	wt_str_configs["config_file"] = cbc3hal_dir + "/etc/wt/wt_run_config_frame16.txt";
	wt_str_configs["cbci2c_file"] = cbc3hal_dir + "/etc/wt/CBC_I2CREGS_CNFG_WT_FRAME16.txt";
	wt_str_configs["output_dir"] = output_dir;
	run_test(frame_16, test_name, cbc3be, cout, wt_str_configs, wt_int_configs, debug );
	print_test(test_name, "END-----------------------------", cout);

	if( !quick ){
	    //Frame 17a
	    test_name = "FRAME[17a]";
	    wt_str_configs.clear();
	    wt_str_configs["cbci2c_file"] = cbc3hal_dir + "/etc/wt/CBC_I2CREGS_CNFG_WT_FRAME17.txt";
	    wt_str_configs["output_dir"] = output_dir;
	    wt_str_configs["output_file"] = "vcthscan-pedestal.root"; 
	    print_test(test_name, "VCTH scan for the pedestal.", cout);
	    run_test(vcth_scan_test, test_name, cbc3be, cout, wt_str_configs, wt_int_configs, debug );
	    print_test(test_name, "END-----------------------------", cout);

	    //Frame 17b
	    test_name = "FRAME[17b]";

	    wt_str_configs.clear();
	    wt_str_configs["output_dir"] = output_dir;
	    cbc3be->rwCbcI2cRegsByName( 1, 0, "TestPulsePotentiometer.pot", value, 1 ); 
	    sprintf( ofname, "vcthscan-tppot%03d.root", value );
	    wt_str_configs["output_file"] = ofname; 

	    wt_int_configs.clear();
	    wt_int_configs["TestPulsePolEnAMux.tp_en"] = 1;
	    wt_int_configs["tp_en"] = 1;
	    print_test(test_name, string("VCTH scan for the test pulse"), cout);
	    run_test(vcth_scan_test, test_name, cbc3be, cout, wt_str_configs, wt_int_configs, debug );
	    print_test(test_name, "END-----------------------------", cout);
	}

	/*
	if( !cbc3be->rwCbcI2cRegs( 1, 1, "offset_tuning-be1cbc1.knt", cbc_id ) ){
	    throw WaferTestException( test_name + " cbci2c register configuration failed." ); 
	}
	*/

	//Frame 18
	test_name = "FRAME[18a]";
	wt_func frame_18a;

	wt_str_configs.clear();
	wt_str_configs["cbci2c_file"] = cbc3hal_dir + "/etc/wt/CBC_I2CREGS_CNFG_WT_FRAME17.txt";
	wt_str_configs["output_dir"] = output_dir;
	wt_int_configs.clear();
	wt_int_configs["VCTH.vcth"] = 200;
	run_test(frame_18a, test_name, cbc3be, cout, wt_str_configs, wt_int_configs, debug );
	print_test(test_name, "END-----------------------------", cout);

	test_name = "FRAME[18b]";
	wt_func frame_18b;

	wt_int_configs.clear();
	wt_int_configs["VCTH.vcth"] = 800;
	run_test(frame_18b, test_name, cbc3be, cout, wt_str_configs, wt_int_configs, debug );
	print_test(test_name, "END-----------------------------", cout);

	//Frame 19

	//Frame 19a hit all 0
	
	test_name = "FRAME[19a]";
	wt_str_configs.clear();
	wt_str_configs["cbci2c_file"] = cbc3hal_dir + "/etc/wt/CBC_I2CREGS_CNFG_WT_FRAME17.txt";
	wt_str_configs["output_file"] = "frame_19a.txt"; 

	wt_int_configs.clear();
	wt_int_configs["VCTH.vcth"] = 0;
	wt_int_configs["40MHzClockOr254DLL.dll"] = dll;

	run_test(frame_19a, test_name, cbc3be, cout, wt_str_configs, wt_int_configs, debug );
	print_test(test_name, "END-----------------------------", cout);


	//Frame 19b hit all 1
	test_name = "FRAME[19b]";
	wt_str_configs.clear();
	wt_str_configs["output_file"] = "frame_19b.txt"; 
	wt_int_configs.clear();
	wt_int_configs["VCTH.vcth"] = 800;
	wt_int_configs["40MHzClockOr254DLL.dll"] = dll;
	wt_int_configs["FcCntrlCompBetaTrigLat.trig_lat"] = 20;
	run_test(frame_19b, test_name, cbc3be, cout, wt_str_configs, wt_int_configs, debug );
	print_test(test_name, "END-----------------------------", cout);

	//Frame 19c stuck bit test at pipeline region in the buffer.
	test_name = "FRAME[19c]";
	wt_str_configs.clear();
	wt_str_configs["output_file"] = "frame_19c.txt"; 
	run_test(frame_19c, test_name, cbc3be, cout, wt_str_configs, wt_int_configs, debug );
	print_test(test_name, "END-----------------------------", cout);
	
	//Frame 19d stuck bit test at l1a counter region in the buffer.
	test_name = "FRAME[19d]";
	wt_str_configs.clear();
	wt_str_configs["output_file"] = "frame_19d.txt"; 
	run_test(frame_19d, test_name, cbc3be, cout, wt_str_configs, wt_int_configs, debug );
	print_test(test_name, "END-----------------------------", cout);

	//Frame 20 check the channel masking 
	test_name = "FRAME[20]";
	wt_str_configs.clear();
	wt_str_configs["cbci2c_file"] = cbc3hal_dir + "/etc/wt/CBC_I2CREGS_CNFG_WT_FRAME17.txt";
	wt_str_configs["output_file"] = "frame_20.txt"; 
	wt_int_configs.clear();
	wt_int_configs["VCTH.vcth"] = 800;
	run_test(frame_20, test_name, cbc3be, cout, wt_str_configs, wt_int_configs, debug );
	print_test(test_name, "END-----------------------------", cout);

	//Frame 21a stub test 1 strip seed & 1 strip window
	wt_str_configs.clear();
	wt_str_configs["cbci2c_file"] = cbc3hal_dir + "/etc/wt/CBC_I2CREGS_CNFG_WT_FRAME21.txt";

	for( int nss = 1; nss < 5; nss ++){

	    for( int nsw = 1; nsw < 5; nsw ++){

		ostringstream oss;
		oss << nss << "ss_" << nsw << "sw";

		test_name = "FRAME[21]";
		test_name += oss.str();
		wt_str_configs["stub_test_type"] = oss.str();
		run_test(stub_scan_test, test_name, cbc3be, cout, wt_str_configs, wt_int_configs, debug );
		print_test(test_name, "END-----------------------------", cout);
	    }
	}
    }
    catch( IpbusUtilsException &e ){
	cerr << e.what() << endl;
	return -1;
    }
    catch( CbcI2cRegIpbusException &e ){
	cerr << e.what() << endl;
	return -1;

    }
    catch( Cbc3BeException &e ){
	cerr << e.what() << endl;
	return -1;
    }
    catch( WaferTestException &e ){
	cerr << e.what() << endl;
	return -1;
    }

    return 0;
}
/*
   cbci2c_file = cbc3hal_dir + "/etc/wt/CBC_I2CREGS_INPUT_SEL.txt";
   ok = cbc3be->rwCbcI2cRegs( 1, 1, cbci2c_file, cbc_id );


   unsigned tmp_value(255);
   ok = cbc3be->rwCbcI2cRegsByName( 1, 0, "TestPulsePotentiometer.pot", tmp_value, 1 ); 
   item_data.clear();
   item_data["delay"] = 10;
   if( !cbc3be->writeAllCbcI2cRegsByType("TestPulseDelayGroup",item_data) ) return -1;
   item_data["vcth"] = 500; 
   if( !cbc3be->writeAllCbcI2cRegsByType("VCTH", item_data) ) return -1;

    string output_dir = wt_str_configs["output_dir"];
    char fname_root[256];
    sprintf(fname_root, "pipeline.root" );
    string name;
    if( output_dir == "" ) name = fname_root;
    else name = output_dir + "/" + fname_root;
    TFile *froot = new TFile (name.c_str(), "recreate");

    TH1F *hpipe = new TH1F("hpipe", ";Pipeline Address", 512, -0.5, 511.5);
    TH1F *hl1c = new TH1F("hl1c", ";L1A counter value", 512, -0.5, 511.5);
   */
