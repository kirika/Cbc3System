/*
 * Author : Kirika Uchida
 */
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <cstdio>
#include <signal.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <fstream>
#include <cstring>
#include <sstream>
#include <stdlib.h>
#include <sys/time.h>
#include <iomanip>
#include <getopt.h>
#include <uhal/uhal.hpp>
#include "ipbus_utils.h"
#include "Cbc3BeInterface.h"
#include "Cbc3BeFc7.h"
#include "CbcI2cRegIpbusInterface.h"

using namespace std;
using namespace uhal;
using namespace cbc3hal;
using namespace ipbutl;

int data_buffer_size = 65536;
string connection_file( "connections.xml" );
string id("board");
string cbci2creg_filelist_fname( "etc/CBC_I2CREGS_CNFG_FILE_LIST_SEU_TEST_FF.txt" );
string run_config_fname( "etc/run_config_seu.txt" );

bool run_end = false;
int ievt(0);
ofstream datfile;
int rawdata_fd(-1);
int errdata_fd(-1);
ofstream logfile;

string time_string(){

    time_t rawtime;
    struct tm * timeinfo;
    time (&rawtime);
    timeinfo = localtime (&rawtime);

    string TimeFormat = "%d %b %Y %H:%M:%S %Z";

    char Buffer[256];
    strftime(Buffer, sizeof(Buffer), TimeFormat.c_str(), timeinfo);

    return Buffer;
}

const uint32_t *evt_buf(0);
ssize_t   evt_size(0);

void close_files(bool save_event = true ){
    if(logfile.is_open()){
	logfile << "RUNEND AT " << time_string() << " TOTAL NUMBER OF EVENTS TAKEN IS " << ievt << endl;
	logfile.close();
    }
    if( rawdata_fd != -1 ){
	if(save_event && evt_size) write(rawdata_fd, evt_buf, evt_size);
	close (rawdata_fd);
    }
    if( errdata_fd != -1 ){
	close (errdata_fd);
    }
    if(datfile.is_open()){
	datfile << "RUNEND AT " << time_string() << " TOTAL NUMBER OF EVENTS TAKEN IS " << ievt << endl;
	datfile.close();
    }
}

void sig_handler(int signo)
{
    if(signo == SIGINT){
	close_files();
	run_end = true;
    }
    if(signo == SIGSEGV){
	close_files();
	run_end = true;
    }
}

void printHelp( char *argv[] )
{
    unsigned indent(50);
    cout << setfill(' ');
    cout << "usage: " << argv[0] << "\n" 
	<< setw(indent) << " --help                      "            << ": prints this help.\n" 
	<< setw(indent) << " --conFile  [connection file]"            << ": connection file name to use.\n" 
	<< setw(indent) << " --boardId  [id]             "            << ": board id to use.\n" 
	<< setw(indent) << " --debug    [value]          "            << ": debug value for Cbc3BeInterface.\n" 
	<< setw(indent) << " --run      [number]         "            << ": run number.\n" 
	<< setw(indent) << " --withHits                  "            << ": all channel hits are expected.\n" 
	<< setw(indent) << " --dataDir                   "            << ": data directory.\n" 
	<< "environment variables and the values if the environment variables are not set:\n"
	<< setw(indent) << "[connection file] : IPB_CONNECTION_FILE, " << connection_file << "\n"
	<< setw(indent) << "[id]              : IPB_BOARD_ID,        " << id << "\n"
	<< endl;

}
int main( int argc, char *argv[] )
{
    setLogLevelTo( uhal::Error() );
    //setLogLevelTo( Debug() );
    //getting environment variables
    char *env_value;
    env_value = getenv( "IPB_CONNECTION_FILE" );
    if( env_value != NULL ) connection_file = env_value; 
    env_value = getenv( "IPB_BOARD_ID" );
    if( env_value != NULL ) id = env_value;

    int debug(0);
    int run(0);
    bool withHits(false);
    string ofname("seu_test.dat");
    string rawfname("seu_test.raw");
    string logfname("seu_test.log");
    string dataDir("./");

    //getting options
    int c;
    while(1)
    {
	static struct option long_options[] = { 
	    { "help"    , no_argument, 0, 'a' },
	    { "conFile" , required_argument, 0, 'b' },
	    { "boardId" , required_argument, 0, 'c' },
	    { "debug"   , required_argument, 0, 'd' },
	    { "run"     , required_argument, 0, 'e' },
	    { "withHits", no_argument, 0, 'f' },
	    { "dataDir" , required_argument, 0, 'g' }
	};
	int option_index = 0;
	c = getopt_long( argc, argv, "ab:c:d:e:fg:", long_options, &option_index );
	if( c == -1 ) {
	    break;
	}

	switch( c )
	{
	    case 'a':
		printHelp( argv );
		return 0;
	    case 'b':
		connection_file = optarg;
		break;
	    case 'c':
		id = optarg;
		break;
	    case 'd':
		debug = strtol(optarg, 0, 0);
		break;
	    case 'e':
		run = strtol(optarg, 0, 0); 
		char tmp[256];
		sprintf( tmp, "seu_test_run%010d.dat", run );
		ofname = tmp;
		sprintf( tmp, "seu_test_run%010d.raw", run );
		rawfname = tmp;
		sprintf( tmp, "seu_test_run%010d.log", run );
		logfname = tmp;
		break;
	    case 'f':
		withHits = true;
		break;
	    case 'g':
		dataDir = optarg;
		break;
	    default:
		abort();
	}
    }

    rawfname = dataDir + "/" + rawfname;
    ofname   = dataDir + "/" + ofname;
    logfname = dataDir + "/" + logfname;

    rawdata_fd = open( rawfname.c_str(), O_WRONLY | O_CREAT, 0444);
    if(rawdata_fd == -1){
	perror("open");
	return -1;
    }
    errdata_fd = open( "err.raw", O_WRONLY | O_CREAT, 0444);
    if(errdata_fd == -1){
	perror("open");
	return -1;
    }
    datfile.open(ofname.c_str());

    logfile.open(logfname.c_str());



    HwInterface     *hw(0);
    Cbc3BeFc7   *cbc3be(0);
    //prepare ipbus communication and check the system id
    try{
	ConnectionManager cm( string( "file://" ) + connection_file ); 
	hw = new HwInterface( cm.getDevice( id ) );
	cbc3be = new Cbc3BeFc7( debug );
	cbc3be->Initialize( connection_file, id );
	//cbc3be->WriteCbcI2cRegsToDB();
	cbc3be->ReadId();
	cbc3be->PrintVersion(logfile);
    }
    catch( const std::exception& e ){
	logfile << e.what() << endl;
	logfile << "IPBUS is not ready" << endl;
	return -1;
    }

    if (signal(SIGINT, sig_handler) == SIG_ERR) {
	printf("\ncan't catch SIGINT\n");
    }
    if (signal(SIGSEGV, sig_handler) == SIG_ERR) {
	printf("\ncan't catch SIGSEGV\n");
    }

    cbc3be->ConfigureCbcs( cbci2creg_filelist_fname );

    int exp_nhits(0);
    int exp_nstubs(0);
    bool exp_sof(false);
    if(withHits){
	exp_nhits  = 254;
    }

    uint32_t *data_buffer;
    uint32_t *prev_data_buffer;
    data_buffer = new uint32_t[data_buffer_size];
    prev_data_buffer = new uint32_t[data_buffer_size];
    BeDataDecoder be_data_decoder;

    cbc3be->WriteBeBoardConfig( run_config_fname );
    cbc3be->InitializeRun();

    //cbc3be->ReadCbcConfig( cbci2creg_filelist_fname );
    cbc3be->ReadAllCbcConfig();
    print_regcnf( cbc3be->GetCbcI2cRegReplies(), logfile, 1 ); 
//    print_regcnf( cbc3be->GetCbcI2cRegReplies(), logfile, 5 ); 

    int min_nword(2000);
    cbc3be->MinNumWordToWait(min_nword);
    cbc3be->DataReadyFlagPollingInterval_usec(0);
    cbc3be->DumpBeBoardConfig(cout, Cbc3BeFc7::DUMP_LEVEL_DEBUG );
    /* this invoke fast_reset for a while. about 5 sec. */
    cbc3be->StartFastSignalGenerator();

    int leftBufferSize(0);
    bool reset_daq_flag(false);

    struct timeval t0, t, dt;
    gettimeofday(&t0,0);
    unsigned be_l1c(0);
    unsigned last_be_l1c(0);
    int prev_nwords(0);
    int prev_raddr(0);
    int prev2_raddr(0);
    uint32_t delim[14];
    for( int i=0; i < 14; i++ ) delim[i] = 0xFFFFFFFF;

    datfile << "RUNSTART AT " << time_string() << endl;

    int ncheck(0);
    while (!run_end){

	if(run_end) break;

	/*
	ncheck++;
	int nwords = 0;
	cbc3be->HasData(0);
	nwords = cbc3be->GetNumNextDataWords();
	if( nwords % 14 != 0 ){
	    cerr << "invalid nword = " << nwords << "." << endl;
	    close_files(false);
	}
	gettimeofday(&t,0);
	timersub(&t,&t0,&dt);
	if( dt.tv_sec > 60 ){
	    string evt_time = time_string();
	    logfile << "I " << evt_time << " " << std::dec << ncheck << " times checked." << endl; 
	    t0 = t;
	}
	*/

	if( cbc3be->WaitData( 1 ) ){


	    int next_data_nwords = cbc3be->GetNumNextDataWords();
	    /*
	    if( next_data_nwords >= cbc3be->GetDataBufferWordSize() ){
		logfile << "E " << time_string() << " DATA BUFFER OVERFLOW. " << std::dec << next_data_nwords << " WORDS IS REPORTED TO BE READ.  RESET IS SENT. " 
		    << ievt << " EVENTS TAKEN SO FAR." <<  endl;
		reset_daq_flag = true;
	    }	
	    int raddr = cbc3be->ReadBeBoardRegister( "cbc_system_stat.data_buffer.raddr" ); 

	    int nwords = leftBufferSize/4; 
	    if( nwords ) logfile << "I " << time_string() << " " << std::dec << nwords << " WORDS LEFT IN THE LAST DAQ." << endl; 
	    uint32_t *Data = &data_buffer[nwords];
	    nwords += cbc3be->ReadData( Data, data_buffer_size-nwords );
	    */
	    uint32_t *Data = data_buffer;
	    int nwords = cbc3be->ReadData( Data, next_data_nwords );
	    //    logfile << ++nth_read << " th read.  # of read words = " << nwords << endl;
	    /*
	    be_data_decoder.SetData( data_buffer, nwords * 4 );
//	    if( data_buffer[0] != 0x00262018 ){
	    if( data_buffer[0] != 0x0026100D ){
		write (errdata_fd, prev_data_buffer, prev_nwords * 4 );
		write ( errdata_fd, delim, 14*4);
		write (errdata_fd, data_buffer, nwords * 4 );
		logfile << "E " << time_string() << " EVENT HEADER " << data_buffer[0] << " AT THE BEGINNING OF THIS BLOCK OF DATA IS INVALID. THIS DATA BLOCK "
		    << std::dec << next_data_nwords << " WORDS ARE DISCARDED. raddr = " << raddr << " , prev raddr = "  << prev_raddr << " , prev2 raddr = " << prev2_raddr << " , "
		    << ievt << " EVENTS TAKEN SO FAR." <<  endl;
		int data_size = cbc3be->ReadDataBufferAll( Data );
		logfile << "E " << time_string() << " All data size = " << data_size << endl; 

		write (rawdata_fd, delim, 14*4);
		write (rawdata_fd, data_buffer, data_size * 4 );
		cbc3be->StopTrigger();
		write (rawdata_fd, delim, 14*4);
		string node = "cbc_system_ctrl.global.daq_reset";
		cbc3be->WriteBeBoardConfig(node,1);
	//	cbc3be->StartTrigger();
	//	continue;
		close_files(false);
		return -1;
	    }	
	    memcpy( prev_data_buffer, data_buffer, nwords*4 );
	    prev_nwords = nwords;
	    prev2_raddr = prev_raddr;
	    prev_raddr = raddr;
	    int this_evti(0);
	    while( be_data_decoder.SetNextEvent() ){ 

		string evt_time = time_string();

		this_evti++;

		be_l1c = be_data_decoder.BeL1ACounter();
		if( ievt != 0 ){
		    if( !( (be_l1c - last_be_l1c) == 1 || ( be_l1c == 0 && last_be_l1c == 0x1FFFFFFF ) ) ){
			logfile << "E " << evt_time << " BE L1A COUNTER JUMPED BY " << be_l1c - last_be_l1c << " FROM " << std::dec << last_be_l1c << " TO " << be_l1c << ". "
			    << ievt << " EVENTS TAKEN SO FAR." <<  endl;
//			write (rawdata_fd, data_buffer, nwords * 4 );
		    }
		}

		evt_buf = be_data_decoder.GetEventPacketPointer();
		evt_size = (ssize_t) be_data_decoder.EventPacketSize() * 4;

		bool error_flag(false);
		vector<int> pa, cntr;

		while( be_data_decoder.SetNextCbc() ){

		    if( !be_data_decoder.GetCbcEventPacketPointer() ){ 
			logfile << "E " << evt_time << " CbcDataPacketDecoder is invalid. " << std::dec 
			    << std::dec << this_evti << " TH EVENT IN THIS DAQ. RESET IS SENT. "
			    << ievt << " EVENTS TAKEN SO FAR." <<  endl;
			write (rawdata_fd, data_buffer, nwords * 4 );
			reset_daq_flag = true;
			break;
		    }

		    const CbcDataPacketDecoder *cbc_data_packet_decoder = be_data_decoder.GetCbcDataPacketDecoder();
		    //cbc_data_packet_decoder->DumpData(logfile);
		    int cbc_id = cbc_data_packet_decoder->Id(); 
		    if( cbc_data_packet_decoder->Error(0) ){
			logfile << "E " << evt_time << " Latency error found at CBC ID = " << cbc_id << endl;
			error_flag = true;
		    }	
		    if( cbc_data_packet_decoder->Error(1) ){
			logfile << "E " << evt_time << " Buffer overflow error found at CBC ID = " << cbc_id << endl;
			error_flag = true;
		    }
		    int nhits = cbc_data_packet_decoder->HitChannels().size();
		    if( nhits != exp_nhits ){
			logfile << "E " << evt_time << " # of hits " << std::dec << nhits << " does not match expected # " << std::dec << exp_nhits << " at CB ID = " << cbc_id << endl;
			error_flag = true;
		    }
		    vector<unsigned> sps = cbc_data_packet_decoder->StubPositions();
		    vector<unsigned> sbs = cbc_data_packet_decoder->StubBends();
		    int nstubs(0);
		    for(unsigned i=0; i<sps.size(); i++){
			if(sps.at(i) != 0 ) nstubs++;
			if(sbs.at(i) != 0 ) {
			    logfile << "E " << evt_time << " Stub bend is not 0 at CB ID = " << cbc_id << endl;
			    error_flag = true;
			}
		    }
		    if(nstubs != exp_nstubs){
			logfile << "E " << evt_time << " # of stubs " << std::dec << nstubs << " does not match expected # " << std::dec << exp_nstubs << " at CB ID = " << cbc_id << endl;
			error_flag = true;
		    }
		    bool sof = cbc_data_packet_decoder->TrigSof();
		    if( sof != exp_sof ){
			logfile << "E " << evt_time << " Stub overflow is " << sof << " at CB ID = " << cbc_id << endl;
			error_flag = true;
		    }
		    bool or254 = cbc_data_packet_decoder->TrigOr254();
		    if( or254 ){
			logfile << "E " << evt_time << " Or254 is reported  at CB ID = " << cbc_id << endl;
			error_flag = true;
		    }	
		    bool trig_err = cbc_data_packet_decoder->TrigErr();
		    if( trig_err ){
			logfile << "E " << evt_time << " Trigger Error is reported  at CB ID = " << cbc_id << endl;
			error_flag = true;
		    }
		    pa.push_back( cbc_data_packet_decoder->PipelineAddress() );
		    cntr.push_back( cbc_data_packet_decoder->L1ACounter() );
//		    if(pa[0] == 0) logfile << "E " << evt_time << " PipelineAddress is 0." << endl; 
		}
		if( reset_daq_flag ) break;

		//if( pa[0] != pa[1] ) { 
		//    logfile << "E " << evt_time << " Pipeline addresses do not match." << endl;
		//    error_flag = true; 
		//}
		//if( cntr[0] != cntr[1] ){ 
		//    logfile << "E " << evt_time << " L1A counters do not match." << endl;
		//    error_flag = true; 
		//}
		//

		ievt++;
		gettimeofday(&t,0);
		timersub(&t,&t0,&dt);
		//if( ievt % 100000 == 0 ) logfile << "I " << evt_time << " " << std::dec << ievt << " events has taken." << endl; 
		if( dt.tv_sec > 3600 ){
		    logfile << "I " << evt_time << " " << std::dec << ievt << " events has taken." << endl; 
		    t0 = t;
		}
//		if( error_flag || ievt %100000 == 0 ){
		if( error_flag ){
//		if( true ){
		    be_data_decoder.ResetEvent();
		    datfile << evt_time << endl; 
		    be_data_decoder.DumpEventBeData(datfile);
		    while( be_data_decoder.SetNextCbc() ){
			const CbcDataPacketDecoder *cbc_data_packet_decoder = be_data_decoder.GetCbcDataPacketDecoder();
			cbc_data_packet_decoder->DumpData(datfile);
		    }
		    write (rawdata_fd, evt_buf, evt_size);
		}
		if(reset_daq_flag) break;
		last_be_l1c = be_l1c;
	    }
	    */
	    if( reset_daq_flag ){
		cbc3be->StopTrigger();
		usleep(32);
		string node = "cbc_system_ctrl.global.daq_reset";
		cbc3be->WriteBeBoardConfig(node,1);
		cbc3be->StartTrigger();
		reset_daq_flag = false;
	    }
	}
    }
    return 0;
}
