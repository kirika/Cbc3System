/*
 * Author : Kirika Uchida
 */
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <cstdio>
#include <signal.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <fstream>
#include <cstring>
#include <sstream>
#include <stdlib.h>
#include <sys/time.h>
#include <iomanip>
#include <getopt.h>
#include <uhal/uhal.hpp>
#include "ipbus_utils.h"
#include "Cbc3BeInterface.h"
#include "Cbc3BeFc7.h"
#include "CbcI2cRegIpbusInterface.h"

using namespace std;
using namespace uhal;
using namespace cbc3hal;

int data_buffer_size = 65536;
string connection_file( "connections.xml" );
string id("board");
string cbci2creg_filelist_fname( "etc/CBC_I2CREGS_CNFG_FILE_LIST_SEU_TEST_FF.txt" );

bool run_end = false;
int ievt(0);
ofstream datfile;
int rawdata_fd(-1);
ofstream logfile;

string time_string(){

    time_t rawtime;
    struct tm * timeinfo;
    time (&rawtime);
    timeinfo = localtime (&rawtime);

    string TimeFormat = "%d %b %Y %H:%M:%S %Z";

    char Buffer[256];
    strftime(Buffer, sizeof(Buffer), TimeFormat.c_str(), timeinfo);

    return Buffer;
}

const uint32_t *evt_buf(0);
ssize_t   evt_size(0);

void sig_handler(int signo)
{
    if(signo == SIGINT){
	if(logfile.is_open()){
	    logfile << "RUNEND AT " << time_string() << " TOTAL NUMBER OF EVENTS TAKEN IS " << ievt << endl;
	    logfile.close();
	}
	if( rawdata_fd != -1 ){
	    if(evt_size) write(rawdata_fd, evt_buf, evt_size);
	    close (rawdata_fd);
	}
	if(datfile.is_open()){
	    datfile << "RUNEND AT " << time_string() << " TOTAL NUMBER OF EVENTS TAKEN IS " << ievt << endl;
	    datfile.close();
	}
	run_end = true;
    }
    if(signo == SIGSEGV){
	if(logfile.is_open()){
	    logfile << "RUNEND AT " << time_string() << " TOTAL NUMBER OF EVENTS TAKEN IS " << ievt << endl;
	    logfile.close();
	}
	if( rawdata_fd != -1 ){
	    if(evt_size) write(rawdata_fd, evt_buf, evt_size);
	    close (rawdata_fd);
	}
	if(datfile.is_open()){
	    datfile << "RUNEND AT " << time_string() << " TOTAL NUMBER OF EVENTS TAKEN IS " << ievt << endl;
	    datfile.close();
	}
	run_end = true;
    }
}

void printHelp( char *argv[] )
{
    unsigned indent(50);
    cout << setfill(' ');
    cout << "usage: " << argv[0] << "\n" 
	<< setw(indent) << " --help                      "            << ": prints this help.\n" 
	<< setw(indent) << " --conFile  [connection file]"            << ": connection file name to use.\n" 
	<< setw(indent) << " --boardId  [id]             "            << ": board id to use.\n" 
	<< setw(indent) << " --debug    [value]          "            << ": debug value for Cbc3BeInterface.\n" 
	<< setw(indent) << " --dataDir                   "            << ": data directory.\n" 
	<< "environment variables and the values if the environment variables are not set:\n"
	<< setw(indent) << "[connection file] : IPB_CONNECTION_FILE, " << connection_file << "\n"
	<< setw(indent) << "[id]              : IPB_BOARD_ID,        " << id << "\n"
	<< endl;

}
int main( int argc, char *argv[] )
{

    setLogLevelTo( uhal::Error() );
    //getting environment variables
    char *env_value;
    env_value = getenv( "IPB_CONNECTION_FILE" );
    if( env_value != NULL ) connection_file = env_value; 
    env_value = getenv( "IPB_BOARD_ID" );
    if( env_value != NULL ) id = env_value;

    int debug(0);
    int run(0);
    bool withHits(false);
    string logfname("read_test.log");
    string dataDir("./");

    //getting options
    int c;
    while(1)
    {
	static struct option long_options[] = { 
	    { "help"    , no_argument, 0, 'a' },
	    { "conFile" , required_argument, 0, 'b' },
	    { "boardId" , required_argument, 0, 'c' },
	    { "debug"   , required_argument, 0, 'd' },
	    { "run"     , required_argument, 0, 'e' },
	    { "withHits", no_argument, 0, 'f' },
	    { "dataDir" , required_argument, 0, 'g' }
	};
	int option_index = 0;
	c = getopt_long( argc, argv, "ab:c:d:e:fg:", long_options, &option_index );
	if( c == -1 ) {
	    break;
	}

	switch( c )
	{
	    case 'a':
		printHelp( argv );
		return 0;
	    case 'b':
		connection_file = optarg;
		break;
	    case 'c':
		id = optarg;
		break;
	    case 'd':
		debug = strtol(optarg, 0, 0);
		break;
	    case 'e':
		run = strtol(optarg, 0, 0); 
		char tmp[256];
		sprintf( tmp, "seu_test_run%010d.dat", run );
		ofname = tmp;
		sprintf( tmp, "seu_test_run%010d.raw", run );
		rawfname = tmp;
		sprintf( tmp, "seu_test_run%010d.log", run );
		logfname = tmp;
		break;
	    case 'f':
		withHits = true;
		break;
	    case 'g':
		dataDir = optarg;
		break;
	    default:
		abort();
	}
    }
