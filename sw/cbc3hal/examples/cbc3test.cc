/*
 * Author : Kirika Uchida
 */

#include <getopt.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <readline/readline.h>
#include <readline/history.h>
#include <cstdio>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <signal.h>
#include <uhal/uhal.hpp>
#include "ipbus_utils.h"
#include "CbcI2cRegIpbusInterface.h"
#include "Fc7SystemRegInterface.h" 
#include "Cbc3BeInterface.h"
#include "Cbc3BeFc7.h"
#include "fc7/MmcPipeInterface.hpp"
#include <TH1F.h>
#include <TPad.h>
#include <TStyle.h>
#include <TH2F.h>
#include <TFile.h>


#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 
#include <sys/types.h>
#include <sys/wait.h>

using namespace std;
using namespace uhal;
using namespace cbc3hal;
using namespace ipbutl;

string connection_file( "connections.xml" );
string id("board");
vector<string> ids;
HwInterface *hw(0);
Cbc3BeFc7 *cbc3be(0);
int error_flag(0);

class Cbc3TestException : public std::exception{
    public:
	Cbc3TestException()throw(){}
	Cbc3TestException( const std::string &estr )throw(){err_str = estr; }
	Cbc3TestException(const Cbc3TestException &e)throw(){ err_str = e.what(); }
	Cbc3TestException& operator= (const Cbc3TestException &e) throw(){ err_str = e.what(); return *this; }
	virtual ~Cbc3TestException()throw(){}
	virtual const char * what() const throw(){ return err_str.c_str(); }
    private:
	std::string err_str;
};
void print_test( const string &test_name, const string &log,  std::ostream &os){
    os << test_name << " " << log << endl;
}
void reset_data( uint32_t *rawdata, int *slvs6, unsigned N ){

    for(unsigned i=0; i<N; i++){
	rawdata[i] = 0;
	if(i%2) slvs6[i/2] = 0;
    }
} 
bool run_rdb_and_fsc( Cbc3BeFc7 *cbc3be, uint32_t *buffer, int nwords ){

    cbc3be->WriteBeBoardConfig( "cbc_system_cnfg.rdb_ctrl.write_block_size", nwords/2 );
    cbc3be->WriteBeBoardConfig( "cbc_system_ctrl.fast_signal_manager.fast_signal_generator_stop", 1 );
    cbc3be->WriteBeBoardConfig( "cbc_system_ctrl.fast_signal_manager.stop_trigger", 1 );
    cbc3be->WriteBeBoardConfig( "cbc_system_ctrl.global.daq_reset", 1 );
    cbc3be->WriteBeBoardConfig( "cbc_system_ctrl.fast_signal_manager.start_trigger", 1 ); 
    cbc3be->WriteBeBoardConfig( "cbc_system_ctrl.rdb_ctrl.reset", 1 );

    cbc3be->StartFscBramController();

    //usleep(1000);
    int read_nwords = cbc3be->ReadRawData( buffer, nwords );
    if( read_nwords != nwords )
	throw Cbc3TestException("ReadRawData() returned unexpected # of words read.");
}
void dump_raw_data( uint32_t *buffer, int n, std::ostream &os ){

    unsigned wpl(2);
    os << setbase(16);
    os << setfill('0');
    os << setw(8);
    os << setbase(16) << setfill('0') << setw(8) << 0 << ' ';
    for(int i=0; i < n; i++){
	os << setbase(16) << setfill('0') << setw(8) << buffer[i] << ' ';
	if( i % wpl == wpl - 1 ){
	    os << endl;
	    if( i != n-1) 
		os << setbase(16) << setfill('0') << setw(8) << i+1 << ' ';
	}
    }
}
void get_slvs5(const uint32_t *buffer, int *slvs5, int n ){

    for( int i=0; i < n; i++){
	slvs5[i] = (buffer[2*i+1] & 0x000000FF);
    }
}
void get_slvs6(const uint32_t *buffer, int *slvs6, int n ){

    for( int i=0; i < n; i++){
	slvs6[i] = (buffer[2*i+1] & 0x0000FF00) >> 8;
    }
}
int find_data_frame_header_position( int *slvs6, int n, int offset=0 ){

    for(int i=offset; i < n; i++){
	if( (slvs6[i] & 0xC0) == 0xC0 ) return i;
    }
    return -1;
}
void get_error_bits(int *slvs6, int hp, bool &e1, bool &e2){
    int p(hp);
    e1 = (slvs6[p] & 0x20) != 0;
    e2 = (slvs6[p] & 0x10) != 0;
}
int get_pipeline_address(int *slvs5, int hp){
    int p(hp);
    int paddr = (slvs5[p] & 0xF) << 5;  
    paddr |= (slvs5[++p] & 0xF8) >> 3; 
    return paddr;
}
int get_l1counter(int *slvs6, int hp){
    int p=hp+1;
    int l1cntr = (slvs6[p] & 0x07) << 6;
    l1cntr |= (slvs6[++p] & 0xFC) >> 2;  
    return l1cntr;
}

void dll_consecutive_trigger_test( const std::string &test_name, Cbc3BeFc7 *cbc3be, ostream &os,
	std::map<std::string,std::string> &str_configs, 
	std::map<std::string,int> &int_configs, 
	int debug ){ 

    ostringstream oss;

    unsigned cbc_id = int_configs["cbc_id"];
    if( !cbc3be->rwCbcI2cRegs( 1, 1, str_configs["cbci2c_file"], cbc_id ) ){
	throw Cbc3TestException( test_name + " cbci2c register configuration failed." ); 
    }
    unsigned value = int_configs["VCTH.vcth"];
    if( !cbc3be->rwCbcI2cRegsByName( 1, 1, "VCTH.vcth", value, cbc_id ) )
	throw Cbc3TestException( test_name + " VCTH.vcth writing failed."); 

    string name("dll_check.root");
    TFile *froot = new TFile(name.c_str(), "update");
    TH2F *h_l1cerr = new TH2F( "h_l1cerr", "Level 1 Counter errors;dll;header position from the previous header", 26, -0.5, 25.5, 50, -0.0, 49.5 );
    TH2F *h_paddr = new TH2F( "h_paddr", "Pipeline address errors;dll;header position from the previous header", 26, -0.5, 25.5, 50, -0.0, 49.5 );
    TH2F *h_err1 = new TH2F( "h_err1", "Error 1;dll;header position from the previous header", 26, -0.5, 25.5, 50, -0.0, 49.5 );
    TH2F *h_err2 = new TH2F( "h_err2", "Error 2;dll;header position from the previous header", 26, -0.5, 25.5, 50, -0.0, 49.5 );
    TH1F *h_header = new TH1F( "h_header", "Header errors;dll", 26, -0.5, 25.5 );
    TH1F *h_all = new TH1F( "h_all", "All errors;dll", 26, -0.5, 25.5 );

    unsigned addr(0x1c);
    cbc_i2c_command i2c_com;
    i2c_com.cbc_id = cbc_id;
    i2c_com.w =0;
    i2c_com.r =1;
    i2c_com.page=1;
    i2c_com.addr=addr;
    i2c_com.value=0; 
    if( !cbc3be->rwCbcI2cReg( i2c_com) )
	throw Cbc3TestException( test_name + " 40MHzClockOr254DLL.dll writing failed."); 
    os << "I2C value at page 1 address 0x1c is " << std::dec << i2c_com.value << endl;

    unsigned N(3000);//32 x 38 x 2 + alpha
    unsigned n = N/2;

    uint32_t *rawdata(0);
    int *slvs5(0);
    int *slvs6(0);

    rawdata = new uint32_t[N];
    slvs6   = new int[n];

    cbc3be->SendFastSignal( "orbit_reset" ); 
    cbc3be->SendFastSignal( "orbit_reset" ); 

   // sleep(10);
   for(unsigned ii=0; ii < 10; ii++){
    int l1c(0), last_l1c(0), diff_l1c(1);
    for(unsigned dll=0; dll < 26; dll++){
//    for(unsigned dll=0; dll < 2; dll++){
//	if(dll==0)continue;
	vector<uint32_t> fscbuf_data;
	unsigned value = 0x81100080 | (addr << 8) | dll;//slow command : 100, cbc_id : 00001, read/write : 00rw, page : 000p, addr/value: 0xaavv
//	cout << std::hex << "command to DLL set : " << std::setw(8) << std::setfill('0') << value << endl; 
	fscbuf_data.push_back(value);
	fscbuf_data.push_back((unsigned) 0x22000000);
	/*
	fscbuf_data.push_back((unsigned) 0xafff0000);
	*/
//	fscbuf_data.push_back((unsigned) 0xa0010200);
//	fscbuf_data.push_back((unsigned) 0xa0010000);
	if(int_configs["orbit_reset_on"] == 1 ){
	    fscbuf_data.push_back((unsigned) 0xa0010001);// orbit reset 
	}
	fscbuf_data.push_back((unsigned) 0xa0011000);// fast reset
	fscbuf_data.push_back((unsigned) 0xa0df0000);
	fscbuf_data.push_back((unsigned) 0xa0010200);
//	fscbuf_data.push_back((unsigned) 0xa0200000);
	fscbuf_data.push_back((unsigned) 0xa0200100);// T1
	fscbuf_data.push_back((unsigned) 0xe0000000);


	cbc3be->WriteFscBram(fscbuf_data);
	cbc3be->WriteBeBoardConfig( "cbc_system_ctrl.fast_signal_manager.start_trigger", 1 ); 
//	sleep(10);
	run_rdb_and_fsc(cbc3be, rawdata, N);

	//dump_raw_data(rawdata,N, os);
	get_slvs6(rawdata, slvs6, n);

	int ofst_incr=0;
	int hp(2),last_hp(0),diff_hp(38), last_diff_hp(38);
	bool e1(false), e2(false);
	int pa(0), last_pa(0), diff_pa(1);
	for(unsigned bi=0; bi<32; bi++){

	    if(bi!=0) ofst_incr = 35;
	    hp = find_data_frame_header_position(slvs6, n, hp + ofst_incr);
	    if(bi!=0 ) diff_hp = hp - last_hp;
	    else if( bi!=1 ){
		if(diff_hp != last_diff_hp) 
		    os << "DLL[" << std::dec << dll << "] BUFFER[" << std::dec << bi << "] The header position changed.  The header arrived after " << diff_hp << " clock cycle from the last header." << " [L1 count] " << l1c << endl; 
	    }	
	    last_diff_hp = diff_hp;
	    last_hp = hp;

	    if(ii < 10 && bi==0){
		os << "DLL[" << std::dec << dll << "] The first header is at " << std::dec << hp << endl; 
	    }
	    if(hp>=0){
		get_error_bits(slvs6, hp, e1, e2);
		pa = get_pipeline_address(slvs6, hp);
		l1c = get_l1counter(slvs6,hp);
		//os << "DLL[" << std::dec << dll << "] BUFFER[" << std::dec << bi << "] Level 1 counter : " << std::dec << l1c << endl;
		if(!(dll==0 && bi==0)){
		    if(l1c == 0){
			if( last_l1c == 511 ) diff_l1c = 1;
			else diff_l1c = l1c - last_l1c;
		    }
		    else{
			diff_l1c = l1c - last_l1c;
		    }
		    if(bi==0 && int_configs["orbit_reset_on"] == 1){
			diff_l1c = 1;
		    }
		}
		last_l1c = l1c;
		if(bi!=0){
		    diff_pa = pa - last_pa;
		}
		last_pa = pa;
	    }
	    /*
	    os << "DLL[" << dll << "] BUFFER[" << std::dec << bi << "] The header arrived after " << diff_hp << " clock cycle from the last header." << " [L1 count] " << l1c
		<< " diff_l1c=" << diff_l1c << " pipeline address  = " << pa << endl;
		*/
	    if(hp<0 || ( e1 || e2 ) || !(diff_hp == 37 || diff_hp == 38) || diff_l1c != 1 ) {
		if( ii < 10 )dump_raw_data(rawdata,N, os);
		oss.str("");
		oss << "ERROR AT DLL[" << std::dec << dll << "] BUFFER[" << std::dec << bi << "] "; 
		h_all->Fill(dll);
		if(hp < 0 ) {
		    oss << "header is not found.";
		    h_header->Fill(dll);
		}
		else if( !(diff_hp == 37 || diff_hp == 38) ){
		    oss << " header is in a wrong position.";
		    h_header->Fill(dll);
		}
		else{ 
		    if( e1 || e2 ){
			oss << " error bits are set e1=" << e1 << " e2=" << e2;
			if(e1) h_err1->Fill(dll, diff_hp);
			if(e2) h_err2->Fill(dll, diff_hp);
		    }
		    if( diff_pa != 1 ){
			oss << " Pipeline address is wrong.";
			h_paddr->Fill(dll, diff_hp);
		    }
		    if( diff_l1c != 1 ) {
			oss << " L1A counter is wrong.";
			h_l1cerr->Fill(dll, diff_hp);
		    }
		}
		oss << " The header arrived after " << diff_hp << " clock cycle from the last header." << " [L1 count] " << l1c;
		if(ii < 10 )os << oss.str() << endl;
		cbc3be->SendFastSignal( "reset" ); 
//		cbc3be->SendFastSignal( "orbit_reset" ); 

		break;
		/*
		delete rawdata;
		delete slvs6;
		throw Cbc3TestException( oss.str() );
		*/
	    }
	}
	/*
	if( !cbc3be->rwCbcI2cReg( i2c_com) )
	    throw Cbc3TestException( test_name + " 40MHzClockOr254DLL.dll writing failed."); 
	os << "DLL[" << dll << "] I2C value at page 1 address 0x1c is " << std::dec << i2c_com.value << endl;
	*/
    }
   }
    h_all->Write(h_all->GetName(), TObject::kOverwrite);
    h_l1cerr->Write(h_l1cerr->GetName(), TObject::kOverwrite);
    h_err1->Write(h_err1->GetName(), TObject::kOverwrite);
    h_err2->Write(h_err2->GetName(), TObject::kOverwrite);
    h_header->Write(h_header->GetName(), TObject::kOverwrite);
    h_paddr->Write(h_paddr->GetName(), TObject::kOverwrite);
    froot->Close();

    delete rawdata;
    delete slvs6;
}
// replace s2 to s3 in s1
std::string rstr( std::string s1, std::string s2, std::string s3 )
{
    std::string::size_type  pos( s1.find( s2 ) );

    while( pos != std::string::npos )
    {
	s1.replace( pos, s2.length(), s3 );
	pos = s1.find( s2, pos + s3.length() );
    }

    return s1;
}
void printHelp( char *argv[] )
{
    unsigned indent(50);
    cout << setfill(' ');
    cout << "usage: " << argv[0] << "\n" 
	<< setw(indent) << " --help                       "        << ": prints this help.\n" 
	<< setw(indent) << " --conFile  [connection file] "        << ": connection file name to use.\n" 
	<< setw(indent) << " --boardId  [id]              "        << ": board id to use.\n" 
	<< setw(indent) << " --debug    [value]           "        << ": debug value for Cbc3BeInterface.\n" 
	<< setw(indent) << " --orbit_reset_on             "        << ": enable orbit reset.\n" 
	<< setw(indent) << " --kinoko-canvas [host:port]  "       << ": drawing to kinoko-canvas is enabled.\n" 
	<< setw(indent) << " --loadSD                     "        << ": load user image on SD card.\n" 
	<< "environment variables and the values if the environment variables are not set:\n"
	<< setw(indent) << "[connection file] : IPB_CONNECTION_FILE, " << connection_file << "\n"
	<< setw(indent) << "[id]              : IPB_BOARD_ID,        " << id << "\n"
	<< endl;
}

int main( int argc, char *argv[] )
{
    setLogLevelTo( uhal::Error() );
    //getting environment variables
    char *env_value;
    env_value = getenv( "IPB_CONNECTION_FILE" );
    if( env_value != NULL ) connection_file = env_value; 
    env_value = getenv( "IPB_BOARD_ID" );
    if( env_value != NULL ) id = env_value;
    env_value = getenv( "CBC3HAL_ROOT" );
    string cbc3hal_dir( env_value );

    bool kc_en(false);
    bool print_version(false);
    int debug(0);
    bool load_sd(false);
    string arg_s;
    bool orbit_reset_on(false);

    //getting options
    int c;
    while(1)
    {
	static struct option long_options[] = { 
	    { "help"            , no_argument, 0, 'a' },
	    { "conFile"         , required_argument, 0, 'b' },
	    { "boardId"         , required_argument, 0, 'c' },
	    { "debug"           , required_argument, 0, 'e' },
	    { "orbit_reset_on"  , no_argument, 0, 'f' },
	    { "kinoko-canvas"   , required_argument, 0, 'g' },
	    { "loadSD"          , no_argument, 0, 'h' }
	};
	int option_index = 0;
	c = getopt_long( argc, argv, "ab:c:e:fg:h", long_options, &option_index );
	if( c == -1 ) {
	    break;
	}

	switch( c )
	{
	    case 'a':
		printHelp( argv );
		return 0;
	    case 'b':
		connection_file = optarg;
		break;
	    case 'c':
		id = optarg;
		break;
	    case 'e':
		debug = strtol(optarg, 0, 0);
		break;
	    case 'f':
		orbit_reset_on = true;
		break;
	    case 'g':
		kc_en = true;
		arg_s = optarg;
		break;
	    case 'h':
		load_sd = true;
		break;
	    default:
		abort();
	}
    }

    /*******************************************/
    //Argument is processed.
    /*******************************************/
    string arg_command;
    if( optind < argc ){
	arg_command = argv[optind++];
    }	
    if(load_sd){
	try{
	    std::string lPassword ( "RuleBritannia" );
	    std::string lFilename ( "UserImage.bin" );
	    ConnectionManager cm( string( "file://" ) + connection_file ); 
	    hw = new HwInterface( cm.getDevice( id ) );
	    fc7::MmcPipeInterface lNode = hw->getNode< fc7::MmcPipeInterface > ( "sysreg.buf_test" );
	    vector<string> lFiles = lNode.ListFilesOnSD ();
	    std::cout << std::endl;
	    std::cout << "Resetting FPGA and loading " << lFilename << " on initialisation..." << std::endl;
	    lNode.RebootFPGA ( lFilename , lPassword );
	    std::cout << "Complete." << std::endl;
	}
	catch ( std::exception& aExc )
	{

	    uhal::log ( uhal::Error() , "Exception " , uhal::Quote ( aExc.what() ) , " caught at " , ThisLocation() );
	    return 1;
	}
	return 0;
    }

    /*******************************************/
    //CBC3 backend board FC7 interface
    //prepare ipbus communication and check the system id
    /*******************************************/
    cbc3be = new Cbc3BeFc7( debug );
    cbc3be->Initialize( connection_file, id );
    hw = cbc3be->GetHwInterface(); 

    string test_name;
    map<string,string> str_configs;
    map<string,int>    int_configs;

    try{
//    string be_config_file = cbc3hal_dir + "/etc/wt/cbc3_system_config_wt.txt";
    string be_config_file = cbc3hal_dir + "/etc/cbc3_system_config.txt";
    test_name = "FC7 CONFIGURATION WITH CBC3 MODULE 0 SETTINGS AND CBC3 I2C BUS INITIALIZATION AND TIMING TUNING";
    print_test(test_name, string("FC7 configuration with ") + be_config_file, cout );
    cbc3be->ConfigureBeBoard( be_config_file, 0 );
    if( !cbc3be->rwCbcI2cRegs( 1, 1, "offset_tuning-be1cbc1.knt", 1 ) ){
	throw Cbc3TestException( test_name + " cbci2c register configuration failed." ); 
    }

    test_name = "DLL CONSECTIVE TRIGGER TEST";
    str_configs.clear();
    str_configs["cbci2c_file"] = cbc3hal_dir + "/etc/wt/CBC_I2CREGS_CNFG_WT_FRAME17.txt";
    int_configs.clear();
    int_configs["cbc_id"] = 1;
    int_configs["VCTH.vcth"] = 800;
    int_configs["orbit_reset_on"] = orbit_reset_on;

    dll_consecutive_trigger_test( test_name, cbc3be, cout,
	    str_configs, 
	    int_configs, 
	    debug ); 
    }
    catch(Cbc3BeException &e){

	cout << e.what() << endl;
    }

}
