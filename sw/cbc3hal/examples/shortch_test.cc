#include <getopt.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <readline/readline.h>
#include <readline/history.h>
#include <cstdio>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <signal.h>
#include <uhal/uhal.hpp>
#include "ipbus_utils.h"
#include "CbcI2cRegIpbusInterface.h"
#include "Fc7SystemRegInterface.h" 
#include "Cbc3BeInterface.h"
#include "Cbc3BeFc7.h"
#include "TH2F.h"
#include "TFile.h"

using namespace std;
using namespace uhal;
using namespace cbc3hal;
using namespace ipbutl;

HwInterface *hw(0);

/*
 * modify here
 */
std::string connection_file("etc/connections.xml");
std::string id("FC7_SINGLE_CBC3_64_CH");

int cbc_id(1);

class Cbc3TestException : public std::exception{
    public:
	Cbc3TestException()throw(){}
	Cbc3TestException( const std::string &estr )throw(){err_str = estr; }
	Cbc3TestException(const Cbc3TestException &e)throw(){ err_str = e.what(); }
	Cbc3TestException& operator= (const Cbc3TestException &e) throw(){ err_str = e.what(); return *this; }
	virtual ~Cbc3TestException()throw(){}
	virtual const char * what() const throw(){ return err_str.c_str(); }
    private:
	std::string err_str;
};
void printHelp( char *argv[] )
{
    unsigned indent(50);
    cout << setfill(' ');
    cout << "usage: " << argv[0] << "\n" 
	<< setw(indent) << " --help                                            "  << ": prints this help.\n" 
	<< setw(indent) << " --debug     [bit0:CBC3INTERFACE,bit1:CBCI2C,bit2:param tuning]"  << ": debug flag. \n" 
	<< setw(indent) << " --conFile   [connection file]                     "  << ": connection file name to use.\n" 
	<< setw(indent) << " --boardId   [id]                                  "  << ": board id to use.\n" 
	<< setw(indent) << " --fc7Config [file name]                           "  << ": fc7 config file to use.\n" 
	<< "environment variables and the values if the environment variables are not set:\n"
	<< setw(indent) << "[connection file] : IPB_CONNECTION_FILE, " << connection_file << "\n"
	<< setw(indent) << "[id]              : IPB_BOARD_ID,        " << id << "\n"
	<< endl;

}
void dump_raw_data( uint32_t *buffer, int n, std::ostream &os ){

    unsigned wpl(2);
    os << setbase(16);
    os << setfill('0');
    os << setw(8);
    os << setbase(16) << setfill('0') << setw(8) << 0 << ' ';
    for(int i=0; i < n; i++){
	os << setbase(16) << setfill('0') << setw(8) << buffer[i] << ' ';
	if( i % wpl == wpl - 1 ){
	    os << endl;
	    if( i != n-1) 
		os << setbase(16) << setfill('0') << setw(8) << i+1 << ' ';
	}
    }
}

void init_run(Cbc3BeFc7 *cbc3be){

    unsigned trig_lat(0);
    cbc3be->rwCbcI2cRegsByName(1,0,"FcCntrlCompBetaTrigLat.trig_lat", trig_lat, cbc_id );
//    cout << "Trigger Latency = " << std::dec << trig_lat << endl;

    char cnode[256];
    sprintf( cnode, "cbc_system_cnfg.cbc_data_processors.cbc%d.latencies.l1a", cbc_id );
    cbc3be->WriteBeBoardConfig( cnode, trig_lat ); 

    cbc3be->SendFastSignal( "reset" ); 
    cbc3be->WriteBeBoardConfig( "cbc_system_ctrl.fast_signal_manager.fast_signal_generator_stop", 1 );
    cbc3be->WriteBeBoardConfig( "cbc_system_ctrl.fast_signal_manager.stop_trigger", 1 );
    cbc3be->WriteBeBoardConfig( "cbc_system_ctrl.global.daq_reset", 1 );
    cbc3be->WriteBeBoardConfig( "cbc_system_ctrl.fast_signal_manager.start_trigger", 1 ); 

}

void get_slvs5(const uint32_t *buffer, int *slvs5, int n ){

    for( int i=0; i < n; i++){
	slvs5[i] = (buffer[2*i+1] & 0x000000FF);
    }
}
void get_slvs6(const uint32_t *buffer, int *slvs6, int n ){

    for( int i=0; i < n; i++){
	slvs6[i] = (buffer[2*i+1] & 0x0000FF00) >> 8;
    }
}

bool run_rdb_and_fsc( Cbc3BeInterface *cbc3be, uint32_t *buffer, int nwords ){

    cbc3be->WriteBeBoardConfig( "cbc_system_cnfg.rdb_ctrl.write_block_size", nwords/2 );
    cbc3be->WriteBeBoardConfig( "cbc_system_ctrl.fast_signal_manager.fast_signal_generator_stop", 1 );
    cbc3be->WriteBeBoardConfig( "cbc_system_ctrl.fast_signal_manager.stop_trigger", 1 );
    cbc3be->WriteBeBoardConfig( "cbc_system_ctrl.global.daq_reset", 1 );
    cbc3be->WriteBeBoardConfig( "cbc_system_ctrl.fast_signal_manager.start_trigger", 1 ); 
    cbc3be->WriteBeBoardConfig( "cbc_system_ctrl.rdb_ctrl.reset", 1 );

    cbc3be->StartFscBramController();

    usleep(1000);
    int read_nwords = cbc3be->ReadRawData( buffer, nwords );
    if( read_nwords != nwords )
	throw Cbc3TestException("ReadRawData() returned unexpected # of words read.");
}
int main( int argc, char *argv[] )
{
    setLogLevelTo( uhal::Error() );

    //getting environment variables
    char *env_value;
    env_value = getenv( "IPB_CONNECTION_FILE" );
    if( env_value != NULL ) connection_file = env_value; 
    env_value = getenv( "IPB_BOARD_ID" );
    if( env_value != NULL ) id = env_value;
    env_value = getenv( "CBC3HAL_ROOT" );
    string cbc3hal_dir( env_value );

    string be_config_file("");
    string cbci2c_file("");
    string output_dir = ("");
    int debug(0);
    int c;
    while(1)
    {
	static struct option long_options[] = { 
	    { "help"     , no_argument, 0, 'a' },
	    { "conFile"  , required_argument, 0, 'b' },
	    { "boardId"  , required_argument, 0, 'c' },
	    { "fc7Config", required_argument, 0, 'h' },
	    { "debug"    , required_argument, 0, 'i' }
	};
	int option_index = 0;
	c = getopt_long( argc, argv, "ab:c:d:e:f:g:h", long_options, &option_index );
	if( c == -1 ) {
	    break;
	}
	switch( c )
	{
	    case 'a':
		printHelp( argv );
		return 0;
	    case 'b':
		connection_file = optarg;
		break;
	    case 'c':
		id = optarg;
		break;
	    case 'i':
		debug = atoi(optarg);
		break;
	    case 'h':
		be_config_file = optarg;
		break;
	    default:
		abort();
	}
    }
    Cbc3BeFc7 *cbc3be = new Cbc3BeFc7( debug );
    cbc3be->Initialize( connection_file, id );

    if (be_config_file == "" ){
	be_config_file = cbc3hal_dir + "/etc/cbc3_system_config_wt.txt";
    }
    if (cbci2c_file == "" ){
	cbci2c_file = "offset_tuning-be1cbc1.knt";
    }
    try{
	//    string be_config_file = cbc3hal_dir + "/etc/cbc3_system_config_m0.txt";
	cbc3be->ConfigureBeBoard( be_config_file, 0 );
	if( !cbc3be->rwCbcI2cRegs( 1, 1, cbci2c_file, 1 ) ){
	    throw Cbc3TestException( " cbci2c register configuration failed." ); 
	}

	cbci2c_file = cbc3hal_dir + "/etc/CBC_I2CREGS_PAGE1_EXAMPLE.txt";
	if( !cbc3be->rwCbcI2cRegs( 1, 1, cbci2c_file, 1 ) ){
	    throw Cbc3TestException( " cbci2c register configuration failed." ); 
	}
	unsigned trig_lat(32);
	cbc3be->rwCbcI2cRegsByName(1,1,"FcCntrlCompBetaTrigLat.trig_lat", trig_lat, cbc_id );
	cout << "Trigger Latency = " << trig_lat << endl;

	unsigned pot(60);
	cbc3be->rwCbcI2cRegsByName( 1, 1, "TestPulsePotentiometer.pot", pot, cbc_id ); 
	unsigned tp_en(1);
	cbc3be->rwCbcI2cRegsByName( 1, 1, "TestPulsePolEnAMux.tp_en", tp_en, cbc_id ); 
	unsigned vcth(500);
	cbc3be->rwCbcI2cRegsByName( 1, 1, "VCTH.vcth", vcth, cbc_id ); 

	int Ncycle(100);
	unsigned nwords = 14 * 3 * Ncycle;
	uint32_t *buffer = new uint32_t[nwords];
	BeDataDecoder be_data_decoder;
	hw = cbc3be->GetHwInterface(); 

	unsigned N(1000);
	unsigned n = N/2;

	uint32_t *rawdata(0);
	int *slvs5(0);
	cbc3be->WriteBeBoardConfig("cbc_system_cnfg.rdb_ctrl.write_block_size", n); 
	rawdata = new uint32_t[N];

	TH2F *h = new TH2F("h_hit", ";channel with test pulse; channel with hit", 254, 0.5, 254.5, 254, 0.5, 254.5);

	const GROUP_CHANNELSET_MAP & gmap = Cbc3BeInterface::GetGroupChannelSetMap();  

	for(int tpg=0; tpg < 8; tpg++){

	    if( gmap.find(tpg) == gmap.end() ) continue;
	    const std::set<CHANNEL_ID> &chset = gmap.find(tpg)->second;

	    int ncbcdata(0);
	    init_run(cbc3be);

	    cout << "===================================================" << endl;
	    cout << "GROUP[" << tpg << "]" << endl;
	    cout << "---------------------------------------------------" << endl;
	    for(int i=0; i<Ncycle; i++){

		unsigned addr=0x0e;
		unsigned value=0xf0;//test pulse delay 15
		value |= ((tpg& 1) << 2 | (tpg& 2) | (tpg& 4) >> 2);

		value |= (0x81100000  | (addr << 8) | value);
		vector<uint32_t> fscbuf_data;

		fscbuf_data.push_back(value);
		fscbuf_data.push_back((unsigned) 0x22000000);// wait for cbci2c module to be free 
		fscbuf_data.push_back((unsigned) 0xa0010001);// orbit reset 
		fscbuf_data.push_back((unsigned) 0xa0011200);// fast reset
		fscbuf_data.push_back((unsigned) 0xa0030000);// puase

		fscbuf_data.push_back((unsigned) 0xa0010010);// test pulse 
		fscbuf_data.push_back((unsigned) 0xa0220000);// puase
		fscbuf_data.push_back((unsigned) 0xa0010100);// T1 

		fscbuf_data.push_back((unsigned) 0xa3200000);// puase
		fscbuf_data.push_back((unsigned) 0xa0010010);// test pulse 
		fscbuf_data.push_back((unsigned) 0xa0220000);// puase
		fscbuf_data.push_back((unsigned) 0xa0010100);// T1

		fscbuf_data.push_back((unsigned) 0xa3200000);// puase
		fscbuf_data.push_back((unsigned) 0xa0010010);// test pulse 
		fscbuf_data.push_back((unsigned) 0xa0220000);// puase
		fscbuf_data.push_back((unsigned) 0xa0010100);// T1

		fscbuf_data.push_back((unsigned) 0xe0000000);


		cbc3be->WriteFscBram(fscbuf_data);
		//cbc3be->WriteBeBoardConfig( "cbc_system_ctrl.rdb_ctrl.reset", 1 );
		cbc3be->StartFscBramController();
		/*
		int read_nwords = cbc3be->ReadRawData(rawdata,N);
		dump_raw_data(rawdata, N, cout);
		*/
	    }

	    cbc3be->MinNumWordToWait(nwords);
	    int read_nwords(0);
	    if( cbc3be->WaitData( 0, false ) ){
		read_nwords = cbc3be->ReadData( buffer );
	    }
	    if( read_nwords != (int)nwords )
		throw Cbc3TestException( " Data read failed."); 

	    be_data_decoder.SetData( buffer, nwords * 4 );
	    while( be_data_decoder.SetNextEvent() ){ 

		while( be_data_decoder.SetNextCbc() ){

		    ncbcdata++;
		    const CbcDataPacketDecoder *cbc_dpd = be_data_decoder.GetCbcDataPacketDecoder();
//		    cbc_dpd->DumpData(cout);
		    vector<unsigned> hits = cbc_dpd->HitChannels(); 
		    std::set<CHANNEL_ID>::const_iterator it_ch = chset.begin();
		    for(; it_ch != chset.end(); it_ch++){
			for(unsigned h_i=0; h_i < hits.size(); h_i++){
			    h->Fill( *it_ch, hits.at(h_i) ); 
			}
		    }
		}
	    }
	    reset_cbc_i2c_reply_fifos( hw );
	}
	TFile f("shortch_test.root", "recreate");
	h->Write();
	f.Close();
	delete h;
    }
    catch( Cbc3BeException &e ){
	cerr << e.what() << endl;
	return -1;
    }
    catch( Cbc3TestException &e ){
	cerr << e.what() << endl;
	return -1;
    }
}
