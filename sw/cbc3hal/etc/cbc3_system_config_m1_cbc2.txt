# Title: CBC SYSTEM CONFIGURATION FOR MODULE TEST 
# Fields: Node Value 
# FieldTypes: string int
#
cbc_system_cnfg.global.be.id       1
cbc_system_cnfg.global.cbc1.active 1
cbc_system_cnfg.global.cbc1.id     1
cbc_system_cnfg.global.cbc1.fe_id  1 
cbc_system_cnfg.global.cbc1.i2c_address  0x42
cbc_system_cnfg.global.cbc2.active 0
cbc_system_cnfg.global.cbc2.id     2
cbc_system_cnfg.global.cbc2.fe_id  1
cbc_system_cnfg.global.cbc2.i2c_address  0x5F 
cbc_system_cnfg.global.cbc3.active 0
cbc_system_cnfg.global.cbc3.id     3
cbc_system_cnfg.global.cbc3.fe_id  1 
cbc_system_cnfg.global.cbc3.i2c_address  0x5F 
cbc_system_cnfg.global.cbc4.active 0
cbc_system_cnfg.global.cbc4.id     4
cbc_system_cnfg.global.cbc4.fe_id  1
cbc_system_cnfg.global.cbc4.i2c_address  0x5F 
cbc_system_cnfg.global.cbc5.active 0
cbc_system_cnfg.global.cbc5.id     5
cbc_system_cnfg.global.cbc5.fe_id  2 
cbc_system_cnfg.global.cbc5.i2c_address  0x5F 
cbc_system_cnfg.global.cbc6.active 0
cbc_system_cnfg.global.cbc6.id     6
cbc_system_cnfg.global.cbc6.fe_id  2
cbc_system_cnfg.global.cbc6.i2c_address  0x5F 
cbc_system_cnfg.global.cbc7.active 0
cbc_system_cnfg.global.cbc7.id     7
cbc_system_cnfg.global.cbc7.fe_id  2 
cbc_system_cnfg.global.cbc7.i2c_address  0x5F 
cbc_system_cnfg.global.cbc8.active 0
cbc_system_cnfg.global.cbc8.id     8
cbc_system_cnfg.global.cbc8.fe_id  2
cbc_system_cnfg.global.cbc8.i2c_address  0x5F 
cbc_system_cnfg.io.fe1.cbc_sel     1 
cbc_system_cnfg.io.fe2.cbc_sel     5
#1:async_l1a, 2: fmc, 4:ipbus, 8: internal
cbc_system_cnfg.fast_signal_manager.fast_signal_enable                          0xC # ipbus and internal sources are enabled.
cbc_system_cnfg.fast_signal_manager.fast_signal_generator.enable                0xF # all fast signals are enabled in the internal fast signal generator 
#cbc_system_cnfg.global.test_out.1 0x0004                                            # trigger 
#cbc_system_cnfg.global.test_out.1 0x0006                                            # orbit reset 
cbc_system_cnfg.global.test_out.1 0x0005                                            # test pulse 
#cbc_system_cnfg.global.test_out.1 0x0007                                            # SCL 
cbc_system_cnfg.global.test_out.2 0x0008                                            # SDA to CBC
cbc_system_cnfg.global.misc.trigger_master_external 0                               # trigger master internal. 
cbc_system_cnfg.rdb_ctrl.cbc_id 1
cbc_system_cnfg.rdb_ctrl.latency 0
cbc_system_cnfg.rdb_ctrl.write_block_size 19456

