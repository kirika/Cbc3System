export CBC3HAL_ROOT=${CBC3SYSTEM}/sw/cbc3hal

for dir in include lib bin
do
	if [ ! -d ${CBC3HAL_ROOT}/${dir} ]; then
		mkdir ${CBC3HAL_ROOT}/${dir}	
	fi
done

export CACTUS_ROOT=/opt/cactus

export PATH=${CBC3HAL_ROOT}/bin:${CBC3HAL_ROOT}/scripts:${CACTUS_ROOT}/bin:${PATH}
export LD_LIBRARY_PATH=${CBC3HAL_ROOT}/lib:${CBC3SYSTEM}/fw/fc7_5.0.1/sw/fc7/fc7/lib/:${CACTUS_ROOT}/lib:${LD_LIBRARY_PATH}

export IPB_CONNECTION_FILE=${CBC3HAL_ROOT}/etc/connections.xml
#export IPB_BOARD_ID=FC7_SINGLE_CBC3_96_CH
export IPB_BOARD_ID=FC7_SINGLE_CBC3_64_CH
#export IPB_BOARD_ID=FC7_SINGLE_CBC3_64
#export IPB_BOARD_ID=FC7_SINGLE_CBC3_V1


