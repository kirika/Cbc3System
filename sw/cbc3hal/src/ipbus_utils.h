/*
 * Author : Kirika Uchida
 */

#ifndef __IPBUS_UTILS_H__
#define __IPBUS_UTILS_H__

#include <uhal/uhal.hpp>
#include <string>
#include <iostream>

namespace ipbutl{

    extern unsigned long n_read;
    extern unsigned long n_write;
    extern unsigned long n_readBlock;
    extern unsigned long n_writeBlock;

    class IpbusUtilsException : public std::exception{
	public:
	    IpbusUtilsException()throw(){}
	    IpbusUtilsException( const std::string &estr )throw(){err_str = estr; }
	    IpbusUtilsException(const IpbusUtilsException &e)throw(){ err_str = e.what(); }
	    IpbusUtilsException& operator= (const IpbusUtilsException &e) throw(){ err_str = e.what(); return *this; }
	    virtual ~IpbusUtilsException()throw(){}
	    virtual const char * what() const throw(){ return err_str.c_str(); }
	private:
	    std::string err_str;
    };


    uint32_t read_ipbus( uhal::HwInterface *hw, const std::string &node_name ); 
    void     write_ipbus( uhal::HwInterface *hw, const std::string &node, uint32_t value );  

    std::vector<uint32_t> readBlock_ipbus( uhal::HwInterface *hw, const std::string &node_name, unsigned nwords );
    void     readBlock_ipbus( uhal::HwInterface *hw, const std::string &node_name, unsigned nwords, void *buffer );
    void     writeBlock_ipbus( uhal::HwInterface *hw, const std::string &node_name, const std::vector<uint32_t> &data );

    void     read_ipbus( uhal::HwInterface *hw, std::map<std::string,uint32_t> &regmap ); 
    void     write_ipbus( uhal::HwInterface *hw, std::map<std::string,uint32_t> &regmap );  

    void     read_ipbus( uhal::HwInterface *hw, const std::vector<std::string> &nodes, std::vector<uint32_t> &values ); 

    void     read_ipbus( uhal::HwInterface *hw, const std::string fname, std::map<std::string,uint32_t> &regmap );
    std::map<std::string,uint32_t>     write_ipbus( uhal::HwInterface *hw, const std::string fname );

    std::vector<std::string>  get_nodes( uhal::HwInterface *hw, const std::string &expression ); 
    void     get_nodes( uhal::HwInterface *hw, const std::string &expression, const std::string &delimiter ); 
    void print_register( std::ostream &os, const std::string &node, uint32_t value );
    void print_registers( std::ostream &os, const std::map<std::string,uint32_t> &regmap );
    void print_registers( std::ostream &os, const std::vector<std::string> &nodes, const std::vector<uint32_t> &values );

    void     read_ipbus( uhal::HwInterface *hw, pugi::xml_node &node );
    int find_child_nodes( const uhal::Node &ipbus_node, pugi::xml_node &node );

    void pugixml_to_json( std::ostream &os, pugi::xml_node &node, std::string name, const std::string &indent = "" );

};
#endif
