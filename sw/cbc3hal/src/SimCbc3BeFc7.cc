#include "SimCbc3BeFc7.h"
#include "uhal/uhal.hpp"
#include <vector>
#include <iomanip>

using namespace std;
using namespace uhal;
using namespace cbc3hal;

SimCbc3BeFc7::SimCbc3BeFc7():
	Cbc3BeFc7(), fBeEventDataPacketEncoder( new BeEventDataPacketEncoder() ), fDataBuffer(0)
{
	fDataBuffer = new uint32_t[fDataBufferSize]; 
}

SimCbc3BeFc7::~SimCbc3BeFc7()
{
	delete fDataBuffer;
}
void SimCbc3BeFc7::InitializeRun()
{

	fBeEventDataPacketEncoder->Configure( BeFwId_SimCbc3BeFc7, CbcData_Frame, 1, 2 );
}

bool SimCbc3BeFc7::WaitData(unsigned timeout_sec, bool usec )
{
	int data_word_size = 25;
	int BeId(1), FeId(1), CbcId(1);
//	static int ievt(1);
	static int l1a_count(0);
	fNumNextDataWords = 0;

//	usleep(200000);
	usleep(1000);

	while( data_word_size - fNumNextDataWords >= fBeEventDataPacketEncoder->HeaderSize() + fBeEventDataPacketEncoder->MaxDataSize() ){  

		fBeEventDataPacketEncoder->ResetBuffer();
		fBeEventDataPacketEncoder->SetBeL1ACounter( ++l1a_count );

		CbcDataPacketEncoder *cbc_data_encoder = fBeEventDataPacketEncoder->GetCbcDataPacketEncoder();
		FeId = 1; CbcId = 1;
		cbc_data_encoder->ResetBuffer();
		cbc_data_encoder->SetError( 0, 0 );
		cbc_data_encoder->SetPipelineAddress( 7 );
		cbc_data_encoder->SetL1ACounter( l1a_count );
		//bool hit(0);
		if( l1a_count % 5 == 4 ){
		    for( int i=1; i <= 254; i++ ){
			if( i % 16 == 5 || i % 16 == 6 ){
			    //cout << i << endl;
			    cbc_data_encoder->SetChannelData( i, 1 );
			}
			else
			    cbc_data_encoder->SetChannelData( i, 0 );
		    }
		}
		else{
		    for( int i=1; i <= 254; i++ ){
			cbc_data_encoder->SetChannelData( i, 0 );
		    }
		}

		std::vector<int> stub_pos(0);
		std::vector<int> stub_bend(0);
		bool trig_sof(0), trig_or254(1), trig_err(0);
		stub_pos.push_back( 10 );
		stub_pos.push_back( 100 );
		stub_bend.push_back( 0xF );
		stub_bend.push_back( 0 );
		cbc_data_encoder->SetTrigData( stub_pos, stub_bend, trig_sof, trig_or254, trig_err );
		cbc_data_encoder->SetHeader(BeId, FeId, CbcId);
		fBeEventDataPacketEncoder->SetCbcDataPacket(); 
		cout << "CBC ID = " << CbcId << " data are encoded." << endl;
		FeId = 2; CbcId = 5;
		cbc_data_encoder->ResetBuffer();
		cbc_data_encoder->SetError( 0, 0 );
		cbc_data_encoder->SetPipelineAddress( 7 );
		cbc_data_encoder->SetL1ACounter( l1a_count );
		//bool hit(0);
		if( l1a_count % 5 == 4 ){
		    for( int i=1; i <= 254; i++ ){
			if( i % 16 == 3 || i % 16 == 4 ){
			    //cout << i << endl;
			    cbc_data_encoder->SetChannelData( i, 1 );
			}
			else
			    cbc_data_encoder->SetChannelData( i, 0 );
		    }
		}
		else{
		    for( int i=1; i <= 254; i++ ){
			cbc_data_encoder->SetChannelData( i, 0 );
		    }
		}

		cbc_data_encoder->SetTrigData( stub_pos, stub_bend, trig_sof, trig_or254, trig_err );
		cbc_data_encoder->SetHeader(BeId, FeId, CbcId);
		cout << "CBC ID = " << CbcId << " data are encoded." << endl;
		fBeEventDataPacketEncoder->SetCbcDataPacket(); 

		fBeEventDataPacketEncoder->SetHeader();
		int data_size = 4 *( fBeEventDataPacketEncoder->HeaderSize() + fBeEventDataPacketEncoder->DataSize() ); 
		cout << "Data Size/4 = " << data_size/4 << endl;
		memcpy( &fDataBuffer[fNumNextDataWords], fBeEventDataPacketEncoder->GetBuffer(), data_size );
		fNumNextDataWords += (fBeEventDataPacketEncoder->PacketSize() ); 
	}
	if( fNumNextDataWords ) return true;
	else return false;
}

std::vector<uint32_t> SimCbc3BeFc7::ReadData()
{
	cout << "ReadData()" << endl;
	std::vector<uint32_t> data(fNumNextDataWords);
	for( int i=0; i < fNumNextDataWords; i++ ) { data[i] = fDataBuffer[i];}
	return data;
}

int SimCbc3BeFc7::ReadData( uint32_t*& pData, int nword )
{
    if(nword && nword < fNumNextDataWords) fNumNextDataWords = nword;
    cout << "ReadData(pData)" << endl;
    for( int i=0; i < fNumNextDataWords; i++ ) { pData[i] = fDataBuffer[i]; cout << std::setw(8) << std::setfill('0') << std::right << std::hex << fDataBuffer[i] << endl;}
    return fNumNextDataWords;
}

