#ifndef __FC7SYSTEMREGINTERFSCE_H__
#define __FC7SYSTEMREGINTERFSCE_H__

#include <ostream>
#include <stdint.h>

namespace uhal{

	class HwInterface;
}

namespace fc7{

	std::string board_id( uint32_t value ); 
	std::string rev_id( uint32_t value );
	std::string version( uint32_t value );

	std::string read_board_id( uhal::HwInterface *hw );
	std::string read_version( uhal::HwInterface *hw );
	std::string read_rev_id( uhal::HwInterface *hw );

	void print_version( uhal::HwInterface *hw, std::ostream &os );
	void dump_config( uhal::HwInterface *hw, std::ostream &os );

}

#endif

