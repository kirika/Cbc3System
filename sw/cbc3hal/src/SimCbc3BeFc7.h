/*
 * Author : Kirika Uchida
 */

#ifndef __SIMCBC3BEFC7_H__
#define __SIMCBC3BEFC7_H__

#include "Cbc3BeFc7.h"
#include <iostream>

namespace cbc3hal{

	class SimCbc3BeFc7 : public Cbc3BeFc7{

		public:
			SimCbc3BeFc7();
			virtual ~SimCbc3BeFc7();
			virtual void InitializeRun();
			virtual void FinalizeRun(){}
			virtual bool HasData(){ return WaitData(); }
			virtual bool WaitData(unsigned timeout_sec = 0, bool usec = false );
			virtual std::vector<uint32_t> ReadData();
			virtual int ReadData( uint32_t*& Data, int nword=0 );

		protected:
			BeEventDataPacketEncoder *fBeEventDataPacketEncoder;
			uint32_t *fDataBuffer;
	};
}

#endif


