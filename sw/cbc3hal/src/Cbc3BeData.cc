/*
 * Author : Kirika Uchida
 */

#include <signal.h>
#include <cstdio>
#include <cstring>
#include <stdint.h>
#include "Cbc3BeData.h"
#include <iostream>
#include <iomanip>

using namespace std;
using namespace cbc3hal;

//function to get a char in data at byte position (pBytePosition) 
//static char Char( const char *buf, unsigned pBytePosition ){ return buf[pBytePosition]; }
//function to set a char in data at byte position (pBytePosition) 
//static void SetChar( char *buf, unsigned pBytePosition, char value ){ buf[pBytePosition] = value; }

//function to get a bit in data at bit position (pPosition) 
static bool Bit( const char *buf, unsigned pPosition ){ return ( buf[pPosition/8] >> ( pPosition%8 ) ) & 1; }
//function to set a bit in data at bit position (pPosition) 
static void SetBit( char *buf, unsigned pPosition, bool value ){ buf[pPosition/8] |= ( value << ( pPosition%8 ) ); }
//function to get the byte in data from bit position (pPosition) 
/*
static short Byte( const char *buf, unsigned pPosition )
{
    short val(0);
    for( unsigned i=0; i<8; i++ ) {
	val |= ( Bit( buf, pPosition+i ) << i );
    }
    return val;
}
*/
static short BitSwappedByte( const char *buf, unsigned pPosition )
{
    short val(0);
    for( unsigned i=0; i<8; i++ ) {
	val |= ( Bit( buf, pPosition+i ) << (7-i) );
    }
    return val;
}	

BeDataDecoder::BeDataDecoder(): 
    fData(0),fEvent(0),fCbcEvent(0),
    fDataSize(0),fEventCount(0),fCbcCount(0), fCbcDataPacketDecoder(0)
{
    fCbcDataPacketDecoderMap[CbcData_Frame] = new CbcFrameDataPacketDecoder();
}
void BeDataDecoder::SetData( const void *data, int data_size )
{
    //signal(SIGSEGV, SIG_DFL);
    fData = (const uint32_t *)data;
    fDataSize = data_size / 4;
    fDataSizeToDecode = fDataSize;

    fEventCount = 0;
    fEvent = fData;
}
int BeDataDecoder::SetNextEvent(){


    if( fEventCount ){
	if( fDataSizeToDecode > 0 ){
	    fEvent = &fEvent[EventPacketSize()]; 
	}
	else{
	    return 0;
	}
    }
    fDataSizeToDecode -= EventPacketSize();
    if( fDataSizeToDecode < 0 ){
	return 0;
    }
    fEventCount ++;

    fCbcDataPacketDecoder = fCbcDataPacketDecoderMap[CbcDataType()];

    fCbcCount = 0;
    fCbcEvent = &fEvent[Offset_EventData_CbcEventData];

    return EventPacketSize();
}
void BeDataDecoder::DumpEventBeData( ostream &os )const
{
    os << "--- Event data header ---" << endl;
    os << "Be Id : " << BeId(); 
    os << " | BeFrimwareType : " << BeFirmwareType(); 
    os << " | CbcDataType : "    << CbcDataType(); 
    os << " | # of CBCs : "      << NumCbcs(); 
    os << " | EventDataSize : "  << EventDataSize() << endl;
    os << "--- BE Event data ---" << endl;
    os << " BE status    : " << BeStatus() << endl;
    os << " L1ACounter   : " << BeL1ACounter() << endl;

}
int BeDataDecoder::SetNextCbc(){

    if( fCbcCount ){
	fCbcEvent = &fCbcEvent[fCbcDataPacketDecoder->PacketSize()]; 
    }
    if( fCbcDataPacketDecoder == 0 ){ 
	cout << "CbcDataPacketDecoder is invalid." << endl; 
	fCbcEvent = 0;
	return -1;
    }
    fCbcDataPacketDecoder->SetData(fCbcEvent);

    if( fCbcEvent >= &fEvent[EventPacketSize()] ){
	fCbcEvent = 0;
	return 0;
    }
    else{
	fCbcCount++;
	return fCbcDataPacketDecoder->PacketSize();
    }
}
void BeDataDecoder::ResetEvent(){
    fCbcCount = 0;
    fCbcEvent = &fEvent[Offset_EventData_CbcEventData];
}
const void*  BeDataDecoder::GetIncompleteEventBuffer(int &buffer_size)const
{ 
    if( fDataSizeToDecode == 0 ){
	buffer_size = 0;
	return 0;
    }
    else{
	buffer_size = ( EventPacketSize() + fDataSizeToDecode ) * 4; 
	return fEvent; 
    }
}
void CbcFrameDataPacketDecoder::DumpData( std::ostream &os )const
{
    os << "--- CBC data header ---" << endl;
    os << " Be Id : " << BeId();
    os << " | Fe Id : " << FeId();
    os << " | Cbc Id : " << Id();
    os << " | DataSize : " << std::dec << DataSize(); 
    os << endl;
    os << "--- CBC data ---" << endl;
    os << " Error[0,1]                : " << Error(0) << " " << Error(1) << endl;
    os << " PipelineAddress           : " << setw(3) << right << PipelineAddress() << endl; 
    os << " L1ACounter                : " << setw(3) << right << L1ACounter() << endl;
    os << " Channel hits[1-253](odd)  : ";
    for(int i=1; i<=254; i++ ){
	if( i%2 == 1 ){
	    os << ChannelData(i);
	}
    }
    os << endl;
    os << " Channel hits[2-254](even) : ";
    for(int i=1; i<=254; i++ ){
	if( i%2 == 0 ){
	    os << ChannelData(i);
	}
    }
    os << endl;
    std::vector<unsigned> stub_pos = StubPositions();
    std::vector<unsigned> stub_bend= StubBends();
    os << " Stubs[1,2,3](pos,bend)    : ";
    for(unsigned i=0; i<stub_pos.size(); i++ ){
	os << "(" << stub_pos.at(i) << "," << stub_bend.at(i) << ")" << "  ";
    }
    os << endl;
    os << " Trig[Sof, or254, Err]     : " << TrigSof() << " " << TrigOr254() << " " << TrigErr() << endl;
}	

std::vector<unsigned> CbcFrameDataPacketDecoder::DataFrame()const
{
    std::vector<unsigned> data_frame;
    char *buf = (char *)&fData[CbcData_Frame_HeaderSize];
    int offset = BitOffset_CbcDataTrigFrame_Error;
    for( unsigned i=0; i < 274; i++ ){
	unsigned bit = offset + i;
	char byte = buf[bit/8];
	if( ( byte >> (bit % 8) ) & 1 ){
	    //	cout << "data frame non zero " << i << endl;
	    data_frame.push_back(i);
	}
    }
    return data_frame;
}
bool CbcFrameDataPacketDecoder::Error( int i )const
{
    char *buf = (char *)&fData[CbcData_Frame_HeaderSize];
    int offset = BitOffset_CbcDataTrigFrame_Error;
    return Bit(buf, offset + i);
}
int CbcFrameDataPacketDecoder::PipelineAddress()const
{
    char *buf = (char *)&fData[CbcData_Frame_HeaderSize];
    int offset = BitOffset_CbcDataTrigFrame_PipelineAddress;

    return (int) (Bit(buf, offset + 8)) | (int) (BitSwappedByte(buf, offset)) << 1;
}
int CbcFrameDataPacketDecoder::L1ACounter()const
{
    char *buf = (char *)&fData[CbcData_Frame_HeaderSize];
    int offset = BitOffset_CbcDataTrigFrame_L1ACounter;
    return (int) (Bit(buf, offset + 8)) | (int) (BitSwappedByte(buf, offset)) << 1;
}
bool CbcFrameDataPacketDecoder::ChannelData( int channel_id )const
{
    char *buf = (char *)&fData[CbcData_Frame_HeaderSize];
    int channel_bit = BitOffset_CbcDataTrigFrame_ChannelData + (channel_id-1);
    return Bit( buf, channel_bit );
}
std::vector<unsigned> CbcFrameDataPacketDecoder::HitChannels()const
{
    std::vector<unsigned> hit_list;
    char *buf = (char *)&fData[CbcData_Frame_HeaderSize];
    for( unsigned i = 1; i <= 254; i++ ){
	int channel_bit = BitOffset_CbcDataTrigFrame_ChannelData + (i-1); 
	if( Bit( buf, channel_bit ) )
	    hit_list.push_back(i);
    }
    return hit_list;
}
std::vector<unsigned> CbcFrameDataPacketDecoder::StubPositions()const
{
    vector<unsigned> list;
    char *buf = (char *)&fData[CbcData_Frame_HeaderSize];
    unsigned offset = BitOffset_CbcDataTrigFrame_TrigData;
    for( unsigned i = 0; i < 3; i++ ){
	unsigned stub_pos(0);
	for( unsigned pos_bit=0; pos_bit < 8; pos_bit++ ){  
	    unsigned bit = Bit( buf, offset + i * 8 + pos_bit );
	    stub_pos |= ( bit << pos_bit );
	}
	//if( stub_pos != 0 )
	list.push_back(stub_pos);
    }
    return list;
}
std::vector<unsigned> CbcFrameDataPacketDecoder::StubBends()const
{
    vector<unsigned> list;
    char *buf = (char *)&fData[CbcData_Frame_HeaderSize];
    unsigned offset = BitOffset_CbcDataTrigFrame_TrigData + BitOffset_CbcTrigFrame_bend;
    for( unsigned i = 0; i < 3; i++ ){
	unsigned stub_bend(0);
	for( unsigned bend_bit=0; bend_bit < 4; bend_bit++ ){  
	    unsigned bit = Bit( buf, offset + i * 4 + bend_bit );
	    stub_bend |= ( bit << bend_bit );
	}
	list.push_back(stub_bend);
    }
    return list;
}
bool CbcFrameDataPacketDecoder::TrigSof()const{
    char *buf = (char *) &fData[CbcData_Frame_HeaderSize];
    return Bit( buf, BitOffset_CbcDataTrigFrame_TrigData + BitOffset_CbcTrigFrame_sof );
}

bool CbcFrameDataPacketDecoder::TrigOr254()const{
    char *buf = (char *) &fData[CbcData_Frame_HeaderSize];
    return Bit( buf, BitOffset_CbcDataTrigFrame_TrigData + BitOffset_CbcTrigFrame_or254 );
}
bool CbcFrameDataPacketDecoder::TrigErr()const{
    char *buf = (char *) &fData[CbcData_Frame_HeaderSize];
    return Bit( buf, BitOffset_CbcDataTrigFrame_TrigData + BitOffset_CbcTrigFrame_err );
}

BeEventDataPacketEncoder::BeEventDataPacketEncoder(): 
    fBuffer(0), fHeaderSize(EventData_HeaderSize), fMaxDataSize(0), fDataSize(BeEventDataSize),
    fNextCbcDataOffset(Offset_EventData_CbcEventData), fCbcCount(0),
    fBeFirmwareType(BeFwId_Cbc3BeFc7), fCbcDataType(CbcData_Frame),   
    fBeId(1), fNumCbcs(1), fCbcDataPacketEncoder(0)  
{
    //cout << "CbcData_Frame = " << CbcData_Frame << endl;
    fCbcDataPacketEncoderMap[CbcData_Frame] = new CbcFrameDataPacketEncoder();
    //cout << "The cbc data type [CbcDat_Hits] is not implemented yet." << endl;
}
void BeEventDataPacketEncoder::Configure( BeFirmwareTypeId befwtype, CbcDataTypeId id, int be_id, int ncbcs )
{
    //cout << "BeEventDataPacketEncoder Configure start" << endl;
    fBeFirmwareType = befwtype;
    cout << "Id = " << id << endl;
    fCbcDataPacketEncoder = fCbcDataPacketEncoderMap[id];
    //cout << fCbcDataPacketEncoder << endl;
    //cout << "BeEventDataPacketEncoder before CbcDataPacketEncoder->Configure()" << endl;
    fCbcDataPacketEncoder->Configure();
    cout << "# of CBC = " << ncbcs << endl;
    fNumCbcs = ncbcs;
    //cout << "BeEventDataPacketEncoder fMaxDataSize" << endl;
    fMaxDataSize = BeEventDataSize + fNumCbcs * ( fCbcDataPacketEncoder->HeaderSize() + fCbcDataPacketEncoder->MaxDataSize() );
    delete fBuffer;
    fBuffer = new uint32_t[fMaxDataSize];
    //cout << "BeEventDataPacketEncoder Configure finished" << endl;
}
void BeEventDataPacketEncoder::ResetBuffer()
{
    fDataSize = BeEventDataSize;
    fNextCbcDataOffset = Offset_EventData_CbcEventData;
    fCbcCount = 0;
    for( int i=0; i < fMaxDataSize; i++ ) fBuffer[i] = 0;
}

bool BeEventDataPacketEncoder::SetCbcDataPacket()
{
    if( fCbcCount >= fNumCbcs ) return false;
    else{
	int data_size = 4 *( fCbcDataPacketEncoder->HeaderSize() + fCbcDataPacketEncoder->DataSize() ); 
	memcpy( &fBuffer[fNextCbcDataOffset], fCbcDataPacketEncoder->GetBuffer(), data_size );
	fNextCbcDataOffset += fCbcDataPacketEncoder->PacketSize();
	fCbcCount++;
	fDataSize += fCbcDataPacketEncoder->PacketSize();
	return true;
    }
}

void CbcDataPacketEncoder::Configure()
{
    delete fBuffer;
    fBuffer = new uint32_t[fHeaderSize+fMaxDataSize];
}

void CbcDataPacketEncoder::ResetBuffer()
{
    for(int i=0; i < fHeaderSize+fMaxDataSize; i++ ) fBuffer[i]=0;
}

    CbcFrameDataPacketEncoder::CbcFrameDataPacketEncoder()
:CbcDataPacketEncoder() 
{
    fHeaderSize = CbcData_Frame_HeaderSize; 
    fMaxDataSize = CbcData_Frame_DataSize; 
    fDataSize = CbcData_Frame_DataSize;
}
void CbcFrameDataPacketEncoder::Configure()
{
    if( fBuffer == 0 ) fBuffer = new uint32_t[fHeaderSize+fMaxDataSize];
}
void CbcFrameDataPacketEncoder::SetError( bool value0, bool value1 )
{
    char *buf = (char *) &fBuffer[CbcData_Frame_HeaderSize];
    int offset = BitOffset_CbcDataTrigFrame_Error;
    SetBit( buf, offset+0, value0 ); 
    SetBit( buf, offset+1, value1 ); 
} 
void CbcFrameDataPacketEncoder::SetPipelineAddress( int value )
{
    char *buf = (char *) &fBuffer[CbcData_Frame_HeaderSize];
    for( int i=0; i < 9; i++ ){
	SetBit( buf, BitOffset_CbcDataTrigFrame_PipelineAddress + i, ( value >> (8-i) ) & 1 ); 
    }
}
void CbcFrameDataPacketEncoder::SetL1ACounter( int value )
{
    char *buf = (char *) &fBuffer[CbcData_Frame_HeaderSize];
    for( int i=0; i < 9; i++ ){
	SetBit( buf, BitOffset_CbcDataTrigFrame_L1ACounter + i, ( value >> (8-i) ) & 1 ); 
    }
}
void CbcFrameDataPacketEncoder::SetChannelData( int channel_id, bool value )
{
    char *buf = (char *) &fBuffer[CbcData_Frame_HeaderSize];
    SetBit( buf, BitOffset_CbcDataTrigFrame_ChannelData + channel_id - 1, value );
}
void CbcFrameDataPacketEncoder::SetTrigData( std::vector<int>& stub_pos, std::vector<int>& stub_bend, bool trig_sof, bool trig_or254, bool trig_err )
{
    char *buf = (char *) &fBuffer[CbcData_Frame_HeaderSize];
    for( unsigned istub=0; istub < stub_pos.size(); istub++){
	for( unsigned pos_bit=0; pos_bit < 8; pos_bit++ ){  
	    unsigned offset = BitOffset_CbcDataTrigFrame_TrigData;
	    SetBit( buf, offset + istub * 8 + pos_bit, (bool) ( ( stub_pos.at(istub) >> pos_bit ) & 1 ) );
	}
    }
    for( unsigned istub=0; istub < stub_bend.size(); istub++){
	for( unsigned bend_bit=0; bend_bit < 4; bend_bit++ ){  
	    unsigned offset = BitOffset_CbcDataTrigFrame_TrigData+BitOffset_CbcTrigFrame_bend ;
	    SetBit( buf, offset + istub * 4 + bend_bit, (bool) ( ( stub_bend.at(istub) >> bend_bit ) & 1 ) );
	}
    }
    SetBit( buf, BitOffset_CbcDataTrigFrame_TrigData + BitOffset_CbcTrigFrame_sof, trig_sof );
    SetBit( buf, BitOffset_CbcDataTrigFrame_TrigData + BitOffset_CbcTrigFrame_or254, trig_or254 );
    SetBit( buf, BitOffset_CbcDataTrigFrame_TrigData + BitOffset_CbcTrigFrame_err, trig_err );
}
