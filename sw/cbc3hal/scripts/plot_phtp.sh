#!/bin/bash

#root -q -b 'root_scripts/find_mean_pedestal.C+( "vcthscan", "be1cbc1")'
#for tppot in `seq 0 5 255` 
#for tppot in `seq 5 5 65` 
#for tppot in `seq 70 5 85` 
for tppot in `seq 100 5 190` 
#for tppot in 95 
do 
    tppot=`printf "%03d" $tppot`
    root -q -b 'root_scripts/find_mean_pulse_peak.C+( "vcthscan", "be1cbc1", "tppot'$tppot'")'
done

root -q -b root_scripts/plot_ph_tp.C+

