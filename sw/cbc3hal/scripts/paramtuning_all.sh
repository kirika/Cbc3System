#!/bin/bash

group_type=8
debug=0
POSITIONAL=()
while [[ $# -gt 0 ]]
do
    key="$1"

    case $key in
	-b|--be_cnfg)
	    be_system_cnfg_file=`readlink -f "$2"`
	    shift # past argument
	    shift # past value
	    ;;
	-c|--cbc_cnfg)
	    cbc_cnfg_file=`readlink -f "$2"`
	    shift
	    shift
	    ;;
	-r|--run_cnfg)
	    run_cnfg_file=`readlink -f "$2"`
	    shift
	    shift
	    ;;
	-g|--group_type)
	    group_type="$2"
	    shift
	    shift
	    ;;
	-d|--debug)
	    debug="$2"
	    shift
	    shift
	    ;;
    esac
done

if [ "$cbc_cnfg_file" = "" ]; then
    cbc_cnfg_file=${CBC3HAL_ROOT}/etc/CBC_I2CREGS_CNFG_FILE_LIST_FOR_PARAM_TUNINGS.txt
fi
if [ "$be_system_cnfg_file" = "" ]; then
    be_system_cnfg_file=${CBC3HAL_ROOT}/etc/cbc3_system_config.txt
fi
if [ "$run_cnfg_file" = "" ]; then
    run_cnfg_file=${CBC3HAL_ROOT}/etc/cbc_param_tunings_and_vcthscan_run_config.txt 
fi

if [ ! -d offset_tuning ]; then
    mkdir offset_tuning
fi

cd offset_tuning
##########################################
# offset_tuning
#-----------------------------------------
( set -o pipefail; time offset_tuning \
    --debug ${debug} \
    --configBe ${be_system_cnfg_file} --configCbc ${cbc_cnfg_file} \
    --configRun ${run_cnfg_file} \
    --group_type $group_type 2>&1 | tee offset_tuning.log );


if [ $? -ne 0 ]; then
    echo "offset_tuning failed."
    exit
fi

# output files are copied to files with dated name.
current_time=`date +%y%m%d-%H%M%S`
for cbc_id in 1 2 5 
do
    file=offset_tuning-be1cbc${cbc_id}.knt 
    if [[ -f $file ]]; then
	cp ${file} ${CBC3HAL_ROOT}/etc/${file}
	cp ${file} offset_tuning-be1cbc${cbc_id}-${current_time}.knt 
    fi
done

cp offset_tuning_be1.root offset_tuning_be1-${current_time}.root
cp offset_tuning_be1.root ${HOME}/public_html
cp offset_tuning.log offset_tuning-${current_time}.log
# offset tuning done.

#------------------------------------------
# vcthscan
#------------------------------------------
if [ ! -d vcthscan ]; then
    mkdir vcthscan 
fi

time vcthscan --outDir vcthscan --group_type $group_type \
	      --debug ${debug} \
	      2>&1 | tee vcthscan.log

# output files are copied to files with dated name.
ftype=vcthscan/vcthscan-pedestal
fname=${ftype}.root
cp $ftype.root $ftype-${current_time}.root 

# scurves are analyzed 
for cbc_id in `sed -n 's/^ *cbc_system_cnfg.global\.cbc\(.\)\.active *1/\1/p'  $be_system_cnfg_file` 
do
    root -q -b '${CBC3HAL_ROOT}/root_scripts/scurve_analyzer.C+("'${fname}'", "be1cbc'$cbc_id'" )'
done
# vcthscan done.
#------------------------------------------
cd ../
