#!/bin/bash

cbc_id=1
fname=current_cbci2creg_cbc${cbc_id}.txt
cbc3daq 'cbci2c reg r=1 file=etc/CBC_I2CREGS_DEFAULT.txt cbc_id='${cbc_id} > $fname 

cbci2creg_plot.py etc/CBC_I2CREGS_DEFAULT.txt $fname 

