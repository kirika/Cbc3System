#!/bin/bash
# Merging frame 4

cbc3daq <<EOF

cbci2c reg r=1 w=1 file=etc/CBC_I2CREGS_CNFG_WT_FRAME8.txt

#ipbus write node=cbc_system_cnfg.global.test_out.1 value=0x0004                                            # 
#ipbus write node=cbc_system_cnfg.fast_signal_manager.fast_signal_enable value=0xC
ipbus write node=cbc_system_ctrl.fast_signal_manager.fast_signal_generator_stop value=1
ipbus write node=cbc_system_ctrl.fast_signal_manager.stop_trigger value=1
ipbus write node=cbc_system_ctrl.global.daq_reset value=1
ipbus write node=cbc_system_ctrl.fast_signal_manager.start_trigger value=1


fscbramctrl write file=etc/FSC_CNFG_WT_FRAME4.txt 
ipbus write node=cbc_system_cnfg.rdb_ctrl.cbc_id value=1
ipbus write node=cbc_system_cnfg.rdb_ctrl.latency value=0
ipbus write node=cbc_system_cnfg.rdb_ctrl.write_block_size value=1000
EOF

sleep 1

cbc3daq <<EOF
rdb_ctrl reset
rdb_ctrl showstat
fscbramctrl start
rdb_ctrl showstat
ipbus read node=cbc_system_stat.cbc_data_processors.cbc1.data_frame_counter
ipbus readBlock node=rdb n=1000 wpl=2

EOF
