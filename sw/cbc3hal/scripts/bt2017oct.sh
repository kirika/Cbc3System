#!/bin/bash
#######################################
# beamtest program setting
RUNDIR_ROOT=${CBC3HAL_ROOT}/runs
BE_CONFIG=${CBC3HAL_ROOT}/etc/bt2917/cbc3_system_config.txt
CBCI2C_CONFIG=${CBC3HAL_ROOT}/etc/bt2917/cbci2c_file_list.txt
#######################################

function finish {

    echo "caut sigint"
    kill -INT "$daq_pid"
    wait "$daq_pid"
    if [[ $kc_pid -ne 0 ]];then
	kill -TERM "$kc_pid" > /dev/null 2>&1
    fi
#    wait "$kc_pid"
}

daq_pid=0
kc_pid=0
trap finish SIGINT
#trap finish SIGTERM

function usage {

    echo ${1} 

}

function get_time {

    date +"%d %b %Y %H:%M:%S %Z"  | tr -d '\n'
}

if [ $# -lt 1 ]; then
    echo [USAGE] ${0##*/} '[normal,write_test,nocanvas,l1lats,slats,vcths] [run config file] '
    exit
fi

mydir=${PWD}
runtype=${1}
run_config_file=${2}
if [[ ${run_config_file%%/*} != "" ]]; then
    run_config_file=${PWD}/${run_config_file}
fi
if [[ ! -f $run_config_file ]];then
    echo "The run config file ${run_config_file} does not exist."
    exit
fi


#--------------------------------------
# for logger
RED='\033[0;31m'
GREEN='\033[0;32m'
BLUE='\033[0;34m'
NC='\033[0m'
#--------------------------------------

#--------------------------------------
# cd to the directory for the run
#--------------------------------------
cd ${RUNDIR_ROOT}

nruns=`ls -1X | sed 'run????' | wc -l` 

if [ "$nruns" -ne 0 ]; then
    prev_run=`ls -1X | awk 'END {print $0}' | sed -n 's/run\([0-9]\+\):*/\1/p'`
    this_run=`echo ${prev_run} | awk '{printf("%04d",$1 + 1)}'`
else
    this_run=0001
fi


this_run_dir="run${this_run}"
mkdir ${this_run_dir}; cd ${this_run_dir}
logfile="run${this_run}.log"
datafile="run${this_run}.raw"
cbcplotfile="run${this_run}-cbc.eps"
echo "----------------------------------------------------------------------------------------" | tee -a ${logfile}
echo -e "${GREEN}["`get_time`"] RUN $this_run starting at ${PWD}${NC}" 2>&1 | tee -a ${logfile}

echo -e "${GREEN}["`get_time`"] FW info : ${BLUE}" \
`cbc3daq 'cbc3be read id'` \
`cbc3daq 'cbc3be read version'` 2>&1 | tee -a ${logfile}
echo -e "${NC}" | tr -d '\n'
echo "----------------------------------------------------------------------------------------" | tee -a ${logfile}

#--------------------------------------
# configurations and initializations 
#--------------------------------------

#--------------------------------------
# backend configuration 
#--------------------------------------
if [ "${run_config_file}" = "" ]; then
    run_config_file=${CBC3HAL_ROOT}/etc/bt2917/run_config.txt
fi
echo -e "${GREEN}["`get_time`"] backend configuration with files ${BE_CONFIG} and ${run_config_file}" 2>&1 | tee -a ${logfile}

# system config
cbc3daq_com='cbc3be configure '${BE_CONFIG} 
echo -e "${BLUE}["`get_time`"] cbc3daq $cbc3daq_com" | tee -a ${logfile}
cbc3daq ''"$cbc3daq_com" 2>&1 | tee -a ${logfile}
# run config
cbc3daq_com='ipbus write file='${run_config_file} 
cbc3daq ''"$cbc3daq_com" 2>&1 | tee -a ${logfile} 

# read back the config
cbc3daq_com='ipbus read node=cbc_system.* '${BE_CONFIG} 
outfile=be_config.txt
cbc3daq ''"$cbc3daq_com" 2>&1 > $outfile 
echo -e "${NC}" | tr -d '\n' 

#--------------------------------------
# cbc i2c registers
#--------------------------------------
cbci2c_cnfg_dir=${CBCI2C_CONFIG%/*}
cbc3daq_com='configureCbcs '${CBCI2C_CONFIG} 
echo -e "${BLUE}["`get_time`"] cbc3daq $cbc3daq_com" | tee -a ${logfile}
cbc3daq ''"$cbc3daq_com" 2>&1 | tee -a ${logfile}
#awk '!/^[#[:blank:]]/{print $0}' ${CBCI2C_CONFIG} > cbc_cnfg_list.txt
#while read line
#do
#    if [ "$comm" = "" ]; then
#
#	cbc_id=`echo $line | awk '{print $2}'`
#
#	filename=`echo $line | awk '{print $3}'`
#	filename=${cbci2c_cnfg_dir}/${filename} 
#	if [ ! -f "$filename" ];then
#	    echo -e "${RED}["`get_time`"] The cbc i2c register setting file : ${filename} for CBC$cbc_id does not exist.${NC}" | tee -a ${logfile}
#	    exit
#	fi
#	outfile=`echo $cbc_id | awk '{printf("cbci2c_cbc%02d.txt", $1)}'`
#
#	cbc3daq_com='cbci2c reg r=1 w=1 file='"${filename} cbc_id=${cbc_id}"
#	echo ""
#	echo -e "${BLUE}["`get_time`"] cbc3daq $cbc3daq_com${NC}" | tee -a ${logfile}
#	cbc3daq ''"$cbc3daq_com" > $outfile
#    fi
#done < cbc_cnfg_list.txt 
#rm cbc_cnfg_list.txt

if [ $runtype = "normal" ]; then

    kinoko-canvas --width=1500 --height=900 localhost 3001 > /dev/null 2>&1 &
#    kinoko-canvas --width=2300 --height=1200 localhost 3001 > /dev/null 2>&1 &
    kc_pid=$!
    sleep 1
    echo -e "${GREEN}["`get_time`"] normal data taking is starting.${NC}" | tee -a ${logfile}
    #################################################################################################################
    options="--data_file $datafile --fsg_en --min_nword 11200  --kc_en localhost:3001"
    echo -e "${GREEN}["'get_time'"] bt2017oct_daq ${options}.${NC}" | tee -a ${logfile}
    bt2017oct_daq ${options}  2>&1 | tee -a ${logfile} & 
    #################################################################################################################
    daq_pid=$!
    wait "$daq_pid"
    wait "$daq_pid"
    mogrify -format png $cbcplotfile
#    wait "$kc_pid"
#    wait "$kc_pid"
elif [[ $runtype = "write_test" ]]; then
    echo -e "${GREEN}["`get_time`"] data taking with no analysis without canvas is starting.${NC}" | tee -a ${logfile}
    #################################################################################################################
#    options="--data_file $datafile --write_test --fsg_en --min_nword 40000  --polint_usec 10" #optimal at 500kHz
#    options="--data_file $datafile --write_test --fsg_en --min_nword 50000  --polint_usec 10" #optimal at 400kHz
    options="--data_file $datafile --write_test --fsg_en --min_nword 5200  --polint_usec 1000" 
    echo -e "${GREEN}["'get_time'"] bt2017oct_daq ${options}.${NC}" | tee -a ${logfile}
    bt2017oct_daq ${options}  2>&1 | tee -a ${logfile} & 
    #################################################################################################################
    daq_pid=$!
    wait "$daq_pid"
    wait "$daq_pid"

elif [[ $runtype = "nocanvas" ]]; then
    echo -e "${GREEN}["`get_time`"] data taking without canvas is starting.${NC}" | tee -a ${logfile}
    #################################################################################################################
    options="--data_file $datafile --fsg_en --min_nword 6400" 
    echo -e "${GREEN}["'get_time'"] bt2017oct_daq ${options}.${NC}" | tee -a ${logfile}
    bt2017oct_daq ${options}  2>&1 | tee -a ${logfile} & 
    #################################################################################################################
    daq_pid=$!
    wait "$daq_pid"
    wait "$daq_pid"
elif [ $runtype = "l1lats" ]; then
    echo -e "${GREEN}[["`get_time`"] l1a latency scan${NC}" | tee -a ${logfile}
elif [ $runtype = "slats" ]; then
    echo -e "${GREEN}[["`get_time`"] stub latency scan${NC}" | tee -a ${logfile}
elif [ $runtype = "vcths" ]; then
    echo -e "${GREEN}[["`get_time`"] vcth scan${NC}" | tee -a ${logfile}
else
    echo -e "${RED}["`get_time`"] the run ${runtype} is not supported${NC}" | tee -a ${logfile}
fi

cd ${mydir}

