#!/usr/bin/python

import sys
import os
import time
import socket

markersize = 3
def print_usage( myname ):
    print '%s [file1] (file2)' % (myname)

def plot_regs ( filename, index, color ):

    ctrlregs = []
    ofstregs = []
    
    f = open( filename, 'r')
    
    for line in f:
        if len(line) < 2 or line[0] == '#' or line[0] == ' ':
	   continue
        items = line.split()
        page = int(items[0],16)
        addr = int(items[1],16)
        value = int(items[2],16)
        if page == 1 or addr == 0:
	   ctrlregs.append([addr,value])
        else:
	   ofstregs.append([addr,value])
    
    x_min = 0 
    x_max = 90 
    n_xgrid = 8 
    y_min = 0
    y_max = 300
    n_ygrid = 5
    command = '.create plot p{0} 0 10 60 45;'.format(index)
    command += 'p{0} set title Page 1; p{0} set xtitle address; p{0} set ytitle value;'.format(index)
    command = command + 'p%d frame %f %f %f %f;' % (index,x_min, x_max, y_min, y_max)
    command = command + 'p%d set color %s;' % (index,color)
    command = command + 'p%d set marker diamond;' % (index)
    command = command + 'p%d set markersize %d;' % (index, markersize)
    command = command + 'p%d plot ' % (index)
    for reg in ctrlregs:
        command = command + '%d %d '% (reg[0], reg[1])
    command = command +  ';'
    
    grid_steps = (x_max - x_min)/ (n_xgrid+1)
    command = command + 'p{0} set color black;'.format(index)
    command = command + 'p{0} set linestyle dot;'.format(index)
    for i in range(n_xgrid+1):
        x = x_min + i * grid_steps
        command = command + 'p{0} drawline {1} {2} {3} {4};'.format(index, x, y_min, x, y_max)
    
    grid_steps = (y_max - y_min)/ (n_ygrid+1)
    command = command + 'p{0} set linestyle dot;'.format(index)
    for i in range(n_ygrid+1):
        y = y_min + i * grid_steps
        command = command + 'p{0} drawline {1} {2} {3} {4};'.format(index, x_min, y, x_max, y)
    
    #print command
    
    s.sendall(command)
    
    x_min = 0 
    x_max = 256 
    n_xgrid = 31 
    y_min = 0
    y_max = 300
    n_ygrid = 5
    command = '.create plot pofst{0} 0 55 100 45;'.format(index)
    command += 'pofst{0} set title Page 2; pofst{0} set xtitle address; pofst{0} set ytitle value;'.format(index)
    command = command + 'pofst{0} frame {1} {2} {3} {4};'.format(index,x_min, x_max, y_min, y_max)
    command = command + 'pofst{0} set color {1};'.format(index,color)
    command = command + 'pofst{0} set marker diamond;'.format(index)
    command = command + 'pofst{0} set markersize {1};'.format(index, markersize)
    command = command + 'pofst{0} plot '.format(index)
    #print 'offset registers'
    ofst_hist = [0] * 254
    for reg in ofstregs:
        command = command + '{0} {1} '.format(reg[0], reg[1])
	ofst_hist[reg[1]-1] = ofst_hist[reg[1]-1] + 1
    command = command +  ';'

#    for i in range(len(ofst_hist)):
#	print i, ofst_hist[i], ' '

    grid_steps = (x_max - x_min)/ (n_xgrid+1)
    command = command + 'pofst{0} set color black;'.format(index)
    command = command + 'pofst{0} set linestyle dot;'.format(index)
    for i in range(n_xgrid+1):
        x = x_min + i * grid_steps
        command = command + 'pofst{0} drawline {1} {2} {3} {4};'.format(index, x, y_min, x, y_max)
    
    grid_steps = (y_max - y_min)/ (n_ygrid+1)
    command = command + 'pofst{0} set linestyle dot;'.format(index)
    for i in range(n_ygrid+1):
        y = y_min + i * grid_steps
        command = command + 'pofst{0} drawline {1} {2} {3} {4};'.format(index, x_min, y, x_max, y)
    
    #print command
    
    s.sendall(command)

    x_min = 60 
    x_max = 180 
    x_step = 1
    n_xgrid = 7 
    y_min = 0
    y_max = 20
    n_ygrid = 5
    command = '.create plot pofst_2{0} 60 10 40 45;'.format(index)
    command += 'pofst_2{0} set title Offsets; pofst_2{0} set xtitle values; pofst_2{0} set ytitle count;'.format(index)
    command = command + 'pofst_2{0} frame {1} {2} {3} {4};'.format(index,x_min, x_max, y_min, y_max)
    command = command + 'pofst_2{0} set color {1};'.format(index,color)
    command = command + 'pofst_2{0} hist {1} {2} '.format(index, x_min, x_step)
    value_sum = 0
    for i in range((x_max - x_min)/x_step):
	value = 0
	for j in range(x_step):

	    value += ofst_hist[x_min + i*x_step + j]

	command += '{0} '.format(value)
	value_sum += value
    print 'sum = ', value_sum
    command += ';'

    s.sendall(command)

if __name__ == '__main__':

    HOST = 'localhost'
    PORT = 3456
    print 'kinoko-canvas %s %d &' % (HOST, PORT)
    os.system('kinoko-canvas  %s %d &' % (HOST, PORT))
    time.sleep(1)
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((HOST,PORT))

    command = '.create textbox pbox 0 0 100 10; pbox frame 0 100 0 10;'
    command = command + 'pbox setfont helvetica-bold 15;'
    index = 0
    filename = ''
    color = 'blue'
    if len(sys.argv) == 1:
	print_usage(sys.argv[0])
	s.sendall('.quit;')
    elif len(sys.argv) > 1:
	filename = sys.argv[1]
	plot_regs( filename, index, color )	
	command = command + 'pbox set color {0};'.format(color)
	command = command + 'pbox drawtext 10 {0} {1};'.format( index * 20, filename)
	if len(sys.argv) > 2:
	    index += 1 
	    filename = sys.argv[2]
	    color = 'red'
	    plot_regs( filename, index, color )
	    command = command + 'pbox set color {0};'.format(color)
	    command = command + 'pbox drawtext 10 {0} {1};'.format( index * 20, filename)

    #print command
    s.sendall(command)	
    while True:
	line = sys.stdin.readline()
	if line == 'q\n':
	    s.sendall('.quit;')
	    sys.exit(0)

