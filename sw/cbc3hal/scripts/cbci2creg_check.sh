#!/bin/bash

if [ $# -lt 1 ]; then
    echo "usage: ${0##*/} [reference file] ([fe_id] [cbc_id])"
    exit -1 
fi

reffile=${1}
fe_id=1
if [ $# -ge 2 ]; then
    fe_id=${2}
fi
cbc_id=1
if [ $# -ge 3 ]; then
    cbc_id=${2}
fi


ctime=`date +%Y%m%d-%H%M%S | tr -d '\n'`
newfile=`printf "FE%02dCBC%02d-%s.txt" ${fe_id} ${cbc_id} ${ctime}`
echo $newfile

arg="cbci2c reg r=1 w=0 file=etc/CBC_I2CREGS_CNFG.txt fe_id=$fe_id cbc_id=$cbc_id"
cbc3daq "$arg" > ${newfile} 
diff -I '^#' ${1} ${newfile} 
if [[ $? -eq 0 ]]; then
    echo "No change found."
else
    echo "Register values changed."
fi


