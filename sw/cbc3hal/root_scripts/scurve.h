#include <TROOT.h>
#include <TString.h>
#include <TRegexp.h>
#include <TF1.h>
double get_time( TString fname ){

    TString tmp;
    TRegexp reg = ("tpdelay[0-9]+");
    tmp = fname(reg); 
    int tpdelay = tmp.ReplaceAll("tpdelay", "").Atoi(); 
    reg = ("triglat[0-9]+");
    tmp = fname(reg);
    int triglat = tmp.ReplaceAll("triglat", "").Atoi(); 
    int time= -triglat*25-tpdelay;

//   cout << "Test pulse delay = " << tpdelay << ", Trigger latency = " << triglat << " time = " << time << endl;

    return time;
}

bool get_scurve_params(TH1F *h, double &midp, double &sigma, double &e_midp, double &e_sigma ){

    TString funcname = h->GetName(); 
    funcname.ReplaceAll("hbe", "fbe");

    TF1 *func = h->GetFunction(funcname); 
    if(!func) return false;

    midp = func->GetParameter(0);
    sigma = func->GetParameter(1);
    e_midp = func->GetParError(0);
    e_sigma = func->GetParError(1);
    return true;
}

